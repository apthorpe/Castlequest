# Placeholder directory to support VS Code Fortran linter

This directory has been created to avoid polluting the local
source directory with temporary `.mod` files.

In `./.vscode/settings.json`, add

    "fortran.linterExtraArgs": [
        "-J${workspaceFolder}/.fmod"
    ],
    "fortran.includePaths": [
        "${workspaceFolder}/.fmod"
    ]

to redirect compiled module files to `${workspaceFolder}/.fmod`
instead of `${workspaceFolder}/src`. Hopefully this is more portable
than hard-coding the project path in `settings.json`. See
[https://github.com/krvajal/vscode-fortran-support/issues/176] for
further info.

Also, set `.gitignore` to ignore changed to the contents of `./.fmod`