# Castlequest (1980)

By Michael S. Holtzman and Mark Kershenblatt.


## Provenance

The U.S. Copyright Office has a deposit related to this game:
[TXu000091366](https://cocatalog.loc.gov/cgi-bin/Pwebrecon.cgi?Search_Arg=TXu000091366&Search_Code=REGS&CNT=10&HIST=1)

On 2021-03-02, Mark Kershenblatt received 78 pages of paper copies
from the USCTO. He scanned them in and sent the scans to Arthur O'Dwyer,
in the form of two PDFs. Arthur rotated and concatenated the PDFs
into the single 78-page PDF in this repository, `castlequest.pdf`.

Arthur O'Dwyer manually transcribed the PDF into the plain text
file in this repository, `castlequest.ocr.txt`. (If you find any
places where the transcription differs from the original PDF,
Arthur will pay a "bug bounty" of $5 per error! Open a pull request
on this repository or send me an email.)

The `src` directory contains `.f` and `.dat` files that have
been mechanically extracted from `castlequest.ocr.txt` using
command-line tools such as `cut -b 17-88`.


## How to compile and play

I'm still deciding how to organize the patches in the long term,
so these instructions may change. For now, my patches are in a
separate git branch named `patches`:

    git checkout patches
    cd src
    make
    ./cquest | asa

In order for `make` to work, you'll need to have either `f77` or
`gfortran` in your path.

In order for your input to be recognized, you'll need to enter
all your text in ALL CAPS. I recommend turning on CAPS LOCK while
you play.

The game's output uses "[carriage control](https://en.wikipedia.org/wiki/ASA_carriage_control_characters)":
when it prints the character `0` in column 1, it's expecting that
the printer hardware will turn that into an extra newline.
Naturally, modern terminals don't do that. But many POSIX systems
(including Mac OSX) come with a utility program named `asa` that
can interpret those carriage-control characters for you.

If your computer lacks `asa`, you can hack it together in a
couple lines of your favorite scripting language; a Python
implementation is provided in the `src` directory.

    ./cquest | python ./asa.py

## Alternate Build Instructions (Extended Dance Remix)

You will need [CMake](https://cmake.org/), a modern Fortran
compiler (`gfortran` works), and a low-level build tool like
`make` or [`ninja`](https://ninja-build.org/).

Similar to above, the build process goes a little like this:

```sh
   cd my_git_files
   git checkout cmakify2
   cd Castlequest
   mkdir build
   cd build
   cmake ..
```

That should create Makefiles or Ninja build files; now run
`ninja` or `make` as appropriate.

This should produce `Castlequest` or `Castlequest.exe`.

**IMPORTANT** Put the `Castlequest` execuable in its own directory
along with `asa.py` and all the `.dat` files (located in `Castlequest/src`)

It should run just like `cquest` above:
```sh
   ./Castlequest | python ./asa.py
```
CMake can do a few more things depending on what additional
tools you have installed (Doxygen, LaTeX, Graphviz, various
software packaging utilities...) but a very simple way to
set up the game files is to use CPack to build an installation
package (a basic `.zip` file with all the program and data
files where they should be to make Castlequest playable).

Inside the `build` directory, run:

```sh
   cpack -G ZIP
```

and this should create a file named something like `Castlequest-0.0.0.2-win64.zip`.
Unzip that somewhere sensible and the executable with `asa.py`
and all the data files will be together in the `Castlequest-0.0.0.2-win64/bin` directory.

CPack supports a ton of installer generators so if
you want the game files in 7Zip format, just substitute
`7Z` for `ZIP`.

If you want to get super fancy and have Linux, MacOS,
or Windows packaging utilities like NSIS or WiX or `nuget`,
just run CPack with no options and it will try to generate
as many packages as it can.

Depending on your setup, it will create `.exe`, `.msi`, or `.nupkg` installers on Windows,
`.deb` and `.rpm` packages on Linux, and `.dmg` (DragNDrop) packages
on MacOS. At absolute bare minimum, you'll get a `.zip` file on every platform.

## Icon Credits

The icons used in the installers and as the project icon are from
https://icon-icons.com/icon/castle/126982 and were originally
created by [Selman Design](https://selmandesign.com/). They are used
and distributed under the
[Creative Commons Attribution 4.0 International Public License](https://creativecommons.org/licenses/by/4.0/legalcode),
a copy of which may be found in the `img` directory.
