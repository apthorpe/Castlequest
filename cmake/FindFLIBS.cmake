set(FLIBS_FOUND OFF)

if(IS_DIRECTORY "${FLIBS_ROOT}")
    set(FLIBS_SOURCE_DIR "${FLIBS_ROOT}/src")

    # Full path to m_multilog.f90
    find_file(FLIBS_MULTILOG_SRC m_multilog.f90
        PATHS "${FLIBS_SOURCE_DIR}/reporting"
    )

    # Full path to command_args.f90
    find_file(FLIBS_COMMAND_ARGS_SRC command_args.f90
        PATHS "${FLIBS_SOURCE_DIR}/strings"
    )

    # # Full path to ftnunit.f90
    # find_file(FLIBS_FTNUNIT_SRC ftnunit.f90
    #     PATHS "${FLIBS_SOURCE_DIR}/funit"
    # )

    # # Full path to ftnunit_hooks.f90
    # find_file(FLIBS_FTNUNIT_HOOKS_SRC ftnunit_hooks.f90
    #     PATHS "${FLIBS_SOURCE_DIR}/funit"
    # )

    # Set FLIBS_FOUND if both FLIBS_MULTILOG_SRC and
    # FLIBS_COMMAND_ARGS_SRC are found
    include(FindPackageHandleStandardArgs)
    find_package_handle_standard_args(FLIBS DEFAULT_MSG
        FLIBS_MULTILOG_SRC FLIBS_COMMAND_ARGS_SRC)
endif()