# Manual Build Instructions

Since moving to CMake as the primary build system and adding new
dependencies like FLIBS for diagnostic logging and argument processing,
it was no longer feasible to use the manual build Makefile with the
current source distribution.

Version information and metadata from CMake is combined with the
template `cquest_version.f90.in` to automatically update code version
and build date. As a result, `cquest_version.f90` does not exist at
build time, at least not when building manually with `make`.

To simplify matters for people uninterested in development
infrastructure and who just want to build and play the game, please
read through and follow these instructions:

* Open a terminal session and `cd` to the root of the Castlequest
  distribution directory
* Switch to the `contrib/make` directory (where this `README` is
  located.)
* Verify the current directory contains `cquery_version.f90` and
  `Makefile`
* Copy the `*.f90` source files from the main source directory `../src`
  into the current directory
* Copy the `*.f90` source files from the directory `../contrib/FLIBS`
  into the current directory
* At this point there should be 21 Fortran source files in the
  current directory along with the Makefile.
* Run `make`. If the source files are all present and your system can
  find `make` and `gfortran`, this should compile everything and
  leave you with the executable `cquest` (or on Windows, `cquest.exe`)

Running the game should be as described in the top-level Castlequest
`README.md`. For example, copy the `cquest` executable into a directory
along with the `*.dat` files listed in the top-level `README.md`
(they should be located in the `data` directory of the Castlequest
distribution). For a better experience, pipe the output of Castlequest
through `asa` or `asa.py` as described in top-level `README.md` or
just run `cquest` if you're impatient.
