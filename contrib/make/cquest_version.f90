!> @file cquest_version.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Contains dummy values of version and other Castlequest
!! library metadata. CMake builds generate this file from the
!! template file src/cquest_version.f90.in

!> @brief Contains dummy values of version and other Castlequest
!! library metadata
module cquest_version
  implicit none

  public

  !> Application name
  character(len=*), parameter :: CODENAME  = "Castlequest"

  !> Application version
  character(len=*), parameter :: VERSION   = "unknown"

  !> Date of build
  character(len=*), parameter :: BUILDDATE = "unknown"

end module cquest_version
