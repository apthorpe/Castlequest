!> @file random_check.f90
!! @author Bob Apthorpe
!! @copyright
!! @brief

!> @brief Random value checker for verifying random values adequately
!! cover a given range.
program random_check
  use, intrinsic :: iso_fortran_env, only: stdout => OUTPUT_UNIT
  use :: m_random, only: rstart, rdm
  implicit none

  integer, parameter :: nval = 1000
  character(len=:), allocatable :: eqn

  ! real     :: rval
  integer  :: ival
  integer  :: j

1 format('--------------------------------------------------------------------------------')
2 format(A)
3 format(I0)

continue

  call rstart()

  eqn = '22, 902: int(RDM() * 7.0 + 57.0)'
  write(unit=stdout, fmt=1)
  write(unit=stdout, fmt=2) eqn
  write(unit=stdout, fmt=1)
  do j = 1, nval
    ival = int(rdm() * 7.0 + 57.0)
    write(unit=stdout, fmt=3) ival
  end do

  eqn = '528: int(RDM() * 6.0 + 58.0)'
  write(unit=stdout, fmt=1)
  write(unit=stdout, fmt=2) eqn
  write(unit=stdout, fmt=1)
  do j = 1, nval
    ival = int(rdm() * 6.0 + 58.0)
    write(unit=stdout, fmt=3) ival
  end do

  eqn = '618: int(RDM() * 9 + 1)'
  write(unit=stdout, fmt=1)
  write(unit=stdout, fmt=2) eqn
  write(unit=stdout, fmt=1)
  do j = 1, nval
    ival = int(rdm() * 9 + 1)
    write(unit=stdout, fmt=3) ival
  end do

  eqn = '905, 920: int(RDM() * 19 + 2)'
  write(unit=stdout, fmt=1)
  write(unit=stdout, fmt=2) eqn
  write(unit=stdout, fmt=1)
  do j = 1, nval
    ival = int(rdm() * 19 + 2)
    write(unit=stdout, fmt=3) ival
  end do

  write(unit=stdout, fmt=1)

end program random_check