# Castlequest Data Files

This directory contains data files holding game instructions, in-game
hints, long and short room descriptions, and long descriptions of
objects, people, and creatures you may encounter.