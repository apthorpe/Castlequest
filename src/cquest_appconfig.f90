!> @file cquest_appconfig.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Provides command-line argument processing and application
!! configuration for Castlequest

!> @brief Provides command-line argument processing and application
!! configuration for Castlequest
!!
!! Underlying argument processing is implemented by command_args from
!! the FLIBS distribution; see http://flibs.sourceforge.net/
module cquest_appconfig
use m_gameoption, only: gameoption_t
implicit none

public :: appconfig_t

private

!> Application configuration structure
type :: appconfig_t
  !> Show usage flag
  logical           :: show_usage   = .false.
  !> Show version flag
  logical           :: show_version = .false.
  !> Halt flag
  logical           :: halt         = .false.
  !> Base logfile name
  character(len=80) :: logbase      = 'castlequest'
  !> Log file names
  character(len=80), dimension(:), allocatable :: logfiles
  !> Object containing gameplay options
  type(gameoption_t) :: gameoption
contains
  !> Initialize contents of appconfig object
  procedure :: init => appconfig_init
  !> Interpret command-line arguments
  procedure :: process_args => appconfig_process_args
  !> Dump contents of appconfig object
  procedure :: dump => appconfig_dump
end type appconfig_t

contains

!> Initialize contents of appconfig object
subroutine appconfig_init(this)
  use cquest_version, only: CODENAME
  use m_textio, only: munch, timestamp
  implicit none
  !> Object reference
  class(appconfig_t), intent(inout) :: this
  continue

  this%show_usage   = .false.
  this%show_version = .false.
  this%halt         = .false.

  this%logbase      = munch(CODENAME) // '_' // timestamp()

  if (allocated(this%logfiles)) then
    deallocate(this%logfiles)
  end if

  call this%gameoption%init()

  return
end subroutine appconfig_init

!> Dump contents of appconfig object
subroutine appconfig_dump(this)
  use, intrinsic :: iso_fortran_env, only: stderr => ERROR_UNIT
  use m_format, only: fmt_a
  use m_textio, only: munch, ltoa
  implicit none

  !> Object reference
  class(appconfig_t), intent(in) :: this

  continue

  write(unit=stderr, fmt=fmt_a) 'show_usage:     [' // ltoa(this%show_usage) // ']'
  write(unit=stderr, fmt=fmt_a) 'show_version:   [' // ltoa(this%show_version) // ']'
  write(unit=stderr, fmt=fmt_a) 'logbase:        [' // munch(this%logbase) // ']'
  call this%gameoption%dump()

  return
end subroutine appconfig_dump

!> @brief Display help / version instructions
subroutine display_version(unit)
  use, intrinsic :: iso_fortran_env, only: stdout => output_unit
  use cquest_version, only: CODENAME, VERSION, BUILDDATE
  implicit none

  !> Output unit number
  integer, intent(in), optional :: unit

  integer :: ofh

1 format(A, ', ', A, ', ', A)

  continue

  if (present(unit)) then
    ofh = unit
  else
    ofh = stdout
  end if

  write(unit=ofh, fmt=1) CODENAME, VERSION, BUILDDATE

  return
end subroutine display_version

!> @brief Display help / usage instructions
subroutine display_usage(unit)
  use, intrinsic :: iso_fortran_env, only: stdout => output_unit
  use m_format, only: fmt_a
  use m_textio, only: munch
  use cquest_version, only: CODENAME
  implicit none

  !> Output unit number
  integer, intent(in), optional :: unit

  character(len=80) :: cmd
  integer :: ofh

  continue

  if (present(unit)) then
    ofh = unit
  else
    ofh = stdout
  end if

  call get_command_argument(number=0, value=cmd)

  write(unit=ofh, fmt=fmt_a) 'Usage:'
  write(unit=ofh, fmt=fmt_a) munch(cmd) // ' --help --usage --version --log'
  write(unit=ofh, fmt=fmt_a) 'Options:'
  write(unit=ofh, fmt=fmt_a) '  -h, -?, --help           Displays basic help and exits'
  write(unit=ofh, fmt=fmt_a) '  -u, --usage              Displays this usage message and exits'
  write(unit=ofh, fmt=fmt_a) '  -v, --version            Displays the code version and exits'
  write(unit=ofh, fmt=fmt_a) '  -g, --debug              Set debugging mode on entry'
  write(unit=ofh, fmt=fmt_a) '  -a, --asa                Enable ASA-formatted output (original F66)'
  write(unit=ofh, fmt=fmt_a) '  -L, --log                Enable diagnostic logging'
  write(unit=ofh, fmt=fmt_a) '  -R, --restore            Automatically load the current save file'
  write(unit=ofh, fmt=fmt_a) 'Version:'
  call display_version(unit=ofh)

  return
end subroutine display_usage

!> @brief Set application configuration from command line arguments
!!
!! @warning Silently ignores options it doesn't understand
subroutine appconfig_process_args(this)
  use, intrinsic :: iso_fortran_env, only: stdout => output_unit
  use cquest_version, only: CODENAME
  use command_args, only: handle_command_options, optarg, opt_true,     &
    opt_value_next, cmd_option
  use m_format, only: fmt_a

  implicit none

  !> Application configuration object
  class(appconfig_t), intent(inout) :: this

  type(cmd_option), dimension(6) :: opts

  continue

  ! -h, --help
  ! -v, --version

  opts(1) = optarg(this%gameoption%write_logs,     opt_true,            &
    'L', 'log',      'Enable diagnostic log')
  opts(2) = optarg(this%gameoption%auto_restore,   opt_true,            &
    'R', 'restore',  'Restore from save file')
  opts(3) = optarg(this%gameoption%use_asa,        opt_true,            &
    'a', 'asa',      'Enable ASA-formatted output (original)')
  opts(4) = optarg(this%gameoption%debug_mode,     opt_true,            &
    'g', 'debug',    'Enable debug mode')
  opts(5) = optarg(this%show_usage,     opt_true,                       &
    'u', 'usage',    'Show detailed usage info and quit')
  opts(6) = optarg(this%show_version,   opt_true,                       &
    'v', 'version',  'Show version and quit')

  ! print *, "   Are you actually parsing the arguments?"

  call handle_command_options(opts)

  if (this%show_usage) then
    call display_usage()
    this%halt = .true.
    return
  end if

  if (this%show_version) then
    call display_version()
    this%halt = .true.
    return
  end if

  if (this%gameoption%debug_mode) then
    write(unit=stdout, fmt='(A)')                                       &
      "   SPOILER ALERT: Debugging mode enabled"
    return
  end if

  return
end subroutine appconfig_process_args

end module cquest_appconfig