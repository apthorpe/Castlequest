!> @file cquest_game.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief High-level implementation of Castlequest game

!> @brief High-level implementation of Castlequest game
module cquest_game
  use :: m_encounter, only: gameencounters_t
  use :: m_gameoption, only: gameoption_t
  use :: m_items, only: gameitems_t
  use :: m_player, only: player_t
  use :: m_where, only: dungeon_t
  implicit none

  private

  public :: resolution_t
  public :: game_t

  ! For testing
  public :: DES

  !> Communicate the results of an encounter or activity
  type :: resolution_t
    !> The current action is complete; resolve passive/environmental
    !! effects
    logical :: to_next_area = .false.
    !> The current action is complete; get next action from user
    logical :: to_next_action = .false.
    !> Re-evaluate the current action
    logical :: reprocess_action = .false.
    !> The current action was fatal; resolve player death
    logical :: to_next_life = .false.
    !> The current action was permanently fatal to the player;
    !! end current game
    logical :: permadeath = .false.
    !> The current action has ended the current game without player
    !! death
    logical :: end_game = .false.

  contains
    !> Initialize resolution object
    procedure :: init => resolution_init

    !> Reset state of resolution object
    procedure :: reset => resolution_init
  end type resolution_t

  !> High-level game implementation class
  type :: game_t
    !> Reference to player object
    type(gameoption_t) :: option

    !> Reference to player object
    type(player_t) :: player

    !> Reference to map
    type(dungeon_t) :: map

    !> Bulk game items object
    type(gameitems_t) :: items

    !> Bulk live encounters object
    type(gameencounters_t) :: encounters
  contains
    !> Initialize subcomponents
    procedure :: init => game_init
    !> Enable debug mode
    procedure :: set_debug => game_set_debug
    !> Disable debug mode    !>
    procedure :: clear_debug => game_clear_debug
    !> Run game
    procedure :: run => game_run
    !> Update game world
    procedure :: update_world => game_update_world
    !> Respond to player actions
    procedure :: update_player => game_update_player
    !> Describe area and resolve random encounters
    procedure :: describe_area => game_describe_area
    !> Resolve thrown weapon
    procedure :: resolve_thrown_weapon => game_resolve_thrown_weapon
    !> Resolve player death
    procedure :: resolve_death => game_resolve_death
    !> Perform post-move actions
    procedure :: check_boat_master_after_move =>                        &
      game_check_boat_master_after_move
    !> Attempt to move to a different area
    procedure :: do_move
    !> Take item
    procedure :: do_take
    !> Drop item
    procedure :: do_drop
    !> Enter an area
    procedure :: do_enter
    !> Exit an area
    procedure :: do_exit
    !> Do violence upon someone or something
    procedure :: do_attack_kill
    !> Throw something
    procedure :: do_throw
    !> Load the gun
    procedure :: do_load
    !> Ahem
    procedure :: do_ahem
    !> Wave something
    procedure :: do_wave
    !> Stab someone or something
    procedure :: do_stab
    !> Give food to someone or something
    procedure :: do_feed
    !> Eat something
    procedure :: do_eat
    !> Drink something
    procedure :: do_drink
    !> Jump to or from somewhere
    procedure :: do_jump
    !> List carried items
    procedure :: do_inventory
    !> Open something
    procedure :: do_open
    !> Close something
    procedure :: do_close
    !> Engage lock
    procedure :: do_lock
    !> Disengage lock
    procedure :: do_unlock
    !> Activate an item
    procedure :: do_on
    !> Ignite something
    procedure :: do_light
    !> Dectivate an item
    procedure :: do_off
    !> Douse flame
    procedure :: do_extinguish
    !> Look around
    procedure :: do_look
    !> Display score
    procedure :: do_score
    !> Break something
    procedure :: do_break
    !> Pour water
    procedure :: do_water_pour
    !> Retreat
    procedure :: do_back
    !> Attempt to swim
    procedure :: do_swim
    !> Melt something
    procedure :: do_melt
    !> Move across something
    procedure :: do_cross
    !> Quit the game
    procedure :: do_quit
    !> Honk
    procedure :: do_honk
    !> Tie rope to something
    procedure :: do_tie
    !> Untie rope from something
    procedure :: do_untie
    !> Read something
    procedure :: do_read
    !> Fill container with nearby fluid
    procedure :: do_fill
    !> Display help
    procedure :: do_help
    !> Invoke debug command GOTO
    procedure :: do_goto
    !> Turn lock left
    procedure :: do_left
    !> Turn lock right
    procedure :: do_right
    !> Resolve gunshot
    procedure :: do_shoot
    !> Wake someone or something
    procedure :: do_wake
    !> Resolve POOF
    procedure :: do_poof
    !> Save game
    procedure :: do_save
    !> Restore game
    procedure :: do_restore
    !> Set debug output mode
    procedure :: do_debug
    !> Set verbose output mode
    procedure :: do_verbose
    !> Set brief output mode
    procedure :: do_brief
  end type game_t

contains

!> @brief Polls user for Y/N response
!!
!! Argument `II` is set to 1 if `Y` or `y`, 0 if `N` or `n`.
!! Routine cycles until one of `[nNyY]` is entered.
subroutine YORN(II)
  use, intrinsic :: iso_fortran_env, only: STDIN => INPUT_UNIT,         &
    STDOUT => OUTPUT_UNIT
  use m_format, only: a
  implicit none

  ! Parameters

  character(len=*), parameter :: cqm_yorn = '  Please answer YES or NO !'

  ! Arguments

  !> User response; 1 if `Y`, 0 if `N`
  integer, intent(out) :: II

  ! Local variables

  character(len=1) :: ANS
  integer :: ierr
  character(len=80) :: errmsg

  ! Formats

1001 format(A1)

  continue

L10: do

    call a%write_line(' ')
    read(unit=STDIN, fmt=1001, iostat=ierr, iomsg=errmsg) ANS

    if (ierr /= 0) then
      ! I/O error
      ! write(UNIT=STDOUT, FMT=3001) ierr, errmsg
      ANS = ' '
    end if

    select case(ANS)
    case ('Y', 'y')
      ! Found a YES
      II = 1
      exit L10
    case ('N', 'n')
      ! Found a NO
      II = 0
      exit L10
    case default
      ! Found something else; complain and ask again
      call a%write_para(cqm_yorn)
    end select

  end do L10

  return
end subroutine YORN

!> @brief Polls user for Y/N response
!!
!! @returns `.false.` if `YORN` sets `II` to 0
!! (response of `N` or `n`), otherwise returns `.true.`
!!
!! @note `YORN()` blocks on user input; will only return 0 or 1
!! upon receiving one of `[nNyY]`
logical function player_agrees() result(agree)
  use, intrinsic :: iso_fortran_env, only: STDIN => INPUT_UNIT,         &
    STDOUT => OUTPUT_UNIT
  implicit none

  ! Local variables
  integer :: II

  continue

  II = 0
  call YORN(II)

  agree = (II /= 0)

  return
end function player_agrees

!> @brief Provide free help
subroutine show_help(intro)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_log, only: log_debug
  use :: cquest_text, only: cqm_help_1a_h, cqm_help_1a_b,               &
    cqm_help_1b_h, cqm_help_1b_b, cqm_help_2_h, cqm_help_2_b,           &
    cqm_help_3_h, cqm_help_3_b, cqm_welcome, cqm_askhelp
  use :: m_format, only: a
  implicit none

  ! Arguments

  !> Flag indicating welcome message should be displayed
  logical, intent(in), optional :: intro

  ! Local variables

  logical :: show_intro

  continue

!  call log_debug("BEGIN show_help")

  show_intro = .false.
  if (present(intro)) then
    if (intro) then
!      call log_debug("show_help: Writing intro - begin")
      call a%write_line(cqm_welcome, spacing=3)
!      call log_debug("show_help: Writing intro - end")
!      call log_debug("show_help: Player agrees - begin")
      show_intro = player_agrees()
!      call log_debug("show_help: Player agrees - end")
    end if
  end if

  ! Skip if intro help is not desired
  if (show_intro) then

    ! Always show Level 1 help
!    call log_debug("show_help: show_intro=T, L1a")
    call a%write_para(cqm_help_1a_h, cqm_help_1a_b)
!    call log_debug("show_help: show_intro=T, L1b")
    call a%write_para(cqm_help_1b_h, cqm_help_1b_b)
!    call log_debug("show_help: show_intro=T, Prompt for L2?")

    ! Show Level 2 help?
    call a%write_para(cqm_askhelp)
!    call log_debug("show_help: show_intro=T, L1, Wait on player_agrees()")
    if (player_agrees()) then
      !
      !     INSERT SECOND LEVEL INSTRUCTIONS.
      !
!      call log_debug("show_help: show_intro=T, L2")
      call a%write_para(cqm_help_2_h, cqm_help_2_b)
      ! Show Level 3 help?
!      call log_debug("show_help: show_intro=T, Prompt for L3?")
      call a%write_para(cqm_askhelp)
!      call log_debug("show_help: show_intro=T, L2, Wait on player_agrees()")
      if (player_agrees()) then
      !
      !     INSERT THIRD LEVEL INSTRUCTIONS.
      !     (Explain how help and hints work)
      !
!        call log_debug("show_help: show_intro=T, L3")
        call a%write_para(cqm_help_3_h, cqm_help_3_b)
      end if

    end if
  end if
!  call log_debug("END show_help")

  return
end subroutine show_help

!> @brief Provides hints ... for a price
subroutine show_hint(OBJECT, ROOM, p_score, BAT_IS_HUNGRY,              &
  WINDOW1_IS_BARRED, CYCLOPS_BLOCKS_WAY, path_through_glacier)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text, only: cqm_help_3_h, cqm_help_3_b,                 &
    cqm_hint_1_h, cqm_hint_1_b, cqm_nohint, cqm_allhints
  use :: m_format, only: a
  use :: m_object_id
  use :: m_score, only: score_t
  implicit none

  ! Arguments

  !> Object ID
  integer, intent(in) :: OBJECT

  !> Current location
  integer, intent(in) :: ROOM

  !> Player score
  type(score_t), intent(inout) :: p_score

  !> Bat hunger state
  logical, intent(in) :: BAT_IS_HUNGRY

  !> Bedroom window barred state
  logical, intent(in) :: WINDOW1_IS_BARRED

  !> Cyclops state
  logical, intent(in) :: CYCLOPS_BLOCKS_WAY

  !> Glacier state
  logical, intent(in) :: path_through_glacier

  ! Local variables

  ! integer :: IHINT
  ! integer :: I
  integer :: JJ
  ! character(len=80) :: MSG
  logical :: pay_for_hint
  ! Formats

! 1002 format('0  Sorry, not available.')
! 1003 format('0  It will cost you five points.', /,                      &
!             '   Do you still want the hint?.')

  continue

  !
  ! Give hint
  !
  pay_for_hint = .false.

  JJ = 0
  ! Item
  select case(OBJECT)
  case(0)
    ! If no subject for hint is given, show help about hints
    call a%write_para(cqm_help_3_h, cqm_help_3_b)
    ! do I = 20, 23
    !   MSG = INST(I)
    !   write(STDOUT, MSG)
    ! end do
    ! Pass through
    JJ = -1
  case(O1_KEROSENE)
    ! Kerosene
    JJ = 29
  case(O2_BULLET)
    ! Silver bullet
    JJ = 1
  case(O6_STAKE)
    ! Wooden stake
    JJ = 2
  case(O8_HUNCHBACK)
    ! Hunchback
    JJ = 3
  case(O9_ROPE)
    ! Coil of rope
    JJ = 4
  case(O10_PAPER, O11_PEN)
    ! Writing paper, Quill pen
    JJ = 5
  case(O13_TORCH)
    ! Acetylene torch
    JJ = 25
  case(O16_HOOK)
    ! Grappling Hook
    JJ = 26
  case(O18_BOTTLE)
    ! Empty bottle
    JJ = 6
  case(O19_CROSS)
    ! Silver cross
    JJ = 7
  case(O25_ACID)
    ! Flask of acid
    JJ = 8
  case(O26_WATER)
    ! Water in bottle
    JJ = 9
  case(N37_WINDOW)
    ! Window
    JJ = 12
  case(N39_MASTER)
    ! Master
    JJ = 13
  case(N45_ROOM)
    ! Room
    select case(ROOM)
    case(R1_BEDROOM)
      ! R1_BEDROOM
      if (WINDOW1_IS_BARRED) then
        JJ = 8
      end if
    case(R20_ATTIC_ENTRANCE)
      ! R20_ATTIC_ENTRANCE
      if (BAT_IS_HUNGRY) then
        JJ = 19
      end if
    case(57:64)
      ! R57 - R64 (UNDERWORLD MAZE)
      JJ = 21
    case(R65_GLACIER_ROOM)
      ! R65_GLACIER
      if (.not. path_through_glacier) then
        JJ = 22
      end if
    case(R86_TALL_TUNNEL)
      ! R86_TALL_TUNNEL
      if (CYCLOPS_BLOCKS_WAY) then
        JJ = 23
      end if
    end select
  case(N46_BOOK)
    ! Book
    JJ = 14
  case(N48_DRAWER)
    ! Drawer
    JJ = 15
  case(N49_BUTLER)
    ! Butler
    JJ = 16
  case(N50_MIRROR)
    ! Mirror
    JJ = 17
  case(N54_BARS)
    ! Bars
    JJ = 18
  case(N55_BAT)
    ! Bat
    JJ = 19
  case(N76_WEREWOLF)
    ! Werewolf
    JJ = 11
  case(N77_GNOME)
    ! Gnome
    JJ = 24
  case(N78_CYCLOPS)
    ! Cyclops
    JJ = 23
  case(N79_GLACIER)
    ! Glacier
    JJ = 27
  case(N80_WIZARD)
    ! Wizard
    JJ = 28
  case default
    JJ = 0
  end select

  if (JJ > 0) then
    ! Confirm player wants to pay for hint
    call a%write_para(cqm_hint_1_h, cqm_hint_1_b)
    ! write(STDOUT, 1003)
    ! call YORN(IHINT)
    ! if (IHINT /= 0) then
    ! Pay for hint?
    if (player_agrees()) then
      call a%write_para(cqm_allhints(JJ))
      ! MSG = HINT(JJ)
      ! write(STDOUT, MSG)
      call p_score%add_to_score(-5)
    end if
  else if (JJ == 0) then
    ! NO HINT AVAILABLE.
    call a%write_para(cqm_nohint)
    ! write(STDOUT,1002)
  end if

  return
end subroutine show_hint

!> @brief Legacy description routine; describe the location or possibly your death
subroutine DES(RAWROOM, debug_mode)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_log, only: log_debug
  use :: cquest_text, only: cq_longtext, cq_form2
  use :: m_format, only: a
  use :: m_gamedata, only: FORM

  implicit none

  character(len=*), parameter :: cqf_d2003 =                            &
    "('  NO DESCRIPTION-MESSAGE NUMBER ', I4)"
  character(len=*), parameter :: cqf_d9797 =                            &
    "('  REQUEST FOR DESCRIPTION #', I4)"

  ! Arguments

  !> @brief Room or description number
  integer, intent(in) :: RAWROOM

  !> Debugging output flag
  logical, intent(in), optional :: debug_mode

  ! Local variables

  logical :: DEBUG
  integer :: II
  integer :: ROOM
  character(len=80) :: FMT
  character(len=80) :: msg

  ! Formats

! 2003 format('0  NO DESCRIPTION-MESSAGE NUMBER ', I4)
! 9797 format('0  REQUEST FOR DESCRIPTION #', I4)
! 9898 format('0  THE FIRST AND LAST LINE OF LONG ARE: ', 2I5)
! 7024 format('0  You have fallen in the dark and broken your neck.')

  continue

  if (present(debug_mode)) then
    DEBUG = debug_mode
  else
    DEBUG = .false.
  end if

  ROOM = RAWROOM
  if (DEBUG) then
    write(unit=msg, fmt=cqf_d9797) ROOM
    call a%write_para(msg)
  end if
!
  if (ROOM == 300) then
    ROOM = 100
  end if

  select case (ROOM)
  case(1:100)
    ! Long description of ROOM
    ! Note: LONG index is [1 .. 400]
    ! ! There are 340 entries in long.dat

    call cq_longtext%show(ROOM)
  case(201:299)
    ! Short description of (ROOM - 200)
    ! Note: FORM index is [1 .. 100]
    ! There are 100 entries in short.dat
    II = ROOM - 200
    FMT = FORM(II)
    write(STDOUT, FMT)
    call log_debug('DES(201-299) should be handled upstream by room%describe - what calls this?')
    ! call a%write_para(map%room(room_id)%short_desc)
  case(400:499)
    ! ! Object/creature description [31 .. 130]
    ! ! Note: FORM2 index is [1 .. 60]
    ! ! There are 60 entries in object.dat
    ! ! Should be case(400:429)
    ! II = ROOM - 369
    ! ! FMT = FORM2(II)
    ! ! write(STDOUT, FMT)
    ! call a%write_para(cq_form2(II))
    call log_debug('DES(400-499) should be handled upstream - what calls this?')
  case(601:699)
    ! ! Object/creature description [1 .. 99]
    ! ! Note: FORM2 index is [1 .. 60]
    ! ! There are 60 entries in object.dat
    ! ! Should be case(601:660)
    ! II = ROOM - 600
    ! ! FMT = FORM2(II)
    ! ! write(STDOUT, FMT)
    ! call a%write_para(cq_form2(II))
    call log_debug('DES(601-699) should be handled upstream - what calls this?')
  case(801)
    ! Player demise #1
    ! call a%write_para(cqm_death_1_h, cqm_death_1_b)
    call log_debug('DES(801) should be handled upstream - what calls this?')
  case(803)
    ! Player demise #2
    ! call a%write_para(cqm_death_2_h, cqm_death_2_b)
    call log_debug('DES(803) should be handled upstream - what calls this?')
  case(805)
    ! Player demise #3
    ! call a%write_para(cqm_death_3_h, cqm_death_3_b)
    call log_debug('DES(805) should be handled upstream - what calls this?')
  case(850)
    ! Player demise #4
    ! call a%write_para(cqm_death_4_h, cqm_death_4_b)
    call log_debug('DES(850) should be handled upstream - what calls this?')
  case(851)
    ! Player demise #5
    ! call a%write_para(cqm_death_5_h, cqm_death_5_b)
    call log_debug('DES(851) should be handled upstream - what calls this?')
  case(852)
    ! Player demise #6
    ! call a%write_para(cqm_death_6_h, cqm_death_6_b)
    call log_debug('DES(852) should be handled upstream - what calls this?')
  case(853)
    ! Player demise #7
    ! call a%write_para(cqm_death_7_h, cqm_death_7_b)
    call log_debug('DES(853) should be handled upstream - what calls this?')
  case(854)
    ! Player demise #8
    ! call a%write_para(cqm_death_8_h, cqm_death_8_b)
    call log_debug('DES(854) should be handled upstream - what calls this?')
  case default
    ! Complain about undocumented ROOM
    write(unit=msg, fmt=cqf_d2003) ROOM
    call a%write_para(msg)
  end select

  return
end subroutine DES

!> Initialize resolution object
subroutine resolution_init(this)
  implicit none
  !> Object reference
  class(resolution_t), intent(inout) :: this
  continue

  this%to_next_area     = .false.
  this%to_next_action   = .false.
  this%reprocess_action = .false.
  this%to_next_life     = .false.
  this%permadeath       = .false.
  this%end_game         = .false.

  return
end subroutine resolution_init

!> Initialize subcomponents
subroutine game_init(this, option)
  use :: cquest_text, only: cq_longtext
  use :: m_format, only: a
  use :: m_random, only: RSTART
  use :: m_words, only: init_wordlists

  implicit none
  ! Arguments

  !> Game object reference
  class(game_t), intent(inout) :: this

  !> Game option flags
  class(gameoption_t), intent(in), optional :: option

  continue

  if (present(option)) then
    this%option = option
  else
    call this%option%init()
  end if

  !=== Initialize game state
  call this%player%init()
  call this%map%init()
  call this%items%init_all()
  call this%encounters%init_all()
  !=== End initialize

  !=== Setup external dependencies
  ! Set ASA mode
  call a%init(this%option%use_asa)

  ! Prepare player command processor
  call init_wordlists()

  ! Initialize intrinsic PRNG
  call RSTART()

  ! Initialize the global realtor
  call cq_longtext%init()
  !=== End Setup

  return
end subroutine game_init

!> Enable debug mode
subroutine game_set_debug(this)
  implicit none

  !> Player object reference
  class(game_t), intent(inout) :: this

  continue

  this%option%debug_mode = .true.

  return
end subroutine game_set_debug

!> Disable debug mode
subroutine game_clear_debug(this)
  implicit none

  !> Player object reference
  class(game_t), intent(inout) :: this

  continue

  this%option%debug_mode = .false.

  return
end subroutine game_clear_debug

!> @brief Encapsulation of Loop 410 open code for room descriptions and
!! state change prior to action polling.
type(resolution_t) function game_describe_area(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_log, only: log_debug
  use :: cquest_text
  use :: m_format, only: a
  use :: m_items, only: NITEMS
  use :: m_object_id
  use :: m_random, only: RDM
  use :: m_textio, only: itoa
  use :: m_where, only: NROOMS, in_master_realm, in_werewolf_territory, &
    in_gnome_territory

  implicit none

  ! Parameters

  ! Debug format
  character(len=*), parameter :: cqf_d6303 =                            &
    "('  ABOUT TO CALL ""DES"": II IS ', I3)"

  ! Probability of hearing random footsteps
  real, parameter :: P_FOOTSTEPS = 0.008

  ! Return variables

  ! Arguments

  !> Reference to player object
  class(game_t), intent(inout) :: this

  ! Local variables

  integer :: II
  character(len=80) :: msg

  continue

  call outcome%init()

L1: do

    if (this%map%room(this%player%room)%prev > 0) then
      ! Show short description
      if (this%option%debug_mode) then
        II = this%player%room + 200
        write(unit=msg, fmt=cqf_d6303) II
        call a%write_para(msg)
      end if
      call this%map%room(this%player%room)%describe()
    else
      ! Show long description
      if (this%option%debug_mode) then
        II = this%player%room
        write(unit=msg, fmt=cqf_d6303) II
        call a%write_para(msg)
      end if
      call this%map%room(this%player%room)%describe(long=.true.)
    end if

    if (this%items%portable%is_carried(O8_HUNCHBACK)) then
      ! The hunchback is following you
      ! call DES(421, this%option%debug_mode)
      ! call a%write_para(cq_form2(421-369))
      call a%write_para(cq_form2(52))
    end if

    if (this%player%is_in(R100_ENDGAME)) then
      ! Last room; show score and quit
      call this%player%score%display_rank(                              &
        this%items%portable%total_score(this%player%moves))
      outcome%end_game = .true.
      exit L1
    end if

!     ...SPECIAL ROOM CONDITIONS...

    ! Set II to 0 to avoid calling DES(II) immediately after the select
    ! block
    II = 0
    select case(this%player%room)
    case(R8_FOYER)
      ! Show butler status
      ! II = this%encounters%a_butler%des_index()
      II = 0
      call a%write_para(cq_form2(this%encounters%a_butler%form2_index()))

    case(R1_BEDROOM)
      ! Describe starting bedroom including state of shutters and window
      if (this%items%s_shutter%is_closed()) then
        ! State of shutters
        ! call DES(417, this%option%debug_mode)
        ! call a%write_para(cq_form2(417-369))
        call a%write_para(cq_form2(48))
      else
        ! State of window
        ! call DES(this%items%s_window_1%des_index(),                     &
        !   this%option%debug_mode)
        call a%write_para(cq_form2(this%items%s_window_1%form2_index()))
        if (this%items%portable%item(O17_STATUE)%in_room(               &
          R29_BELOW_WINDOW)) then
          ! Shiny statue
          ! call DES(428, this%option%debug_mode)
          ! call a%write_para(cq_form2(428-369))
          call a%write_para(cq_form2(59))
        end if
        II = 0
      end if
      if (this%items%s_rope%is_hanging()) then
        ! II = 413
        II = 0
        call a%write_para(cq_form2(44))
      end if

    case(R10_SMOKING_ROOM)
      ! Smoking room window state
      ! II = this%items%s_window_2%des_index()
      II = 0
      call a%write_para(cq_form2(this%items%s_window_2%form2_index()))

    case(R13_LIBRARY)
      ! State of passage in the library
      call a%write_para(cqm_f1098)
      if (this%items%s_passage%is_open()) then
        ! call DES(412, this%option%debug_mode)
        ! call a%write_para(cq_form2(412-369))
        call a%write_para(cq_form2(43))
      end if
      II = 0

    case(R17_MIRROR)
      ! State of passage in single mirror room
      if (this%items%s_passage%is_open()) then
        ! call DES(412, this%option%debug_mode)
        ! call a%write_para(cq_form2(412-369))
        call a%write_para(cq_form2(43))
      end if
      II = 0

    case(R29_BELOW_WINDOW)
      ! On a rope underneath a window
      if (this%items%s_rope%is_hanging()) then
        ! II = 413
        II = 0
        call a%write_para(cq_form2(44))
      end if
      if (this%items%portable%item(O4_KEY)%place > -2) then
        call this%items%s_note%write_about_hunchback()
      end if

    case(R40_FAR_SIDE_OF_MOAT)
      ! Far side of moat
      if (this%map%room(R80_WINE_CELLAR)%door%is_open()) then
        call a%write_para(cqm_f1110)
      end if
      II = 0

    case(R43_MASTER_CHAMBER)
      ! Master state in Master chamber
      ! II = this%encounters%a_master%des_index()
      II = 0
      call a%write_para(cq_form2(this%encounters%a_master%form2_index()))

      if (this%items%portable%item(O4_KEY)%place > -2) then
        call this%items%s_note%write_about_hunchback()
      end if

    case(R47_FIRE_ROOM)
      ! Fire state in fire room
      if (this%map%fire_blocks_way) then
        call a%write_para(cqm_f1100)
      end if

    case(R65_GLACIER_ROOM)
      ! State of glacier
      if (this%map%path_through_glacier) then
        call a%write_para(cqm_f1116)
      end if

    case(R83_LARGE_CAVERN_SOUTH, R84_TOP_OF_PRECIPICE)
      ! State of precipice
      if (this%map%path_to_precipice) then
        call a%write_para(cqm_f1011)
      end if

    case(R86_TALL_TUNNEL)
      ! ...CYCLOPS IN ROOM...
      if (this%encounters%a_cyclops%left_hole()) then
        call a%write_para(cqm_f1117)
      else
        if (this%items%portable%item(O27_CIGAR)%place == -3) then
          call a%write_para(cqm_f1119h, (/cqm_f1119b/))
        else
          call a%write_para(cqm_f1118)
        end if
      end if

    case(R91_BORDER, R92_FOREST, R93_THRONE_ROOM)
      if (this%encounters%a_wizard%blocks_way()) then
        ! State of wizard in the throne room
        if (this%player%is_in(R93_THRONE_ROOM)) then
          call a%write_para(cqm_f1128)
        end if
        if (this%items%portable%is_carried(O12_SWORD)) then
          ! call DES(318+this%player%room, this%option%debug_mode)
          call a%write_para(cq_form2(this%player%room - 51))
        end if
      end if

    case(R99_ELEVATOR_BETWEEN_FLOORS)
      ! State of final door
      if (this%map%path_through_final_door) then
        call a%write_para(cqm_f1136)
      end if

    case default
      if (this%items%portable%item(O4_KEY)%place > -2) then
        call this%items%s_note%write_about_hunchback()
      end if
    end select

    ! Show long room description (II = [1..100])
    if (II > 0) then
      if (II <= 100) then
        call cq_longtext%show(II)
      else
        call log_debug('describe_area: Show non-room DES('              &
          // itoa(II) // ')')
        call DES(II, this%option%debug_mode)
      end if
    end if

    ! Additional descriptions
    if (in_master_realm(this%player%room)                               &
      .and. this%items%portable%is_carried(O3_AXE)) then
      call a%write_para(cqm_f1141)
    end if

    II = 0
    call this%items%portable%display_here(this%player%score,            &
      this%player%room, this%map%path_to_precipice)

    if (this%encounters%a_werewolf%is_attacking()                       &
      .or. (this%player%moves > 15                                      &
      .and. in_werewolf_territory(this%player%room))) then
      call this%encounters%a_werewolf%resolve_fight(II,                 &
        this%option%debug_mode)
    end if

    if (this%encounters%a_gnome%is_attacking()                          &
      .or. in_gnome_territory(this%player%room)) then
      call this%encounters%a_gnome%resolve_fight(II,                    &
        this%option%debug_mode)
    end if

    ! Your encounter with wolf or gnome did not end well
    if (II > 0) then
      outcome = this%resolve_death()

      if (outcome%permadeath) then
        outcome%end_game = .true.
        exit L1
      else
        outcome%to_next_area = .true.
        exit L1
      end if
    end if

    if (this%encounters%a_werewolf%is_attacking()                       &
      .and. this%items%portable%is_carried(O8_HUNCHBACK)) then
      ! Hunchback takes one for the team
      ! call DES(429, this%option%debug_mode)
      ! call a%write_para(cq_form2(429-369))
      call a%write_para(cq_form2(60))
      call this%encounters%a_hunchback%expire()
      call this%items%portable%consume(O8_HUNCHBACK)
      call this%encounters%a_werewolf%calm()
      call this%player%lose_item(1)
    end if

    if (RDM() < P_FOOTSTEPS) then
      ! Footsteps...
      call a%write_para(cqm_f1145)
    end if

    exit L1
  end do L1

  return
end function game_describe_area

!> @brief Encapsulation of Label 379 open code common to THROW and STAB
!! actions.
subroutine game_resolve_thrown_weapon(this, cmd)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: cquest_log, only: log_debug
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_items, only: NITEMS
  use :: m_object_id
  implicit none

  !> Game implementation object
  class(game_t), intent(inout) :: this

  !> Player action object
  type(action_t), intent(in) :: cmd

  continue

  call log_debug("Beginning L379")

  select case(cmd%object)
  case(N55_BAT)
    call log_debug("VERBing the Bat")
    if (this%player%is_in(R20_ATTIC_ENTRANCE)                           &
      .and. this%encounters%a_bat%is_hungry()) then
      ! call DES(419, this%option%debug_mode)
      ! call a%write_para(cq_form2(419-369))
      call a%write_para(cq_form2(50))
    else
      call a%write_para(cqm_f1003)
    end if

  case(N49_BUTLER)
    call log_debug("VERBing the Butler")
    if (this%player%is_in(R8_FOYER)) then
      if (this%encounters%a_butler%is_dead()) then
        call a%write_para(cqm_f1005)
      else
        call this%encounters%a_butler%expire()
        call a%write_para(cqm_f1055)
      end if
    else
      call a%write_para(cqm_f1003)
    end if

  case(N78_CYCLOPS)
    call log_debug("VERBing the Cyclops")
    if (this%player%is_in(R86_TALL_TUNNEL)                              &
      .and. this%encounters%a_cyclops%blocks_way()) then
      call a%write_para(cqm_f1123)
    else
      call a%write_para(cqm_f1003)
    end if

  case(O8_HUNCHBACK)
    call log_debug("VERBing the Hunchback")
    if (this%items%portable%item(O8_HUNCHBACK)%at_hand_in(              &
      this%player%room)) then
      ! Hunchback dies and leaves game
      call this%encounters%a_hunchback%expire()
      if (this%items%portable%is_carried(O8_HUNCHBACK)) then
        ! Remove from inventory count if carried
        call this%player%lose_item(1)
      end if
      call this%items%portable%consume(cmd%object)
      call a%write_para(cqm_f1056)
    else
      call a%write_para(cqm_f1003)
    end if

  case(N76_WEREWOLF)
    call log_debug("VERBing the Werewolf")
    if (this%encounters%a_werewolf%is_attacking()) then
      call a%write_para(cqm_f1003)
    else
      ! call DES(420, this%option%debug_mode)
      ! call a%write_para(cq_form2(420-369))
      call a%write_para(cq_form2(51))
    end if

  case default

    ! I don't know what you're trying to stab or throw a sword or axe at
    call log_debug("VERBing not the Bat, Butler, Cyclops, Hunchback, or Werewolf")
    call a%write_para(cqm_f1016)

  end select

  call log_debug("Ending L379")

  return
end subroutine game_resolve_thrown_weapon

!> Resolve player death, ranking, and reincarnation
type(resolution_t) function game_resolve_death(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use cquest_text, only: cqm_death_4_h, cqm_death_4_b,                  &
    cqm_death_5_h, cqm_death_5_b, cqm_death_6_h, cqm_death_6_b,         &
    cqm_death_7_h, cqm_death_7_b, cqm_death_8_h, cqm_death_8_b
  use :: m_format, only: a
  use :: m_items, only: NITEMS
  use :: m_object_id
  use :: m_random, only: RDM

  implicit none

  ! Parameters

  ! Index of each item (treasure) to scatter out of inventory
  integer, dimension(*), parameter :: ISCATTER = (/                     &
    O7_CHAMPAGNE, O12_SWORD, O17_STATUE, O19_CROSS, O23_RUBY,           &
    O24_JADE, O28_SAPPHIRE, O29_MONEY, O30_SWAN /)

  ! Arguments
  !> Player object
  class(game_t), intent(inout) :: this

  ! Local variables

  integer :: II
  integer :: IDX

  continue

  call outcome%init()

  ! Set permadeath as default case; if resurrection is possible,
  ! permadeath will be set to false
  outcome%permadeath = .true.

  this%player%ndeaths = this%player%ndeaths + 1
  this%player%items_held = 0
  call this%player%score%add_to_score(-10)
  call this%items%s_lamp%turn_off()
  call this%encounters%a_gnome%calm()
  call this%encounters%a_werewolf%calm()
  ! Q: Why not simply set these to .false. without testing
  ! (ITEMS() == -1)?
  if (this%items%s_gun%is_carried()) then
    call this%items%s_gun%unload()
  end if
  if (this%items%s_bottle%is_carried()) then
    call this%items%s_bottle%clear()
  end if

  do IDX = 1, 9
    II = ISCATTER(IDX)
    if (this%items%portable%is_carried(II)) then
      call this%items%portable%item(II)%scatter(lo_room=57, hi_room=63)
    end if
  end do

  select case(this%player%ndeaths)
  case(1)
    ! call DES(850, this%option%debug_mode)
    ! Player demise #4
    call a%write_para(cqm_death_4_h, cqm_death_4_b)
    ! call YORN(II)
    ! if (II /= 0) then
    if (player_agrees()) then
      ! call DES(851, this%option%debug_mode)
      ! Player demise #5
      call a%write_para(cqm_death_5_h, cqm_death_5_b)

      ! RESET LOCATION, ITEMS, ETC. AFTER DEATH.
      call this%player%move_to(to=R1_BEDROOM, from=0)
      do II = 1, NITEMS
        if (this%items%portable%is_carried(II)) then
          call this%items%portable%item(II)%scatter(lo_room=2, hi_room=20)
        end if
      end do
      ! cycle L20
      outcome%permadeath = .false.
    end if
  case(2)
    ! call DES(852, this%option%debug_mode)
    ! Player demise #6
    call a%write_para(cqm_death_6_h, cqm_death_6_b)
    ! call YORN(II)
    ! if (II /= 0) then
    if (player_agrees()) then
      ! call DES(853, this%option%debug_mode)
      ! Player demise #7
      call a%write_para(cqm_death_7_h, cqm_death_7_b)
      call this%player%move_to(to=R9_UPSTAIRS_HALLWAY, from=0)
      do II = 1, NITEMS
        if (this%items%portable%is_carried(II)) then
          call this%items%portable%item(II)%scatter(lo_room=2, hi_room=20)
        end if
      end do
      outcome%permadeath = .false.
    end if
  case(3)
    ! call DES(854, this%option%debug_mode)
    ! Player demise #8
    call a%write_para(cqm_death_8_h, cqm_death_8_b)
  end select

  if (outcome%permadeath) then
    call this%player%score%display_rank(this%items%portable%total_score(this%player%moves))
  end if

  return
end function game_resolve_death

!> @brief Boat and Master status checking routine, post-player action
!!
!! Called from `do_cross()` and `do_move`
type (resolution_t) function game_check_boat_master_after_move(this)    &
  result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  use :: m_object_id
  implicit none

  ! Arguments

  !> Outcome object
  class(game_t), intent(inout) :: this

  continue

  call outcome%init()

L1: do

    !     ...BOAT IN CASTLE...
    if (this%player%route_is(from=R12_GARDEN, to=R8_FOYER)              &
      .or. this%player%route_is(from=R29_BELOW_WINDOW, to=R1_BEDROOM))  &
      then
      ! If going from garden to foyer or from outside to the starting
      ! bedroom (via the window)
      if (this%items%portable%is_carried(O14_BOAT)) then
        ! You can't take the boat into the house that way
        call a%write_para(cqm_f1081)
        call this%player%rollback_move()
        outcome%to_next_action = .true.
        exit L1
      end if
      ! But without the boat...
      if (this%encounters%a_master%is_sleeping()                        &
        .or. this%encounters%a_master%is_pinned()) then
        call this%encounters%a_master%rise()
      end if
      this%map%room(this%player%room)%prev =                            &
        mod(this%map%room(this%player%room)%prev                        &
          + this%player%brief_to_int() + 1, 6)
      outcome%to_next_area = .true.
      exit L1
    end if

    !      ...MASTER AWAKE...
    ! If the MASTER is awake when you go from the Master's chamber to
    ! the anteroom
    if (this%player%route_is(from=R43_MASTER_CHAMBER,                   &
      to=R41_HUGE_ANTEROOM) .and. this%encounters%a_master%is_up()) then
        ! Something bad happens
      call a%write_para(cqm_f1094)
      outcome%to_next_life = .true.
      exit L1
    end if

    if (this%encounters%a_master%is_sleeping()                          &
      .or. this%encounters%a_master%is_pinned()) then
      call this%encounters%a_master%rise()
    end if

    ! Increment counter tracking last time the long description of the
    ! room was displayed.
    this%map%room(this%player%room)%prev =                              &
      mod(this%map%room(this%player%room)%prev                          &
        + this%player%brief_to_int() + 1, 6)
    outcome%to_next_area = .true.
    exit L1
  end do L1

  return
end function game_check_boat_master_after_move

!> Implement actions 1-10 (V1_NORTH through V10_DOWN).
!! Move in a direction.
type(resolution_t) function do_move(this, current_action) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  use :: m_items, only: NITEMS, light_available
  use :: m_object_id
  use :: m_random, only: RDM
  use :: m_where, only: is_dark_room, in_winding_maze, is_impassible,   &
    random_room
  implicit none

  ! Parameters

  character(len=80), parameter :: cqf_mv9501 =                          &
    "('  RETURN FROM MOVE: ROOM = ', I8, ' LROOM = ', I8)"
  character(len=80), parameter :: cqf_mv8005 =                          &
    "('0  SPECIAL MOVE FROM ',I3,' TO ',I3)"

  ! Arguments

  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Currently processed action ID; may be different
  !! from `cmd%action`
  integer, intent(in) :: current_action

  ! Local variables

  integer :: II
  integer :: K
  character(len=80) :: msg

  continue

  call outcome%init()

  !     ---MOVE---
  ! ACTION(1:10)  Move N, NE, E, SE, S, SW, W, NW, U, D

  L1: do
  L103: do
      ! If you're in Rooms 41-94 (dark area)
      ! And you don't have either a lit lamp or a lit match
      if (is_dark_room(this%player%room)                                &
        .and. .not. light_available(this%player%room,                   &
        this%items%s_lamp, this%items%s_match)) then
        ! Something bad happens
        ! write(unit=STDOUT, fmt=1077)
        call a%write_para(cqm_f1077)

        outcome%to_next_life = .true.
        exit L1
      end if
      ! It's not only democracy that dies in darkness...

      ! Just put one foot in front of the other...
      II    = this%player%last_room
      K     = this%map%room(this%player%room)%leads_to(current_action)
      call this%player%move_to(to=K, save_last=.true.)

      if (this%option%debug_mode) then
        write(unit=msg, fmt=cqf_mv9501) this%player%room, this%player%last_room
      end if

      ! If player is now in Winding Maze or dead end [57-64],
      ! randomize the player's previous location to be deeper in the
      ! Winding Maze [58-63]
      if (in_winding_maze(this%player%room)) then
        this%player%last_room = random_room(lo_room=58, hi_room=63)
      end if

      ! ROOM = +10 -> goto 102
      ! ROOM =   0 -> goto 102
      ! ROOM = -10 -> ROOM = +10 -> continue

      if (this%player%room < 0) then
        this%player%room = (-this%player%room)
      else
        ! This is the 102 check inlined
        if (is_impassible(this%player%room)) then
          ! Can't go there; stay where you were
          call a%write_para(cqm_f1006)
          call this%player%rollback_move()
          outcome%to_next_action = .true.
          exit L1
        end if
        exit L103
      end if

      ! Moving between dim corridor and locked room
      if (this%player%route_is(from=R2_DIM_CORRIDOR,                   &
        to=R4_LOCKED_ROOM, both_ways=.true.)) then
        if (this%map%room(R2_DIM_CORRIDOR)%door%is_open()) then
          ! Door is open; proceed
          exit L103
        else if (this%map%room(R2_DIM_CORRIDOR)%door%is_closed()) then
          ! Door is closed
          call a%write_para(cqm_f1042)
        else if (this%map%room(R2_DIM_CORRIDOR)%door%is_locked()) then
          ! Door is locked
          call a%write_para(cqm_f1019)
        end if
        ! You shall not pass
        call this%player%rollback_move()
        outcome%to_next_action = .true.
        exit L1
      end if
  !     ...KITCHEN...
      if (this%player%is_in(R7_BRICK_WALL)) then
        if (this%map%room(R6_KITCHEN)%door%is_open()) then
          ! Door is open; proceed
          exit L103
        else if (this%map%room(R6_KITCHEN)%door%is_closed()) then
          ! Door is closed
          call a%write_para(cqm_f1042)
        else if (this%map%room(R6_KITCHEN)%door%is_locked()) then
          ! Door is locked
          call a%write_para(cqm_f1021)
        end if
        ! You shall not pass
        call this%player%rollback_move()
        outcome%to_next_action = .true.
        exit L1
      end if
  !     ...SECRET PASSAGE...
      if (this%player%route_is(from=R17_MIRROR, to=R13_LIBRARY,        &
        both_ways=.true.)) then
        ! If you're moving between the single mirror chamber and
        ! the library
        if (this%items%s_passage%is_closed()) then
          ! Stay where you are if you haven't opened the secret
          ! passage
          ! This traps 102 cases
          call a%write_para(cqm_f1006)
          call this%player%rollback_move()
          outcome%to_next_action = .true.
          exit L1
        end if
        exit L103
      end if
  !     ...TRAP DOOR...
      if (this%player%route_is(from=R3_PARLOR, to=R2_DIM_CORRIDOR))    &
        then
        ! If you're heading upstairs from the parlor to the dim corridor
        if (this%items%portable%is_carried(O4_KEY)) then
          ! If you have the key, fall into room 16 with no way back up
          call this%player%move_to(to=R16_DARK_EW_PASSAGE, from=0)
          call a%write_para(cqm_f1043)
        end if
        ! Proceed
        exit L103
      end if
  !     ...ATTIC...
      if (this%player%route_is(to=R21_ATTIC, from=R20_ATTIC_ENTRANCE,  &
        both_ways=.true.)) then
        ! Moving between the attic entrance and the attic
        if (this%encounters%a_bat%is_hungry()) then
          ! The bat blocks your way
          call a%write_para(cqm_f1049)
          call this%player%rollback_move()
          outcome%to_next_action = .true.
          exit L1
        end if
        ! Proceed
        exit L103
      end if
  !     ...COMBINATION LOCK...
      if (this%player%route_is(from=R21_ATTIC, to=R23_LOW_EW_PASSAGE)) &
        then
        ! Entering the combination locked room from the attic
        if (this%player%nlockparts_good == 3) then
          ! If it's unlocked then proceed
          exit L103
        else
          ! Otherwise stay where you are
          call a%write_para(cqm_f1066)
          call this%player%rollback_move()
          outcome%to_next_action = .true.
          exit L1
        end if
      end if
  !     ...MIRROR ROOM...
      if (this%player%was_in(R27_MIRROR_MAZE)) then
        if (RDM() < 0.20) then
          ! If you roll low, randomly exit the maze
          ! Q: Should index be int(RDM() * 10 + 1); range of [1..10]
          this%player%room = this%map%mirror_maze%find_exit()
          exit L103
        else
          ! Otherwise stay where you are
          call this%player%move_to(to=R27_MIRROR_MAZE,                     &
            from=R27_MIRROR_MAZE)
          this%map%room(this%player%room)%prev = 1
          outcome%to_next_area = .true.
          exit L1
        end if
      end if
  !     ...ROPE OUT WINDOW...
      if (this%player%route_is(from=R1_BEDROOM, to=R29_BELOW_WINDOW,       &
        both_ways=.true.)) then
        if (this%items%s_rope%is_hanging()) then
          exit L103
        else
          call a%write_para(cqm_f1006)
          call this%player%rollback_move()
          outcome%to_next_action = .true.
          exit L1
        end if
      end if
  !     ...JUMP FROM SMOKING ROOM...
      if (this%player%route_is(from=R10_SMOKING_ROOM,                      &
        to=R39_BELOW_SMALL_WINDOW)) then
        if (this%items%s_window_2%is_open()) then
            this%player%last_room = 0
          exit L103
        else
          call a%write_para(cqm_f1006)
          call this%player%rollback_move()
          outcome%to_next_action = .true.
          exit L1
        end if
      end if
  !     ...FIRE...
      if (this%player%route_is(from=R47_FIRE_ROOM,                         &
        to=R48_GLOWING_ROCK_ROOM)) then
        if (this%map%fire_blocks_way) then
          call a%write_para(cqm_f1101)
          call this%player%rollback_move()
          outcome%to_next_action = .true.
          exit L1
        else
          exit L103
        end if
      end if
  !      ...END OF GAME...
      if (this%player%route_is(from=R71_ELEVATOR_BOTTOM,                   &
        to=R70_ELEVATOR_TOP)) then
        ! Taking the elevator from the bottom floor to the top floor
        do K = 1, NITEMS
          if (this%items%portable%item(K)%in_room(R71_ELEVATOR_BOTTOM)) then
            call this%items%portable%item(K)%place_in(R70_ELEVATOR_TOP)
          end if
        end do
        II = this%items%portable%total_score(this%player%moves)
        if (II >= this%player%score%MMAX                                   &
          .and. this%items%portable%is_carried(O3_AXE)) then
          ! If you've found everything and you have the axe
          call this%player%move_to(to=R99_ELEVATOR_BETWEEN_FLOORS, from=0)
        end if
        exit L103
      else if (this%player%route_is(from=R70_ELEVATOR_TOP,                 &
        to=R71_ELEVATOR_BOTTOM)) then
        ! Taking the elevator from the top floor
        ! to the bottom floor
        do K = 1, NITEMS
          if (this%items%portable%item(K)%in_room(R70_ELEVATOR_TOP)) then
            call this%items%portable%item(K)%place_in(R71_ELEVATOR_BOTTOM)
          end if
        end do
        II = this%items%portable%total_score(this%player%moves)
        if (II >= this%player%score%MMAX                                   &
          .and. this%items%portable%is_carried(O3_AXE)) then
          ! If you've found everything and you have the axe
          call this%player%move_to(to=R99_ELEVATOR_BETWEEN_FLOORS,         &
            from=0)
        end if
        exit L103
      end if
  !      ...HATCH (GOING DOWN)...
      if (this%player%route_is(from=R40_FAR_SIDE_OF_MOAT,                  &
        to=R80_WINE_CELLAR)) then
        if (this%map%room(R80_WINE_CELLAR)%door%is_open()) then
          exit L103
        else
          ! This traps 102 cases
          call a%write_para(cqm_f1006)
          call this%player%rollback_move()
          outcome%to_next_action = .true.
          exit L1
        end if
      end if
  !     ...HATCH (GOING UP)...
      if (this%player%route_is(from=R80_WINE_CELLAR,                       &
        to=R40_FAR_SIDE_OF_MOAT)) then
        if (this%map%room(R80_WINE_CELLAR)%door%is_open()) then
          exit L103
        else
          call a%write_para(cqm_f1042)
          call this%player%rollback_move()
          outcome%to_next_action = .true.
          exit L1
        end if
      end if
  !     ...PRECIPICE...
      if (this%player%route_is(from=R84_TOP_OF_PRECIPICE,                  &
        to=R83_LARGE_CAVERN_SOUTH, both_ways=.true.)) then
        if (.not. this%map%path_to_precipice) then
          ! This traps 102 cases
          call a%write_para(cqm_f1006)
          call this%player%rollback_move()
          outcome%to_next_action = .true.
          exit L1
        end if
        exit L103
      end if
  !     ...MELT ICE ...
      if (this%player%route_is(from=R65_GLACIER_ROOM,                      &
        to=R90_COLD_TUNNEL)) then
        if (.not. this%map%path_through_glacier) then
          ! This traps 102 cases
          call a%write_para(cqm_f1006)
          call this%player%rollback_move()
          outcome%to_next_action = .true.
          exit L1
        end if
        exit L103
      end if
  !     ...CYCLOPS SHAPED HOLE...
      if (this%player%route_is(from=R86_TALL_TUNNEL,                       &
        to=R88_CYCLOPS_LAIR)) then
        if (this%encounters%a_cyclops%blocks_way()) then
          ! This traps 102 cases
          call a%write_para(cqm_f1006)
          call this%player%rollback_move()
          outcome%to_next_action = .true.
          exit L1
        end if
        exit L103
      end if
  !     ...THRONE ROOM...
      if (this%player%route_is(from=R92_FOREST, to=R93_THRONE_ROOM)) then
        if (this%items%portable%is_carried(O12_SWORD)) then
          exit L103
        else
          call a%write_para(cqm_f1127)
          call this%player%rollback_move()
          outcome%to_next_action = .true.
          exit L1
        end if
      end if
  !     ...WIZARD. . .
      if (this%player%route_is(from=R93_THRONE_ROOM,                       &
        to=R94_WIZARDS_CACHE)) then
        if (this%encounters%a_wizard%blocks_way()) then
          call a%write_para(cqm_f1128)
          call this%player%rollback_move()
          outcome%to_next_action = .true.
          exit L1
        else
          exit L103
        end if
      end if
      if (this%player%route_is(from=R71_ELEVATOR_BOTTOM,                   &
        to=R70_ELEVATOR_TOP)) then
        if (this%items%portable%is_carried(O3_AXE)) then
          II = this%items%portable%total_score(this%player%moves)
          if (II >= this%player%score%MMAX) then
            call this%player%move_to(to=R99_ELEVATOR_BETWEEN_FLOORS,       &
              from=0)
          end if
        end if
        exit L103
      end if
      if (this%player%route_is(from=R99_ELEVATOR_BETWEEN_FLOORS,           &
        to=R95_WAREHOUSE_MAIN)) then
        if (.not. this%map%path_through_final_door) then
          ! Can't go there; stay where you were
          call a%write_para(cqm_f1006)
          call this%player%rollback_move()
          outcome%to_next_action = .true.
          exit L1
        end if
        exit L103
      end if

      write(unit=msg, fmt=cqf_mv8005) this%player%last_room, this%player%room
      call a%write_para(msg)

      outcome%to_next_area = .true.
      exit L1

      exit L103
    end do L103

    outcome = this%check_boat_master_after_move()

    exit L1
  end do L1

  return
end function do_move

!> Implement action 11 (V11_TAKE). TAKE
type(resolution_t) function do_take(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_items, only: NITEMS
  use :: m_object_id
  use :: m_where, only: is_wet_room
  implicit none

  ! Arguments

  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  ! Local variables

  integer :: II

  continue

  call outcome%init()

  !     ---TAKE---
  ! ACTION(11)  TAKE
  L1: do

    if (this%player%is_encumbered() .and. cmd%object_is_not(O26_WATER)  &
      .and. cmd%object_is_not(O5_BLOOD)) then
      ! If you're at your encumbrance limit you can't pick up
      ! anything but water or blood. Later we'll check if you can
      ! pick those up otherwise you're overencumbered and you can't
      ! take any more
      ! write(unit=STDOUT, fmt=1012)
      call a%write_para(cqm_f1012)
      exit L1
    end if

    ! Take the rope while it's hanging
    if (cmd%object_is(O9_ROPE)                                          &
      .and. this%items%s_rope%is_hanging()) then
      call this%items%s_rope%untie()
      call this%items%portable%carry(O9_ROPE)
      call this%player%gain_item(1)
      call a%write_para(cqm_f1008)
      exit L1
    end if

    ! Try taking rope while it's tied to the hook
    if (this%items%s_rope%tied_to_hook()                                &
      .and. ((cmd%object_is(O9_ROPE))                                   &
      .or. (cmd%object_is(O16_HOOK)))) then
      ! You can scale the precipice at the south end of canyon
      if (this%map%path_to_precipice                                    &
        .and. this%player%is_in(R83_LARGE_CAVERN_SOUTH)) then
        call a%write_para(cqm_f1111)
        exit L1
      end if

      ! Either the hook or the rope are not in this room
      if ((.not. this%items%portable%item(O9_ROPE)%in_room(             &
        this%player%room))                                              &
        .or. .not. this%items%portable%item(O16_HOOK)%in_room(          &
        this%player%room)) then
        call a%write_para(cqm_f1003)
        exit L1
      end if

      ! Ok, fine, take the rope and hook
      call this%items%portable%carry(O9_ROPE)
      call this%items%portable%carry(O16_HOOK)
      call this%player%gain_item(2)

      ! But now you can't scale the precipice. Are you happy now?
      if (this%map%path_to_precipice) then
        this%map%path_to_precipice = .false.
      end if

      ! write(unit=STDOUT, fmt=1008)
      call a%write_para(cqm_f1008)
      exit L1
    end if

    ! Take water while next to moat, falls, or island
    if (cmd%object_is(O26_WATER)                                        &
      .and. is_wet_room(this%player%room)) then
      ! Note: fill_with() performs more checks than fill_with_water()
      call this%items%s_bottle%fill_with(cmd%object)
      exit L1
    end if

    select case(cmd%object)
    case(N31_ALL)
      ! TAKE ALL -- pick up everything in the room, except blood
      ! or water.
      do II = 1, NITEMS
        ! Is it blood or water?
        if (II == O5_BLOOD .or. II == O26_WATER) cycle
        ! Is it here in the room?
        if (this%items%portable%item(II)%in_room(this%player%room)) then
          ! Add to inventory
          call this%items%portable%carry(II)
          call this%player%gain_item(1)
          if ((II == O9_ROPE) .and. this%items%s_rope%is_hanging()) then
            call this%items%s_rope%untie()
          end if
          if (this%player%is_encumbered()) then
            ! Encumbered! Complain and stop picking things up
            call a%write_para(cqm_f1012)
            exit L1
          end if
        end if
      end do
      call a%write_para(cqm_f1008)
      exit L1

    case(O8_HUNCHBACK)
      ! Take hunchback!
      if (this%encounters%a_hunchback%will_follow()) then
        ! If you've fed him, you can take the hunchback
        call this%items%portable%carry(cmd%object)
        call this%player%gain_item(1)
        call a%write_para(cqm_f1008)
      else
        ! Hungry, hungry hunchback
        call a%write_para(cqm_f1093)
        end if
      exit L1

    case(N46_BOOK)
      ! Take book
      if (this%player%is_in(R13_LIBRARY)                                   &
        .or. this%player%is_in(R10_SMOKING_ROOM)) then
        ! Try taking a book from the smoking room or library
        call a%write_para(cqm_f1033)
      else
        ! Try taking a book anywhere else
        call a%write_para(cqm_f1003)
      end if
      exit L1

    case(N49_BUTLER)
      ! Take the butler!?
      if (this%encounters%a_butler%is_dead()) THEN
        call a%write_para(cqm_f1146)
      else
        call a%write_para(cqm_f1034)
      end if
      exit L1

    case(N52_NOTE)
      ! Take note (heh.)
      call a%write_para(cqm_f1045)
      exit L1

    case default
      ! Do nothing - pass through

    end select

    ! You can't take imaginary items
    if (cmd%object > NITEMS) then
      call a%write_para(cqm_f1016)
      exit L1
    else if (cmd%object < 1) then
      call a%write_para(cqm_f1003)
      exit L1
    end if

    ! At this point we've verified that ITEMS(cmd%object) is valid

    ! You already have whatever it is
    if (this%items%portable%is_carried(cmd%object)) then
      call a%write_para(cqm_f1096)
      exit L1
    end if

    ! Take gun from parlor or stake from garden
    if ((cmd%object_is(O20_GUN) .and. this%player%is_in(R3_PARLOR))     &
        .or. (cmd%object_is(O6_STAKE)                                   &
        .and. this%player%is_in(R12_GARDEN))) then
      call this%items%portable%carry(cmd%object)
      call this%player%gain_item(1)
      call a%write_para(cqm_f1008)
      exit L1
    end if

    ! Q: Is the 'gone forever' check necessary? It may only apply to rope
    if ((.not. this%items%portable%item(cmd%object)%in_room(            &
      this%player%room))                                                &
      .and. this%items%portable%item(cmd%object)%place /= -2) then
      call a%write_para(cqm_f1003)
      exit L1
    end if

    ! Take water or blood
    if (cmd%object_is(O26_WATER) .or. cmd%object_is(O5_BLOOD)) then
      ! Note: fill_with() performs more checks than the other
      ! fill methods
      call this%items%s_bottle%fill_with(cmd%object)
      exit L1
    end if

    ! Take whatever it is
    call this%items%portable%carry(cmd%object)
    call this%player%gain_item(1)
    call a%write_para(cqm_f1008)
    exit L1

  end do L1

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_take

!> Implement action 12 (V12_DROP). Drop something carried.
type(resolution_t) function do_drop(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_items, only: NITEMS
  use :: m_object_id
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  ! Local variables

  integer :: II

  continue

  call outcome%init()

  !     ---DROP---
  ! ACTION(V12_DROP)  DROP drop

  L1: do

    if (cmd%object < 1) then
      ! Nothing
      call a%write_para(cqm_f1004)
      exit L1
    else if (cmd%object_is(N31_ALL)) then
      ! ALL
      ! Drop everything
      do II = 1, NITEMS
        if (this%items%portable%is_carried(II)) then
          call this%items%portable%item(II)%place_in(this%player%room)
        end if
      end do
      if (this%items%s_bottle%is_full()) then
        call this%items%s_bottle%clear()
      end if
      if (this%items%s_gun%is_loaded()) then
        call this%items%s_gun%unload()
      end if
      this%player%items_held = 0
      call a%write_para(cqm_f1008)
      exit L1
    else if (cmd%object > NITEMS) then
      ! A thing you couldn't be carrying (except ALL which is handled
      ! immediately above. THE cmd%object==31 check MUST occur before this
      ! check OR this check should be:
      !     (cmd%object > NITEMS .and cmd%object /= 31)
      ! OR
      !     (cmd%object > NITEMS + 1)
      ! though the latter makes ALL dependent on NITEMS which the
      ! longer first alternative doesn't
      call a%write_para(cqm_f1002)
      exit L1
    end if

    ! At this point cmd%object is guaranteed to be [1 .. NITMES]

    if ((cmd%object_is(O9_ROPE) .or. cmd%object_is(O16_HOOK))         &
      .and. this%items%s_rope%tied_to_hook()) then
      if (this%items%s_rope%is_carried()                                         &
        .and. this%items%portable%is_carried(O16_HOOK)) then
        ! Drop rope with grappling hook
        call this%items%s_rope%item%place_in(this%player%room)
        call this%items%portable%item(O16_HOOK)%place_in(this%player%room)
        call this%player%lose_item(2)
        if (this%player%is_in(R84_TOP_OF_PRECIPICE)) then
          this%map%path_to_precipice = .true.
          call a%write_para(cqm_f1011)
        else
          call a%write_para(cqm_f1112)
        end if
      else
        call a%write_para(cqm_f1004)
      end if
      exit L1
    end if

    ! Can't drop something not in your inventory
    if (.not. this%items%portable%is_carried(cmd%object)) then
      call a%write_para(cqm_f1004)
      exit L1
    end if

    ! Drop a specific thing you have
    if (this%player%is_in(R27_MIRROR_MAZE)) then
      ! If in MIRROR MAZE, put object in the upstairs hallway
      call this%items%portable%item(cmd%object)%place_in(               &
        R9_UPSTAIRS_HALLWAY)
    else
      ! Otherwise drop it in this room
      call this%items%portable%item(cmd%object)%place_in(               &
        this%player%room)
    end if

    ! Blood or water (but not the bottle; see also POUR)
    ! Note that you don't drop the bottle so NUMB does not decrement
    if ((cmd%object_is(O5_BLOOD))                                       &
      .or. (cmd%object_is(O26_WATER))) then
      ! Empty contents of bottle
      call this%items%s_bottle%clear()
      call a%write_para(cqm_f1008)
      exit L1
    end if

    if ((cmd%object_is(O2_BULLET) .or. cmd%object_is(O20_GUN))          &
      .and. this%items%s_gun%is_loaded()) then
      ! Dropping bullet or gun while gun is loaded?
      call this%items%s_gun%unload()
      if (cmd%object_is(O20_GUN)) then
        call this%player%lose_item(1)
        call this%items%portable%item(O2_BULLET)%place_in(              &
          this%player%room)
      end if
      call a%write_para(cqm_f1008)
      exit L1
    end if
    call this%player%lose_item(1)

    ! Drop a full bottle
    if (cmd%object_is(O18_BOTTLE)                                       &
      .and. this%items%s_bottle%is_full()) then
      ! Drop contents, then clear bottle
      if (this%items%s_bottle%contains_blood()) then
        call this%items%portable%item(O5_BLOOD)%place_in(               &
          this%player%room)
      else
        call this%items%portable%item(O26_WATER)%place_in(              &
          this%player%room)
      end if
      call this%items%s_bottle%clear()
      call a%write_para(cqm_f1008)
      exit L1
    end if

    ! In the foyer
    if (this%player%is_in(R8_FOYER)) then
      if (this%encounters%a_butler%is_alert()) then
        ! Butler is awake or holding note (which he shouldn't have
        ! just yet... or should he?)
        if (this%items%portable%item(O11_PEN)%in_room(R8_FOYER)         &
          .and. this%items%portable%item(O10_PAPER)%in_room(R8_FOYER))  &
          then
          ! If you've dropped both the pen and the paper here,
          ! they vanish and the butler is now holding a note
          this%items%portable%item(O10_PAPER)%place = -3
          this%items%portable%item(O11_PEN)%place = -3
          call this%encounters%a_butler%write_note()
          ! call DES(402, this%option%debug_mode)
          ! call a%write_para(cq_form2(402-369))
          call a%write_para(cq_form2(33))
        else
          call a%write_para(cqm_f1008)
        end if
      else
        ! Butler is asleep, gone, or dead
        call a%write_para(cqm_f1008)
      end if
    else
      call a%write_para(cqm_f1008)
    end if

    exit L1
  end do L1

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_drop

!> Implement action 13 (V13_ENTER). ENTER
type(resolution_t) function do_enter(this, revised_action)              &
  result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  use :: m_object_id
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Revised player action
  integer, intent(inout) :: revised_action

  continue

  call outcome%init()

  !     ---ENTER---
  ! ACTION(13)  ENTE, IN enter

  revised_action = this%map%room(this%player%room)%enter_to()
  if (revised_action == 0) then
    ! Unresolved enter direction
    call a%write_para(cqm_f1013)
    ! Signals 'cycle L25' on exit
    outcome%to_next_action = .true.
  else
    ! Increment move counter and resolve player action (J /= 0)
    call this%player%advance()
    ! Signals 'cycle L26' on exit
    outcome%reprocess_action = .true.
  end if

  return
end function do_enter

!> Implement action 14 (V14_EXIT). Exit a location
type(resolution_t) function do_exit(this, revised_action)               &
  result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  use :: m_object_id
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Revised player action
  integer, intent(inout) :: revised_action

  ! Local variables
  integer :: from_room
  integer :: to_room

  continue

  call outcome%init()

  revised_action = this%map%room(this%player%room)%leave_to()
  if (revised_action == 0) then
    if (this%player%last_room > 0) then
      from_room = this%player%room
      to_room = this%player%last_room
      call this%player%move_to(to=to_room, from=from_room,              &
        with_advance=.true.)
      ! Signals 'cycle L20' on exit
      outcome%to_next_area = .true.
    else
      call a%write_para(cqm_f1035)
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
    end if
  else
    call this%player%advance()
    ! Signals 'cycle L26' on exit
    outcome%reprocess_action = .true.
  end if

  return
end function do_exit

!> Implement action 15 (V15_ATTACK) and action 16 (V16_KILL). Kill
!! someone or something.
type(resolution_t) function do_attack_kill(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none

  ! Arguments

  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  continue

  call outcome%init()

  !     ---ATTACK---
  ! ACTION(15)  ATTA attack
  ! ACTION(16)  KILL kill

  L1: do

    ! case(N76_WEREWOLF)
    if (cmd%object_is(N77_GNOME)                                        &
      .and. this%encounters%a_gnome%is_attacking()) then
      call a%write_para(cqm_f1107)
      ! Signals 'cycle L25' on exit
      exit L1
    end if

    ! case(N76_WEREWOLF)
    ! case(N49_BUTLER)
    ! case(N55_BAT)
    if (cmd%object_is(N76_WEREWOLF)                                     &
      .or. cmd%object_is(N49_BUTLER)                                    &
      .or. cmd%object_is(N55_BAT) ) then

      if (cmd%object_is(N55_BAT)                                        &
        .and. ((.not. this%player%is_in(R20_ATTIC_ENTRANCE))            &
              .or. (this%encounters%a_bat%is_gone()))) then
        call a%write_para(cqm_f1003)
        ! Signals 'cycle L25' on exit
        exit L1
      else if (cmd%object_is(N49_BUTLER)                                &
        .and. ((.not. this%player%is_in(R8_FOYER))                      &
              .or. this%encounters%a_butler%is_dead())) then
        call a%write_para(cqm_f1003)
        ! Signals 'cycle L25' on exit
        exit L1
      else if (cmd%object_is(N76_WEREWOLF)                              &
        .and. this%encounters%a_werewolf%is_calm()) then
        call a%write_para(cqm_f1003)
        ! Signals 'cycle L25' on exit
        exit L1
      end if

      call a%write_para(cqm_f1039)
      if (player_agrees()) then
        if (cmd%object_is(N76_WEREWOLF)) then
          ! call DES(414, this%option%debug_mode)
          ! call a%write_para(cq_form2(414-369))
          call a%write_para(cq_form2(45))
        else if (cmd%object_is(N49_BUTLER)) then
          ! call DES(415, this%option%debug_mode)
          ! call a%write_para(cq_form2(415-369))
          call a%write_para(cq_form2(46))
        else if (cmd%object_is(N55_BAT)) then
          ! call DES(416, this%option%debug_mode)
          ! call a%write_para(cq_form2(416-369))
          call a%write_para(cq_form2(47))
        end if
      end if
      ! Signals 'cycle L25' on exit
      exit L1
    end if

    ! case(O8_HUNCHBACK)
    if (cmd%object_is(O8_HUNCHBACK)) then
      call a%write_para(cqm_f1039)
      if (player_agrees()) then
        if (this%items%portable%item(O8_HUNCHBACK)%at_hand_in(          &
          this%player%room)) then
          if (this%items%portable%is_carried(O8_HUNCHBACK)) then
            call this%player%lose_item(1)
          end if
          call this%items%portable%consume(O8_HUNCHBACK)
          ! call DES(422, this%option%debug_mode)
          ! call a%write_para(cq_form2(422-369))
          call a%write_para(cq_form2(53))
        else
          call a%write_para(cqm_f1003)
        end if
      end if
      ! Signals 'cycle L25' on exit
      exit L1
    end if

    ! case(N78_CYCLOPS)
    if (cmd%object_is(N78_CYCLOPS)) then
      if (this%player%is_in(R86_TALL_TUNNEL)                            &
        .and. this%encounters%a_cyclops%blocks_way()) then
        call a%write_para(cqm_f1122)
      else
        call a%write_para(cqm_f1003)
      end if
      ! Signals 'cycle L25' on exit
      exit L1
    end if

    ! case(N39_MASTER)
    if (cmd%object_is(N39_MASTER)                                       &
      .and. this%encounters%a_master%is_alive()) then
      call a%write_para(cqm_f1039)
      if (player_agrees()) then
        call a%write_para(cqm_f1086)
      end if
      ! Signals 'cycle L25' on exit
      exit L1
    end if

    ! case(N80_WIZARD)
    if (cmd%object_is(N80_WIZARD)) then
      if (this%player%is_in(R93_THRONE_ROOM)                            &
        .and. this%encounters%a_wizard%blocks_way()) then
        call a%write_para(cqm_f1129h, (/cqm_f1129b/))
      else
        call a%write_para(cqm_f1003)
      end if
      exit L1
    end if

    ! case default
    ! call DES(418, this%option%debug_mode)
    ! call a%write_para(cq_form2(418-369))
    call a%write_para(cq_form2(49))
    exit L1

  end do L1

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_attack_kill

!> Implement action 17 (V17_THROW). Throw something
type(resolution_t) function do_throw(this, cmd, revised_action)         &
  result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_items, only: NITEMS
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd
  !> Revised command action for reprocessing
  integer, intent(inout) :: revised_action

  ! Local variables

  integer :: II

  continue

  call outcome%init()

  !     ---THROW---
  ! ACTION(17)  THRO throw

  L1: do

    ! select case(cmd%object)
    ! case(O26_WATER)
    ! case(O16_HOOK)
    ! case(O9_ROPE)
    ! case(O6_STAKE)
    ! case(O25_ACID)
    ! case(O27_CIGAR)
    ! case(O12_SWORD, O3_AXE)
    ! case default
    ! end select

    if (cmd%object_is(O26_WATER)                                        &
      .and. this%player%is_in(R47_FIRE_ROOM)) then
      if (this%items%portable%is_carried(O26_WATER)) then
        call this%items%s_bottle%water%consume()
        call this%items%s_bottle%clear()
        this%map%fire_blocks_way = .false.
        call a%write_para(cqm_f1103)
        outcome%to_next_action = .true.
        exit L1
      else
        call a%write_para(cqm_f1102)
        outcome%to_next_action = .true.
        exit L1
      end if
    end if

    ! Grappling hook
    if (cmd%object_is(O16_HOOK)) then
      if (this%player%is_in(R83_LARGE_CAVERN_SOUTH)) then
        ! Use the grappling hook to scale the precipice
        this%map%path_to_precipice = .true.
        call this%items%portable%item(O16_HOOK)%place_in(R84_TOP_OF_PRECIPICE)
        call this%items%s_rope%item%place_in(R84_TOP_OF_PRECIPICE)
        call this%player%lose_item(2)
        call a%write_para(cqm_f1011)
        outcome%to_next_action = .true.
        exit L1
      else
        ! Throw grappling hook
        ! Treat it as drop
        revised_action = V12_DROP
        outcome%reprocess_action = .true.
        exit L1
      end if
    end if

    ! Throw rope
    if (cmd%object_is(O9_ROPE)) then
      if (this%items%s_rope%tied_to_hook()) then
        if (this%player%is_in(R83_LARGE_CAVERN_SOUTH)) then
          this%map%path_to_precipice = .true.
          call this%items%portable%item(O16_HOOK)%place_in(             &
            R84_TOP_OF_PRECIPICE)
          call this%items%s_rope%item%place_in(R84_TOP_OF_PRECIPICE)
          call this%player%lose_item(2)
          call a%write_para(cqm_f1011)
          outcome%to_next_action = .true.
          exit L1
        else
          ! Throw rope
          ! Treat it as drop; no interference with early filtering
          revised_action = V12_DROP
          outcome%reprocess_action = .true.
          exit L1
        end if
      else if (this%player%is_in(R1_BEDROOM)) then
        if (this%items%s_window_1%is_open()) then
          if (this%items%s_rope%is_loose()                              &
            .or. this%items%s_rope%is_gone()) then
            ! Oops
            call this%items%s_rope%lose_forever()
            call this%player%lose_item(1)
            call this%items%portable%consume(O9_ROPE)
            call a%write_para(cqm_f1071)
          else if (this%items%s_rope%tied_to_bed()) then
            ! Rappel!
            call this%items%s_rope%hang()
            call a%write_para(cqm_f1070)
            call this%player%score%apply_rope_score()
            call this%player%lose_item(1)
            call this%items%s_rope%item%place_in(this%player%room)
          else
            ! Already hanging out of window
            call a%write_para(cqm_f1072)
          end if
        else
          ! Throw rope in room 1 with window not open, any state of
          ! ROPE. Can't treat as drop because drop does early
          ! filtering on rope. Manually walking through DROP ROPE
          ! logic downstream of 217; it's treated as just a normal
          ! item drop
          call this%items%portable%item(cmd%object)%place_in(           &
            this%player%room)
          call this%player%lose_item(1)
          call a%write_para(cqm_f1008)
        end if
        outcome%to_next_action = .true.
        exit L1

      else
        ! Throw rope in any room but 1
        ! Treat it as drop; no interference with early
        ! rope filtering
        revised_action = V12_DROP
        outcome%reprocess_action = .true.
        exit L1
      end if
    end if

    if (cmd%object > NITEMS) then
      call a%write_para(cqm_f1005)
      outcome%to_next_action = .true.
      exit L1
    end if

    if (cmd%object < 1) then
      call a%write_para(cqm_f1004)
      outcome%to_next_action = .true.
      exit L1
    end if

    if (.not. this%items%portable%item(cmd%object)%at_hand_in(          &
      this%player%room)) then
      call a%write_para(cqm_f1004)
      outcome%to_next_action = .true.
      exit L1
    end if

    ! Not sword or axe
    if (cmd%object_is_not(O12_SWORD)                                    &
      .and. cmd%object_is_not(O3_AXE)) then
      if (cmd%object_is(O6_STAKE)                                       &
        .and. this%player%is_in(R43_MASTER_CHAMBER)) then
          ! Stab the count!
        if (this%encounters%a_master%in_coffin()) then
          call a%write_para(cqm_f1003)
          outcome%to_next_action = .true.
          exit L1
        end if
        if (this%encounters%a_master%is_dead()                          &
          .or. .not. this%player%is_in(R43_MASTER_CHAMBER)) then
          call a%write_para(cqm_f1005)
          outcome%to_next_action = .true.
          exit L1
        end if
        if (this%items%portable%is_carried(O6_STAKE)) then
          call a%write_para(cqm_f1087h, (/cqm_f1087b/))
          ! Kill the Master!
          call this%encounters%a_master%expire()
          ! Consume the wooden stake!
          call this%items%portable%consume(O6_STAKE)
          ! Lighten your inventory!
          call this%player%lose_item(1)
          ! Add to your score!
          call this%player%score%add_to_score(25)
          ! Set all mirror maze exits to R2_DIM_CORRIDOR!
          call this%map%mirror_maze%open_emergency_exits()
          call a%write_para(cqm_f1130)
          call a%write_para(cqm_f1131)
          call a%write_para(cqm_f1132)
        else
          call a%write_para(cqm_f1086)
        end if
        outcome%to_next_action = .true.
        exit L1
      end if

      ! Acid
      if (cmd%object_is(O25_ACID)) then
        ! You are trying to throw acid
        if (.not. this%items%portable%is_carried(O25_ACID)) then
          call a%write_para(cqm_f1004)
          outcome%to_next_action = .true.
          exit L1
        end if

        call this%player%lose_item(1)
        if (this%player%is_in(R1_BEDROOM)) then
          if (this%items%s_shutter%is_closed()) then
            call a%write_para(cqm_f1063)
            call this%items%portable%consume(O25_ACID)
            call this%player%score%reduce_mmax(10)
            outcome%to_next_action = .true.
            exit L1
          else
            call this%items%s_window_1%open()
            call a%write_para(cqm_f1061)
            call this%items%portable%consume(O25_ACID)
            outcome%to_next_action = .true.
            exit L1
          end if
        end if

        if (this%encounters%a_werewolf%is_attacking()) then
          call a%write_para(cqm_f1062)
          call this%encounters%a_werewolf%calm()
          call this%items%portable%consume(O25_ACID)
    !             Reduce MMAX by ten, because the acid is needed to
    !             get passed the bars and retrieve the statue.
          ! call log_debug('L26: Throw acid at werewolf; '          &
          !   // 'lose bedroom window opportunity')
          call this%player%score%reduce_mmax(10)
          outcome%to_next_action = .true.
          exit L1
        end if

        if ((this%player%is_in(R10_SMOKING_ROOM))                      &
          .and. (.not. this%items%s_window_2%is_nailed())) then
          call this%items%s_window_2%open()
          call a%write_para(cqm_f1061)
        else
          call a%write_para(cqm_f1063)
          call this%items%portable%consume(O25_ACID)
          ! call log_debug('L26: Throw acid at closed smoking '     &
          !   // 'room window; lose bedroom window opportunity')
          call this%player%score%reduce_mmax(10)
        end if

        outcome%to_next_action = .true.
        exit L1
      end if

      if (cmd%object_is(O9_ROPE)) then
        if (this%items%s_rope%tied_to_hook()) then
          if (this%player%is_in(R83_LARGE_CAVERN_SOUTH)) then
            this%map%path_to_precipice = .true.
            call this%items%portable%item(O16_HOOK)%place_in(           &
              R84_TOP_OF_PRECIPICE)
            call this%items%s_rope%item%place_in(R84_TOP_OF_PRECIPICE)
            call this%player%lose_item(2)
            call a%write_para(cqm_f1011)
            outcome%to_next_action = .true.
            exit L1
          else
            ! Throw rope
            ! Treat it as drop; no interference with early filtering
            revised_action = V12_DROP
            outcome%reprocess_action = .true.
            exit L1
          end if
        end if

        if (this%player%is_in(R1_BEDROOM)) then
          if (this%items%s_window_1%is_open()) then
            if (this%items%s_rope%is_loose()                            &
              .or. this%items%s_rope%is_gone()) then
              ! Oops
              call this%items%s_rope%lose_forever()
              call this%player%lose_item(1)
              call this%items%portable%consume(O9_ROPE)
              call a%write_para(cqm_f1071)
            else if (this%items%s_rope%tied_to_bed()) then
              ! Rappel!
              call this%items%s_rope%hang()
              call a%write_para(cqm_f1070)
              call this%player%score%apply_rope_score()
              call this%player%lose_item(1)
              call this%items%s_rope%item%place_in(this%player%room)
            else
              ! Already hanging out of window
              call a%write_para(cqm_f1072)
            end if
          else
            ! Throw rope in room 1 with window not open, any state of ROPE
            ! Can't treat as drop because drop does early filtering on rope
            ! Manually walking through DROP ROPE logic downstream of 217
            ! it's treated as just a normal item drop
            call this%items%portable%item(cmd%object)%place_in(         &
              this%player%room)
            call this%player%lose_item(1)
            call a%write_para(cqm_f1008)
          end if
          outcome%to_next_action = .true.
          exit L1

        else
          ! Throw rope in any room but 1
          ! Treat it as drop; no interference with early rope filtering
          revised_action = V12_DROP
          outcome%reprocess_action = .true.
          exit L1
        end if
      end if

      ! Grappling hook
      if (cmd%object_is(O16_HOOK)) then
        if (this%player%is_in(R83_LARGE_CAVERN_SOUTH)) then
          this%map%path_to_precipice = .true.
          call this%items%portable%item(O16_HOOK)%place_in(R84_TOP_OF_PRECIPICE)
          call this%items%s_rope%item%place_in(R84_TOP_OF_PRECIPICE)
          call this%player%lose_item(2)
          call a%write_para(cqm_f1011)
          outcome%to_next_action = .true.
          exit L1
        else
          ! Throw grappling hook
          ! Treat it as drop
          revised_action = V12_DROP
          outcome%reprocess_action = .true.
          exit L1
        end if
      end if

      ! You have a cigar
      if (cmd%object_is(O27_CIGAR)) then
        ! You're in the tall tunnel room and there's no hole in the
        ! door
        if (this%player%is_in(R86_TALL_TUNNEL)                         &
          .and. this%encounters%a_cyclops%blocks_way()) then
            this%items%portable%item(O27_CIGAR)%place = -3
          call this%player%lose_item(1)
          call a%write_para(cqm_f1119h, (/cqm_f1119b/))
          outcome%to_next_action = .true.
          exit L1
        else
          ! Treat it as drop
          revised_action = V12_DROP
          outcome%reprocess_action = .true.
          exit L1
        end if
      else
        ! Treat it as drop
        revised_action = V12_DROP
        outcome%reprocess_action = .true.
        exit L1
      end if
    else
      ! Sword or axe
      if (cmd%object_is(O12_SWORD)) then
        if (this%player%is_in(R99_ELEVATOR_BETWEEN_FLOORS)              &
        .and. (.not. this%map%path_through_final_door)) then
          this%map%path_through_final_door = .true.
          call a%write_para(cqm_f1136)
          outcome%to_next_action = .true.
          exit L1
        else if (this%player%is_in(R93_THRONE_ROOM)                     &
          .and. this%encounters%a_wizard%blocks_way()) then
          call this%items%portable%item(O13_TORCH)%place_in(            &
            R86_TALL_TUNNEL)
          call a%write_para(cqm_f1133)
          outcome%to_next_life = .true.
          exit L1
        end if
      end if

      ! Stab gnome!
      if (this%encounters%a_gnome%is_attacking()) then
        call this%encounters%a_gnome%resolve_blade_fight(II,            &
          this%player, this%items%portable, cmd%object,                 &
          this%map%path_to_precipice)

        if (II == 0) then
          ! Victory!
          outcome%to_next_action = .true.
          exit L1
        else
          ! I have some bad news for you
          outcome%to_next_life = .true.
          exit L1
        end if
      end if

      if (cmd%object_is(O12_SWORD)) then
        call this%items%portable%item(O12_SWORD)%place_in(              &
          this%player%room)
      else if (cmd%object_is(O3_AXE)                                    &
        .and. this%player%room < R95_WAREHOUSE_MAIN) then
        call this%items%portable%item(O3_AXE)%place_in(this%player%room)
      end if

      call this%player%lose_item(1)

      if (this%encounters%a_werewolf%is_attacking()) then
        cmd%object = N76_WEREWOLF
      else if (this%items%portable%item(O8_HUNCHBACK)%at_hand_in(       &
        this%player%room)) then
        cmd%object = O8_HUNCHBACK
      end if

      if (this%player%is_in(R8_FOYER)) then
        cmd%object = N49_BUTLER
      end if

      if (this%player%is_in(R20_ATTIC_ENTRANCE)) then
        cmd%object = N55_BAT
      end if

      if (this%player%is_in(R86_TALL_TUNNEL)) then
        cmd%object = N78_CYCLOPS
      end if

      if (this%player%is_in(R93_THRONE_ROOM)                                 &
        .and. this%encounters%a_wizard%blocks_way()) then
        call a%write_para(cqm_f1129h, (/cqm_f1129b/))
        outcome%to_next_action = .true.
        exit L1
      end if

      if (this%player%is_in(R6_KITCHEN)) then
        ! If in kitchen and throwing axe or sword
        ! Translate to BREAK BOARDS and reprocess
        cmd%object = N56_BOARD
        revised_action = V37_BREAK
        outcome%reprocess_action = .true.
        exit L1
      else
        ! Treat the same as stab for werewolf, hunchback, butler, bat, and cyclops
        call this%resolve_thrown_weapon(cmd)
        outcome%to_next_action = .true.
        exit L1
      end if
    end if

    exit L1
  end do L1

  return
end function do_throw

!> Implement action 18 (V18_LOAD). LOAD
type(resolution_t) function do_load(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  continue

  call outcome%init()

  !     ---LOAD---
  ! ACTION(18)  LOAD load
  if (cmd%no_object()) then
    ! If you don't specify, assume you're loading the gun
    cmd%object = O20_GUN
  end if

  if (cmd%object_is(O20_GUN)) then
    if (this%items%s_gun%is_carried()) then
      if (this%items%portable%is_carried(O2_BULLET)) then
        if (this%items%s_gun%is_empty()) then
          ! If the gun isn't loaded, load the bullet and open an
          ! inventory slot
          call this%player%lose_item(1)
        end if
        ! The gun is now loaded. Be careful.
        call this%items%s_gun%load()
        call a%write_para(cqm_f1008)
      else
        ! You can't load a gun if you aren't carrying a bullet
        call a%write_para(cqm_f1007)
      end if

    else
      ! You can't load a gun if you aren't carrying it
      call a%write_para(cqm_f1004)
    end if

  else
    ! You can't load anything but the gun
    call a%write_para(cqm_f1002)
  end if

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_load

!> Implement action 19 (V19_AHEM). AHEM
type(resolution_t) function do_ahem(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  continue

  call outcome%init()

  !    ---FUCK---
  ! ACTION(19)  FUCK ahem
  if (cmd%no_object()) then
    ! In general
    call a%write_para(cqm_f1047)
  else
    ! This thing in particular
    call a%write_para(cqm_f1079)
  end if

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_ahem

!> Implement action 20 (V20_WAVE). WAVE
type(resolution_t) function do_wave(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  continue

  call outcome%init()

  !     ---WAVE---
  ! ACTION(20)  WAVE, SHOW show, wave

  L1: do
    if (cmd%object < 1) then
      call a%write_para(cqm_f1004)
      exit L1
    end if

    if (.not. this%items%portable%is_carried(cmd%object)) then
      call a%write_para(cqm_f1004)
      exit L1
    end if

    if (cmd%object_is(O3_AXE)) then
      select case(this%player%room)
      case(R95_WAREHOUSE_MAIN)
        if (this%player%revealed_letter(1)) then
          call a%write_para(cqm_f1095)
        else
          call a%write_para(cqm_f1137)
          this%player%revealed_letter(1) = .true.
          call this%player%score%add_to_score(3)
        end if
        exit L1
      case(R96_WAREHOUSE_WEST)
        if (this%player%revealed_letter(2)) then
          call a%write_para(cqm_f1095)
        else
          call a%write_para(cqm_f1138)
          this%player%revealed_letter(2) = .true.
          call this%player%score%add_to_score(2)
        end if
        exit L1
      case(R97_WAREHOUSE_NORTH)
        if (this%player%revealed_letter(3)) then
          call a%write_para(cqm_f1095)
        else
          call a%write_para(cqm_f1139)
          this%player%revealed_letter(3) = .true.
          call this%player%score%add_to_score(3)
        end if
        exit L1
      case(R98_WAREHOUSE_EAST)
        if (this%player%revealed_letter(4)) then
          call a%write_para(cqm_f1095)
        else
          call a%write_para(cqm_f1140)
          this%player%revealed_letter(4) = .true.
          call this%player%score%add_to_score(2)
        end if
        exit L1
      end select
    end if

    if (this%player%is_in(R93_THRONE_ROOM)) then
      if (this%encounters%a_wizard%blocks_way()) then
        call a%write_para(cqm_f1134h, (/cqm_f1134b/))
        call a%write_para(cqm_f1135h, (/cqm_f1135b/))
        call this%encounters%a_wizard%flee()
      else
        call a%write_para(cqm_f1095)
      end if
      exit L1
    end if

    if ((.not. this%player%is_in(R43_MASTER_CHAMBER))                   &
      .or. (this%encounters%a_master%in_coffin()                        &
      .or. this%encounters%a_master%is_dead())                          &
      .or. cmd%object_is_not(O19_CROSS)) then
      call a%write_para(cqm_f1095)
      exit L1
    end if

    if (this%items%portable%is_carried(O19_CROSS)) then
      call this%encounters%a_master%pin()
      ! call DES(425, this%option%debug_mode)
      ! call a%write_para(cq_form2(425-369))
      call a%write_para(cq_form2(56))
    else
      call a%write_para(cqm_f1004)
    end if
    exit L1

  end do L1

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_wave

!> Implement action 21 (V21_STAB). STAB
type(resolution_t) function do_stab(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  ! Local variables
  integer :: II

  continue

  call outcome%init()

  !     ---STAB---
  ! ACTION(21)  STAB stab

  L1: do
    ! Stab the count!
    if (cmd%object_is(N39_MASTER)) then
      if (this%encounters%a_master%in_coffin()) then
        call a%write_para(cqm_f1003)
      else if (this%encounters%a_master%is_alive()                      &
        .and. this%player%is_in(R43_MASTER_CHAMBER)) then
        if (this%items%portable%is_carried(O6_STAKE)) then
          call a%write_para(cqm_f1087h, (/cqm_f1087b/))
          call this%encounters%a_master%expire()
          call this%items%portable%consume(O6_STAKE)
          call this%player%lose_item(1)
          call this%player%score%add_to_score(25)
          ! Set all mirror maze exits to R2_DIM_CORRIDOR
          call this%map%mirror_maze%open_emergency_exits()
          call a%write_para(line_1=cqm_f1130,                           &
            line_n=(/ character(len=80) :: cqm_f1131, cqm_f1132 /))
        else
          call a%write_para(cqm_f1086)
        end if
      else
        call a%write_para(cqm_f1005)
      end if

      outcome%to_next_action = .true.
      exit L1
    end if

    ! Wizard
    if (cmd%object_is(N80_WIZARD)) then
      call this%items%portable%item(O13_TORCH)%place_in(R86_TALL_TUNNEL)
      call a%write_para(cqm_f1133)
      outcome%to_next_action = .true.
      exit L1
    end if

    ! No sword, bro.
    if (.not. this%items%portable%is_carried(O12_SWORD)) then
      call a%write_para(cqm_f1060)
      outcome%to_next_action = .true.
      exit L1
    end if

    ! Stab gnome!
    if (cmd%object_is(N77_GNOME)) then
      if (this%encounters%a_gnome%is_attacking()) then
        call this%encounters%a_gnome%resolve_blade_fight(II,            &
          this%player, this%items%portable, cmd%object,                 &
          this%map%path_to_precipice)

        if (II == 0) then
          ! Victory!
          outcome%to_next_action = .true.
        else
          ! I have some bad news for you
          outcome%to_next_life = .true.
        end if
        exit L1
      end if
    end if

    ! We only get here if the sword is being carried
    call this%items%portable%item(O12_SWORD)%place_in(this%player%room)
    call this%player%lose_item(1)

    call this%resolve_thrown_weapon(cmd)
    ! Signals 'cycle L25' on exit
    outcome%to_next_action = .true.
  end do L1

  return
end function do_stab

!> Implement action 22 (V22_FEED). FEED
type(resolution_t) function do_feed(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  continue

  call outcome%init()

  !     ---FEED---
  ! ACTION(22)  FEED feed

  if(N55_BAT == cmd%object) then
    if (this%player%is_in(R20_ATTIC_ENTRANCE)) then
      ! The bat is here
      if (this%items%portable%is_carried(O5_BLOOD)) then
        ! Bats love bottled blood
        call this%items%portable%consume(O5_BLOOD)
        call this%encounters%a_bat%eat()
        call this%items%s_bottle%clear()
        call a%write_para(cqm_f1051)
      else if (this%items%portable%is_carried(O22_FOOD)) then
        ! Not so fond of TV dinners tho
        call a%write_para(cqm_f1052)
      else
        ! I can't feed the bat nothing, Kent!
        call a%write_para(cqm_f1050)
      end if
    else
      ! You are not where the bat is
      call a%write_para(cqm_f1003)
    end if
  else if (this%items%portable%is_carried(O22_FOOD)) then
    select case(cmd%object)
    case(N55_BAT)
      if (this%player%is_in(R20_ATTIC_ENTRANCE)) then
        ! The bat is here
        if (this%items%portable%is_carried(O5_BLOOD)) then
          ! Bats love bottled blood
          call this%items%portable%consume(O5_BLOOD)
          call this%encounters%a_bat%eat()
          call this%items%s_bottle%clear()
          call a%write_para(cqm_f1051)
        else if (this%items%portable%is_carried(O22_FOOD)) then
          ! Not so fond of TV dinners tho
          call a%write_para(cqm_f1052)
        else
          ! I can't feed the bat nothing, Kent!
          call a%write_para(cqm_f1050)
        end if
      else
        ! You are not where the bat is
        call a%write_para(cqm_f1003)
      end if

    case(N49_BUTLER)
      ! You're trying to feed the butler
      if (this%player%is_in(R8_FOYER)) then
        ! The butler is here
        if (this%encounters%a_butler%is_dead()) then
          ! This is an ex-butler
          call a%write_para(cqm_f1005)
        else
          ! Feeding time is over
          call a%write_para(cqm_f1092)
        end if
      else
        ! You aren't where the butler is
        call a%write_para(cqm_f1003)
      end if

    case(N78_CYCLOPS)
      ! You're trying to feed the cyclops
      ! You aren't where the cyclops is
      if (this%player%is_in(R86_TALL_TUNNEL)                            &
        .and. this%encounters%a_cyclops%blocks_way()) then
        call a%write_para(cqm_f1124)
      else
        call a%write_para(cqm_f1003)
      end if

    case(O8_HUNCHBACK)
      ! You want to feed the hunchback
      if (this%items%portable%item(O8_HUNCHBACK)%in_room(               &
        this%player%room)) then
        ! And the hunchback is in the room
        ! nomnomnom
        call this%encounters%a_hunchback%eat()
        call this%items%portable%consume(O22_FOOD)
        call this%player%lose_item(1)
        call a%write_para(cqm_f1090)
      else
        ! Hunchback is AWOL
        call a%write_para(cqm_f1003)
      end if

    case(N76_WEREWOLF)
      ! You're trying to feed the werewolf
      if (this%encounters%a_werewolf%is_attacking()) then
        ! The werewolf is here!
        call a%write_para(cqm_f1091)
      else
        ! Werewolf is AWOL
        call a%write_para(cqm_f1003)
      end if

    case default
      ! I don't know what you're trying to feed
      call a%write_para(cqm_f1002)

    end select
  else
    ! You have no food
    call a%write_para(cqm_f1097)
  end if

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_feed

!> Implement action 23 (V23_EAT). EAT
type(resolution_t) function do_eat(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player
  type(action_t), intent(in) :: cmd

  continue

  call outcome%init()

  !     ---EAT---
  ! ACTION(23)  EAT  eat
  if (cmd%object_is(O22_FOOD)) then
    if (this%items%portable%item(O22_FOOD)%at_hand_in(                  &
      this%player%room)) then
      call a%write_para(cqm_f1017)
      this%items%portable%item(O22_FOOD)%place = -3
    else
      ! There's no food nearby
      call a%write_para(cqm_f1004)
    end if
  else
    ! Everything else is inedible
    call a%write_para(cqm_f1016)
  end if

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_eat

!> Implement action 24 (V24_DRINK). Drink a liquid.
type(resolution_t) function do_drink(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_items, only: NITEMS
  use :: m_object_id
  use :: m_where, only: is_wet_room
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  continue

  call outcome%init()

  !     ---DRINK---
  ! ACTION(24)  DRIN drink
  if (cmd%object > NITEMS) then
    call a%write_para(cqm_f1016)
  else if (cmd%object < 1) then
    call a%write_para(cqm_f1004)
  else
    if (cmd%object_is(O26_WATER)                                        &
      .and. is_wet_room(this%player%room)) then
      call a%write_para(cqm_f1018)
    else if (this%items%portable%item(cmd%object)%at_hand_in(           &
      this%player%room)) then
      select case(cmd%object)
      case(O5_BLOOD)
        call a%write_para(cqm_f1016)
      case(O7_CHAMPAGNE)
        call a%write_para(cqm_f1104)
      case(O25_ACID)
        call a%write_para(cqm_f1113)
        call this%items%portable%consume(O25_ACID)
      case(O26_WATER)
        call this%items%s_bottle%water%consume()
        call this%items%s_bottle%clear()
        call a%write_para(cqm_f1018)
      case default
        call a%write_para(cqm_f1002)
      end select
    else
      call a%write_para(cqm_f1004)
    end if
  end if

  outcome%to_next_action = .true.

  return
end function do_drink

!> Implement action 25 (V25_JUMP). Jump off or out of something.
type(resolution_t) function do_jump(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  continue

  call outcome%init()

  !     ---JUMP---
  ! ACTION(25)  JUMP jump

  select case(this%player%room)
  case(R42_STEEP_LEDGE)
    ! call DES(805, this%option%debug_mode)
    ! Player demise #3
    call a%write_para(cqm_death_3_h, cqm_death_3_b)
    ! Signals 'exit L25' on exit
    outcome%to_next_life = .true.

  case(R1_BEDROOM)
    if (this%items%s_window_1%is_open()) then
      ! call DES(801, this%option%debug_mode)
      ! Player demise #1
      call a%write_para(cqm_death_1_h, cqm_death_1_b)

      ! Signals 'exit L25' on exit
      outcome%to_next_life = .true.
    else
      call a%write_para(cqm_f1080)
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
    end if

  case(R10_SMOKING_ROOM)
    if (this%items%s_window_2%is_open()) then
      call this%player%move_to(to=R39_BELOW_SMALL_WINDOW, from=0)
      outcome = this%check_boat_master_after_move()
    else
      call a%write_para(cqm_f1080)
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
    end if

  case(R38_TOWER)
    call a%write_para(cqm_f1059)
    ! Signals 'exit L25' on exit
    outcome%to_next_life = .true.

  case(R67_ABOVE_WATERFALL, R76_SMALL_LEDGE)
    call a%write_para(cqm_f1105h, (/ cqm_f1106b /))
    ! Signals 'exit L25' on exit
    outcome%to_next_life = .true.

  case default
    call a%write_para(cqm_f1080)
    ! Signals 'cycle L25' on exit
    outcome%to_next_action = .true.

  end select

  return
end function do_jump

!> Implement action 26 (V26_INVENTORY). List what player is carrying.
type(resolution_t) function do_inventory(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_object_id
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  continue

  call outcome%init()
  call this%items%portable%show_inventory(this%player%items_held,       &
    this%items%s_gun, this%items%s_bottle)
  outcome%to_next_action = .true.

  return
end function do_inventory

!> Implement action 27 (V27_OPEN). Open something.
type(resolution_t) function do_open(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  use :: m_where, only: near_doors
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  continue

  call outcome%init()

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  L1: do

    !     ---OPEN---
    ! ACTION(27)  OPEN open

    if (cmd%no_object() .and. near_doors(this%player%room)) then
      cmd%object = N47_DOOR
    end if

    select case (cmd%object)
    case(O7_CHAMPAGNE)
      ! Champagne
      call a%write_para(cqm_f1104)
      exit L1

    case(N47_DOOR)
      ! Door

      ! Kitchen
      if (this%player%is_in(R6_KITCHEN)) then
        if (this%map%room(R6_KITCHEN)%door%is_open()) then
          ! Open - cannot open more than it already is
          call a%write_para(cqm_f1020)
          exit L1
        else if (this%map%room(R6_KITCHEN)%door%is_closed()) then
          ! Door is closed - can open
          call this%map%room(R6_KITCHEN)%door%open()
          call this%player%move_to(to=R7_BRICK_WALL, from=R6_KITCHEN)
          call this%player%score%apply_combination_score()
          outcome%to_next_area = .true.
          exit L1
        else if (this%map%room(R6_KITCHEN)%door%is_locked()) then
          ! Door is locked
          call a%write_para(cqm_f1021)
          exit L1
        end if

      end if

      if (this%map%room(this%player%room)%door%is_static()) then
        ! Cannot open or close
        call a%write_para(cqm_f1036)
        exit L1
      end if

      if (.not. this%map%room(this%player%room)%door%exists()) then
          ! No door here
        call a%write_para(cqm_f1025)
        exit L1
      end if

      if (this%map%room(this%player%room)%door%is_locked()) then
        ! Locked
        call a%write_para(cqm_f1019)
      else if (this%map%room(this%player%room)%door%is_open()) then
          ! Open
        call a%write_para(cqm_f1020)
      else if (this%map%room(this%player%room)%door%is_closed()) then
        ! Closed
        call this%map%room(this%player%room)%door%open()
        call a%write_para(cqm_f1008)
      end if
      exit L1

    case(N38_SHUTTERS)
      ! Shutters
      if (.not. this%player%is_in(R1_BEDROOM)) then
        call a%write_para(cqm_f1003)
        exit L1
      end if
      call this%items%s_shutter%open()
      call a%write_para(cqm_f1022)
      ! call DES(428, this%option%debug_mode)
      ! call a%write_para(cq_form2(428-369))
      call a%write_para(cq_form2(59))
      exit L1

    case(N37_WINDOW)
      ! Window
      if (this%player%is_in(R1_BEDROOM)) then
        if (this%items%s_shutter%is_closed()) then
          call a%write_para(cqm_f1003)
          exit L1
        end if
      else
        if (.not. this%player%is_in(R10_SMOKING_ROOM)) then
          call a%write_para(cqm_f1003)
          exit L1
        end if
      end if

      call a%write_para(cqm_f1023)
      exit L1

    case(N48_DRAWER)
      ! Drawer
      if (this%player%is_in(R1_BEDROOM)) then
        if (this%items%portable%item(O19_CROSS)%place == 0) then
          call this%items%portable%item(O19_CROSS)%place_in(R1_BEDROOM)
        end if

        if (this%items%portable%item(O19_CROSS)%in_room(R1_BEDROOM)) then
          call a%write_para(cqm_f1024)
          call this%player%score%add_to_score(this%items%portable%item(O19_CROSS)%value)
          this%items%portable%item(O19_CROSS)%value = 0
          exit L1
        else
          call a%write_para(cqm_f1008)
          exit L1
        end if

      else
        call a%write_para(cqm_f1003)
        exit L1
      end if

    case(N33_LOCK)
      ! Lock
      call a%write_para(cqm_f1026)
      exit L1

    case(N46_BOOK)
      ! Book
      if (this%player%is_in(R13_LIBRARY)) then
        if (this%items%portable%item(O4_KEY)%place == -3) then
          call this%items%portable%item(O4_KEY)%place_in(this%player%room)
        end if
        if (this%items%portable%item(O4_KEY)%in_room(this%player%room)) then
          call a%write_para(cqm_f1027)
          exit L1
        end if
      end if
      call a%write_para(cqm_f1028)
      exit L1

    case(N40_COFFIN)
      if (this%player%is_in(R43_MASTER_CHAMBER)) then
        ! Coffin
        if (this%encounters%a_master%is_alive()) then
          if (this%items%s_sun%is_up()) then
            call this%encounters%a_master%sleep()
          else
            call this%encounters%a_master%rise()
          end if
        end if
        ! call DES(this%encounters%a_master%des_index(),                  &
        !   this%option%debug_mode)
        call a%write_para(                                              &
          cq_form2(this%encounters%a_master%form2_index()))
      else
        call a%write_para(cqm_f1013)
      end if
      exit L1

    case default
      ! Infer door near cyclops
      if (this%player%is_in(R86_TALL_TUNNEL)                            &
        .or. this%player%is_in(R88_CYCLOPS_LAIR)) then
        ! Assuming door between tall cavern and cyclops' lair
        call a%write_para(cqm_f1125)
        exit L1
      else
        call a%write_para(cqm_f1013)
        exit L1
      end if

    end select

    exit L1
  end do L1

  return
end function do_open

!> Implement action 28 (V28_CLOSE). Close something.
type(resolution_t) function do_close(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  use :: m_where, only: near_doors
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  continue

  call outcome%init()
  outcome%to_next_action = .true.

  !     ---CLOSE---
  ! ACTION(28)  CLOS close
  if (cmd%no_object() .and. near_doors(this%player%room)) then
    ! Close the door (47) if nothing else is specified
    ! inside the house
    cmd%object = N47_DOOR
  end if

  L1: do
    select case(cmd%object)
    case(N47_DOOR)
      ! Open doors

      if (this%player%is_in(R7_BRICK_WALL)) then
        ! Brick wall
        call this%player%move_to(to=R6_KITCHEN, from=0)
        call a%write_para(cqm_f1008)
        exit L1
      end if

      if (this%map%room(this%player%room)%door%is_static()) then
        call a%write_para(cqm_f1036)
        exit L1
      end if

      ! BUGFIX IN ORIGINAL: Typo; code is `GOTO 272` but should be `GOTO 273`
      if (this%map%room(this%player%room)%door%does_not_exist()) then
        call a%write_para(cqm_f1025)
        exit L1
      end if

      ! ! When compared against original source code, this code makes no sense
      ! ! It is believe to be an error in the original code where the original
      ! ! code reads `GOTO 272` but should be `GOTO 273`
      ! if (map%room(player%room)%door%does_not_exist()) then
      !   if (map%room(R6_KITCHEN)%door%is_locked()) then
      !     write(unit=STDOUT, fmt=1021)
      !   else if (map%room(R6_KITCHEN)%door%is_open()) then
      !     write(unit=STDOUT, fmt=1020)
      !   else if (.not. map%room(R6_KITCHEN)%door%is_closed()) then
      !   ! exit L1
      !     cycle L25
      !   end if

      !   call map%room(R6_KITCHEN)%door%open()
      !   call player%move_to(to=R7_BRICK_WALL, from=R6_KITCHEN)
      !   call p_score%apply_combination_score()

      !   ! cycle L20
      !   ! Signals 'cycle L20' on exit
      !   outcome%to_next_area = .true.
      !   exit L1
      ! end if

      if (this%map%room(this%player%room)%door%is_open()) then
        call this%map%room(this%player%room)%door%close()
      end if

      call a%write_para(cqm_f1008)

      exit L1

    case(N38_SHUTTERS)
      if (this%player%is_in(R1_BEDROOM)) then
        call this%items%s_shutter%close()
        call a%write_para(cqm_f1008)
        exit L1
      else
        call a%write_para(cqm_f1003)
        exit L1
      end if

    case(N37_WINDOW)
      if (this%player%is_in(R1_BEDROOM)) then
        if (this%items%s_window_1%is_broken()) then
          call a%write_para(cqm_f1031)
          exit L1
        else
          call a%write_para(cqm_f1008)
          exit L1
        end if
      else if (this%player%is_in(R10_SMOKING_ROOM)) then
        if (this%items%s_window_2%is_broken()) then
          call a%write_para(cqm_f1031)
          exit L1
        else
          call a%write_para(cqm_f1008)
          exit L1
        end if
      else
        call a%write_para(cqm_f1003)
        exit L1
      end if

    ! Book
    case(N46_BOOK)
      call a%write_para(cqm_f1032)
      exit L1

    ! Drawer
    case(N48_DRAWER)
      if (this%player%is_in(R1_BEDROOM)) then
        if (this%items%portable%item(O19_CROSS)%in_room(R1_BEDROOM)) then
          call this%items%portable%consume(O19_CROSS)
        end if
        call a%write_para(cqm_f1008)
        exit L1
      else
        call a%write_para(cqm_f1003)
        exit L1
      end if

    ! Coffin
    case(N40_COFFIN)
      if (this%encounters%a_master%is_up() .or. this%encounters%a_master%is_dead()) then
        call a%write_para(cqm_f1005)
        exit L1
      else if (.not. this%encounters%a_master%is_up() .and. this%encounters%a_master%is_alive()) then
        call this%encounters%a_master%init()
      end if
      call a%write_para(cqm_f1008)
      exit L1

    case default
      call a%write_para(cqm_f1005)
      exit L1

    end select

    exit L1
  end do L1

  return
end function do_close

!> Implement action 29 (V29_LOCK). LOCK
type(resolution_t) function do_lock(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player
  type(action_t), intent(inout) :: cmd

  continue

  call outcome%init()

  if (cmd%object_is(N47_DOOR)) then
    if (this%player%is_in(R21_ATTIC)) then
      this%player%nlockparts_good = 0
      call this%map%room(R21_ATTIC)%door%lock()
      call a%write_para(cqm_f1008)
    else if (.not. this%map%room(this%player%room)%door%exists()) then
      call a%write_para(cqm_f1025)
    else if (this%map%room(this%player%room)%door%is_static()) then
      call a%write_para(cqm_f1036)
    else
      if (this%items%portable%is_carried(O4_KEY)) then
        call this%map%room(this%player%room)%door%lock()
        call a%write_para(cqm_f1019)
      else
        call a%write_para(cqm_f1067)
      end if
    end if
  else
    call a%write_para(cqm_f1002)
  end if

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_lock

!> Implement action 30 (V30_UNLOCK). UNLOCK
type(resolution_t) function do_unlock(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  use :: m_where, only: near_doors

  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  continue

  call outcome%init()

  !     ---UNLOCK---
  ! ACTION(30)  UNLO unlock

  if (cmd%no_object() .and. near_doors(this%player%room)) then
    cmd%object = N47_DOOR
  end if
  if (cmd%object_is(N47_DOOR)) then
    if (this%player%is_in(R21_ATTIC)) then
      call a%write_para(cqm_f1068)
    else
      if (.not. this%map%room(this%player%room)%door%exists()) then
        call a%write_para(cqm_f1025)
      else if (this%map%room(this%player%room)%door%is_static()) then
        ! (-2=WILL NOT OPEN/CLOSE, -1=NO DOOR HERE, 0=LOCKED,
        !  1=CLOSED, 2=OPEN)
        !> @todo Determine what message should display here
        !! (Unlock with DOOR(ROOM) == -2)
        call a%write_para(cqm_f1036)
      else
        if (this%items%portable%is_carried(O4_KEY)) then
          call this%map%room(this%player%room)%door%unlock()
          call a%write_para(cqm_f1008)
        else
          call a%write_para(cqm_f1067)
        end if
      end if
    end if
  else
    call a%write_para(cqm_f1016)
  end if

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_unlock

!> Implement action 31 (V31_ON). ON
type(resolution_t) function do_on(this, cmd, revised_action)            &
  result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player
  type(action_t), intent(inout) :: cmd
  !> Reprocess this action using a different verb ID
  integer, intent(out) :: revised_action

  continue

  call outcome%init()

  if (cmd%no_object()) then
    cmd%object = O21_LAMP
  end if

  revised_action = V32_LIGHT

  outcome%reprocess_action = .true.

  return
end function do_on

!> Implement action 32 (V32_LIGHT). LIGHT
type(resolution_t) function do_light(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_items, only: NITEMS
  use :: m_object_id
  use :: m_where, only: NROOMS, in_lower_realm
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  ! Local variables
  integer :: II

  continue

  call outcome%init()

    !     ---LIGHT---
    ! ACTION(32)  LIGH light

L1: do

    ! Handle empty or uncarryable object
    if (cmd%object > NITEMS) then
      call a%write_para(cqm_f1002)
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    else if (cmd%object < 1) then
      call a%write_para(cqm_f1004)
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    end if

    ! You can't light something you aren't carrying and isn't here
    if (.not. this%items%portable%item(cmd%object)%at_hand_in(this%player%room)) &
      then
      call a%write_para(cqm_f1004)
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    end if

    select case(cmd%object)
    case(O13_TORCH)
      if (this%items%s_match%is_lit()) then
        call this%items%s_torch%turn_on()
        call a%write_para(cqm_f1121)
      else
        call a%write_para(cqm_f1075)
      end if
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.

    case(O15_MATCH)
      call this%items%s_match%turn_on()
      if (in_lower_realm(this%player%room)                              &
        .and. this%items%s_lamp%is_dark()) then
        ! Down the the hole underground with no lamp
        outcome = this%describe_area()

        if (.not. (outcome%end_game .or. outcome%to_next_area)) then
          outcome%to_next_action = .true.
        end if
        exit L1
      else
        call a%write_para(cqm_f1038)
        ! Signals 'cycle L25' on exit
        outcome%to_next_action = .true.
      end if

    case(O21_LAMP)
    ! If lamp is in room and player lights the lamp
      if (this%items%s_lamp%is_empty()) then
        call a%write_para(cqm_f1009)
      else
        if (this%items%s_lamp%is_off()) then
          II = 0
        end if
        call this%items%s_lamp%turn_on()
        if (II == 0 .and. in_lower_realm(this%player%room)) then
          ! Down the the hole underground with no lamp
          outcome = this%describe_area()

          if (.not. (outcome%end_game                                   &
            .or. outcome%to_next_area)) then
            outcome%to_next_action = .true.
          end if
          exit L1

        else
          call a%write_para(cqm_f1038)
        end if
      end if
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.

    case(O27_CIGAR)
      if ((.not. this%items%s_match%item%is_carried()) .or. this%items%s_match%is_dark()) then
        call a%write_para(cqm_f1075)
      else if (this%player%is_in(R86_TALL_TUNNEL)                            &
        .and. this%encounters%a_cyclops%blocks_way()) then
        ! If cigar location is -3 (thrown?)
        if (this%items%portable%item(O27_CIGAR)%place == -3) then
          call a%write_para(cqm_f1120h, (/ cqm_f1120h /))
          call a%write_para(cqm_f1117)
          call this%items%portable%consume(O27_CIGAR)
          call this%encounters%a_cyclops%flee()
        else
          call a%write_para(cqm_f1002)
        end if
      else if (this%items%portable%is_carried(O27_CIGAR)) then
        ! If you have the cigar
        ! call DES(803, this%option%debug_mode)
        ! Player demise #2
        call a%write_para(cqm_death_2_h, cqm_death_2_b)

        ! Signals 'exit L25' on exit
        outcome%to_next_life = .true.
        exit L1
      else
        call a%write_para(cqm_f1002)
      end if
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.

    case default
      call a%write_para(cqm_f1016)
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.

    end select

    exit L1
  end do L1

  return
end function do_light

!> Implement action 33 (V33_OFF). OFF
type(resolution_t) function do_off(this, cmd, revised_action)           &
  result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: m_action, only: action_t
  use :: cquest_text
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player
  type(action_t), intent(inout) :: cmd
  !> Reprocess this action using a different verb ID
  integer, intent(out) :: revised_action

  continue

  call outcome%init()

  if (cmd%no_object()) then
    ! Assume you want to turn off the lamp if you don't specify
    ! what you're turning off
    cmd%object = O21_LAMP
  end if

  revised_action = V34_EXTINGUISH
  outcome%reprocess_action = .true.

  return
end function do_off

!> Implement action 34 (V34_EXTINGUISH). Extinguish some kind of fire
!! or flame
type(resolution_t) function do_extinguish(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_items, only: NITEMS
  use :: m_object_id
  use :: m_where, only: in_lower_realm
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  continue

  call outcome%init()

  !     --EXTINGUISH---
  ! ACTION(34)  EXTI extinguish

  L1: do
    ! Fire
    if (cmd%object_is(N34_FIRE)) then
      ! You have water
      if (this%items%portable%is_carried(O26_WATER)) then
        call this%items%s_bottle%water%consume()
        call this%items%s_bottle%clear()
        this%map%fire_blocks_way = .false.
        call a%write_para(cqm_f1103)
      else
        call a%write_para(cqm_f1102)
      end if
      exit L1
    end if

    ! An item you can't have
    if (cmd%object > NITEMS) then
      call a%write_para(cqm_f1002)
      exit L1
    end if

    ! Nothing
    if (cmd%object < 1) then
      call a%write_para(cqm_f1004)
      exit L1
    end if

    ! Extinguish something you don't have and isn't in the room
    if (.not. this%items%portable%item(cmd%object)%at_hand_in(this%player%room)) then
      call a%write_para(cqm_f1004)
      exit L1
    end if

    select case (cmd%object)
    case (O21_LAMP)
      ! Extinguish the lamp
      ! Lamp
      call this%items%s_lamp%turn_off()
      call a%write_para(cqm_f1037)
      if (in_lower_realm(this%player%room)                                   &
        .and. this%items%s_lamp%is_dark() .and. this%items%s_match%is_dark()) then
        call a%write_para(cqm_f1064)
      end if

    case (O15_MATCH)
      ! Match
      call this%items%s_match%turn_off()
      call a%write_para(cqm_f1037)
      if (in_lower_realm(this%player%room)                                   &
        .and. this%items%s_lamp%is_dark() .and. this%items%s_match%is_dark()) then
        call a%write_para(cqm_f1064)
      end if

    case (O13_TORCH)
      ! Acetylene torch
      call this%items%s_torch%turn_off()
      call a%write_para(cqm_f1008)

    case default
      ! Something else
      call a%write_para(cqm_f1016)
    end select
    exit L1
  end do L1

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_extinguish

!> Implement action 35 (V35_LOOK). Describe surroundings
type(resolution_t) function do_look(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_object_id

  implicit none
  ! Arguments

  !> Game implementation object
  class(game_t), intent(inout) :: this

  continue

  call outcome%init()

  this%map%room(this%player%room)%prev = 0
  ! Signals 'cycle L20' on exit
  outcome%to_next_area = .true.

  return
end function do_look

!> Implement action 36 (V36_SCORE). Display score
type(resolution_t) function do_score(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  use :: m_object_id
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  ! Local variables
  integer :: tmp_score
  character(len=80) :: tcqm

  ! Formats
9998 format('  You made ', I4, ' moves, and scored ', I4, ' points.')

  continue

  call outcome%init()

  tmp_score = this%player%score%SCORE                                   &
    + this%items%portable%total_score(this%player%moves)
  write(unit=tcqm, fmt=9998) this%player%moves, tmp_score
  call a%write_para(tcqm)
  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_score

!> Implement action 37 (V37_BREAK). Destroy something.
type(resolution_t) function do_break(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  continue

  call outcome%init()
  !     ---BREAK---
  ! ACTION(N37_WINDOW)  BREA, CHOP break, chop

  L1: do
    if (cmd%object_is(N56_BOARD)) then
      if (this%player%is_in(R6_KITCHEN)) then
        if (this%items%portable%item(O3_AXE)%at_hand_in(                &
          this%player%room)) then
          call a%write_para(cqm_f1041)
          call this%map%room(R6_KITCHEN)%door%unlock()
        else
          call a%write_para(cqm_f1039)
          if (player_agrees()) then
            call a%write_para(cqm_f1040h, (/cqm_f1040b/))
          end if
        end if
      else
        call a%write_para(cqm_f1003)
      end if
    else if (cmd%object_is(N37_WINDOW)) then
      if (this%player%is_in(R1_BEDROOM)) then
        if (.not. this%items%s_window_1%is_nailed()) then
          call a%write_para(cqm_f1046)
        else
          call this%items%s_window_1%break()
          call a%write_para(cqm_f1008)
        end if
      else if (this%player%is_in(R10_SMOKING_ROOM)) then
        if (.not. this%items%s_window_2%is_nailed()) then
          call a%write_para(cqm_f1046)
        else
          call this%items%s_window_2%open()
          call a%write_para(cqm_f1008)
        end if
      else
        call a%write_para(cqm_f1003)
      end if
    else if (cmd%object_is(N50_MIRROR)) then
      if (this%player%is_in(R55_HONEYMOON_SUITE)) then
        call a%write_para(cqm_f1111)
      else if (this%player%is_in(R17_MIRROR)) then
        if (this%items%s_passage%is_open()) then
          call a%write_para(cqm_f1003)
        else
          call this%items%s_passage%open()
          call this%player%score%add_to_score(10)
          ! call DES(412, this%option%debug_mode)
          ! call a%write_para(cq_form2(412-369))
          call a%write_para(cq_form2(43))
        end if
      else if (this%player%is_in(R27_MIRROR_MAZE)) then
        call a%write_para(cqm_f1048)
        ! Signals 'exit L25' on exit
        outcome%to_next_life = .true.
        exit L1
      else
        call a%write_para(cqm_f1003)
      end if
    else if (cmd%object_is(N47_DOOR)) then
      if (this%player%is_in(R99_ELEVATOR_BETWEEN_FLOORS)) then
        this%map%path_through_final_door = .true.
        call a%write_para(cqm_f1136)
      else
        call a%write_para(cqm_f1084)
      end if
    else
      call a%write_para(cqm_f1084)
    end if
    ! Signals 'cycle L25' on exit
    outcome%to_next_action = .true.
    exit L1
  end do L1

  return
end function do_break

!> Implement actions 38 (V38_WATER) and 39 (V39_POUR). Attempt to pour
!! a liquid (water, blood, kerosene, acid, champagne)
type(resolution_t) function do_water_pour(this, cmd, revised_action)    &
  result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_items, only: NITEMS
  use :: m_object_id
  use :: m_player, only: player_t
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd
  !> Reprocess this action using a different verb ID
  integer, intent(inout) :: revised_action

  continue

  call outcome%init()

L1: do
    if (cmd%object > NITEMS) then
      call a%write_para(cqm_f1005)
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    end if
    if (cmd%object < 1) then
      call a%write_para(cqm_f1003)
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    end if
    if (.not. this%items%portable%is_carried(cmd%object)) then
      call a%write_para(cqm_f1003)
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    end if

    ! If blood, acid, or water
    if (cmd%object_is(O5_BLOOD) .or. cmd%object_is(O25_ACID)      &
      .or. cmd%object_is(O26_WATER)) then
      ! Pour blood, acid, or water
      ! Treat it as drop
      revised_action = V12_DROP
      ! Signals 'cycle L26' on exit
      outcome%reprocess_action = .true.
      exit L1
    else if (cmd%object_is(O7_CHAMPAGNE)) then
      ! Champagne
      call a%write_para(cqm_f1104)
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    else if (cmd%object_is(O1_KEROSENE)) then
      ! Oil
      if (this%items%s_lamp%item%is_carried()) then
        ! Fill the lamp if you have it
        ! With no arguments, refuel() sets turns_lit to 0
        call this%items%s_lamp%refuel()
        ! Q: Should this set LMOVE to -75 instead?
        ! Consume oil
        call this%items%portable%consume(O1_KEROSENE)
        call this%player%lose_item(1)
        ! Signals 'cycle L25' on exit
        outcome%to_next_action = .true.
        exit L1
      else
        ! Pour oil
        ! Treat it as drop
        revised_action = V12_DROP
        ! Signals 'cycle L26' on exit
        outcome%reprocess_action = .true.
        exit L1
      end if
    else
      ! You're pouring something else
      call a%write_para(cqm_f1005)
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    end if
    exit L1
  end do L1

  return
end function do_water_pour

!> Implement action 40 (V40_BACK). BACK
type(resolution_t) function do_back(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  ! Local variables
  integer :: from_rm
  integer :: to_rm

  continue

  call outcome%init()

  if (this%player%last_room > 0) then
    from_rm = this%player%room
    to_rm = this%player%last_room
    call this%player%move_to(to=to_rm, from=from_rm,                    &
      with_advance=.true.)
    ! Signals 'cycle L20' on exit
    outcome%to_next_area = .true.
  else
    call a%write_para(cqm_f1035)
    ! Signals 'cycle L25' on exit
    outcome%to_next_action = .true.
  end if

  return
end function do_back

!> Implement action 41 (V41_SWIM). Attempt to swim.
type(resolution_t) function do_swim(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  use :: m_object_id
  use :: m_where, only: is_wet_room
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  continue

  call outcome%init()

  if (is_wet_room(this%player%room)) then
    call a%write_para(cqm_f1076)
  else
    call a%write_para(cqm_f1013)
  end if

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_swim

!> Implement action 42 (V42_MELT). Melt glacier.
type(resolution_t) function do_melt(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player command
  type(action_t), intent(inout) :: cmd

  continue

  call outcome%init()

  !     ---MELT---
  ! ACTION(42)  MELT melt

  L1: do
    if (cmd%object_is(N79_GLACIER)) then
      if (this%player%is_in(R65_GLACIER_ROOM)) then
        if (this%items%portable%is_carried(O13_TORCH)                        &
          .and. this%items%s_torch%is_lit()) then
          if (this%map%path_through_glacier) then
            ! If you've already melted a large hole, you melt the
            ! rest of the glacier. Oops.
            call a%write_para(cqm_f1144h, (/cqm_f1144b/))
            ! Signals 'exit L25' on exit
            outcome%to_next_life = .true.
            exit L1
          else
            ! If the glacier didn't have a hole melted in it,
            ! it does now.
            this%map%path_through_glacier = .true.
            call a%write_para(cqm_f1116)
            if (this%items%s_bottle%contains_water()) then
              call this%items%portable%display_here(this%player%score,  &
                this%player%room, this%map%path_to_precipice)
            else
              call this%items%portable%item(O26_WATER)%place_in(this%player%room)
            end if
            ! ! Signals 'cycle L25' on exit
          end if
        else
          ! You don't have the torch or it's not lit
          call a%write_para(cqm_f1005)
          ! ! Signals 'cycle L25' on exit
        end if
      else
        ! Not in glacier room
        call a%write_para(cqm_f1003)
        ! ! Signals 'cycle L25' on exit
      end if
    else
      ! Not melting glacier
      call a%write_para(cqm_f1016)
      ! ! Signals 'cycle L25' on exit
    end if

    ! cycle L25
    ! Signals 'cycle L25' on exit
    outcome%to_next_action = .true.
    exit L1
  end do L1

  return
end function do_melt

!> Implement action 43 (V43_CROSS). Move across an expanse or gap.
!! Or moat.
type(resolution_t) function do_cross(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  use :: m_object_id
  implicit none
  ! Arguments

  !> Game implementation object
  class(game_t), intent(inout) :: this

  ! logical :: has_boat

  continue

  call outcome%init()

  ! has_boat = this%items%portable%is_carried(O14_BOAT)

  !     ---CROSS---
  ! ACTION(43)  CROS cross

L1: do

    select case(this%player%room)
    case(R32_SIDE_OF_MOAT, R40_FAR_SIDE_OF_MOAT)
      if (this%items%portable%is_carried(O14_BOAT)) then
        if (this%player%is_in(R32_SIDE_OF_MOAT)) then
          call this%player%move_to(to=R40_FAR_SIDE_OF_MOAT, from=0)
        else
          call this%player%move_to(to=R32_SIDE_OF_MOAT, from=0)
        end if
        outcome = this%check_boat_master_after_move()
        exit L1
      else
        call a%write_para(cqm_f1082)
        outcome%to_next_action = .true.
        exit L1
      end if
    case(R68_BASE_OF_WATERFALL, R81_ISLAND)
      if (this%items%portable%is_carried(O14_BOAT)) then
        if (this%player%is_in(R68_BASE_OF_WATERFALL)) then
          call this%player%move_to(to=R81_ISLAND, from=0)
        else
          call this%player%move_to(to=R68_BASE_OF_WATERFALL, from=0)
        end if
        outcome = this%check_boat_master_after_move()
        exit L1
      else
        call a%write_para(cqm_f1082)
        outcome%to_next_action = .true.
        exit L1
      end if
    case default
      call a%write_para(cqm_f1013)
      outcome%to_next_action = .true.
      exit L1
    end select
  end do L1

  return
end function do_cross

!> Implement action 44 (V44_QUIT). Display score and end game
type(resolution_t) function do_quit(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  continue

  call outcome%init()

  !     ---QUIT---
  ! ACTION(44)  QUIT quit*

  call this%player%score%display_rank(                                  &
    this%items%portable%total_score(this%player%moves))

  outcome%end_game = .true.

  return
end function do_quit

!> Implement action 45 (V45_HONK). Honk!
type(resolution_t) function do_honk(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  continue

  call outcome%init()

  !     ---HONK---
  ! ACTION(45)  HONK honk
  if (this%player%room < R95_WAREHOUSE_MAIN) then
    call a%write_para(cqm_f1095)
    ! Signals 'cycle L25' on exit
    outcome%to_next_action = .true.
  else if (this%player%is_in(R99_ELEVATOR_BETWEEN_FLOORS)) then
    if (this%player%revealed_letter(1)) then
      call this%player%score%add_to_score(2)
    end if
    if (this%player%revealed_letter(2)) then
      call this%player%score%add_to_score(1)
    end if
    if (this%player%revealed_letter(3)) then
      call this%player%score%add_to_score(1)
    end if
    if (this%player%revealed_letter(4)) then
      call this%player%score%add_to_score(1)
    end if
    this%player%room = R100_ENDGAME
    call this%player%clear_brief()
    ! Signals 'cycle L20' on exit
    outcome%to_next_area = .true.
  else
    if (this%player%is_in(R95_WAREHOUSE_MAIN)                           &
      .or. this%player%is_in(R97_WAREHOUSE_NORTH)) then
      call a%write_para(cqm_f1142h, (/cqm_f1142b/))
    else if (this%player%is_in(R96_WAREHOUSE_WEST)                      &
      .or. this%player%is_in(R98_WAREHOUSE_EAST)) then
      call a%write_para(cqm_f1143)
    end if
    ! Set certain permadeath
    this%player%ndeaths = 3
    outcome%to_next_life = .true.
  end if

  return
end function do_honk

!> Implement action 46 (V46_TIE). Tie the rope to something
type(resolution_t) function do_tie(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  !> Reference to player action
  type(action_t), intent(in) :: cmd

  continue

  call outcome%init()

  !     ---TIE---
  ! ACTION(46)  TIE  tie
  L1: do
    if (cmd%object_is(O9_ROPE)) then
      if (this%items%s_rope%is_carried()) then
        if ((.not. this%player%is_in(R1_BEDROOM))                       &
          .or. this%items%s_rope%tied_to_hook()) then
          if (this%items%portable%is_carried(O16_HOOK)                  &
            .and. this%items%s_rope%is_carried()) then
            call this%items%s_rope%tie_to_hook()
            call a%write_para(cqm_f1010)
          else
            call a%write_para(cqm_f1013)
          end if
        else
          call this%items%s_rope%tie_to_bed()
          call this%items%s_rope%item%place_in(R1_BEDROOM)
          call a%write_para(cqm_f1069)
        end if
      else
        call a%write_para(cqm_f1003)
      end if
    else if (cmd%object_is(O16_HOOK)) then
      if (this%items%portable%is_carried(O16_HOOK)                      &
        .and. this%items%s_rope%is_carried()) then
        call this%items%s_rope%tie_to_hook()
        call a%write_para(cqm_f1010)
      else
        call a%write_para(cqm_f1013)
      end if
    else
      call a%write_para(cqm_f1002)
    end if

    exit L1
  end do L1

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_tie

!> Implement action 47 (V47_UNTIE). Untie a tied rope
type(resolution_t) function do_untie(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player action
  type(action_t), intent(in) :: cmd

  continue

  call outcome%init()

  !     ---UNTIE---
  ! ACTION(47)  UNTI unti

  L1: do
    if (cmd%object_is(O9_ROPE)) then
      if (.not. this%items%s_rope%tied_to_hook()) then
        if (.not. this%items%s_rope%item%in_room(this%player%room)) then
          call a%write_para(cqm_f1003)
          exit L1
        end if
        if (this%items%s_rope%tied_to_bed()) then
          call this%items%s_rope%untie()
          call a%write_para(cqm_f1008)
          exit L1
        end if
        if (.not. this%items%s_rope%is_hanging()) then
          call a%write_para(cqm_f1013)
          exit L1
        end if
        call this%items%s_rope%lose_forever()
        call this%items%portable%consume(O9_ROPE)
        call a%write_para(cqm_f1071)
! Check if STATUE, MONEY, and SWORD found. If not, reduce MMAX (which
! determines when every treasure has been found) because the
! rope is necessary to find each of these treasures.
        if (this%items%portable%item(O12_SWORD)%value /= 0) then
          call this%player%score%reduce_mmax(10)
        end if
        if (this%items%portable%item(O17_STATUE)%value /= 0) then
          call this%player%score%reduce_mmax(10)
        end if
        if (this%items%portable%item(O29_MONEY)%value /= 0) then
          call this%player%score%reduce_mmax(10)
        end if
        exit L1
      end if
      if (cmd%object_is_not(O16_HOOK)) then
        call a%write_para(cqm_f1002)
        exit L1
      end if
    end if

    if (this%items%portable%item(O16_HOOK)%at_hand_in(this%player%room))  &
      then
      if (this%map%path_to_precipice) then
        call a%write_para(cqm_f1071)
        call this%items%s_rope%item%place_in(R83_LARGE_CAVERN_SOUTH)
        call this%items%s_rope%untie()
      else
        call this%items%s_rope%untie()
        call a%write_para(cqm_f1008)
      end if
    else
      call a%write_para(cqm_f1003)
    end if

    exit L1
  end do L1

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_untie

!> Implement action 48 (V48_READ). Read notes or books
type(resolution_t) function do_read(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDIN => INPUT_UNIT,         &
    STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player action
  type(action_t), intent(in) :: cmd

  continue

  call outcome%init()

  !     ---READ---
  ! ACTION(48)  READ read

  select case(cmd%object)
  case(N46_BOOK)
    ! Read a book
    if (this%player%is_in(R13_LIBRARY)                                  &
      .or. this%player%is_in(R10_SMOKING_ROOM)) then
      call a%write_para(cqm_f1028)
    else
      call a%write_para(cqm_f1003)
    end if
  case(N52_NOTE)
    ! Read the butler's note
    if (this%encounters%a_butler%holding_note()) then
      call this%player%score%apply_butler_score()
      if (.not. this%items%s_note%is_blank()) then
        call this%items%s_note%write_about_book()
        call this%encounters%a_butler%pass_out()
        call a%write_para(cqm_f1030)
      else
        call this%items%s_note%write_about_hunchback()
        call this%encounters%a_butler%sleep()
        call a%write_para(cqm_f1029)
      end if
    else
      call a%write_para(cqm_f1003)
    end if
  case default
    call a%write_para(cqm_f1002)
  end select

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_read

!> Implement action 49 (V49_FILL). Fill container with nearby fluid
type(resolution_t) function do_fill(this, cmd, revised_action)          &
  result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_items, only: NITEMS
  use :: m_object_id
  use :: m_where, only: is_wet_room
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player
  type(action_t), intent(inout) :: cmd
  !> Reprocess this action using a different verb ID
  integer, intent(inout) :: revised_action

  continue

  call outcome%init()

  !     ---FILL---
  ! ACTION(49)  FILL fill

  L1: do
    if (cmd%object > NITEMS) then
      ! call log_debug("cmd%object > NITEMS: out of bounds high")
      ! Fill a thing you can actually have
      call a%write_para(cqm_f1002)
      outcome%to_next_action = .true.
      exit L1
    end if

    if (cmd%object < 1) then
      ! call log_debug("cmd%object < 1: out of bounds low")
      ! Fill an unspecified thing
      call a%write_para(cqm_f1004)
      outcome%to_next_action = .true.
      exit L1
    end if

    if (.not. this%items%portable%is_carried(cmd%object)) then
      ! call log_debug(".not. portable%is_carried(cmd%object): item not carried")
      ! You can't fill something you aren't carrying
      call a%write_para(cqm_f1004)
      outcome%to_next_action = .true.
      exit L1
    end if

    if (cmd%object_is(O18_BOTTLE)) then
      ! You try to fill the bottle
      if (this%items%s_bottle%is_full()) then
        ! call log_debug("cmd%object_is(O18_BOTTLE) .and. s_bottle%is_full(): Full bottle")
        ! The bottle is full
        call a%write_para(cqm_f1099)
        outcome%to_next_action = .true.
        exit L1
      end if

      ! There's water here; try to fill the bottle with water
      if (is_wet_room(room_id=this%player%room, omit_island=.true.)     &
        .or. this%items%portable%item(O26_WATER)%in_room(this%player%room)) &
        then
        ! call log_debug("Water here or near: cmd%object = O26_WATER")
        cmd%object = O26_WATER
      end if

      ! There's blood here; try to fill the bottle with blood
      if (this%items%portable%item(O5_BLOOD)%in_room(this%player%room)) then
        cmd%object = O5_BLOOD
        ! call log_debug("Blood here: cmd%object = O5_BLOOD")
      end if

      ! There's nothing here you can put in a bottle
      if (cmd%object_is(O18_BOTTLE)) then
        ! call log_debug("cmd%object is bottle, not blood or water")
        call a%write_para(cqm_f1005)
        outcome%to_next_action = .true.
        exit L1
      else
        ! Take blood or water as appropriate (cmd%object is either 26 or 5)
        ! Rephrase command as TAKE WATER or TAKE BLOOD and re-evaluate
        revised_action = V11_TAKE
        outcome%reprocess_action = .true.
        ! call log_debug("revised_action is V11_TAKE; outcome%reprocess_action = .true.")
        exit L1
      end if

    end if

    if ((cmd%object_is(O21_LAMP))                                       &
      .and. this%items%portable%is_carried(O1_KEROSENE)) then
      ! Fill lamp with kerosene

      ! Q: Why is turns_lit set to -75 instead of 0?
      call this%items%s_lamp%refuel(-75)
      call this%items%portable%consume(O1_KEROSENE)
      call this%player%lose_item(1)
      call a%write_para(cqm_f1008)
      ! call log_debug("Fill lamp and consume oil")
    else
      call a%write_para(cqm_f1005)
      ! call log_debug("Not lamp or no oil")
    end if

    outcome%to_next_action = .true.
    exit L1

  end do L1

  return
end function do_fill

!> Implement action 50 (V50_HELP). Display help or game hints
type(resolution_t) function do_help(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDIN => INPUT_UNIT,         &
    STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  ! use :: m_gamedata, only: show_hint
  use :: m_object_id
  use :: m_where, only: is_valid_room
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player action
  type(action_t), intent(in) :: cmd

  continue

  call outcome%init()

  !     ---HINT---
  ! ACTION(50)  HELP, HINT help, hint*

  call show_hint(cmd%object, this%player%room, this%player%score,       &
    this%encounters%a_bat%is_hungry(),                                  &
    this%items%s_window_1%is_barred(),                                  &
    this%encounters%a_cyclops%blocks_way(),                             &
    this%map%path_through_glacier)

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_help

!> Implement action 51 (V51_GOTO). Debugging command to teleport
!! to a given room
type(resolution_t) function do_goto(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDIN => INPUT_UNIT,         &
    STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  use :: m_object_id
  use :: m_where, only: is_valid_room
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  continue

  call outcome%init()

  !     --GOTO--
  ! ACTION(51)  GOTO goto*

  ! GOTO is acknowledged if the player is in DEBUG mode
  if (this%option%debug_mode) then
  GLOOP: do
  !      write(unit=STDOUT, fmt=1115)
      call a%write_para(cqm_f1115)
      read(unit=STDIN, fmt='(I3)') this%player%room
      if (is_valid_room(this%player%room)) then
        exit GLOOP
      end if
    end do GLOOP
  else
  !    write(unit=STDOUT, fmt=1114)
    call a%write_para(cqm_f1114)
  end if

  ! cycle L25
  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_goto

!> Implement action 52 (V52_LEFT). Turn combination lock left
type(resolution_t) function do_left(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  !> Reference to player action
  type(action_t), intent(in) :: cmd

  continue

  call outcome%init()

  !     ---LEFT---
  ! ACTION(52)  L, LEFT    left

  if (this%player%is_in(R21_ATTIC)) then
    if (this%player%nlockparts_good <= 0) then
      if (cmd%object_is(N42_COMBO1_L8)) then
        this%player%nlockparts_good = 1
  !        write(unit=STDOUT, fmt=1008)
        call a%write_para(cqm_f1008)
      else
  !        write(unit=STDOUT, fmt=1014)
        call a%write_para(cqm_f1014)
        this%player%nlockparts_good = 0
      end if
    else if (this%player%nlockparts_good == 1                           &
      .or. cmd%object /= N44_COMBO3_L59) then
  !      write(unit=STDOUT, fmt=1014)
      call a%write_para(cqm_f1014)
      this%player%nlockparts_good = 0
    else
      this%player%nlockparts_good = 3
      call this%player%score%apply_unlock_score()
      call this%map%room(R21_ATTIC)%door%unlock()
  !      write(unit=STDOUT, fmt=1015)
      call a%write_para(cqm_f1015)
    end if
  else
  !    write(unit=STDOUT, fmt=1013)
    call a%write_para(cqm_f1013)
  end if

  ! cycle L25
  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_left

!> Implement action 53 (V53_RIGHT). Turn combination lock right
type(resolution_t) function do_right(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this
  !> Reference to player
  type(action_t), intent(in) :: cmd

  continue

  call outcome%init()

  !     ---RIGHT---
  ! ACTION(53)  R, RIGH    right

  if (this%player%is_in(R21_ATTIC)) then
    if (this%player%nlockparts_good == 1                                &
      .and. cmd%object_is(N43_COMBO2_R31)) then
      this%player%nlockparts_good = 2
  !      write(unit=STDOUT, fmt=1008)
      call a%write_para(cqm_f1008)
    else
  !      write(unit=STDOUT, fmt=1014)
      call a%write_para(cqm_f1014)
      this%player%nlockparts_good = 0
    end if
  else
  !    write(unit=STDOUT, fmt=1013)
    call a%write_para(cqm_f1013)
  end if

  ! cycle L25
  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_right

!> Implement action 54 (V54_SHOOT). Fire the gun.
type(resolution_t) function do_shoot(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  use :: m_random, only: RDM
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  !> Reference to player
  type(action_t), intent(inout) :: cmd

  ! Local variables

  integer :: II

  continue

  call outcome%init()

  !     ---SHOOT---
  ! ACTION(54)  FIRE, SHOO fire, shoot

L1: do
    if (.not. this%items%s_gun%is_carried()) then
      ! You can't shoot a gun you aren't carrying
    !    write(STDOUT, 1002)
      call a%write_para(cqm_f1002)
      ! cycle L25
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    end if

    if (this%items%s_gun%is_empty()) then
      ! You can't shoot an unloaded gun
    !    write(STDOUT, 1054)
      call a%write_para(cqm_f1054)
      ! cycle L25
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    end if

    ! Empty the pistol
    call this%items%s_gun%unload()
    if (this%encounters%a_gnome%is_attacking()                                          &
      .and. (cmd%no_object() .or. cmd%object_is(N77_GNOME))) then

      ! Target defaults to gnome if he's here.
      ! If the gnome is here and you're not specifically aiming at
      ! something other than the gnome
    !    write(STDOUT, 1108)
      call a%write_para(cqm_f1108)
      ! Kill the gnome. It was you or him.
      ! It was self-defense; the hunchback saw everything
      call this%encounters%a_gnome%calm()
      ! The bullet lands in the square room
      call this%items%portable%item(O2_BULLET)%place_in(R44_SQUARE_ROOM)
      ! cycle L25
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    end if

    select case(cmd%object)
    case(N76_WEREWOLF)
      ! Shoot werewolf
      if (this%encounters%a_werewolf%is_attacking()) then
        if (RDM() < (1.0 - this%encounters%a_werewolf%p_defeat)) then
    !        write(STDOUT, 1065)
          call a%write_para(cqm_f1065)
          call this%items%portable%item(O2_BULLET)%place_in(this%player%room)
          II = 1
          call this%encounters%a_werewolf%resolve_fight(II)
          if (II == 0) then
            ! cycle L25
            ! Signals 'cycle L25' on exit
            outcome%to_next_action = .true.
            exit L1
          else
            ! exit L25
            outcome%to_next_life = .true.
            exit L1
          end if
        else
          call this%encounters%a_werewolf%calm()
          call this%items%portable%item(O2_BULLET)%place_in(this%player%room)
    !        write(STDOUT, 1053)
          call a%write_para(cqm_f1053h, (/cqm_f1053b/))
          ! call DES(602, this%option%debug_mode)
          call a%write_para(cq_form2(2))

          ! cycle L25
          ! Signals 'cycle L25' on exit
          outcome%to_next_action = .true.
          exit L1
        end if
      else
    !      write(STDOUT, 1003)
        call a%write_para(cqm_f1003)
        ! cycle L25
        ! Signals 'cycle L25' on exit
        outcome%to_next_action = .true.
        exit L1
      end if
    case(O8_HUNCHBACK)
      ! Shoot hunchback
      if (this%items%portable%item(O8_HUNCHBACK)%at_hand_in(this%player%room)) then
        if (this%items%portable%is_carried(O8_HUNCHBACK)) then
          call this%player%lose_item(1)
        end if
        call this%encounters%a_hunchback%expire()
        call this%items%portable%consume(O8_HUNCHBACK)
        call this%items%portable%consume(O2_BULLET)
      else
    !      write(STDOUT, 1003)
        call a%write_para(cqm_f1003)
      end if
      ! cycle L25
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    case(N49_BUTLER)
      ! Shoot butler
      if (this%player%is_in(R8_FOYER)) then
        if (this%encounters%a_butler%is_dead()) then
    !        write(STDOUT, 1057)
          call a%write_para(cqm_f1057)
          call this%items%s_gun%load()
          call this%player%gain_item(1)
        else
          call this%encounters%a_butler%expire()
          call this%items%portable%consume(O2_BULLET)
    !        write(STDOUT, 1055)
          call a%write_para(cqm_f1055)
        end if
      else
    !      write(STDOUT, 1003)
        call a%write_para(cqm_f1003)
      end if
      ! cycle L25
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    case(N55_BAT)
      ! Shoot bat
      if (this%encounters%a_bat%is_hungry()) then
    !      write(STDOUT, 1058)
        call a%write_para(cqm_f1058)
        call this%items%portable%item(O2_BULLET)%place_in(this%player%room)
      else
    !      write(STDOUT, 1057)
        call a%write_para(cqm_f1057)
        call this%items%s_gun%load()
        call this%player%gain_item(1)
      end if
      ! cycle L25
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    case(N78_CYCLOPS)
      ! Shoot cyclops
      if (this%encounters%a_cyclops%left_hole()) then
    !      write(STDOUT, 1057)
        call a%write_para(cqm_f1057)
        call this%items%s_gun%load()
        call this%player%gain_item(1)
      else
    !      write(STDOUT, 1126)
        call a%write_para(cqm_f1126)
        call this%items%portable%consume(O2_BULLET)
      end if
      ! cycle L25
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    case(N80_WIZARD)
      ! Shoot wizard
    !    write(STDOUT, 1095)
      call a%write_para(cqm_f1095)
      ! cycle L25
      ! Signals 'cycle L25' on exit
      outcome%to_next_action = .true.
      exit L1
    end select

    ! if (OBJECT == 76 .or. a_werewolf%is_attacking()) then
    ! Shoot wolf if he's there
    ! Q: Should this be "if (OBJECT == 0 .and. a_werewolf%is_attacking()) then" ?
    if (this%encounters%a_werewolf%is_attacking()) then
      ! Assume you're shooting the wolf
      if (RDM() < (1.0 - this%encounters%a_werewolf%p_defeat)) then
          ! Shot at wolf and missed
    !      write(STDOUT, 1065)
        call a%write_para(cqm_f1065)
        call this%items%portable%item(O2_BULLET)%place_in(this%player%room)
        II = 1
        call this%encounters%a_werewolf%resolve_fight(II)
        if (II == 0) then
          ! Evade!
        else
          ! You are Purina Wolf Chow
          ! exit L25
          outcome%to_next_life = .true.
          exit L1
        end if
      else
        ! Plugged the varmint
        call this%encounters%a_werewolf%calm()
        call this%items%portable%item(O2_BULLET)%place_in(this%player%room)
    !      write(STDOUT, 1053)
        call a%write_para(cqm_f1053h, (/cqm_f1053b/))
        ! call DES(602, this%option%debug_mode)
        call a%write_para(cq_form2(2))
      end if
    else
    !    write(STDOUT, 1003)
      call a%write_para(cqm_f1003)
    end if
    ! cycle L25
    ! Signals 'cycle L25' on exit
    outcome%to_next_action = .true.
    exit L1
  end do L1

  return
end function do_shoot

!> Implement action 55 (V55_WAKE). Rouse someone from their slumber.
type(resolution_t) function do_wake(this, cmd) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_format, only: a
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  !> Reference to player action
  type(action_t), intent(inout) :: cmd

  continue

  !     ---WAKE---
  ! ACTION(55)  WAKE wake
  call outcome%init()

  L1: do
    if (cmd%object_is(N49_BUTLER)) then
      ! Wake butler
      if (this%encounters%a_butler%cannot_wake()) then
  !        write(unit=STDOUT, fmt=1044)
        call a%write_para(cqm_f1044)
      else
        call this%encounters%a_butler%wake()
        if (this%items%portable%item(O10_PAPER)%place == -3             &
            .or. (this%items%portable%item(O10_PAPER)%in_room(R8_FOYER) &
            .and. this%items%portable%item(O11_PEN)%in_room(R8_FOYER))) &
          then
          call this%encounters%a_butler%write_note()
          this%items%portable%item(O10_PAPER)%place = -3
          this%items%portable%item(O11_PEN)%place = -3
        end if
        ! call DES(this%encounters%a_butler%des_index(), this%option%debug_mode)
        call a%write_para(                                              &
          cq_form2(this%encounters%a_butler%form2_index()))
      end if
      exit L1
      ! cycle L25
    else if (cmd%object_is(N39_MASTER)) then
      ! Wake count
      if (.not. this%player%is_in(R43_MASTER_CHAMBER)) then
  !        write(unit=STDOUT, fmt=1003)
        call a%write_para(cqm_f1003)
        exit L1
        ! cycle L25
      end if
      if ((.not. this%encounters%a_master%in_coffin())                  &
        .and. this%encounters%a_master%is_alive()) then
        call this%encounters%a_master%rise()
        ! call DES(this%encounters%a_master%des_index(), this%option%debug_mode)
        call a%write_para(                                              &
          cq_form2(this%encounters%a_master%form2_index()))
        exit L1
        ! cycle L25
      end if
    end if

  !    write(unit=STDOUT, fmt=1005)
    call a%write_para(cqm_f1005)
    ! cycle L25
    exit L1
  end do L1

  ! Signals 'cycle L25' on exit
  outcome%to_next_action = .true.

  return
end function do_wake

!> Implement action 56 (V56_POOF). Transit between empty room
!! (R56_EMPTY_ROOM) and boudoir (R15_BOUDOIR)
type(resolution_t) function do_poof(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  use :: m_object_id
  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  continue

  !     ---POOF---
  ! ACTION(56)  POOF poof

  call outcome%init()

  ! If you're not in either the empty room or boudoir,
  ! nothing happens.
  if (this%player%is_in(R56_EMPTY_ROOM)                                    &
    .or. this%player%is_in(R15_BOUDOIR)) then
    if (.not. this%map%room(R2_DIM_CORRIDOR)%door%is_locked()) then
      ! The hunchback teleports into the Mirror Maze
      if (this%items%portable%is_carried(O8_HUNCHBACK)) then
        call this%items%portable%item(O8_HUNCHBACK)%place_in(R57_MAZE_SHORT_WINDING)
      end if
      ! Drop the rowboat if you have it
      if (this%items%portable%is_carried(O14_BOAT)) then
        call this%items%portable%item(O14_BOAT)%place_in(this%player%room)
      end if
      if (this%player%is_in(R56_EMPTY_ROOM)) then
        ! Room 56 -> room 15 (empty room to boudoir)
        call this%player%move_to(to=R15_BOUDOIR, from=0)
      else
        ! Room 15 -> room 56 (boudoir to empty room)
        call this%player%move_to(to=R56_EMPTY_ROOM, from=0)
      end if
      ! cycle L20
      outcome%to_next_area = .true.
    else
      ! cycle L25
      outcome%to_next_action = .true.
    end if
  else
    ! cycle L25
    outcome%to_next_action = .true.
  end if

  if (outcome%to_next_action) then
    ! Paired with cycle L25
  !    write(unit=STDOUT, fmt=1095)
    call a%write_para(cqm_f1095)
  end if

  return
end function do_poof

!> Implement action 57 (V57_SAVE). Write serialized game objects to save
!! file.
type(resolution_t) function do_save(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  use :: m_statevector, only: state_vector_t
  use :: m_object_id
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(in) :: this

  ! Local variables

  ! State vector object
  type(state_vector_t) :: state_vector

  ! Interim score
  integer :: my_score

  character(len=80) :: msg

  ! Formats
9998 format('  You made ', I4, ' moves, and scored ', I4, ' points.')

  continue

  !     ---SUSPEND---
  ! ACTION(57)  SAVE, SUSP save, suspend*
  call state_vector%init()
  call state_vector%save_game(this%player, this%map, this%items,        &
    this%encounters)

  ! Show score and quit after saving
  my_score = this%player%score%SCORE                                    &
    + this%items%portable%total_score(this%player%moves)
  write(unit=msg, fmt=9998) this%player%moves, my_score
  call a%write_para(msg)

  call outcome%init()
  outcome%end_game = .true.

  return
end function do_save

!> Implement action 58 (V58_RESTORE). Restore game objects from save
!! file.
type(resolution_t) function do_restore(this, current_move_number)       &
  result(outcome)
  use :: m_statevector, only: state_vector_t
  use :: m_items, only: NITEMS
  use :: m_object_id
  use :: m_where, only: NROOMS

  implicit none
  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  !> Number of moves taken in current game
  integer, intent(in) :: current_move_number

  ! Local variables
  ! State vector object
  type(state_vector_t) :: state_vector

  continue

  !     ---RESTORE---
  ! ACTION(58)  REST restore*
  call state_vector%init()
  call state_vector%restore_game(current_move_number, this%player,      &
    this%map, this%items, this%encounters)

  ! cycle L20
  call outcome%init()
  outcome%to_next_area = .true.

  return
end function do_restore

!> Implement action 59 (V59_DEBUG). Enable debut output.
type(resolution_t) function do_debug(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDIN => INPUT_UNIT,       &
    STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  implicit none
  ! Parameters
  ! Debug password
  character(len=4), parameter :: PW = '..  '
  ! Password prompt
  character(len=*), parameter :: cqm_pwask = '  ENTER THE PASSWORD:'

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  ! Local variables
  ! User-entered password
  character(len=4) :: MYPW

  ! Formats

7102 format(A4)

  continue
  !     ---DEBUG---
  ! ACTION(60)  DEBU, debug
  call a%write_para(cqm_pwask, (/' '/))

  read(unit=STDIN, fmt=7102) MYPW

  if (MYPW == PW) then
    call this%set_debug()
    call a%write_para(cqm_f1008)
  else
    call a%write_para(cqm_f1016)
  end if

  call outcome%init()
  outcome%to_next_action = .true.

  return
end function do_debug

!> Implement action 60 (V61_VERBOSE). Set verbose output mode
!! and disable debut output.
type(resolution_t) function do_verbose(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  continue
  !     ---VERBOSE---
  ! ACTION(60)  VERB, LONG long, verbose*

  call this%clear_debug()
  call this%player%clear_brief()
  call a%write_para(cqm_f1008)

  call outcome%init()
  outcome%to_next_action = .true.

  return
end function do_verbose

!> Implement action 61 (V61_BRIEF). Set brief output.
type (resolution_t) function do_brief(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  implicit none

  ! Arguments
  !> Game implementation object
  class(game_t), intent(inout) :: this

  continue
  !     ---BRIEF---
  ! ACTION(61)  BRIE brief*

  call this%player%set_brief()
  call a%write_para(cqm_f1073)

  call outcome%init()
  outcome%to_next_action = .true.

  return
end function do_brief

!> Tasks triggered when moving to a new area
function game_update_world(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_log, only: log_debug
  use :: cquest_text
  use :: m_format, only: a
  use :: m_statevector, only: state_vector_t
  use :: m_items, only: light_available
  use :: m_object_id
  use :: m_textio, only: itoa
  use :: m_where, only: is_dark_room, in_winding_maze
  implicit none

  ! Parameters

  character(len=*), parameter :: cqm_gp6301 = '  TOP OF LOOP (STATEMENT 20)'
  character(len=*), parameter :: cqf_gp6302 = "('  NUMBER OF MOVES IS ', I3)"

  ! Outcome object; result status
  type(resolution_t) :: outcome

  ! Arguments

  !> Game implemetationb object
  class(game_t), intent(inout) :: this

  ! Local variables

  integer :: I
  character(len=80) :: msg

  ! Game state vector object
  type(state_vector_t) :: state_vector

  ! Formats

! 6301 format('0  TOP OF LOOP (STATEMENT 20)')
! 6302 format('0  NUMBER OF MOVES IS ', I3)

  continue

  call outcome%init()

  if (this%option%debug_mode) then
    ! write(unit=STDOUT, fmt=6301)
    call a%write_para(cqm_gp6301)
  end if

  call log_debug('L20: top of main event loop')

L24: do
    call log_debug('L24: top of pre-action block')

    ! Restore on load
    if (this%option%auto_restore) then
      this%option%auto_restore = .false.
      call state_vector%init()
      call state_vector%restore_game(this%player%moves, this%player,    &
        this%map, this%items, this%encounters)
      ! cycle L20
      outcome%to_next_area = .true.
      exit L24
    end if

    if (is_dark_room(this%player%room)                                  &
      .and. .not. light_available(this%player%room,                     &
      this%items%s_lamp, this%items%s_match)) then
      ! Complain about the darkness
      ! write(unit=STDOUT, fmt=1064)
      call a%write_para(cqm_f1064)
      ! Get next player action
      call log_debug('L24a: No light in room '                          &
        // itoa(this%player%room))
      outcome%to_next_action = .true.
      exit L24
    end if

    call this%player%advance()

    if (this%option%debug_mode) then
      ! write(unit=STDOUT, fmt=6302) player%moves
      write(unit=msg, fmt=cqf_gp6302) this%player%moves
      call a%write_para(msg)
    end if
    call log_debug('L24b: Total moves made is '                         &
      // itoa(this%player%moves))

    ! Update location of free-range hunchback

    ! ROOM(57-63) = Winding Maze, ROOM(64) = Dead end
    ! ITEMS(8) = Hunchback
    ! If hunchback is in the maze or the dead end, he reappears in maze
    ! [57 .. 63]
    if (in_winding_maze(this%items%portable%item(O8_HUNCHBACK)%place))  &
      then
      I = this%items%portable%item(O8_HUNCHBACK)%place
      call this%items%portable%item(O8_HUNCHBACK)%scatter(              &
        lo_room=57, hi_room=63)
      call log_debug('L24c: Free-range hunchback moves from '           &
        // itoa(I) // ' to '                                            &
        // itoa(this%items%portable%item(O8_HUNCHBACK)%place))
    end if

    ! Update state of sun

    if (this%player%moves > 100) then
      ! !t gets dark after turn 100
      if (this%items%s_sun%is_up()) then
        if (this%encounters%a_master%is_alive()) then
          ! write(unit=STDOUT, fmt=1089)
          call a%write_para(cqm_f1089)
          call log_debug('L24d: Sunset on turn '                      &
            // itoa(this%player%moves))
        end if
        ! Deviation from original: only call sunset after turn 100 if
        ! sun still is up. User experience should not change.
        call this%items%s_sun%set()
      end if
    end if

    ! Update state of match

    ! If match is lit
    if (this%items%s_match%is_lit()) then
      call this%items%s_match%burn_one_turn(this%player%items_held)
      call log_debug('L24e1: Burn match for one turn')
      if (this%items%s_match%is_empty()) then
        ! You discover you're out of matches. Crap.
        ! write(unit=STDOUT, fmt=1088)
        call a%write_para(cqm_f1088)
        call log_debug('L24e2: Matches all consumed. Uh oh.')
        !  Check to see if glacier is melted yet, and if cyclops has crashed
        !  through the door yet. (These both require the match). If not, decrease
        !  MMAX because match is needed to get them.  (MMAX determines when
        !  the player has found everything).
        if (this%encounters%a_cyclops%blocks_way()) then
          ! Can't put cyclops through door now
          call this%player%score%reduce_mmax(10)
          call log_debug('L24e3: Remove cyclops score opportunity')
        end if
        if (.not. this%map%path_through_glacier) then
          ! Can't melt glacier now
          call this%player%score%reduce_mmax(10)
          call log_debug('L24e4: Remove glacier melt score '            &
            // 'opportunity')
        end if
        ! cycle L20
        outcome%to_next_area = .true.
        exit L24
      end if
    end if

    ! Update state of lamp

    ! To quell log noise, don't call this unless the lamp is lit
    if (this%items%s_lamp%is_lit()                                      &
      .and. .not. this%items%s_lamp%is_empty()) then
      ! Note: this does nothing if lamp is not lit
      call this%items%s_lamp%burn_one_turn(                             &
        this%items%portable%item(O1_KEROSENE), this%player%items_held)
      call log_debug('L24f1: Burn lit lamp for one turn; lit for '      &
        // itoa(this%items%s_lamp%turns_lit)                            &
        // ' turns since refueling')
      if (this%items%s_lamp%is_empty()) then
        ! Warn about moving in darkness
        ! write(unit=STDOUT, fmt=1064)
        call a%write_para(cqm_f1064)
        call log_debug('L24f2: Lamp out of fuel. Uh oh.')
        ! Get player's next action
        exit L24
      end if
    end if

    outcome = this%describe_area()

    exit L24

    outcome%to_next_action = .true.

    exit L24
  end do L24
  call log_debug('L24: Bottom of pre-action block')
  !=== End Game Actions

  return
end function game_update_world

!> Resolve player actions in a single area
function game_update_player(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_log, only: log_debug, log_error
  use :: cquest_text
  use :: m_action, only: action_t
  use :: m_debug, only: show_state, log_state, log_inventory
  use :: m_format, only: a
  use :: m_items, only: light_available
  use :: m_object_id
  use :: m_textio, only: itoa
  use :: m_where, only: is_dark_room, in_winding_maze

  implicit none

  ! Outcome object
  type(resolution_t) :: outcome

  ! Arguments

  !> Game implementation object
  class(game_t), intent(inout) :: this

  ! Local variables

  ! Player command object
  type(action_t) :: cmd

  integer :: J

  continue

  call outcome%init()
  call cmd%init()

L25: do

  !=== Player Turn
    call log_debug('L25: Top of player action block')

    if (this%option%debug_mode) then
      call show_state('L25: Before user command', this%player,          &
        this%map, this%player%revealed_letter, this%items,              &
        this%encounters)
    end if

    call log_state('L25: Before user command', this%player, this%map,   &
      this%player%revealed_letter, this%items, this%encounters)

    call log_inventory(this%items%portable)

    call cmd%get_command(this%option%write_logs)

    call log_debug('L25a: Player action:object is '                   &
      // itoa(cmd%action) // ':' // itoa(cmd%object))
!
!     BRANCH DEPENDING ON VERB READ.
!
    J = cmd%action

L26:  do
      call log_debug('L26: Enter action processor with action = '     &
        // itoa(J))

      !=== Process One Action
      call outcome%reset()
      select case(J)
      case(1:10)
        !     ---MOVE---
        ! ACTION(1:10)  Move N, NE, E, SE, S, SW, W, NW, U, D
        call log_debug('L26:ACTION ' // itoa(J) // ': MOVE')

        outcome = this%do_move(J)

      case(V11_TAKE)
        !     ---TAKE---
        ! ACTION(11)  TAKE
        call log_debug('L26:ACTION ' // itoa(J) // ': TAKE')

        outcome = this%do_take(cmd)

      case(V12_DROP)
      !     ---DROP---
      ! ACTION(12)  DROP drop
        call log_debug('L26:ACTION ' // itoa(J) // ': DROP')

        outcome = this%do_drop(cmd)

      case(V13_ENTER)
        !     ---ENTER---
        ! ACTION(13)  ENTE, IN enter
        call log_debug('L26:ACTION ' // itoa(J) // ': ENTER')

        outcome = this%do_enter(J)

      case(V14_EXIT)
        !     ---LEAVE---
        ! ACTION(14)  EXIT, LEAV, OUT exit, leave, out
        call log_debug('L26:ACTION ' // itoa(J) // ': EXIT')

        outcome = this%do_exit(J)

      case(V15_ATTACK, V16_KILL)
        !     ---ATTACK---
        ! ACTION(15)  ATTA attack
        ! ACTION(16)  KILL kill
        call log_debug('L26:ACTION ' // itoa(J) // ': ATTACK/KILL')

        outcome = this%do_attack_kill(cmd)

      case(V17_THROW)
        !     ---THROW---
        ! ACTION(17)  THRO throw
        call log_debug('L26:ACTION ' // itoa(J) // ': THROW')

        outcome = this%do_throw(cmd, J)

      case(V18_LOAD)
        !     ---LOAD---
        ! ACTION(18)  LOAD load
        call log_debug('L26:ACTION ' // itoa(J) // ': LOAD')

        outcome = this%do_load(cmd)

      case(V19_AHEM)
        !    ---FUCK---
        ! ACTION(19)  FUCK ahem
        call log_debug('L26:ACTION ' // itoa(J) // ': AHEM')

        outcome = this%do_ahem(cmd)

      case(V20_WAVE)
        !     ---WAVE---
        ! ACTION(20)  WAVE, SHOW show, wave
        call log_debug('L26:ACTION ' // itoa(J) // ': WAVE')

        outcome = this%do_wave(cmd)

      case(V21_STAB)
        !     ---STAB---
        ! ACTION(21)  STAB stab
        call log_debug('L26:ACTION ' // itoa(J) // ': STAB')

        outcome = this%do_stab(cmd)

      case(V22_FEED)
        !     ---FEED---
        ! ACTION(22)  FEED feed
        call log_debug('L26:ACTION ' // itoa(J) // ': FEED')

        outcome = this%do_feed(cmd)

      case(V23_EAT)
        !     ---EAT---
        ! ACTION(23)  EAT  eat
        call log_debug('L26:ACTION ' // itoa(J) // ': EAT')

        outcome = this%do_eat(cmd)

      case(V24_DRINK)
        !     ---DRINK---
        ! ACTION(24)  DRIN drink
        call log_debug('L26:ACTION ' // itoa(J) // ': DRINK')

        outcome = this%do_drink(cmd)

      case(V25_JUMP)
        !     ---JUMP---
        ! ACTION(25)  JUMP jump
        call log_debug('L26:ACTION ' // itoa(J) // ': JUMP')

        outcome = this%do_jump()

      case(V26_INVENTORY)
        !     ---INVENTORY---
        ! ACTION(26)  INVE inventory*
        call log_debug('L26:ACTION ' // itoa(J) // ': INVENTORY')

        outcome = this%do_inventory()

      case(V27_OPEN)
        !     ---OPEN---
        ! ACTION(27)  OPEN open
        call log_debug('L26:ACTION ' // itoa(J) // ': OPEN')

        outcome = this%do_open(cmd)

      case(V28_CLOSE)
        !     ---CLOSE---
        ! ACTION(28)  CLOS close
        call log_debug('L26:ACTION ' // itoa(J) // ': CLOSE')

        outcome = this%do_close(cmd)

      case(V29_LOCK)
        !     ---LOCK---
        ! ACTION(29)  LOCK lock
        call log_debug('L26:ACTION ' // itoa(J) // ': LOCK')

        outcome = this%do_lock(cmd)

      case(V30_UNLOCK)
        !     ---UNLOCK---
        ! ACTION(30)  UNLO unlock
        call log_debug('L26:ACTION ' // itoa(J) // ': UNLOCK')

        outcome = this%do_unlock(cmd)

      case(V31_ON)
        !     ---ON---
        ! ACTION(31)  ON   on
        call log_debug('L26:ACTION ' // itoa(J) // ': ON')

        outcome = this%do_on(cmd, J)

      case(V32_LIGHT)
        !     ---LIGHT---
        ! ACTION(32)  LIGH light
        call log_debug('L26:ACTION ' // itoa(J) // ': LIGHT')

        outcome = this%do_light(cmd)

      case(V33_OFF)
        !     ---OFF---
        ! ACTION(33)  OFF  off
        call log_debug('L26:ACTION ' // itoa(J) // ': OFF')

        outcome = this%do_OFF(cmd, J)

      case(V34_EXTINGUISH)
        !     --EXTINGUISH---
        ! ACTION(34)  EXTI extinguish
        call log_debug('L26:ACTION ' // itoa(J) // ': EXTINGUISH')

        outcome = this%do_extinguish(cmd)

      case(V35_LOOK)
        !     ---LOOK---
        ! ACTION(35)  LOOK look
        call log_debug('L26:ACTION ' // itoa(J) // ': LOOK')

        outcome = this%do_look()

      case(V36_SCORE)
        !     ---SCORE---
        ! ACTION(36)  SCOR score*
        call log_debug('L26:ACTION ' // itoa(J) // ': SCORE')

        outcome = this%do_score()

      case(V37_BREAK)
        !     ---BREAK---
        ! ACTION(N37_WINDOW)  BREA, CHOP break, chop
        call log_debug('L26:ACTION ' // itoa(J) // ': BREAK')

        outcome = this%do_break(cmd)

      case(V38_WATER, V39_POUR)
        !     ---WATER---
        ! ACTION(38)  WATE water
        !     ---POUR---
        ! ACTION(39)  POUR pour
        call log_debug('L26:ACTION ' // itoa(J) // ': WATER/POUR')

        outcome = this%do_water_pour(cmd, J)

      case(V40_BACK)
        !     ---BACK---
        ! ACTION(40)  BACK back
        call log_debug('L26:ACTION ' // itoa(J) // ': BACK')

        outcome = this%do_back()

      case(V41_SWIM)
        !     ---SWIM---
        ! ACTION(41)  SWIM swim
        call log_debug('L26:ACTION ' // itoa(J) // ': SWIM')

        outcome = this%do_swim()

      case(V42_MELT)
        !     ---MELT---
        ! ACTION(42)  MELT melt
        call log_debug('L26:ACTION ' // itoa(J) // ': MELT')

        outcome = this%do_melt(cmd)

      case(V43_CROSS)
        !     ---CROSS---
        ! ACTION(43)  CROS cross
        call log_debug('L26:ACTION ' // itoa(J) // ': CROSS')

        outcome = this%do_cross()

      case(V44_QUIT)
        !     ---QUIT---
        ! ACTION(44)  QUIT quit*
        call log_debug('L26:ACTION ' // itoa(J) // ': QUIT')

        outcome = this%do_quit()

      case(V45_HONK)
        !     ---HONK---
        ! ACTION(45)  HONK honk
        call log_debug('L26:ACTION ' // itoa(J) // ': HONK')

        outcome = this%do_honk()

      case(V46_TIE)
        !     ---TIE---
        ! ACTION(46)  TIE  tie
        call log_debug('L26:ACTION ' // itoa(J) // ': TIE')

        outcome = this%do_tie(cmd)

      case(V47_UNTIE)
        !     ---UNTIE---
        ! ACTION(47)  UNTI unti
        call log_debug('L26:ACTION ' // itoa(J) // ': UNTIE')

        outcome = this%do_untie(cmd)

      case(V48_READ)
        !     ---READ---
        ! ACTION(48)  READ read
        call log_debug('L26:ACTION ' // itoa(J) // ': READ')

        outcome = this%do_read(cmd)

      case(V49_FILL)
        !     ---FILL---
        ! ACTION(49)  FILL fill
        call log_debug('L26:ACTION ' // itoa(J) // ': FILL')

        outcome = this%do_fill(cmd, J)

      case(V50_HELP)
        !     ---HINT---
        ! ACTION(50)  HELP, HINT help, hint*
        call log_debug('L26:ACTION ' // itoa(J) // ': HELP')

        outcome = this%do_help(cmd)

      case(V51_GOTO)
        !     --GOTO--
        ! ACTION(51)  GOTO goto*
        call log_debug('L26:ACTION ' // itoa(J) // ': GOTO')

        outcome = this%do_goto()

      case(V52_LEFT)
        !     ---LEFT---
        ! ACTION(52)  L, LEFT    left
        call log_debug('L26:ACTION ' // itoa(J) // ': LEFT')

        outcome = this%do_left(cmd)

      case(V53_RIGHT)
        !     ---RIGHT---
        ! ACTION(53)  R, RIGH    right
        call log_debug('L26:ACTION ' // itoa(J) // ': RIGHT')

        outcome = this%do_right(cmd)

      case(V54_SHOOT)
        !     ---SHOOT---
        ! ACTION(54)  FIRE, SHOO fire, shoot
        call log_debug('L26:ACTION ' // itoa(J) // ': SHOOT')

        outcome = this%do_shoot(cmd)

      case(V55_WAKE)
        !     ---WAKE---
        ! ACTION(55)  WAKE wake
        call log_debug('L26:ACTION ' // itoa(J) // ': WAKE')

        outcome = this%do_wake(cmd)

      case(V56_POOF)
        !     ---POOF---
        ! ACTION(56)  POOF poof
        call log_debug('L26:ACTION ' // itoa(J) // ': POOF')

        outcome = this%do_poof()

      case(V57_SAVE)
        !     ---SUSPEND---
        ! ACTION(57)  SAVE, SUSP save, suspend*
        call log_debug('L26:ACTION ' // itoa(J) // ': SUSPEND')

        outcome = this%do_save()

      case(V58_RESTORE)
        !     ---RESTORE---
        ! ACTION(58)  REST restore*
        call log_debug('L26:ACTION ' // itoa(J) // ': RESTORE')

        outcome = this%do_restore(this%player%moves)

      case(V59_DEBUG)
        !     ---DEBUG---
        ! ACTION(59)  DEBU debug*
        call log_debug('L26:ACTION ' // itoa(J) // ': DEBUG')
        outcome = this%do_debug()

      case(V60_VERBOSE)
        !     ---VERBOSE---
        ! ACTION(60)  VERB, LONG long, verbose*
        call log_debug('L26:ACTION ' // itoa(J) // ': VERBOSE')
        outcome = this%do_verbose()

      case(V61_BRIEF)
        !     ---BRIEF---
        ! ACTION(61)  BRIE brief*
        call log_debug('L26:ACTION ' // itoa(J) // ': BRIEF')
        outcome = this%do_brief()

      case default
        ! Should never get here
        call log_error('L26: Unknown action ' // itoa(J))

        call a%write_para(cqm_f1201)
        call outcome%reset()
        outcome%to_next_action = .true.
        ! STOP
      end select
      ! cycle L25 ! OUR GOAL!
      !=== End Process One Action

      call log_debug('Fell out of action selector with action '       &
        // itoa(J))

      if (outcome%permadeath) then
        call log_debug('Outcome is permadeath')
        ! exit L20
        exit L25
      else if (outcome%end_game) then
        call log_debug('Outcome is end_game')
        ! exit L20
        exit L25
      else if (outcome%to_next_area) then
        call log_debug('Outcome is to_next_area')
        ! cycle L20
        exit L25
      else if (outcome%to_next_life) then
        call log_debug('Outcome is to_next_life')
        exit L25
      else if (outcome%to_next_action) then
        call log_debug('Outcome is to_next_action')
        cycle L25
      else if (outcome%reprocess_action) then
        call log_debug('Outcome is reprocess_action')
        cycle L26
      else
        ! Error condition
        ! Should never get here either
        call log_error('Unrecognized response')
        stop
      end if

      ! Should never get here either
      stop

!--------------------------- SUPER SERIOUSLY YOU SHOULD NEVER GET HERE

      exit L26
    end do L26

    call log_debug('gameloop_player - L26: Exit action processor')

    !=== End Player Turn
    exit L25
  end do L25
  call log_debug('gameloop_player - L25: Exit player action block')

  return
end function game_update_player

!> Main game event loop
function game_run(this) result(outcome)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_log, only: log_debug, log_error, log_notice
  use :: m_format, only: a
  ! use :: m_gamedata, only: show_help
  use :: m_object_id
  use :: m_textio, only: itoa

  implicit none

  ! Parameters

  character(len=*), parameter :: msg_debugon = "Debugging mode enabled"

  ! Returned variable

  ! Outcome object, return status
  type(resolution_t) :: outcome

  ! Arguments

  !> Structure holding game option flags
  class(game_t), intent(inout) :: this

  continue

  call outcome%init()

  if (this%option%debug_mode) then
    call a%write_para(msg_debugon)
    call log_debug(msg_debugon)
  end if

  !=== Setup
  call log_notice("Starting new game")

  !=== Show initial help screen unless auto_restore is true
  if (.not. this%option%auto_restore) then
    call show_help(intro=.true.)
  end if

! Main event loop
L20: do
    !=== Game Actions
    outcome = this%update_world()

    if (outcome%end_game) then
      exit L20
    else if (outcome%to_next_area) then
      cycle L20
    ! -- else fall out of L24 immediately to L25
    end if
    !=== End Game Actions

    outcome = this%update_player()

    !=== Player Actions
    if (outcome%permadeath) then
      call log_debug('Outcome is permadeath')
      exit L20
    else if (outcome%end_game) then
      call log_debug('Outcome is end_game')
      exit L20
    else if (outcome%to_next_area) then
      call log_debug('Outcome is to_next_area')
      cycle L20
    else if (outcome%to_next_life) then
      !=== Reaper Turn
      outcome = this%resolve_death()
      !=== End Reaper Turn

      if (outcome%permadeath) then
        exit L20
      else
        !???
        cycle L20
      end if

      !=== End Player Actions

    else if (outcome%to_next_action                                   &
      .or. outcome%reprocess_action) then
      call log_debug('Outcome is legal but unexpected')
      stop
    else
      ! Error condition
      call log_error('Unrecognized response')
      stop
    end if

    stop ! Should never get here either
!--------------------------- SUPER SERIOUSLY YOU SHOULD NEVER GET HERE

    ! call log_debug('game_run - L25: Exit player action block')

    ! !=== Reaper Turn
    ! outcome = this%resolve_death()
    ! !=== End Reaper Turn

    ! if (outcome%permadeath) then
    !   exit L20
    ! end if

    ! call log_debug('game_run - L20: --- end do ---')
  end do L20

  return
end function game_run

end module cquest_game