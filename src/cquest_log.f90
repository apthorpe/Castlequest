!> @file cquest_log.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Provides syslog-style log facilities as an abstraction layer
!! above the lower-level log management system

!> @brief Provides syslog-style log facilities as an abstraction layer
!! above the lower-level log management system
!!
!! Log management is implemented by m_multilog from the FLIBS
!! distribution; see http://flibs.sourceforge.net/
module cquest_log
  use :: m_multilog, only: log_t

  implicit none

  private

  public :: log_all
  public :: log_panic
  public :: log_alert
  public :: log_critical
  public :: log_error
  public :: log_warning
  public :: log_notice
  public :: log_info
  public :: log_debug

  public :: initialize_logs
  public :: finalize_logs

  !> Diagnostic log object
  type(log_t) :: l_diag

  !> Warning/error log object
  type(log_t) :: l_warn

contains

!> Create and initialize diagnostic logs
subroutine initialize_logs(write_logs, logbase, CODENAME, VERSION,      &
  BUILDDATE, filenames)
  use :: m_multilog, only: add_log, log_group_configure, log_msg,       &
    LOG_SV_DEBUG => DEBUG,                                              &
    LOG_SV_WARNING => WARNING,                                          &
    LOG_SV_ALL => ALL

  implicit none

  !> If true, log info will be written to file
  logical, intent(in) :: write_logs

  !> Base logfile name
  character(len=*), intent(in) :: logbase

  !> Software name
  character(len=*), intent(in) :: CODENAME

  !> Software version
  character(len=*), intent(in) :: VERSION

  !> Software build date
  character(len=*), intent(in) :: BUILDDATE

  !> List of log file names
  character(len=80), dimension(:), allocatable, intent(out) :: filenames

  character (len=80), dimension(:), allocatable :: logfiles

  continue

  allocate(logfiles(2))
  logfiles(1) = trim(adjustl(logbase)) // '_diagnostic.log'
  logfiles(2) = trim(adjustl(logbase)) // '_warning.log'

  call l_diag%startup(logfiles(1), append=.false.,                      &
      info_lvl=LOG_SV_DEBUG)

  call l_warn%startup(logfiles(2), append=.false.,                      &
      info_lvl=LOG_SV_WARNING)

  call add_log(l_diag)
  call add_log(l_warn)

  call log_group_configure('timestamp', .true.)

  ! By default logs do not echo to stdout. Still, ensure it's disabled.
  call l_diag%configure('writeonstdout', .false.)
  call l_warn%configure('writeonstdout', .false.)
  ! If logs are not enabled, disable writeonfile as well. This prevents
  ! warnings about writing to an empty log group
  call l_diag%configure('writeonfile', write_logs)
  call l_warn%configure('writeonfile', write_logs)

  call log_msg('Starting ' // CODENAME // ', ' // VERSION               &
                // ', created on ' // BUILDDATE, LOG_SV_ALL)

  call log_msg('Opening all log facilities.', LOG_SV_ALL)

  if (allocated(filenames)) then
      deallocate(filenames)
  end if
  call move_alloc(logfiles, filenames)

  return
end subroutine initialize_logs

!> Gracefully close open log files
subroutine finalize_logs(filenames)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use m_multilog, only: log_msg, shutdown_log_group, LOG_SV_ALL => ALL

  !> List of log file names
  character(len=80), dimension(:), allocatable, intent(inout)           &
    :: filenames

  integer :: i
  integer :: lct
  integer :: lfh
  integer :: nfiles
  integer :: filesize
  integer :: stat
  logical :: is_found

  continue

  call log_msg('Closing all log facilities.', LOG_SV_ALL)
  call shutdown_log_group()

  ! Delete empty log files
  if (allocated(filenames)) then
    lct = 0
    nfiles = size(filenames)
    if (nfiles > 0) then
      do i = 1, nfiles
        inquire(file=filenames(i), exist=is_found, size=filesize)
        if (is_found) then
          if (filesize < 10) then
            open(newunit=lfh, iostat=stat, file=filenames(i),           &
              status='old')
            if (stat == 0) then
              close(lfh, status='delete')
            end if
          else
            if (lct == 0) then
              ! Add blank line before first "Wrote log..." message
              write(unit=STDOUT, fmt='(A)') '   '
            end if
            lct = lct + 1
            write(unit=STDOUT, fmt='(A)') '   Wrote log '               &
              // trim(adjustl(filenames(i)))
          end if
        end if
      end do
    end if
    deallocate(filenames)
  end if

  return
end subroutine finalize_logs

!> Write message to all log facilities
subroutine log_all(msg)
  use m_multilog, only: log_msg, LOG_SV_ALL => ALL

  implicit none

  !> Log message
  character (len=*), intent(in) :: msg

  continue

  call log_msg(msg, LOG_SV_ALL)

!    write(*,*) 1.0 / 0.0

  return
end subroutine log_all

!> Write message to panic log
subroutine log_panic(msg)
  use m_multilog, only: log_msg, LOG_SV_PANIC => ALL

  implicit none

  !> Log message
  character (len=*), intent(in) :: msg

  continue

  call log_msg(msg, LOG_SV_PANIC)

  return
end subroutine log_panic

!> Write message to alert log
subroutine log_alert(msg)
  use m_multilog, only: log_msg, LOG_SV_ALERT => WARNING

  implicit none

  !> Log message
  character (len=*), intent(in) :: msg

  continue

  call log_msg(msg, LOG_SV_ALERT)

  return
end subroutine log_alert

!> Write message to critical log
subroutine log_critical(msg)
  use m_multilog, only: log_msg, LOG_SV_CRITICAL => ERROR

  implicit none

  !> Log message
  character (len=*), intent(in) :: msg

  continue

  call log_msg(msg, LOG_SV_critical)

  return
end subroutine log_critical

!> Write message to error log
subroutine log_error(msg)
  use m_multilog, only: log_msg, LOG_SV_ERROR => ERROR

  implicit none

  !> Log message
  character (len=*), intent(in) :: msg

  continue

  call log_msg(msg, LOG_SV_ERROR)

  return
end subroutine log_error

!> Write message to warning log
subroutine log_warning(msg)
  use m_multilog, only: log_msg, LOG_SV_WARNING => WARNING

  implicit none

  !> Log message
  character (len=*), intent(in) :: msg

  continue

  call log_msg(msg, LOG_SV_WARNING)

  return
end subroutine log_warning

!> Write message to notice log
subroutine log_notice(msg)
  use m_multilog, only: log_msg, LOG_SV_NOTICE => INFO

  implicit none

  !> Log message
  character (len=*), intent(in) :: msg

  continue

  call log_msg(msg, LOG_SV_NOTICE)

  return
end subroutine log_notice

!> Write message to info log
subroutine log_info(msg)
  use m_multilog, only: log_msg, LOG_SV_INFO => INFO

  implicit none

  !> Log message
  character (len=*), intent(in) :: msg

  continue

  call log_msg(msg, LOG_SV_INFO)

  return
end subroutine log_info

!> Write message to debug log
subroutine log_debug(msg)
  use m_multilog, only: log_msg, LOG_SV_DEBUG => DEBUG

  implicit none

  !> Log message
  character (len=*), intent(in) :: msg

  continue

  call log_msg(msg, LOG_SV_DEBUG)

  return
end subroutine log_debug

end module cquest_log
