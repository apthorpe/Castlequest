!> @file cquest_text.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Portions of Castlequest narrative text and text display classes

!> @brief Portions of Castlequest narrative text and text display classes
module cquest_text
  implicit none

  public :: cq_longtext

  public

  !> Room description class
  type :: roomdesc_t
    !> Long description of room
    character(len=80), dimension(:), allocatable :: long
  contains
    !> Function to count number of lines in `long` array. May be zero.
    procedure :: nlast => roomdesc_nlast
    !> Writes room description using global ASA-capable output writer
    procedure :: show => roomdesc_show
  end type roomdesc_t

  !> Describes all rooms
  type :: realtor_t
    !> All room descriptions
    type(roomdesc_t), dimension(100) :: room
  contains
    !> Initialize
    procedure :: init => realtor_init
    !> Wrapper around roomdesc_show
    procedure :: show => realtor_show
  end type realtor_t

  !> Global realtor object
  type(realtor_t) :: cq_longtext

  ! Messages as plain text. These are taken from the source code,
  ! usually with a reference to the original format number

  !> Welcome banner
  character(len=*), parameter :: cqm_welcome =                         &
    ' Welcome to CASTLEQUEST!! Would you like instructions?'
  !> Welcome banner referencing original format number in `main`

  character(len=*), parameter :: cqm_f1001 = cqm_welcome
    ! ' Welcome to CASTLEQUEST!! Would you like instructions?'

  character(len=*), parameter :: cqm_f1002 = &
    '  That would be a neat trick.'
  character(len=*), parameter :: cqm_f1003 = &
    '  I don''t see that here.'
  character(len=*), parameter :: cqm_f1004 = &
    '  You don''t have it with you.'
  character(len=*), parameter :: cqm_f1005 = &
    '  I don''t think I can do that.'
  character(len=*), parameter :: cqm_f1006 = &
    '  There is no way to go in that direction.'
  character(len=*), parameter :: cqm_f1007 = &
    '  Load the gun with what?'
  character(len=*), parameter :: cqm_f1008 = &
    '  OK'
  character(len=*), parameter :: cqm_f1009 = &
    '  The lantern seems to be out of fluid.'
  character(len=*), parameter :: cqm_f1010 = &
    '  The rope is tied securely to the grappling hook.'
  character(len=*), parameter :: cqm_f1011 = &
    '  A rope is hooked to the top of the precipice.'
  character(len=*), parameter :: cqm_f1012 = &
    '  You can''t carry anything else.'
  character(len=*), parameter :: cqm_f1013 = &
    '  I don''t know how to apply that here.'
  character(len=*), parameter :: cqm_f1014 = &
    '  You blew it. I think you''d better start over.'
  character(len=*), parameter :: cqm_f1015 = &
    '  The lock is now open.'
  character(len=*), parameter :: cqm_f1016 = &
    '  You can''t be serious.'
  character(len=*), parameter :: cqm_f1017 = &
    '  YUMMY! That was good.'
  character(len=*), parameter :: cqm_f1018 = &
    '  GLUG GLUG GLUG GLUG BELCH!!'
  character(len=*), parameter :: cqm_f1019 = &
    '  The door is locked.'
  character(len=*), parameter :: cqm_f1020 = &
    '  The door is already open.'
  character(len=*), parameter :: cqm_f1021 = &
    '  The door is boarded up.'
  character(len=*), parameter :: cqm_f1022 = &
    '  The shutter is open, but there are bars over the window.'
  character(len=*), parameter :: cqm_f1023 = &
    '  The window is nailed shut.'
  character(len=*), parameter :: cqm_f1024 = &
    '  There is a silver cross in the drawer!'
  character(len=*), parameter :: cqm_f1025 = &
    '  I see no door here.'
  character(len=*), parameter :: cqm_f1026 = &
    '  It wouldn''t be much of a lock if you could just open it.'
  character(len=*), parameter :: cqm_f1027 = &
    '  A skeleton key falls out of the book.'
  character(len=*), parameter :: cqm_f1028 = &
    '  A literary classic, but we don''t have time to read.'
  character(len=*), parameter :: cqm_f1029 = &
    '  "The master loves Shakespeare".'
  character(len=*), parameter :: cqm_f1030 = &
    '  "Look behind the mirror."'
  character(len=*), parameter :: cqm_f1031 = &
    '  You can''t close a broken window.'
  character(len=*), parameter :: cqm_f1032 = &
    '  I never liked classics, anyway.'
  character(len=*), parameter :: cqm_f1033 = &
    '  Sorry, but you don''t have your library card.'
  character(len=*), parameter :: cqm_f1034 = &
    '  The butler is lame, and could not keep up with you.'
  character(len=*), parameter :: cqm_f1035 = &
    '  You can''t get back the way you came.'
  character(len=*), parameter :: cqm_f1036 = &
    '  The door will neither open nor close.'
  character(len=*), parameter :: cqm_f1037 = &
    '  The light is out.'
  character(len=*), parameter :: cqm_f1038 = &
    '  The light is burning dimly.'
  character(len=*), parameter :: cqm_f1039 = &
    '  With what?? Your bare hands??'
  character(len=*), parameter :: cqm_f1040h = &
    '  You just broke every bone in your hand.'
  character(len=*), parameter :: cqm_f1040b = &
    '  You might try an axe or something....'
  character(len=*), parameter :: cqm_f1041 = &
    '  The door can now be opened.'
  character(len=*), parameter :: cqm_f1042 = &
    '  The door is closed.'
  character(len=*), parameter :: cqm_f1043 = &
    '  You have fallen through a trap door and find...'
  character(len=*), parameter :: cqm_f1044 = &
    '  The butler cannot be aroused.'
  character(len=*), parameter :: cqm_f1045 = &
    '  Take note of what??'
  character(len=*), parameter :: cqm_f1046 = &
    '  The window IS already broken.'
  character(len=*), parameter :: cqm_f1047 = &
    '  You had better watch your mouth.'
  character(len=*), parameter :: cqm_f1048 = &
    '  The mirrors shatter in an explosion of flying glass.'
  character(len=*), parameter :: cqm_f1049 = &
    '  A huge vampire bat hangs from the doorframe and blocks your way.'
  character(len=*), parameter :: cqm_f1050 = &
    '  Feed the bat with what??'
  character(len=*), parameter :: cqm_f1051 = &
    '  The bat gulps down the blood and flitters away.'
  character(len=*), parameter :: cqm_f1052 = &
    '  Bats don''t eat food, they eat blood. Like yours.'
  character(len=*), parameter :: cqm_f1053h = &
    '  You killed a werewolf. An old gypsy woman'
  character(len=*), parameter :: cqm_f1053b = &
    '  appears and drags away the body.'
  character(len=*), parameter :: cqm_f1054 = &
    '  Unfortunately the gun is not loaded.'
  character(len=*), parameter :: cqm_f1055 = &
    '  You killed a deaf-mute butler (Not very sporting of you).'
  character(len=*), parameter :: cqm_f1056 = &
    '  You just murdered an innocent hunchback.'
  character(len=*), parameter :: cqm_f1057 = &
    '  There is nothing here to shoot.'
  character(len=*), parameter :: cqm_f1058 = &
    '  The bullet does not penetrate the bat''s thick hide.'
  character(len=*), parameter :: cqm_f1059 = &
    '  You swan dive off the tower and drown in the moat .'
  character(len=*), parameter :: cqm_f1060 = &
    '  You don''t have the sword with you.'
  character(len=*), parameter :: cqm_f1061 = &
    '  The acid dissolves the bars.  The window is clear.'
  character(len=*), parameter :: cqm_f1062 = &
    '  The werewolf howls in pain and runs away.'
  character(len=*), parameter :: cqm_f1063 = &
    '  The acid has burned a hole in the floor.'
  character(len=*), parameter :: cqm_f1064 = &
    '  It is now pitch dark. If you procede you may stumble and fall.'
  character(len=*), parameter :: cqm_f1065 = &
    '  Your bullet misses.'
  character(len=*), parameter :: cqm_f1066 = &
    '  A combination lock bars the door.'
  character(len=*), parameter :: cqm_f1067 = &
    '  I''m afraid you don''t have the key.'
  character(len=*), parameter :: cqm_f1068 = &
    '  Unless you''re a safecracker, I suggest you use the combination.'
  character(len=*), parameter :: cqm_f1069 = &
    '  The rope is anchored securely to the bed.'
  character(len=*), parameter :: cqm_f1070 = &
    '  The rope is dangling out the window.'
  character(len=*), parameter :: cqm_f1071 = &
    '  The rope slithers out the window and falls out of reach.'
  character(len=*), parameter :: cqm_f1072 = &
    '  The rope is already out the window.'
  character(len=*), parameter :: cqm_f1073 = &
    '  OK, from now on I will give only short descriptions.'
  character(len=*), parameter :: cqm_f1074 = &
    '  You have nothing to carry it in.'
  character(len=*), parameter :: cqm_f1075 = &
    '  You have nothing to light it with.'
  character(len=*), parameter :: cqm_f1076 = &
    '  I hate to tell you this, but I can''t swim.'
  character(len=*), parameter :: cqm_f1077 = &
    '  You fall in the dark and break your neck.'
  character(len=*), parameter :: cqm_f1078 = &
    '  You haven''t any water.'
  character(len=*), parameter :: cqm_f1079 = &
    '  You bring it to the height of ecstasy.'
  character(len=*), parameter :: cqm_f1080 = &
    '  Jump from where??'
  character(len=*), parameter :: cqm_f1081 = &
    '  Something you''re carrying won''t fit into the house.'
  character(len=*), parameter :: cqm_f1082 = &
    '  You dont have a boat to cross in.'
  character(len=*), parameter :: cqm_f1083h = &
    '  You''re lamp is getting dim.  Perhaps you'
  character(len=*), parameter :: cqm_f1083b = &
    '  should look for more fuel.'
  character(len=*), parameter :: cqm_f1084 = &
    '  You are not strong enough to break it.'
  character(len=*), parameter :: cqm_f1085 = &
    '  I took the liberty of filling the lamp with kerosene.'
  character(len=*), parameter :: cqm_f1086 = &
    '  A vampire can only be killed with a wooden stake.'
  character(len=*), parameter :: cqm_f1087h = &
    '  The vampire clutches at the stake and dies,'
  character(len=*), parameter :: cqm_f1087b = &
    '  leaving only a pile of dust.'
  character(len=*), parameter :: cqm_f1088 = &
    '  Your match has burnt out.'
  character(len=*), parameter :: cqm_f1089 = &
    '  You''d better hurry.  The sun is setting.'
  character(len=*), parameter :: cqm_f1090 = &
    '  The hunchback gobbles down the food and smiles at you.'
  character(len=*), parameter :: cqm_f1091 = &
    '  Werewolves eat only fresh meat.'
  character(len=*), parameter :: cqm_f1092 = &
    '  The butler is not allowed to eat.'
  character(len=*), parameter :: cqm_f1093 = &
    '  It''s not a good idea to take a hungry hunchback.'
  character(len=*), parameter :: cqm_f1094 = &
    '  Count Vladimir clamps his fangs on your neck!!'
  character(len=*), parameter :: cqm_f1095 = &
    '  Nothing happens.'
  character(len=*), parameter :: cqm_f1096 = &
    '  You already have it.'
  character(len=*), parameter :: cqm_f1097 = &
    '  You don''t have any food.'
  character(len=*), parameter :: cqm_f1098 = &
    '  A copy of Shakespeare''s "HAMLET" lies on the desk.'
  character(len=*), parameter :: cqm_f1099 = &
    '  The bottle is already full.'
  character(len=*), parameter :: cqm_f1100 = &
    '  A wall of fire bars the way to the NE.'
  character(len=*), parameter :: cqm_f1101 = &
    '  You can''t get through the fire.'
  character(len=*), parameter :: cqm_f1102 = &
    '  You don''t have any water.'
  character(len=*), parameter :: cqm_f1103 = &
    '  The fire smoulders and goes out.'
  character(len=*), parameter :: cqm_f1104 = &
    '  I''m sorry.  I don''t have a corkscrew.'
  character(len=*), parameter :: cqm_f1105h = &
    '  You plummet headlong into the crashing surf'
  character(len=*), parameter :: cqm_f1106b = &
    '  and are *SPLATTERED* on the rocks below.'
  character(len=*), parameter :: cqm_f1107 = &
    '  The gnome is very nimble and dodges out of your reach.'
  character(len=*), parameter :: cqm_f1108 = &
    '  You killed a dirty little gnome.'
  character(len=*), parameter :: cqm_f1109 = &
    '  You missed him, JERK!!'
  character(len=*), parameter :: cqm_f1110 = &
    '  There is a large opening in the ground.'
  character(len=*), parameter :: cqm_f1111 = &
    '  It is out of reach.'
  character(len=*), parameter :: cqm_f1112 = &
    '  The grappling hook and the rope are lying on the ground.'
  character(len=*), parameter :: cqm_f1113 = &
    '  INDE--URP--GESTION !!!'
  character(len=*), parameter :: cqm_f1114 = &
    '  THAT IS A PRIVILEGED INSTRUCTION.'
  character(len=*), parameter :: cqm_f1115 = &
    '  ROOM #---'
  character(len=*), parameter :: cqm_f1116 = &
    '  Some ice has melted, leaving a large hole.'
  character(len=*), parameter :: cqm_f1117 = &
    '  There is a cyclops-shaped hole in the door.'
  character(len=*), parameter :: cqm_f1118 = &
    '  There is a fairly large cyclops staring at you.'
  character(len=*), parameter :: cqm_f1119h = &
    '  The cyclops turns to you and says:'
  character(len=*), parameter :: cqm_f1119b = &
    '     "Hey buddy!.  Got a light??"'
  character(len=*), parameter :: cqm_f1120h = &
    '  The cyclops chokes from the rancid tobacco, and'
  character(len=*), parameter :: cqm_f1120b = &
    '  crashes through the door in search of water.'
  character(len=*), parameter :: cqm_f1121 = &
    '  The torch is burning noisily.'
  character(len=*), parameter :: cqm_f1122 = &
    '  The cyclops hurls you against the wall and chuckles quietly.'
  character(len=*), parameter :: cqm_f1123 = &
    '  The cyclops flings you across the room and laughs hysterically.'
  character(len=*), parameter :: cqm_f1124 = &
    '  Boy are you dumb!  A cyclops doesn''t eat food.'
  character(len=*), parameter :: cqm_f1125 = &
    '  The door is way too heavy for you to move it.'
  character(len=*), parameter :: cqm_f1126 = &
    '  The cyclops does not even feel the impact of the bullet.'
  character(len=*), parameter :: cqm_f1127 = &
    '  Some magical power will not let you pass.'
  character(len=*), parameter :: cqm_f1128 = &
    '  A powerful wizard blocks your way with his staff.'
  character(len=*), parameter :: cqm_f1129h = &
    '  The wizard''s eyes flare as he raises his staff.'
  character(len=*), parameter :: cqm_f1129b = &
    '  His awesome magic prevents you from attacking.'
  character(len=*), parameter :: cqm_f1130 = &
    '  A note materializes on the wall which reads:'
  character(len=*), parameter :: cqm_f1131 = &
    '  EMERGENCY EXIT--The mirror maze will lead you'
  character(len=*), parameter :: cqm_f1132 = &
    '  to the locked door.  The exit lies within.'
  character(len=*), parameter :: cqm_f1133 = &
    '  The wizard raises his staff. You are blinded by a '  &
    // 'sudden explosion of light.'
  character(len=*), parameter :: cqm_f1134h = &
    '  The walls of the cavern tremble as you unleash the'
  character(len=*), parameter :: cqm_f1134b = &
    '  terrible power contained in the sword.'
  character(len=*), parameter :: cqm_f1135h = &
    '  The wizard, sensing a stronger power than his own,'
  character(len=*), parameter :: cqm_f1135b = &
    '  flees in a blinding flash and a cloud of smoke.'
  character(len=*), parameter :: cqm_f1136 = &
    '  There is a passable hole in the door.'
  character(len=*), parameter :: cqm_f1137 = &
    '  The letter "H" appears for an instant on the wall.'
  character(len=*), parameter :: cqm_f1138 = &
    '  A mystical voice says "OH".'
  character(len=*), parameter :: cqm_f1139 = &
    '  The letter "N" forms out of mist.'
  character(len=*), parameter :: cqm_f1140 = &
    '  A large "K" emerges from the floor.'
  character(len=*), parameter :: cqm_f1141 = &
    '  Your axe is trembling slightly.'
  character(len=*), parameter :: cqm_f1142h = &
    '  The ceiling begins to vibrate and crumbles,'
  character(len=*), parameter :: cqm_f1142b = &
    '  crushing you under tons of concrete.'
  character(len=*), parameter :: cqm_f1143 = &
    '  The floor erupts violently, swallowing you in a sea of molten lava.'
  character(len=*), parameter :: cqm_f1144h = &
    '  The glacier begins to melt in a torrential flood,'
  character(len=*), parameter :: cqm_f1144b = &
    '  and swallows you in a sea of icy cold water.'
  character(len=*), parameter :: cqm_f1145 = &
    '  I think I hear footsteps behind you.'
  character(len=*), parameter :: cqm_f1146 = &
    '  What do you want with a heavy, dead butler?'

  ! Utility messages
  character(len=*), parameter :: cqm_f1201 = '  ?'

  ! Intro text (from show_help())

  !> Help prompt
  character(len=80), parameter :: cqm_askhelp =                         &
    '  Would you like more detailed instructions?'

  ! Help and instruction text (from `inst.dat`)

  !> Level 1 help, 1st para, head
  character(len=80), parameter :: cqm_help_1a_h =                       &
    '  You are in a remote castle somewhere in Eastern Europe.'
  !> Level 1 help, 1st para, body
  character(len=80), dimension(*), parameter :: cqm_help_1a_b = (/      &
    character(len=80) ::                                                &
    '  I will be your eyes and hands.  Direct me with words such',      &
    '  as "LOOK", "TAKE", or "DROP".  To move, enter compass points',   &
    '  (N,NE,E,SE,S,SW,W,NW), UP, or DOWN.  To get a list of what',     &
    '  you are carrying, say "INVENTORY".  To save the current game',   &
    '  so it can be finished later say "SAVE".  Say "RESTORE" as',      &
    '  your first command to finish a game that had been saved.' /)
  !> Level 1 help, 2nd para, head
  character(len=80), parameter :: cqm_help_1b_h =                       &
    '  The object of the game is to find the master of the castle'
  !> Level 1 help, 2nd para, body
  character(len=80), dimension(*), parameter :: cqm_help_1b_b = (/      &
    character(len=80) ::                                                &
    '  and kill him, while accumulating as many treasures as possible.',&
    '  You get maximum points for depositing the treasures in the',     &
    '  vault.  Notice that the descriptions of treasures have an',      &
    '  exclamation point.  Be wary, as many dangers await you in',      &
    '  in the castle.' /)

  !> Level 2 help, 1st para, head
  character(len=80), parameter :: cqm_help_2_h =                        &
    '  To supress the long room descriptions, type "BRIEF".  To'
  !> Level 2 help, 1st para, body
  character(len=80), dimension(*), parameter :: cqm_help_2_b = (/       &
    character(len=80) ::                                                &
    '  return to the long room descriptions, use the command "LONG".',  &
    '  "SCORE" will give you your present score in the game.  "HELP"',  &
    '  will give you a hint about an object in the room, but it will',  &
    '  cost you some points.  To end your explorations, say "QUIT".',   &
    '  Good luck. (you''ll need it).' /)

  !> Level 3 help, 1st para, head
  character(len=80), parameter :: cqm_help_3_h =                        &
    '  To aid you in your travels, you may ask for a hint by'
  !> Level 2 help, 1st para, body
  character(len=80), dimension(*), parameter :: cqm_help_3_b = (/       &
    character(len=80) ::                                                &
    '  saying "HINT object", where "object" is the item that you',      &
    '  need help with (e.g. "HELP CROSS").  Saying "HELP ROOM"',        &
    '  will give you some help concerning the room you''re in.' /)

  !> Ask hint, head
  character(len=80), parameter :: cqm_hint_1_h =                        &
    '  It will cost you five points.'
  !> Ask hint, body
  character(len=80), dimension(*), parameter :: cqm_hint_1_b = (/       &
    character(len=80) ::                                                &
    '  Do you still want the hint?.' /)

  !> No hint available
  character(len=80), parameter :: cqm_nohint =                          &
    '  Sorry, not available.'

  !> @brief All 29 game hints
  !!
  !! Taken from `hint.dat` / `HINT`
  character(len=80), dimension(29), parameter :: cqm_allhints = (/      &
    character(len=80) ::                                                &
    '  A werewolf can be killed with a silver bullet.',                 &
    '  A vampire can be killed with a wooden stake.',                   &
    '  A well fed hunchback is an asset in the castle.',                &
    '  The rope can be used to climb out the bedroom window.',          &
    '  The butler can write notes, if given the tools.',                &
    '  The bottle can hold liquids, such as blood or water.',           &
    '  The cross will immobilize the vampire.',                         &
    '  The acid will dissolve the bars over the window.',               &
    '  Water will extinguish a troublesome fire.',                      &
    '  The surgeon general has warned you about smoking.',              &
    '  The werewolf can be killed with a silver bullet.',               &
    '  You can get out the window by dissolving the bars.',             &
    '  The count can only be killed with a stake through the heart.',   &
    '  Try opening the book by shakespeare.',                           &
    '  There''s a surprise in the drawer.',                             &
    '  The butler can write notes if he has the proper tools.',         &
    '  Try breaking the mirror in the single mirror chamber.',          &
    '  Acid will dissolve the bars and clear the window.',              &
    '  The bat will leave after it has been fed.',                      &
    '  You need an axe or something to break the boards.',              &
    '  You''ll have to get out of the maze on your own.',               &
    '  I think there is something behind the ice.',                     &
    '  Try "smoking" him out.',                                         &
    '  Gnomes are a problem, but you can "give" them the axe.',         &
    '  The torch can generate a lot of heat (but not light).',          &
    '  The hook is useful for scaling steep walls.',                    &
    '  Try thinking of some way to get through the ice.',               &
    '  The wizard can be overcome by magic, not by force.',             &
    '  Your lamp can operate on fossil fuels.' /)

  !> Death announcement 1, head (7001)
  character(len=80), parameter :: cqm_death_1_h =                       &
    '  You leap through the open window and are'
  !> Death announcement 1, body (7002)
  character(len=80), dimension(*), parameter :: cqm_death_1_b = (/      &
    character(len=80) ::                                                &
    '  dashed into pieces on the rocks below.' /)

  !> Death announcement 2, head (7004)
    character(len=80), parameter :: cqm_death_2_h =                     &
    '  A thick black smoke fills the room, engulfing you in'
  !> Death announcement 2, body (7005)
  character(len=80), dimension(*), parameter :: cqm_death_2_b = (/      &
    character(len=80) ::                                                &
    '  lethal choking fumes (smoking is bad for your health).' /)

  !> Death announcement 3, head (7007)
  character(len=80), parameter :: cqm_death_3_h =                       &
    '  You leap into the pit and fall for hours.  You land '
  !> Death announcement 2, body (7008)
  character(len=80), dimension(*), parameter :: cqm_death_3_b = (/      &
    character(len=80) ::                                                &
    '  on some moist "undead" bodies and are eaten alive.' /)

  !> Death announcement 4, head (7010)
  character(len=80), parameter :: cqm_death_4_h =                       &
    '  My, My.  You seem to have bitten the dust.'
  !> Death announcement 4, body (7011-7012)
  character(len=80), dimension(*), parameter :: cqm_death_4_b = (/      &
    character(len=80) ::                                                &
    '  I can attempt to reincarnate you, but I''m',                     &
    '  not very good at it. Should I try?' /)

  !> Death announcement 5, head (7013)
  character(len=80), parameter :: cqm_death_5_h =                       &
    '  You fall weightlessly through a thick mist.'
  !> Death announcement 5, body (7014)
  character(len=80), dimension(*), parameter :: cqm_death_5_b = (/      &
    character(len=80) ::                                                &
    '  Your head is spinning as you emerge and find...', ' ', ' ' /)

  !> Death announcement 6, head (7015)
  character(len=80), parameter :: cqm_death_6_h =                       &
    '  You seem to have died again.  I can try and '
  !> Death announcement 6, body (7016-7017)
  character(len=80), dimension(*), parameter :: cqm_death_6_b = (/      &
    character(len=80) ::                                                &
    '  reincarnate you, but you''re taxing my patience.',               &
    '  Would you like me to try??' /)

  !> Death announcement 7, head (7018)
  character(len=80), parameter :: cqm_death_7_h =                       &
    '  You float aimlessly through a green mist which'
  !> Death announcement 7, body (7019-7020)
  character(len=80), dimension(*), parameter :: cqm_death_7_b = (/      &
    character(len=80) ::                                                &
    '  transcends time and space. You regain your ',                    &
    '  senses and realize that ...', ' ' /)

  !> Death announcement 8, head (7021)
  character(len=80), parameter :: cqm_death_8_h =                       &
    '  You did it again, didn''t you? I''m afraid that'
  !> Death announcement 8, body (7022-7023)
  character(len=80), dimension(*), parameter :: cqm_death_8_b = (/      &
    character(len=80) ::                                                &
    '  all the mist has evaporated.  I''m so sorry,',                   &
    '  but this means you are dead for good.'/)

  ! Long room descriptions
  ! Long descriptions from `long.dat`/`FORM` are in `realtor_init`

  !> Short descriptions, 1-30 are portable items, 31-60 show map and
  !! encounter status. Taken from `short.dat`/`FORM2`.
  character(len=80), dimension(*), parameter :: cq_form2 =              &
    (/ character(len=80) ::                                             &
    '  There is a can of kerosene here.',                               &
    '  There is a silver bullet here.',                                 &
    '  There is a blood stained hatchet here. ',                        &
    '  There is an ornate skeleton key here! ',                         &
    '  There is a small pool of blood here. ',                          &
    '  To one side lies a wooden stake. ',                              &
    '  There is a bottle of vintage champagne here!',                   &
    '  A nasty hunchback eyes you from a corner of the room. ',         &
    '  There is a long piece of rope lying on the floor. ',             &
    '  There is some "HORROR HOTEL" writing paper here. ',              &
    '  There is an old quill pen here. ',                               &
    '  There is an ivory-handled sword here!',                          &
    '  There is a rusty acetylene torch here.',                         &
    '  Off to the side is an old rowboat. ',                            &
    '  There are matches from the Vampire Diner here. ',                &
    '  There is a heavy steel grappling hook here.',                    &
    '  A gold statue is glistening in the light!',                      &
    '  An empty bottle is discarded nearby. ',                          &
    '  There is a silver cross nearby! ',                               &
    '  There is an old gun here. ',                                     &
    '  There is a small kerosene lamp here. ',                          &
    '  Somebody left some tasty food here. ',                           &
    '  There is a very large ruby here!',                               &
    '  Perched on the ground is a valuable jade figure!',               &
    '  There is a small flask of nitric acid here. ',                   &
    '  You are standing in a small puddle of water. ',                  &
    '  There is a fine cuban cigar here. ',                             &
    '  A sapphire sparkles on the ground nearby!',                      &
    '  There is lots of money here!',                                   &
    '  A delicate crystal swan lies off to one side!',                  &
    '  The butler is sound asleep.',                                    &
    '  The butler is motioning that he wants to write you a note.',     &
    '  The butler is holding out a note.',                              &
    '  The butler is sleeping once again.',                             &
    '  The butler is lying in a heap on the floor.',                    &
    '  The window is nailed shut.',                                     &
    '  The window is broken, but bars block your way.',                 &
    '  The window is now open.',                                        &
    '  The window is open.',                                            &
    '  Your sword is glowing dimly.',                                   &
    '  Your sword is glowing very brightly.',                           &
    '  Your sword is brilliantly lit.',                                 &
    '  A secret tunnel descends into darkness.',                        &
    '  A rope hangs from the window.',                                  &
    '  The werewolf snarls as he wards off your attack.',               &
    '  The butler has a black belt and repels your attack.',            &
    '  Attack the bat??  That''s gross.  I won''t do it.',              &
    '  The shutters are closed.',                                       &
    '  Kill what, fool?',                                               &
    '  The blade does not penetrate the bat''s thick hide.',            &
    '  Werewolves can only be killed with silver.',                     &
    '  A smiling hunchback is following you.',                          &
    '  You attack the hunchback and break his neck.',                   &
    '  The coffin is closed.',                                          &
    '  The Count is asleep in the coffin.',                             &
    '  The Count is frightened by the cross and cowers in the coffin.', &
    '  The Count sits up and prepares for breakfast-namely you!',       &
    '  The vampire is gone for good (Ding Dong, the Count is dead).',   &
    '  Something on the ground outside is glistening brightly.',        &
    '  The hunchback drives away the werewolf and dies in the struggle.' /)

contains

!> Function to count number of lines in `long` array. May be zero.
integer function roomdesc_nlast(this) result(nlast)
  implicit none
  !> Object reference
  class(roomdesc_t), intent(in) :: this
  continue

  if (allocated(this%long)) then
    nlast = size(this%long)
  else
    nlast = 0
  end if

  return
end function roomdesc_nlast

!> Writes room description using global ASA-capable output writer
subroutine roomdesc_show(this)
  use :: m_format, only: a
  implicit none
  ! Arguments
  !> Object reference
  class(roomdesc_t), intent(in) :: this

  ! Local variables
  integer :: nlast

  continue

  if (allocated(this%long)) then
    nlast = size(this%long)
  else
    nlast = 0
  end if

  if (nlast > 1) then
    call a%write_para(this%long(1), this%long(2:nlast))
  else if (nlast == 1) then
    call a%write_para(this%long(1))
  else
    ! Unallocated text array - no nothing
    ! Probably should log this as an error
  end if

  return
end subroutine roomdesc_show

!> @brief Set all long room descriptions.
!!
!! Long descriptions taken from `long.dat`/`FORM`
subroutine realtor_init(this)
  implicit none
  ! Arguments

  !> Object reference
  class(realtor_t), intent(inout) :: this

  continue

  !> Long description for room 1
  this%room(1) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a large, tarnished brass bed in an old, musty bedroom. ',&
    '  cobwebs hang from the ceiling.  A few rays of light filter through ', &
    '  the shutters.  There is a nightstand nearby with a single wooden ', &
    '  drawer.  The door west creaks in the breeze.  A macabre portrait ', &
    '  hangs to the left of an empty fireplace. ' /) )

  !> Long description for room 2
  this%room(2) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a dim corridor lit by gaslight.  Doors exit ',           &
    '  to the east and west.  A stairway leads down. ' /) )

  !> Long description for room 3
  this%room(3) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in the parlor, an old fashioned sitting room.  A display ', &
    '  case of dueling pistols hangs over the mantle. Stairs lead up to ', &
    '  a dimly lit corridor.  Open double doors lead west.  Two wide ',    &
    '  hallways lead north and south. ' /) )

  !> Long description for room 4
  this%room(4) = roomdesc_t( (/ character(len=80) ::                      &
    '  A cool wind blows up a stone stairway which descends ',             &
    '  down into a large stone room.  A note written in blood ',           &
    '  reads "VERY CLEVER OF YOU TO MAKE IT THIS FAR".',                   &
    '  The door leads east, back to the hall.' /) )

  !> Long description for room 5
  this%room(5) = roomdesc_t( (/ character(len=80) ::                      &
    '  This is the dining room.  A long table is set for 12 guests. ',     &
    '  A swinging door leads north, and an arched passage leads ',         &
    '  south.  Open double doors exit to the east. ' /) )

  !> Long description for room 6
  this%room(6) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are now in the kitchen.  Twelve Swanson''s frozen entrees rest ', &
    '  on the counter, below a microwave oven.  "THE BEGINNER''S GUIDE TO ', &
    '  COOKING" lies on a small table.  A swinging door exits south. ',    &
    '  Other doors lead east and north. ' /) )

  !> Long description for room 7
  this%room(7) = roomdesc_t( (/ character(len=80) ::                      &
    '  The door opens to a brick wall.  ---DEAD END--- ',                  &
    ' ',                                                                   &
    '  A note on the wall reads "L 8 R 31 L 59". ' /) )

  !> Long description for room 8
  this%room(8) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in the foyer.  An umbrella near the door is dripping on the', &
    '  thick pile carpet.  A black cape is draped neatly over the banister', &
    '  of a grand staircase leading up.  A magnificentl archway leads north.', &
    '  Corridors lead south and southeast, a small hallway heads west,',   &
    '  and a narrow stairway goes down.' /) )

  !> Long description for room 9
  this%room(9) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in the upstairs hallway, a long corridor with passages',    &
    '  to the north, east, and west.  Stairs lead up and down.' /) )

  !> Long description for room 10
  this%room(10) = roomdesc_t( (/ character(len=80) ::                      &
    '  This is the smoking room.  Several cans of tobacco line a shelf',   &
    '  above a small bookcase.  A large box of cigars lies on a table.',   &
    '  A Honeywell air purifier hums quietly beneath the window.  The',    &
    '  only exit is back north, the way you came in.' /) )

  !> Long description for room 11
  this%room(11) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in the workshop.  A myriad of tools clutter the workbench ', &
    '  and surrounding tables.  A thick layer of sawdust covers the floor. ', &
    '  Footprints in the sawdust indicate that you are not alone. ' /) )

  !> Long description for room 12
  this%room(12) = roomdesc_t( (/ character(len=80) ::                      &
    '  This is the garden.  Tomato plants are growing neatly in rows. ',   &
    '  A narrow path goes east and a wider one goes west.' /) )

  !> Long description for room 13
  this%room(13) = roomdesc_t( (/ character(len=80) ::                      &
    '  This is the library.  All four walls are lined with bookcases. ',   &
    '  The room is brightly lit, although there is no apparent source ',   &
    '  of light. ' /) )

  !> Long description for room 14
  this%room(14) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a vast room full of laboratory equipment.  Several ',    &
    '  experiments appear to be in progress, and the remnants of some ',   &
    '  that failed are strewn about the room. A giant door hewn out of ',  &
    '  granite leads south and an entrance leads west into darkness. ' /) )

  !> Long description for room 15
  this%room(15) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a small boudoir.  The pink walls reflect the',           &
    '  soft lighting with a warm glow.  The door exits east. ' /) )

  !> Long description for room 16
  this%room(16) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a dark stone E/W passage.' /) )

  !> Long description for room 17
  this%room(17) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a low, dark chamber.  A single mirror',                  &
    '  is set into the far wall.  The exit goes south.' /) )

  !> Long description for room 18
  this%room(18) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a dark E/W corridor.  A small but walkable',             &
    '  tunnel leads off to the north.' /) )

  !> Long description for room 19
  this%room(19) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in an extremely dark, unfinished room.  The only light',    &
    '  emanates from a narrow slit in the ceiling.' /) )

  !> Long description for room 20
  this%room(20) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a musty room that appears to be an entrance to ',        &
    '  an attic.  A small passage leads north and stairs descend',         &
    '  down behind you. ' /) )

  !> Long description for room 21
  this%room(21) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in an old attic filled with old-fashioned clothes, a pile ',&
    '  of newspapers and some antiques.  An entrance to a cedar closet ',  &
    '  is to the east and there is a door to a crawlspace to the west. ' /) )

  !> Long description for room 22
  this%room(22) = roomdesc_t( (/ character(len=80) ::                      &
    '  You''re in a cedar closet that smells of fresh cedar.  Racks of more', &
    '  old-fashioned clothes lie to your right and left. ' /) )

  !> Long description for room 23
  this%room(23) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are crawling along a low passage that leads east and west.' /) )

  !> Long description for room 24
  this%room(24) = roomdesc_t( (/ character(len=80) ::                      &
    '  This is the laundry room.  Tattered clothes are scattered about. ', &
    '  An old-fashioned washing machine sits rotting in the corner. ',     &
    '  On a shelf lies a box of Snowy Bleach and some Bounce fabric ',     &
    '  softener.  The exit leads west and stairs lead up.' /) )

  !> Long description for room 25
  this%room(25) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a storage room filled with empty cardboard boxes ',      &
    '  and some large crates filled with dirt. The door goes east. ' /) )

  !> Long description for room 26
  this%room(26) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are at the side of a dirt road that runs north and south.',     &
    '  Fresh tracks in the road seem to indicate that a horse-drawn',      &
    '  carriage has passed here recently.  A narrow path leads east.' /) )

  !> Long description for room 27
  this%room(27) = roomdesc_t( (/ character(len=80) ::                      &
    '  This is the mirror maze.  A myriad of mirrors reflect your',        &
    '  image in a dazzling array of light.  The reflections make it',      &
    '  impossible to discren a direction.' /) )

  !> Long description for room 28
  this%room(28) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a narrow "L" shaped corridor, which leads',              &
    '  west and south.' /) )

  !> Long description for room 29
  this%room(29) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are standing far below a large window.  Rows of',               &
    '  tall, exotic flowers form a wall around you.' /) )

  !> Long description for room 30
  this%room(30) = roomdesc_t( (/ character(len=80) ::                      &
    '  You''re at the end of a dirt road.  A woodland path',               &
    '  goes west.  The road leads north.' /) )

  !> Long description for room 31
  this%room(31) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a small pantry full of various foodstuffs.  To',         &
    '  your left is a beautiful china closet filled with dishes',          &
    '  pilfered from various restaurants.  On you right is an',            &
    '  open drawer full of plastic knives and forks.' /) )

  !> Long description for room 32
  this%room(32) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are at the bank of a wide moat which surrounds the',            &
    '  castle.  A small town can be seen far in the distance.',            &
    '  The road goes south.' /) )

  !> Long description for room 33
  this%room(33) = roomdesc_t( (/ character(len=80) ::                      &
    '  This is the armor room.  Suits of armor are lined up in',           &
    '  rows throughout the room.  Medieval instruments of war',            &
    '  are on display along each wall.  The door exits south.' /) )

  !> Long description for room 34
  this%room(34) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are at the proverbial fork in the road.  Paths',                &
    '  lead east, northwest, and southwest.' /) )

  !> Long description for room 35
  this%room(35) = roomdesc_t( (/ character(len=80) ::                      &
    '  This is the torture chamber.  A matched set of thumb-',             &
    '  screws hangs on the far wall.  A large rack occupies',              &
    '  the center of the room.  A skeleton hangs from its',                &
    '  thumbs above you, swaying gently.  An arch leads NE.' /) )

  !> Long description for room 36
  this%room(36) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a small room with a high ceiling.  A huge',              &
    '  granite slab rests in the middle of the room.  The',                &
    '  exits lead SE and NW.' /) )

  !> Long description for room 37
  this%room(37) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are at the bottom of a towering spiral stairway.',              &
    '  A low passage exits south.' /) )

  !> Long description for room 38
  this%room(38) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are at the top of the castle tower.  Far off in',               &
    '  the distance you can see a dirt road leading to the',               &
    '  moat.  Across the moat is a small village.  A small ',              &
    '  path winds west from the south end of the road.' /) )

  !> Long description for room 39
  this%room(39) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are standing below a small window.  A small',                   &
    '  path winds its way east.' /) )

  !> Long description for room 40
  this%room(40) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are on the far side of the moat.  You can see',                 &
    '  a full view of the castle here in all its deadly',                  &
    '  splendor.  A small town can be glimpsed far off in',                &
    '  the distance.  An old sign nailed to a tree reads:',                &
    '       "YOU CAN''T REACH THE VILLAGE FROM HERE!"' /) )

  !> Long description for room 41
  this%room(41) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a huge anteroom to an even larger, mysterious',          &
    '  chamber.  A chilling wind seems to blow at you from all',           &
    '  sides, and a deathlike, vapid black mist surrounds your',           &
    '  feet.  Hundreds of sinister looking bats cling to the',             &
    '  ceiling and eye you with a spine-tingling anticipatory',            &
    '  pleasure.  Two dark, foreboding passages exit to the east',         &
    '  and west, and a steep sloping corridor descends NW.' /) )

  !> Long description for room 42
  this%room(42) = roomdesc_t( (/ character(len=80) ::                      &
    '  You''re on a steep ledge above a seemingly bottomless pit.',        &
    '  Skeletal remains still cling to various rocks jutting out',         &
    '  of the pit.  Horrible, blood-curdling wails can be heard',          &
    '  from deep within the pit, obviously cries from the dead.',          &
    '  A steep corridor rises to the SE.' /) )

  !> Long description for room 43
  this%room(43) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in the chamber of the master of the castle,  ',             &
    '  Count Vladimir!  Pictures depicting scenes of tranquil',            &
    '  Transylvanian countrysides line the walls.  A huge',                &
    '  portrait of Vladimir''s brother, Count Dracula, hangs',             &
    '  upon the near wall.  In the center of the room is a',               &
    '  large, ominous, mahogany coffin.' /) )

  !> Long description for room 44
  this%room(44) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a perfectly square room carved out of solid',            &
    '  rock.  Stone steps lead up.  An arched passage exits',              &
    '  south.  Above the arch is carved the message:',                     &
    '  ',                                                                  &
    '       "ABANDON HOPE ALL YE WHO ENTER HERE".' /) )

  !> Long description for room 45
  this%room(45) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a long sloping N/S passage.  The darkness',              &
    '  seems to thicken around you as you walk.' /) )

  !> Long description for room 46
  this%room(46) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a narrow room which extends out of sight',               &
    '  to the east.  Sloping paths exit north and south.',                 &
    '  It is getting warmer here.' /) )

  !> Long description for room 47
  this%room(47) = roomdesc_t( (/ character(len=80) ::                      &
    '  This is the fire room.  The stone walls are gutted',                &
    '  from centuries of evil fires.  It is very hot here.',               &
    '  A low trail leads west and a smaller one leads NE.',                &
    '  A sloping trail goes north.' /) )

  !> Long description for room 48
  this%room(48) = roomdesc_t( (/ character(len=80) ::                      &
    '  You''re in a huge cavernous room carved out of a strange',          &
    '  glowing rock.  Small drops of water drip from various',             &
    '  limestone stalactites on the ceiling high above you.',              &
    '  A narrowing path heads west.  Another passage goes',                &
    '  north.  A large door with an awning goes south.' /) )

  !> Long description for room 49
  this%room(49) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in the blue room.  The entire room is a deep',              &
    '  shade of royal blue.  Exits go north, south and east.' /) )

  !> Long description for room 50
  this%room(50) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a narrow E/W passage.  A faint noise of ',               &
    '  rushing water can be heard.  A small crawl goes south.' /) )

  !> Long description for room 51
  this%room(51) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a twisty tunnel that goes NE and SW.  It',               &
    '  seems to be cooler here.' /) )

  !> Long description for room 52
  this%room(52) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are inside a small, low room with strange engravings',          &
    '  carved in all four walls.  Embedded in the rocky floor',            &
    '  is a large compass made of marble and mother-of-pearl.' /) )

  !> Long description for room 53
  this%room(53) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are outside a small room.  The path goes back',                 &
    '  east, from whence you came.' /) )

  !> Long description for room 54
  this%room(54) = roomdesc_t( (/ character(len=80) ::                      &
    '  The path narrows here and continues NE under a ',                   &
    '  carefully constructed heart-shaped arch.  A',                       &
    '  wider path leads west.  It is very quiet here.' /) )

  !> Long description for room 55
  this%room(55) = roomdesc_t( (/ character(len=80) ::                      &
    '  This is the honeymoon suite.  The entire room is',                  &
    '  finished in red.  In the center of the room lies a ',               &
    '  heart shaped bed.  To one side is a heart shaped',                  &
    '  bath.  A large mirror is mounted on the ceiling above',             &
    '  the bed.  The only exit is back the way you came.' /) )

  !> Long description for room 56
  this%room(56) = roomdesc_t( (/ character(len=80) ::                      &
    '  This room has absolutely nothing in it.  Your footsteps',           &
    '  echo hollowly as you walk the length of the room.  On',             &
    '  the bare floor is scrawled the word "POOF".' /) )

  !> Long description for room 57
  this%room(57) = roomdesc_t( (/ character(len=80) ::                      &
    '  You''re in a long and winding maze of passages.' /) )

  !> Long description for room 58
  this%room(58) = roomdesc_t( (/ character(len=80) ::                      &
    '  You''re in a winding maze of long passages.' /) )

  !> Long description for room 59
  this%room(59) = roomdesc_t( (/ character(len=80) ::                      &
    '  You''re in a maze of long and winding passages.' /) )

  !> Long description for room 60
  this%room(60) = roomdesc_t( (/ character(len=80) ::                      &
    '  You''re in a long and winding maze of passages.' /) )

  !> Long description for room 61
  this%room(61) = roomdesc_t( (/ character(len=80) ::                      &
    '  You''re in a short and winding maze of passages.' /) )

  !> Long description for room 62
  this%room(62) = roomdesc_t( (/ character(len=80) ::                      &
    '  You''re in a maze of short and winding passages.' /) )

  !> Long description for room 63
  this%room(63) = roomdesc_t( (/ character(len=80) ::                      &
    '  You''re in a maze of short and winding passages.' /) )

  !> Long description for room 64
  this%room(64) = roomdesc_t( (/ character(len=80) ::                      &
    '  DEAD END.' /) )

  !> Long description for room 65
  this%room(65) = roomdesc_t( (/ character(len=80) ::                      &
    '  This is the glacier room.  The walls are covered with',             &
    '  dazzling shapes of ice which reflect the light from your',          &
    '  lamp in a million colors.  In the far side of the room',            &
    '  are magnificent ice sculptures of animals unknown to',              &
    '  Mankind.  A faint "X" is scratched in the ice on one wall.',        &
    '  Icy passages exit SW and east.  A steep trail goes up.' /) )

  !> Long description for room 66
  this%room(66) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a long bending tunnel which leads north',                &
    '  and east.  The walls are damp here, and you can',                   &
    '  distinctly hear the sound of rushing water.' /) )

  !> Long description for room 67
  this%room(67) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are on a ledge overlooking the underground waterfall.',         &
    '  Torrents of water cascade over smoothly polished rocks',            &
    '  and crash loudly several hundred feet below.  The water',           &
    '  sprays a fine, refreshing mist throughout the cavern.',             &
    '  Exotic marine creatures dance below the surface of the',            &
    '  still water at the base of the waterfall.  Thousands of',           &
    '  pennies are visible beneath the water where previous ',             &
    '  explorers have wished for luck in escaping from the cave.',         &
    '  High above you a rainbow bridges the mist, displaying the',         &
    '  entire visible spectrum.  Stone steps lead down.' /) )

  !> Long description for room 68
  this%room(68) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are at the base of a magnificent underground waterfall.',       &
    '  A cool mist rising off the surface of the water almost obscures',   &
    '  a small island.  A tunnel goes west and stone steps lead up.' /) )

  !> Long description for room 69
  this%room(69) = roomdesc_t( (/ character(len=80) ::                      &
    '  This is the main lobby.  A gaudy crystal chandelier hangs',         &
    '  from the center of a tastelessly finished room.  The decor',        &
    '  seems to be from the depression, although junk like this',          &
    '  is timeless.  A passageway leads north and down.  To the',          &
    '  east is an open elevator.' /) )

  !> Long description for room 70
  this%room(70) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are inside an elevator.  The light panel indicates that',       &
    '  you are on the upper of two levels.  The "THIS CAR NEXT" ',         &
    '  sign is lit.' /) )

  !> Long description for room 71
  this%room(71) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are inside an elevator.  The light panel indicates that',       &
    '  you are on the lower of two levels.  The "THIS CAR NEXT"',          &
    '  sign is lit.' /) )

  !> Long description for room 72
  this%room(72) = roomdesc_t( (/ character(len=80) ::                      &
    '  This is the safe deposit vault, an immense room with polished',     &
    '  steel walls.  A closed circuit T.V. camera hums quietly above',     &
    '  you as it pans back and forth across the room.  To the east is',    &
    '  an open elevator.  Engraved on the far wall is the message:',       &
    '          "DEPOSIT TREASURES HERE FOR FULL CREDIT"' /) )

  !> Long description for room 73
  this%room(73) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a room full of old and decaying skeletons.  The',        &
    '  rotting forms of many werewolves can be identified among',          &
    '  the bodies.  Along the near wall is a desk with a crystal',         &
    '  ball and a nameplaque that says "OLD GYPSY WOMAN".  The',           &
    '  gypsy is nowhere to be seen.  A path leads east and a small',       &
    '  tunnel goes south.' /) )

  !> Long description for room 74
  this%room(74) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a small room which is the junction of four small',       &
    '  tunnels.  The tunnels lead north, south, east and (you',            &
    '  guessed it) west.' /) )

  !> Long description for room 75
  this%room(75) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in an immense cavern filled with tropical flora',           &
    '  and fauna, reminiscent of an Amazon rain forest.  Wild',            &
    '  parrots and toucans fly aimlessly overhead.  The sound',            &
    '  of rushing water is coming from the SE.  A small tunnel',           &
    '  goes north and a rocky trail leads west.' /) )

  !> Long description for room 76
  this%room(76) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are up on a ledge which affords a partial view of a',           &
    '  large underground waterfall.  Across the water you can',            &
    '  see a larger ledge which provides a better vantage point.',         &
    '  A climbable cliff goes down and NW.' /) )

  !> Long description for room 77
  this%room(77) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a room whose walls are covered with graffiti.',          &
    '  The messages range from "FRODO LIVES" to filthy remarks',           &
    '  concerning the parental lineage of werewolves.  A rocky',           &
    '  path goes south.  A low tunnel leads east.' /) )

  !> Long description for room 78
  this%room(78) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are at the east end of a long, damp earthen tunnel.  Above',    &
    '  you dangle rows of tomato plant roots arranged neatly in rows.',    &
    '  A steep but climbable trail plummets down.' /) )

  !> Long description for room 79
  this%room(79) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are at the west end of a long, damp earthen tunnel.  Water',    &
    '  drips down from the roof and seeps into the muddy floor.  The',     &
    '  tunnel continues east, and a drier passage leads west.' /) )

  !> Long description for room 80
  this%room(80) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in the remains of an old wine cellar, apparantly the',      &
    '  victim of a cave in.  Casks of once fine wine lie crushed in',      &
    '  the rubble.  A battered keg of GENESEE sits off in the corner.',    &
    '  The room smells like a Rathskellar band party.  A muddy path',      &
    '  goes east, and steps lead up to a door in the ceiling.' /) )

  !> Long description for room 81
  this%room(81) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are on a small island near a large waterfall.  The sound of ',  &
    '  crashing surf can be clearly heard, although you cannot quite',     &
    '  make out the form of the waterfall through the thick mist.  A',     &
    '  message traced out in the sand reads "GILLIGAN WAS HERE".  There',  &
    '  are pieces of a wreck (the S.S. MINNOW?) scattered about.' /) )

  !> Long description for room 82
  this%room(82) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in the north end of a large cavern that extends',           &
    '  far to the south.  A low crawl leads to the east.' /) )

  !> Long description for room 83
  this%room(83) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in the south end of a large cavern.  The',                  &
    '  cavern wall before you rises vertically, ending at a',              &
    '  ledge well out of your reach.  The cavern continues',               &
    '  north and out of sight.  A tiny trail heads east.' /) )

  !> Long description for room 84
  this%room(84) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are at the edge of a sheer vertical drop overlooking',          &
    '  an immense N/S cavern.  Narrow paths head away to the',             &
    '  east and west.' /) )

  !> Long description for room 85
  this%room(85) = roomdesc_t( (/ character(len=80) ::                      &
    '  This is the disco room.  Multicolored lasers pulsate',              &
    '  wildly to the beat of badly mixed music.  A stairway',              &
    '  down is barely visible through the glare.  A large',                &
    '  passage exits south, and a smaller one leads west.' /) )

  !> Long description for room 86
  this%room(86) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a tall tunnel leading east and west.  A small',          &
    '  trail goes SE.  An immense wooden door heads south.' /) )

  !> Long description for room 87
  this%room(87) = roomdesc_t( (/ character(len=80) ::                      &
    '  You have entered the land of the living dead, a',                   &
    '  large, desolate room.  Although it is apparently',                  &
    '  uninhabited, you can hear the awful sounds of',                     &
    '  thousands of lost souls weeping and moaning.',                      &
    '  In the east corner are stacked the remains of ',                    &
    '  dozens of previous adventurers who were less',                      &
    '  fortunate than yourself.  To the north is a ',                      &
    '  foreboding passage.  A path goes west.' /) )

  !> Long description for room 88
  this%room(88) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in the lair of the cyclops.  The broken bones of',          &
    '  previous explorers are strewn about the room.  One huge',           &
    '  contact lens lies on a high table.  The cyclops seems',             &
    '  to be gone.  A splintered door exits north.' /) )

  !> Long description for room 89
  this%room(89) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in an extremely cold chamber imbedded deep',                &
    '  within the glacier.  Stalagtites of ice hang from',                 &
    '  the ceiling far above you.  An ominous tunnel leaves',              &
    '  to the west.' /) )

  !> Long description for room 90
  this%room(90) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a long, bending tunnel.  With your lamp',                &
    '  you can barely make out exits to the south and east.' /) )

  !> Long description for room 91
  this%room(91) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a tremendous cavern divided by a white line',            &
    '  through its center.  The north side of the cavern is',              &
    '  green and fresh, a startling change from the callous',              &
    '  terrain of the cave.  A sign at the border proclaims ',             &
    '  this to be the edge of the wizard''s realm.  A rocky',              &
    '  and forlorn trail leads east, and a plush green path ',             &
    '  wanders north.' /) )

  !> Long description for room 92
  this%room(92) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in an immense forest of tall trees.  Melodic',              &
    '  chanting can be heard in the distance.  The trees seem',            &
    '  to be guiding you along a N/S path.' /) )

  !> Long description for room 93
  this%room(93) = roomdesc_t( (/ character(len=80) ::                      &
    '  This is the wizard''s throne room.  Scattered about',               &
    '  the room are various magical items.  A long message',               &
    '  in ancient runes is carved into the southern wall.  It',            &
    '  translates roughly as "Beware the power of the Wizard,',            &
    '  for he is master of this place".  Two green paths go',              &
    '  south and east, and a marble walk leads west.' /) )

  !> Long description for room 94
  this%room(94) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in the wizard''s cache, a large room whose walls',          &
    '  are inlaid with jewels.  A majestic marble walk leads to',          &
    '  the east.' /) )

  !> Long description for room 95
  this%room(95) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in a room of mammoth proportions which seems',              &
    '  to be some sort of warehouse.  On a nearby table are',              &
    '  several clipboards and a massive pile of order forms.',             &
    '  To your right is a large loading dock and a truck bay.',            &
    '  The room opens to the north, east and west.' /) )

  !> Long description for room 96
  this%room(96) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in the western end of the Castlequest warehouse.',          &
    '  To one side sits an empty kerosene drum.  Scattered',               &
    '  all over the floor are shreds of foam rubber used to',              &
    '  pack delicate crystal swans for shipping.  The empty',              &
    '  tins of countless T.V. dinners are discarded around',               &
    '  a full garbage can.' /) )

  !> Long description for room 97
  this%room(97) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in the north end of the Castlequest warehouse.',            &
    '  There is a large box of BIC CLICs to your left, all',               &
    '  empty (they don''t write "first time, every time").',               &
    '  On a shelf to your right is a huge supply of Burpee',               &
    '  tomato plant seeds.' /) )

  !> Long description for room 98
  this%room(98) = roomdesc_t( (/ character(len=80) ::                      &
    '  You are in the eastern end of the Castlequest warehouse.',          &
    '  In a box near the corner are the empty cartridges from',            &
    '  many silver bullets.  Through a small window you can spy',          &
    '  the Vampire Diner (talk about greasy spoons).  ' /) )

  !> Long description for room 99
  this%room(99) = roomdesc_t( (/ character(len=80) ::                    &
    '  The elevator has screeched to a halt between two floors.' /) )

  !> Long description for room 100
  this%room(100) = roomdesc_t( (/ character(len=80) ::                   &
    '  You feel the elevator jump as you are wisked up towards',           &
    '  ground level.  You emerge in the open air in the village',          &
    '  square amidst cheers from the local villagers.  Banners',           &
    '  proclaiming the death of count Vladimir hang from most',            &
    '  of the old buildings around the square.  The mayor ',               &
    '  presents you with a key to the city and makes your ',               &
    '  birthday a holiday.  You watch the sun rise as you ',               &
    '  bask in your newfound fame.' /) )

  return
end subroutine realtor_init

!> Describe room if it exists
subroutine realtor_show(this, room_id)
  implicit none
  ! Arguments
  !> Object reference
  class(realtor_t), intent(in) :: this

  !> Room index [1..100]
  integer, intent(in) :: room_id

  continue

  if (room_id > 0 .and. room_id < 100) then
    call this%room(room_id)%show()
  else
    ! Probably should complain that the rool index is out of range
  end if

  return
end subroutine realtor_show

end module cquest_text