!> @file m_action.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Contains player action classes and functions

!> @brief Contains player action classes and functions
module m_action
  implicit none

  private

  public :: action_t

  !> Interprets and manages player actions
  type :: action_t
  !   private
    !> Verb ID
    integer :: action = 0
    !> Noun ID
    integer :: object = 0
    !> True if verb detected
    logical :: has_verb = .false.
    !> True if noun detected
    logical :: has_noun = .false.
    !> Verb, processed to U12
    character(len=4) :: verb4 = 'VERB'
    !> Noun, processed to U12
    character(len=4) :: noun4 = 'NOUN'
    !> Raw input
    character(len=20) :: raw_cmd = ''
    !> Lightly processed command pair
    character(len=:), allocatable :: raw_pair
  contains
    !> Object initializer
    procedure :: init => action_init
    !> Reset working properties
    procedure :: reset => action_reset
    !> Obtain and parse player input
    procedure :: get_command => action_read_vn
    !> True if `has_noun` is true
    procedure :: has_object => action_has_object
    !> True if `has_noun` is false
    procedure :: no_object => action_no_object
    !> True if noun matches given object ID
    procedure :: object_is => action_object_is
    !> True if noun does not match given object ID
    procedure :: object_is_not => action_object_is_not
  end type action_t

contains

!> Initializes action object
subroutine action_init(this)
  implicit none
  !> Object reference
  class(action_t), intent(inout) :: this
  continue

  call this%reset()

  return
end subroutine action_init

!> Resets working attributes of action object
subroutine action_reset(this)
  implicit none
  !> Object reference
  class(action_t), intent(inout) :: this
  continue

  this%action   = 0
  this%object   = 0
  this%has_verb = .false.
  this%has_noun = .false.
  this%verb4    = '    '
  this%noun4    = '    '
  this%raw_cmd  = '                    '
  if (allocated(this%raw_pair)) then
    deallocate(this%raw_pair)
  end if

  return
end subroutine action_reset

!> @brief User input scanner and sentence verifier
!! Guaranteed to set `VERB` and `this%action` > 0 (valid verb);
!! `NOUN` may or may not be set.
!!
!! If `NOUN` is set `this%object` > 0 (valid noun) EXCEPT in the
!! case of `this%action` = V19_AHEM.
!!
!! If `NOUN` is not set, `this%object` = 0 - this condition must
!! be trapped downstream in cases where it is used as an
!! array (object) index. FORTRAN 66 tolerated an array index
!! out of bounds (0); modern Fortran does not and may segfault.
subroutine action_read_vn(this, log_mode)
  use, intrinsic :: iso_fortran_env, only: STDIN => INPUT_UNIT,         &
    STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  use :: m_object_id, only: V10_DOWN, V12_DROP, V19_AHEM
  use :: m_textio, only: ucase, munch, itoa
  use :: m_words, only: verb_list, noun_list
  use :: cquest_log, only: log_debug, log_warning

  !--- Parameters

  ! Length of processed noun or verb
  integer, parameter :: lwordmax = 4

  ! Single blank
  character(len=1),  parameter :: BLANK  = ' '

  ! Word-sized blank
  character(len=4), parameter :: BLANKS = '    '

  !--- Arguments

  !> Action object reference
  class(action_t), intent(inout) :: this

  !> True if diagnostic logging is enabled
  logical, intent(in), optional :: log_mode

  !--- Local variables

  logical :: dbg
  character(len=80) :: logmsg

  character(len=:), allocatable :: nuline
  integer :: lword
  integer :: lline

  integer :: j

  integer :: ierr
  character(len=80) :: errmsg

  character(len=80) :: trvnm

  !--- Formats

201  format('action_t%get_command: Found ', A, ' [', A, "]")
202  format('action_t%get_command: No ', A, ' found')
1001 format( A20 )
2001 format('  ', A, ' what???')
2002 format('  Do WHAT with the ', A, '??')
! 3001 format('0  Issue reading STDIN: (', I0, ') ', A)

  ! Confused response
  character(len=*), parameter :: HUH   = "  I didn't get that!! "

  continue

  dbg = present(log_mode)
  if (dbg) then
    dbg = log_mode
  end if

  call this%reset()

  ! Repeat until a valid sentence is found
L1: do
    ! Repeat until a verb is found
    this%has_verb = .false.
L5: do while (.not. this%has_verb)
      ! Set initial conditions
      call this%reset()

      ierr = 0
      errmsg = repeat(' ', 80)
      read(unit=STDIN, fmt=1001, iostat=ierr, iomsg=errmsg) this%raw_cmd

      if (ierr /= 0) then
        ! I/O error
        call a%write_para(HUH)
        cycle L5
      end if

      ! Trim leading and trailing spaces:
      ! nuline is either empty (length 0) or length >= 1,
      ! starting and ending with not-a-space
      nuline = munch(this%raw_cmd)
      lline = len(nuline)

      ! Handle empty line (no commands)
      if (lline == 0) then
        ! Blank line - complain and cycle to L5
        call a%write_para(HUH)
        cycle L5
      end if

      ! Convert to upper case
      call ucase(nuline)

      if (dbg) then
        call log_debug("action_t%get_command: Lightly processed input " &
          // "is [" // nuline // "]")
      end if

      ! nuline is now guaranteed to be length >= 1 (not empty)
      this%has_verb = .true.
      j = index(nuline, ' ')
      if (j > 1) then
        ! Set verb length to j - 1;
        ! Truncate the verb to first 12 (lwordmax) characters
        lword = min(j - 1, lwordmax)
        this%verb4(1:lword) = nuline(1:lword)
        ! Truncate full verb from nuline
        nuline = trim(adjustl(nuline(j:lline)))
        ! We know lline >= 1 because we found an embedded space
        lline = len(nuline)

        this%has_noun = .true.
        ! Note: we could inline the has_noun section below but this is
        ! nested as deeply as we might want
      else
        ! Set verb length to line length (lline);
        ! Truncate the verb to first 12 (lwordmax) characters
        lword = min(lline, lwordmax)
        this%verb4(1:lword) = nuline(1:lword)
        ! No embedded space (j == 0) implies no noun
        ! We are done parsing
        exit L5
      end if

      if (this%has_noun) then
        ! Look for another embedded space to find end of noun
        j = index(nuline, ' ')
        if (j > 1) then
          ! Found embedded space; there are more than 2 words
          ! Length of noun is (j - 1)
          ! Truncate the verb to first 12 (lwordmax) characters
          lword = min(j - 1, lwordmax)
        else
          ! No embedded space; nuline contains only one word - our noun
          ! Length of noun is lline
          ! Truncate the verb to first 12 (lwordmax) characters
          lword = min(lline, lwordmax)
        end if
        this%noun4(1:lword) = nuline(1:lword)
        exit L5
      end if

      ! We should only get here on I/O error
      if (.not. this%has_verb) then
        call a%write_para(HUH)
      end if

    end do L5

    ! Detect valid verbs; this%action > 0
    ! call log_debug("RV: Before verb scanning, action = " // itoa(this%action))
    this%action = verb_list%lookup(this%verb4)

    if (this%has_noun) then
      ! Detect valid nouns; this%object > 0
      ! call log_debug("RV: Before noun scanning, object = " // itoa(this%object))
      this%object = noun_list%lookup(this%noun4)
    end if

    ! ahem
    if (this%action == V19_AHEM) then
      exit L1
    end if

    ! Handle invalid sentences
    if (this%action /= 0) then
      ! VERB found
      if (this%has_noun .and. this%object == 0) then
        ! Expected NOUN not found
        write(unit=trvnm, fmt=2001) trim(this%verb4)
        call a%write_para('  ' // trim(trvnm))
      else
        ! Valid sentence detected
        exit L1
      end if
    else
      ! VERB not found
      if (this%has_noun .and. this%object /= 0) then
        ! Expected NOUN found
        write(unit=trvnm, fmt=2002) trim(this%noun4)
        call a%write_para('  ' // trim(trvnm))
      else
        ! Neither VERB or NOUN found
        call a%write_para(HUH)
      end if
    end if
    ! Anything reaching this point is an invalid sentence
    call log_debug("RV: Cannot understand [" // this%verb4 // " "     &
      // this%noun4 // "]")
  end do L1

  ! The meaning of the verb D changes with context:
  ! D <EMPTY> -> DOWN
  ! D <NOUN>  -> DROP <NOUN>
  if (this%action == V10_DOWN .and. this%has_noun) then
    this%action = V12_DROP
  end if

  if (dbg) then
    if (this%has_verb) then
      write(logmsg, fmt=201) 'verb', this%verb4
      call log_debug(logmsg)
    else
      write(logmsg, fmt=202) 'verb'
      call log_warning(logmsg)
    end if

    if (this%has_noun) then
      write(logmsg, fmt=201) 'noun', this%noun4
    else
      write(logmsg, fmt=202) 'noun'
    end if
    call log_debug(logmsg)
  end if

  return
end subroutine action_read_vn

!> True if `has_noun` is true
logical function action_has_object(this) result(has_object)
  implicit none
  !> Action object reference
  class(action_t), intent(in) :: this
  continue
  has_object = this%has_noun
  return
end function action_has_object

!> True if `has_noun` is false
logical function action_no_object(this) result(no_object)
  implicit none
  !> Action object reference
  class(action_t), intent(in) :: this
  continue
  no_object = .not. this%has_noun
  return
end function action_no_object

!> True if noun matches given object ID
logical function action_object_is(this, object_id) result(object_is)
  implicit none
  !> Action object reference
  class(action_t), intent(in) :: this
  !> Object ID to match
  integer, intent(in) :: object_id
  continue
  object_is = (this%object == object_id)
  return
end function action_object_is

!> True if noun does not match given object ID
logical function action_object_is_not(this, object_id)                  &
  result(object_is_not)
  implicit none
  !> Action object reference
  class(action_t), intent(in) :: this
  !> Object ID to match
  integer, intent(in) :: object_id
  continue
  object_is_not = .not. this%object_is(object_id)
  return
end function action_object_is_not

end module m_action
