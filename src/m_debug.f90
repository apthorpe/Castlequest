!> @file m_debug.f90
!! @author Michael S. Holtzman
!! @author Mark Kershenblatt
!! @copyright See LICENSE
!! @brief Contains diagnostic routines

!> @brief Contains diagnostic routines
module m_debug
  implicit none
  private

  public :: show_state
  public :: log_state
  public :: log_inventory

contains

!> Show internal game state and diagnostics
subroutine show_state(msg, player, map, revealed_letter,       &
  gameitems, gameencounters)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: m_encounter, only: gameencounters_t
  use :: m_items, only: gameitems_t
  use :: m_player, only: player_t
  use :: m_where, only: dungeon_t
  implicit none

  !> Short debug identifier
  character(len=*), intent(in) :: msg
  !> Player object
  type(player_t), intent(in) :: player
  !> Map object
  type(dungeon_t), intent(in) :: map
  !> Array showing state of revealed letters
  logical, dimension(4), intent(in) :: revealed_letter

  !> Bulk game items object
  type(gameitems_t), intent(in) :: gameitems

  !> Bulk live encounters object
  type(gameencounters_t), intent(in) :: gameencounters

  ! Local variables

  logical, dimension(18) :: LSAVAR
  integer, dimension(1:23) :: SAVAR

  ! Formats

50  format('  ', /, '   DBGINFO: ', A)
100 format(                                                             &
  '   GUN    = ', L1,                                                   &
  ', BAT    = ', L1,                                                    &
  ', FIRE   = ', L1,                                                    &
  ', PREC   = ', L1,                                                    &
  ', BLOOD  = ', L1,                                                    &
  ', BOTTLE = ', L1, /,                                                 &
  '   WATER  = ', L1,                                                   &
  ', GNOME  = ', L1,                                                    &
  ', WOLF   = ', L1,                                                    &
  ', HOLE   = ', L1,                                                    &
  ', MELT   = ', L1,                                                    &
  ', TORCH  = ', L1, /,                                                 &
  '   WIZ    = ', L1,                                                   &
  ', MASECT = ', L1,                                                    &
  ', MAS1   = ', L1,                                                    &
  ', MAS2   = ', L1,                                                    &
  ', MAS3   = ', L1,                                                    &
  ', MAS4   = ', L1)

200 format(                                                             &
  '   ROOM   = ', I3,                                                   &
  ', LROOM  = ', I3,                                                    &
  ', BUT    = ', I3,                                                    &
  ', SHUTTR = ', I3,                                                    &
  ', ROPE   = ', I3,                                                    &
  /, '   HUNCH  = ', I3,                                                &
  ', MATCH  = ', I3,                                                    &
  ', MASTER = ', I3,                                                    &
  ', NOTVAL = ', I3,                                                    &
  ', LOKVAL = ', I3,                                                    &
  /, '   BUTVAL = ', I3,                                                &
  ', ROPVAL = ', I3,                                                    &
  ', WIND1  = ', I3,                                                    &
  ', WIND2  = ', I3,                                                    &
  ', SCORE  = ', I3,                                                    &
  /, '   NUMOVE = ', I3,                                                &
  ', SUN    = ', I3,                                                    &
  ', NUMB   = ', I3,                                                    &
  ', NOTE   = ', I3,                                                    &
  ', IPASS  = ', I3,                                                    &
  /, '   LAMP   = ', I3,                                                &
  ', MMOVE  = ', I3,                                                    &
  ', LMOVE  = ', I3)

  continue

  write(unit=STDOUT, fmt=50) MSG

  LSAVAR = .false.
  LSAVAR(1)  = gameitems%s_gun%to_logical()
  LSAVAR(2)  = gameencounters%a_bat%to_logical()
  LSAVAR(3)  = map%fire_blocks_way
  LSAVAR(4)  = map%path_to_precipice
  LSAVAR(5)  = gameitems%s_bottle%contains_blood()
  LSAVAR(6)  = gameitems%s_bottle%is_full()
  LSAVAR(7)  = gameitems%s_bottle%contains_water()
  LSAVAR(8)  = gameencounters%a_gnome%to_logical()
  LSAVAR(9)  = gameencounters%a_werewolf%to_logical()
  LSAVAR(10) = gameencounters%a_cyclops%to_logical()
  LSAVAR(11) = map%path_through_glacier
  LSAVAR(12) = gameitems%s_torch%to_logical()
  LSAVAR(13) = gameencounters%a_wizard%to_logical()
  LSAVAR(14) = map%path_through_final_door
  LSAVAR(15:18) = revealed_letter
  write(unit=STDOUT, fmt=100) LSAVAR

  SAVAR = 0
  SAVAR( 1) = player%room
  SAVAR( 2) = player%last_room
  SAVAR( 3) = gameencounters%a_butler%get_state_()
  SAVAR( 4) = gameitems%s_shutter%get_state_()
  SAVAR( 5) = gameitems%s_rope%get_state_()
  SAVAR( 6) = gameencounters%a_hunchback%get_state_()
  SAVAR( 7) = gameitems%s_match%get_state_()
  SAVAR( 8) = gameencounters%a_master%get_state_()
  SAVAR( 9) = player%score%NOTVAL
  SAVAR(10) = player%score%LOKVAL
  SAVAR(11) = player%score%BUTVAL
  SAVAR(12) = player%score%ROPVAL
  SAVAR(13) = gameitems%s_window_1%get_state_()
  SAVAR(14) = gameitems%s_window_2%get_state_()
  SAVAR(15) = player%score%SCORE
  SAVAR(16) = player%moves
  SAVAR(17) = gameitems%s_sun%get_state_()
  SAVAR(18) = player%items_held
  SAVAR(19) = gameitems%s_note%get_state_()
  SAVAR(20) = gameitems%s_passage%get_state_()
  SAVAR(21) = gameitems%s_lamp%get_state_()
  SAVAR(22) = gameitems%s_match%turns_lit
  SAVAR(23) = gameitems%s_lamp%turns_lit

  write(unit=STDOUT, fmt=200) SAVAR

  write(unit=STDOUT, fmt=*)

  return
end subroutine show_state

!> Write internal game state and diagnostics to debug log
subroutine log_state(msg, player, map, revealed_letter,        &
  gameitems, gameencounters)
  use :: cquest_log, only: log_debug
  use :: m_encounter, only: gameencounters_t
  use :: m_items, only: gameitems_t
  use :: m_player, only: player_t
  use :: m_textio, only: ltoa, itoa
  use :: m_where, only: dungeon_t
  implicit none

  !> Short debug identifier
  character(len=*), intent(in) :: msg
  !> Player object
  type(player_t), intent(in) :: player
  !> Map object
  type(dungeon_t), intent(in) :: map
  !> Array showing state of revealed letters
  logical, dimension(4), intent(in) :: revealed_letter

  !> Bulk game items object
  type(gameitems_t), intent(in) :: gameitems

  !> Bulk live encounters object
  type(gameencounters_t), intent(in) :: gameencounters

  ! Local variables

  continue

  call log_debug('State at ' // adjustl(MSG))

  call log_debug(                                                       &
       ' GUN:' //    ltoa(gameitems%s_gun%to_logical(), '1', .true.)    &
    // ' TORCH:' //  ltoa(gameitems%s_torch%to_logical(), '1', .true.)  &
    // ' BOTTLE:' // ltoa(gameitems%s_bottle%is_full(), '1', .true.)    &
    // ' BLOOD:' //  ltoa(gameitems%s_bottle%contains_blood(), '1', .true.)    &
    // ' WATER:' //  ltoa(gameitems%s_bottle%contains_water(), '1', .true.))

  call log_debug(                                                       &
       ' GNOME:' //  ltoa(gameencounters%a_gnome%to_logical(), '1', .true.)    &
    // ' WOLF:' //   ltoa(gameencounters%a_werewolf%to_logical(), '1', .true.) &
    // ' BAT:' //    ltoa(gameencounters%a_bat%to_logical(), '1', .true.)      &
    // ' WIZ:' //    ltoa(gameencounters%a_wizard%to_logical(), '1', .true.))

  call log_debug(                                                       &
       ' FIRE:' //   ltoa(map%fire_blocks_way, '1', .true.)             &
    // ' PREC:' //   ltoa(map%path_to_precipice, '1', .true.)           &
    // ' HOLE:' //   ltoa(gameencounters%a_cyclops%to_logical(), '1', .true.)  &
    // ' MELT:' //   ltoa(map%path_through_glacier, '1', .true.))

  call log_debug(                                                       &
       ' MASECT:' // ltoa(map%path_through_final_door, '1', .true.)     &
    // ' MAS[1:4]:' // ltoa(revealed_letter(1), '1', .true.)            &
                    // ltoa(revealed_letter(2), '1', .true.)            &
                    // ltoa(revealed_letter(3), '1', .true.)            &
                    // ltoa(revealed_letter(4), '1', .true.))

  call log_debug(                                                       &
       ' ROOM:' //   itoa(player%room)                                  &
    // ' LROOM:' //  itoa(player%last_room)                             &
    // ' SCORE:' //  itoa(player%score%SCORE)                           &
    // ' NUMOVE:' // itoa(player%moves)                                 &
    // ' NUMB:' //   itoa(player%items_held))

  call log_debug(                                                       &
       ' SHUTTR:' // itoa(gameitems%s_shutter%get_state_())             &
    // ' WIND1:' //  itoa(gameitems%s_window_1%get_state_())            &
    // ' WIND2:' //  itoa(gameitems%s_window_2%get_state_())            &
    // ' SUN:' //    itoa(gameitems%s_sun%get_state_())                 &
    // ' ROPE:' //   itoa(gameitems%s_rope%get_state_())                &
    // ' NOTE:' //   itoa(gameitems%s_note%get_state_())                &
    // ' IPASS:' //  itoa(gameitems%s_passage%get_state_()))

  call log_debug(                                                       &
       ' BUT:' //    itoa(gameencounters%a_butler%get_state_())         &
    // ' HUNCH:' //  itoa(gameencounters%a_hunchback%get_state_())      &
    // ' MASTER:' // itoa(gameencounters%a_master%get_state_()))

  call log_debug(                                                       &
       ' LAMP:' //   itoa(gameitems%s_lamp%get_state_())                &
    // ' LMOVE:' //  itoa(gameitems%s_lamp%turns_lit)                   &
    // ' MATCH:' //  itoa(gameitems%s_match%get_state_())               &
    // ' MMOVE:' //  itoa(gameitems%s_match%turns_lit))

  call log_debug(                                                       &
       ' NOTVAL:' // itoa(player%score%NOTVAL)                          &
    // ' LOKVAL:' // itoa(player%score%LOKVAL)                          &
    // ' BUTVAL:' // itoa(player%score%BUTVAL)                          &
    // ' ROPVAL:' // itoa(player%score%ROPVAL))

  return
end subroutine log_state

!> Write player inventory to debug log
subroutine log_inventory(items)
  use :: cquest_log, only: log_debug
  use :: m_items, only: NITEMS, itemtracker_t
  use :: m_textio, only: munch, itoa
  implicit none

  ! Parameters

  ! Items printed per row
  integer, parameter :: IMAX = 3

  ! Arguments

  !> Portable item list
  type(itemtracker_t), intent(in) :: items

  ! Local variables
  integer :: i
  integer :: ict
  character(len=:), allocatable :: tmpmsg
  character(len=80) :: logmsg
  logical :: is_next

  continue

  ict = 0
  is_next = .false.
  tmpmsg = 'Carrying: '
  do i = 1, NITEMS
    if (items%item(i)%is_carried()) then
      ict = ict + 1
      if (is_next) then
        tmpmsg = tmpmsg // ', '
      else
        is_next = .true.
      end if
      tmpmsg = tmpmsg // '(' // itoa(i) // ') '                         &
        // munch(items%item(i)%name)
      if (mod(ict, IMAX) == 0) then
        logmsg = tmpmsg
        call log_debug(munch(logmsg))
        ict = 0
        is_next = .false.
        tmpmsg = 'Carrying : '
      end if
    end if
  end do

  if (ict > 0) then
    logmsg = tmpmsg
    call log_debug(munch(logmsg))
  end if

  return
end subroutine log_inventory

end module m_debug
