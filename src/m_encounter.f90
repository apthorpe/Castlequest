!> @file m_encounter.f90
!! @author Michael S. Holtzman
!! @author Mark Kershenblatt
!! @copyright See LICENSE
!! @brief Contains person/creature encounter classes and functions to
!! resolves combat
!--------------------------------------------------------------
!> @brief Contains person/creature encounter classes and functions to
!! resolves combat
!!
!! Routines migrated here from `ggnome.f90` and `wwolf,f90`
module m_encounter
  use :: m_stateful, only: stateful_t
  use :: m_object_id
  implicit none

  private

  public :: gnome_t
  public :: werewolf_t
  public :: hunchback_t
  public :: butler_t
  public :: bat_t
  public :: wizard_t
  public :: master_t
  public :: cyclops_t
  public :: gameencounters_t

  !> Properties of random encounter opponents
  type, extends(stateful_t) :: random_encounter_t
    ! !> Name
    ! character(len=16) :: name
    ! !> Being status
    ! logical :: attacking = .false.
    !> Probability of evading attacker on initial encounter, otherwise
    !! attacker pursues
    real :: p_evasion = 0.960
    !> Probability of attack after initial encounter
    real :: p_attack = 0.40
    !> Initial attacker to-kill probability
    real :: p_base_kill = 0.075
    !> Sustained attack bonus, awarded on every miss during pursuit
    real :: p_increment = 0.015
    !> Cumulative attacker to-kill probability
    real :: p_kill = 0.075
    !> Player to-kill probability
    real :: p_defeat = 0.8
  contains
    !> Reset attack state and cumulative to-kill probability
    procedure :: reset => attacker_reset
    !> True if random encounter is actively hostile
    procedure :: is_attacking => attacker_is_attacking
    !> True if random encounter is not actively hostile
    procedure :: is_calm => attacker_is_calm
    !> Set state to attacking (1)
    procedure :: attack => attacker_attack
    !> Set state to calm (0)
    procedure :: calm => attacker_calm
    !> Set state from logical value (attacking = 1 = true)
    procedure :: from_logical => attacker_from_logical
    !> Set logical value from state (attacking = 1 = true)
    procedure :: to_logical => attacker_is_attacking
  end type random_encounter_t
!    real :: p_defeat = 0.8
!    real :: p_defeat = 0.85

  !> Properties of gnome
  type, extends(random_encounter_t) :: gnome_t
  contains
    !> Set combat statistics and state of gnome
    procedure :: init => gnome_init
    !> Resolve fight with gnome
    procedure :: resolve_fight => gnome_resolve_fight
    !> Resolve edged-weapon fight with gnome
    procedure :: resolve_blade_fight => gnome_resolve_blade_fight
  end type gnome_t

  !> Properties of werewolf
  type, extends(random_encounter_t) :: werewolf_t
    !> Probability werewolf quietly withdraws (otherwise miss and run)
    real :: p_quiet_miss = 0.9
  contains
    !> Set combat statistics and state of werewolf
    procedure :: init => werewolf_init
    !> Resolve fight with werewolf
    procedure :: resolve_fight => werewolf_resolve_fight
  end type werewolf_t

  !> Properties of hunchback
  type, extends(stateful_t) :: hunchback_t
  contains
    !> Set initial state of hunchback
    procedure :: init => hunchback_init
    !> Logical function indicating hunchback is cranky and immobile with
    !! hunger
    procedure :: is_hungry => hunchback_is_hungry
    !> Logical function indicating hunchback will follow the player
    procedure :: will_follow => hunchback_will_follow
    !> Logical function indicating hunchback has left the game
    procedure :: is_gone => hunchback_is_gone
    !> Transition hunchback status from hungry
    procedure :: eat => hunchback_eat
    !> Transition hunchback status to gone
    procedure :: expire => hunchback_expire
  end type hunchback_t

  !> Properties of butler
  type, extends(stateful_t) :: butler_t
  contains
    !> Set initial state of butler
    procedure :: init => butler_init
    !> Logical function indicating butler is asleep
    procedure :: is_sleeping => butler_is_sleeping
    !> Logical function indicating butler is awake
    procedure :: is_awake => butler_is_awake
    !> Logical function indicating butler is holding a note
    procedure :: holding_note => butler_holding_note
    !> Logical function indicating butler has left the game
    procedure :: is_gone => butler_is_gone
    !> Logical function indicating butler has left the game by dying
    procedure :: is_dead => butler_is_dead
    !> Logical function indicating butler is aware, either awake or
    !! holding note
    procedure :: is_alert => butler_is_alert
    !> Logical function indicating butler cannot be awakened;
    !! either deep asleep or dead
    procedure :: cannot_wake => butler_cannot_wake
    !> Calculate DES index from butler state
    procedure :: des_index => butler_des_index
    !> Calculate FORM2 index from butler state
    procedure :: form2_index => butler_form2_index
    !> Transition butler status to asleep (0)
    procedure :: sleep => butler_sleep
    !> Transition butler status to awake (1)
    procedure :: wake => butler_wake
    !> Transition butler status to holding note (2)
    procedure :: write_note => butler_write_note
    !> Transition butler status to gone (3)
    procedure :: pass_out => butler_pass_out
    !> Transition butler status to dead (4)
    procedure :: expire => butler_expire
  end type butler_t

  !> Properties of bat
  type, extends(stateful_t) :: bat_t
  contains
    !> Set initial state of bat
    procedure :: init => bat_init
    !> Logical function indicating bat is cranky and immobile with
    !! hunger
    procedure :: is_hungry => bat_is_hungry
    !> Logical function indicating bat has left
    procedure :: is_gone => bat_is_gone
    !> Transition bat status from hungry
    procedure :: eat => bat_eat
    !> Set state from logical value (hungry = 1 = true)
    procedure :: from_logical => bat_from_logical
    !> Set logical value from state (hungry = 1 = true)
    procedure :: to_logical => bat_is_hungry
  end type bat_t

  !     WIZ    = WIZARD STILL AROUND?
  !> Properties of wizard
  type, extends(stateful_t) :: wizard_t
  contains
    !> Set initial state of wizard
    procedure :: init => wizard_init
    !> Logical function indicating wizard is cranky and is blocking
    !! your way
    procedure :: blocks_way => wizard_blocks_way
    !> Logical function indicating wizard has left
    procedure :: is_gone => wizard_is_gone
    !> Transition wizard status from blocking way to gone
    procedure :: flee => wizard_flee
    !> Set state from logical value (hungry = 1 = true)
    procedure :: from_logical => wizard_from_logical
    !> Set logical value from state (hungry = 1 = true)
    procedure :: to_logical => wizard_blocks_way
  end type wizard_t

  !     MASTER = (0=IN COFFIN,1=ASLEEP,2=PINNED,3=UP,4=DEAD)
  !> Properties of Master
  type, extends(stateful_t) :: master_t
  contains
    !> Set initial state of the Master
    procedure :: init => master_init
    !> Calculate DES index from state of the Master
    procedure :: des_index => master_des_index
    !> Calculate FORM2 index from butler state
    procedure :: form2_index => master_form2_index
    !> Logical function indicating the Master is in his coffin (state 0)
    procedure :: in_coffin => master_in_coffin
    !> Logical function indicating the Master is sleeping (state 1)
    procedure :: is_sleeping => master_is_sleeping
    !> Logical function indicating the Master is pinned (state 2)
    procedure :: is_pinned => master_is_pinned
    !> Logical function indicating the Master is up (state 3)
    procedure :: is_up => master_is_up
    !> Logical function indicating the Master is dead (state 4)
    procedure :: is_dead => master_is_dead
    !> Logical function indicating the Master is not dead (state < 4)
    procedure :: is_alive => master_is_alive
    !> Transition the Master status to asleep
    procedure :: sleep => master_sleep
    !> Transition the Master status to pinned
    procedure :: pin => master_pin
    !> Transition the Master status to up
    procedure :: rise => master_rise
    !> Transition the Master status to dead
    procedure :: expire => master_expire
  end type master_t

  !> Properties of cyclops
  type, extends(stateful_t) :: cyclops_t
  contains
    !> Set initial state of cyclops
    procedure :: init => cyclops_init
    !> Logical function indicating cyclops is cranky and is blocking
    !! your way
    procedure :: blocks_way => cyclops_blocks_way
    !> Logical function indicating cyclops has departed, leaving a hole
    !! in the door
    procedure :: left_hole => cyclops_left_hole
    !> Transition cyclops status from blocking way to gone
    procedure :: flee => cyclops_flee
    !> Set state from logical value (left_hole = 1 = true)
    procedure :: from_logical => cyclops_from_logical
    !> Set logical value from state (left_hole = 1 = true)
    procedure :: to_logical => cyclops_left_hole
  end type cyclops_t

  !> Group of live encounters
  type :: gameencounters_t
    !> Reference to state of gnome
    type(gnome_t) :: a_gnome
    !> Reference to state of werewolf
    type(werewolf_t) :: a_werewolf
    !> Reference to state of hunchback
    type(hunchback_t) :: a_hunchback
    !> Reference to state of butler
    type(butler_t) :: a_butler
    !> Reference to state of bat
    type(bat_t) :: a_bat
    !> Reference to state of wizard
    type(wizard_t) :: a_wizard
    !> Reference to state of master
    type(master_t) :: a_master
    !> Reference to state of cyclops
    type(cyclops_t) :: a_cyclops
  contains
    !> Bulk initialize referenced encounter objects
    procedure :: init_all => gameencounters_init_all
  end type gameencounters_t

  ! WIZ, BAT 0=GONE, 1=BLOCKS THE WAY
  ! GNOME, WOLF 0=GONE 1=ATTACKS
  ! HUNCH, 0=HUNGRY 1=FOLLOWING 2=GONE
  ! BUT, 0=SLEEPING,1=AWAKE,2=HOLDING NOTE,3=GONE FOR GOOD, 4=DEAD AS A DOORNAIL
  ! MASTER 0=IN COFFIN,1=ASLEEP,2=PINNED,3=UP,4=DEAD

contains

!> Reset attacking state and cumulative to-kill probability
subroutine attacker_reset(this)
  implicit none

  !> Attacker object reference
  class(random_encounter_t), intent(inout) :: this

  continue

  this%state = 0
  ! this%attacking = .false.
  this%p_kill = this%p_base_kill

  return
end subroutine attacker_reset

!> @brief True if random encounter is actively hostile
!!
!! Also aliased as `to_logical()`
logical function attacker_is_attacking(this) result(is_attacking)
  implicit none
  !> Attacker object reference
  class(random_encounter_t), intent(in) :: this
  continue
  is_attacking = (this%state >= 1)
  return
end function

!> True if random encounter is not actively hostile
logical function attacker_is_calm(this) result(is_calm)
  implicit none
  !> Attacker object reference
  class(random_encounter_t), intent(in) :: this
  continue
  is_calm = (this%state <= 0)
  return
end function

!> Set state to attacking (1)
subroutine attacker_attack(this)
  implicit none
  !> Attacker object reference
  class(random_encounter_t), intent(inout) :: this
  continue
  this%state = 1
  return
end subroutine

!> Set state to calm (0)
subroutine attacker_calm(this)
  implicit none
  !> Attacker object reference
  class(random_encounter_t), intent(inout) :: this
  continue
  this%state = 0
  return
end subroutine

!> Set state from logical value
subroutine attacker_from_logical(this, lval)
  implicit none
  !> Attacker object reference
  class(random_encounter_t), intent(inout) :: this
  !> Logical state
  logical, intent(in) :: lval
  continue
  if (lval) then
    call this%attack()
  else
    call this%calm()
  end if
  return
end subroutine attacker_from_logical

!> Set name and combat statistics for gnome
subroutine gnome_init(this)
  implicit none

  !> Gnome object reference
  class(gnome_t), intent(inout) :: this

  continue

  this%name         = "gnome           "
  this%id           = N77_GNOME
  ! this%state        = 0
  this%p_evasion    = 0.97
  this%p_attack     = 0.02
  this%p_base_kill  = 0.075
  this%p_increment  = 0.2
  this%p_defeat     = 0.80

  call this%reset()

  return
end subroutine gnome_init

!> @brief Resolve probability and consequences of gnome encounter
!!
!! Relies on persistent module variable `m_encounter::p_gnome_hit`
subroutine gnome_resolve_fight(this, AM_I_DEAD, debug_mode)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: m_format, only: a
  use :: m_random, only: RDM
  implicit none

  ! Parameters
  character(len=*), parameter :: cqm_g1001 =                            &
    '  There is an ugly little gnome in the room with you!'
  character(len=*), parameter :: cqm_g1002 =                            &
    '  IT GETS YOU!!'
  character(len=*), parameter :: cqm_g1003 =                            &
    '  It misses by an elf-hair!'
  character(len=*), parameter :: cqm_g1004 =                            &
    '  He shoots a poisoned dart at you!'

  ! Arguments

  !> Gnome state
  class(gnome_t), intent(inout) :: this

  !> Player state
  integer, intent(out) :: AM_I_DEAD

  !> Debugging output flag. Optional; defaults to false if not specified
  logical, intent(in), optional :: debug_mode

  ! Local variables

  real :: VAL
  logical :: DEBUG
  character(len=80) :: msg

  ! Formats

8001 format('  GNOME: ', L2,' XLIM: ', F5.3)

  continue

  if (present(debug_mode)) then
    DEBUG = debug_mode
  else
    DEBUG = .false.
  end if

  VAL = RDM()

FIGHT: do
    ! First appearance of gnome
    if (this%is_calm()) then
      ! p_gnome_hit = 0.075
      this%p_kill = this%p_base_kill
      ! if (VAL <= 0.970) then
      if (VAL <= this%p_evasion) then
        exit FIGHT
      else
        call this%attack()
        ! this%state = 1
      end if
    end if

    ! GNOME APPEARS!
    call a%write_para(cqm_g1001)
    ! if (VAL <= 0.980) then
    if (VAL <= (1.0 - this%p_attack)) then
      exit FIGHT
    end if

    ! GNOME ATTACKS!
    call a%write_para(cqm_g1004)

    VAL = RDM()
    ! if (VAL >= p_gnome_hit) then
    if (VAL >= this%p_kill) then
    ! GNOME MISSES!
      ! p_gnome_hit = p_gnome_hit + 0.20
      this%p_kill = this%p_kill + this%p_increment
      call a%write_para(cqm_g1003)
    else
      ! YOU DIE.
      AM_I_DEAD = 1
      call a%write_para(cqm_g1002)
    end if

    exit FIGHT
  end do FIGHT

  if (DEBUG) then
    write(unit=msg, fmt=8001) this%is_attacking(), this%p_kill
    call a%write_para(msg)
  end if

  return
end subroutine gnome_resolve_fight

!> @brief Resolve a particular kind of combat with the gnome involving
!! flying cutlery
!!
!! @note Replaces GOTO 770
subroutine gnome_resolve_blade_fight(this, IDEAD, player,      &
  portable, OBJECT, path_to_precipice)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text
  use :: m_format, only: a
  use :: m_items, only: NITEMS, itemtracker_t
  use :: m_object_id, only: O3_AXE, O12_SWORD
  use :: m_random, only: RDM
  use :: m_player, only: player_t
  implicit none

  ! Arguments

  !> Gnome state
  class(gnome_t), intent(inout) :: this

  !> Player death status (0 = OK, 1 = DEAD)
  integer, intent(inout) :: IDEAD

  !> Player object
  type(player_t), intent(inout) :: player

  !> Catalog of portable items
  type(itemtracker_t), intent(inout) :: portable

  !> Object ID
  integer, intent(in) :: OBJECT

  !> Precipice flag
  logical, intent(in) :: path_to_precipice

  !> Die roll
  real :: VAL

  ! Formats

! 1108 format('0  You killed a dirty little gnome.')
! 1109 format('0  You missed him, JERK!!')

  continue

  select case(OBJECT)
  case(O3_AXE)
    ! If object is axe
    ! The axe lands here
    call portable%item(O3_AXE)%place_in(player%room)
  case(O12_SWORD, N77_GNOME)
    ! If object is gnome or sword
    ! The sword lands here
    call portable%item(O12_SWORD)%place_in(player%room)
  end select
  call player%lose_item(1)

  IDEAD = 0
  VAL = RDM()
  ! if (VAL < 0.8) then
  if (VAL < this%p_defeat) then
    ! The gnome felt lucky but was just a punk
    ! write(unit=STDOUT, fmt=1108)
    call a%write_para(cqm_f1108)
    call this%resolve_fight(IDEAD)
    call this%calm()
    ! this%attacking = .false.
    ! this%state = 0
    call portable%display_here(player%score, player%room, path_to_precipice)
  else
    ! You missed him and now the gnome retaliates!
    ! write(unit=STDOUT, fmt=1109)
    call a%write_para(cqm_f1109)
    call this%resolve_fight(IDEAD)
  end if

  return
end subroutine gnome_resolve_blade_fight

!> Set name and combat statistics for werewolf
subroutine werewolf_init(this)
  implicit none

  !> Werewolf object reference
  class(werewolf_t), intent(inout) :: this

  continue

  this%name         = "werewolf        "
  this%id           = N76_WEREWOLF
  ! this%state        = 0
  this%p_evasion    = 0.96
  this%p_quiet_miss = 0.9
  this%p_attack     = 0.4
  this%p_base_kill  = 0.075
  this%p_increment  = 0.015
  this%p_defeat     = 0.85

  call this%reset()

  return
end subroutine werewolf_init

!> @brief Resolve probability and consequences of werewolf encounter
!!
!! Relies on persistent module variable `m_encounter::p_wwolf_hit`
subroutine werewolf_resolve_fight(this, AM_I_DEAD, debug_mode)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: m_format, only: a
  use :: m_random, only: RDM
  implicit none

  ! Parameters
  character(len=*), parameter :: cqm_w1001 =                            &
    '  There is a fearsome werewolf in the room with you!'
  character(len=*), parameter :: cqm_w1002 =                            &
    '  The werewolf attacks you with its sharp claw!'
  character(len=*), parameter :: cqm_w1003 =                            &
    '  It severs your jugular vein and mortally wounds you!'
  character(len=*), parameter :: cqm_w1004 =                            &
    '  It just misses your neck!'
  character(len=*), parameter :: cqm_w1005h =                            &
    '  A nasty werewolf lunges at you, takes a swipe at'
  character(len=*), parameter :: cqm_w1005b =                            &
    '  your neck, misses and runs away.'

  ! Arguments

  !> Werewolf state
  class(werewolf_t), intent(inout) :: this

  !> On entry: Aggravation (1 = player shot and missed).
  !! On exit: Player vitality (0 = alive, 1 = dead)
  integer, intent(inout) :: AM_I_DEAD

  !> True to enable diagnostic output. Optional. Assumed false if not
  !! specified.
  logical, intent(in), optional :: debug_mode

  ! Local variables

  real :: VAL
  logical :: DEBUG
  character(len=80) :: msg

  ! Formats

8001 format('  WOLF: ', L2, ' XLIM: ', F5.3)

  continue

  if (present(debug_mode)) then
    DEBUG = debug_mode
  else
    DEBUG = .false.
  end if

FIGHT: do
    VAL = RDM()
    if (AM_I_DEAD <= 0) then
      if (this%is_calm()) then
        ! p_wwolf_hit = 0.075
        this%p_kill = this%p_base_kill
        ! if (VAL <= 0.960) then
        if (VAL <= this%p_evasion) then

          AM_I_DEAD = 0
          ! SWIPE AND RUN AWAY.
          ! if (VAL >= .900) then
          if (VAL >= this%p_quiet_miss) then
            call a%write_para(cqm_w1005h, (/cqm_w1005b/))
          end if

          exit FIGHT
        else
          call this%attack()
          ! this%attacking = .true.
          ! this%state = 1
        end if
      end if

      ! WEREWOLF APPEARS
      call a%write_para(cqm_w1001)

      VAL = RDM()
      ! if (VAL > 0.400) then
      if (VAL > this%p_attack) then
        exit FIGHT
      end if
    end if

    call a%write_para(cqm_w1002)

    AM_I_DEAD = 0
    VAL = RDM()
    ! if (VAL <= p_wwolf_hit) then
    if (VAL <= this%p_kill) then
      ! GETS YOU.
      AM_I_DEAD = 1
      call a%write_para(cqm_w1003)
      exit FIGHT
    else
      ! HE MISSES.
      ! p_wwolf_hit = p_wwolf_hit + 0.15
      this%p_kill = this%p_kill + this%p_increment
      call a%write_para(cqm_w1004)
    end if
    exit FIGHT
  end do FIGHT

  if (DEBUG) then
    write(unit=msg, fmt=8001) this%is_attacking(), this%p_kill
    call a%write_para(msg)
  end if

  return
end subroutine werewolf_resolve_fight

!> Set initial state of hunchback
subroutine hunchback_init(this)
  implicit none
  ! Arguments

  !> Hunchback object reference
  class(hunchback_t), intent(inout) :: this

  continue
  this%name  = 'hunchback       '
  this%id    = 8
  this%state = 0
  return
end subroutine hunchback_init

!> Logical function indicating hunchback is cranky and immobile with
!! hunger
logical function hunchback_is_hungry(this) result(is_hungry)
  implicit none
  ! Arguments

  !> Hunchback object reference
  class(hunchback_t), intent(in) :: this

  continue

  is_hungry = (this%state <= 0)
  ! if (is_hungry) then
  !   this%state = 0
  ! end if

  return
end function hunchback_is_hungry

!> Logical function indicating hunchback will follow the player
logical function hunchback_will_follow(this) result(will_follow)
  implicit none
  ! Arguments

  !> Hunchback object reference
  class(hunchback_t), intent(inout) :: this

  continue
  will_follow = (this%state == 1)
  return
end function hunchback_will_follow

!> Logical function indicating hunchback has left the game
logical function hunchback_is_gone(this) result(is_gone)
  implicit none
  ! Arguments

  !> Hunchback object reference
  class(hunchback_t), intent(in) :: this

  continue

  is_gone = (this%state >= 2)
  ! if (is_gone) then
  !   this%state = 2
  ! end if

  return
end function hunchback_is_gone

!> Transition hunchback status from hungry
subroutine hunchback_eat(this)
  implicit none
  ! Arguments

  !> Hunchback object reference
  class(hunchback_t), intent(inout) :: this

  continue
  this%state = 1
  return
end subroutine hunchback_eat

!> Transition hunchback status to gone
subroutine hunchback_expire(this)
  implicit none
  ! Arguments

  !> Hunchback object reference
  class(hunchback_t), intent(inout) :: this

  continue
  this%state = 2
  return
end subroutine hunchback_expire

!> Set initial state of butler
subroutine butler_init(this)
  implicit none
  ! Arguments

  !> Butler object reference
  class(butler_t), intent(inout) :: this

  continue
  this%name  = 'butler          '
  this%id    = N49_BUTLER
  this%state = 0
  return
end subroutine butler_init

!> Logical function indicating butler is asleep
logical function butler_is_sleeping(this) result(is_sleeping)
  implicit none
  ! Arguments

  !> Butler object reference
  class(butler_t), intent(in) :: this

  continue

  is_sleeping = (this%state <= 0)

  return
end function butler_is_sleeping

!> Logical function indicating butler is awake
logical function butler_is_awake(this) result(is_awake)
  implicit none
  ! Arguments

  !> Butler object reference
  class(butler_t), intent(in) :: this

  continue

  is_awake = (this%state == 1)

  return
end function butler_is_awake

!> Logical function indicating butler is holding a note
logical function butler_holding_note(this) result(holding_note)
  implicit none
  ! Arguments

  !> Butler object reference
  class(butler_t), intent(in) :: this

  continue

  holding_note = (this%state == 2)

  return
end function butler_holding_note

!> Logical function indicating butler has left the game
logical function butler_is_gone(this) result(is_gone)
  implicit none
  ! Arguments

  !> Butler object reference
  class(butler_t), intent(in) :: this

  continue

  is_gone = (this%state == 3)

  return
end function butler_is_gone

!> Logical function indicating butler has left the game by dying
logical function butler_is_dead(this) result(is_dead)
  implicit none
  ! Arguments

  !> Butler object reference
  class(butler_t), intent(in) :: this

  continue

  is_dead = (this%state >= 4)

  return
end function butler_is_dead

!> Logical function indicating butler is aware, either awake or holding
!! note
logical function butler_is_alert(this) result(is_alert)
  implicit none
  ! Arguments

  !> Butler object reference
  class(butler_t), intent(in) :: this

  continue

  is_alert = (this%is_awake() .or. this%holding_note())

  return
end function butler_is_alert

!> Logical function indicating butler cannot be awakened, either gone
!! or dead
logical function butler_cannot_wake(this) result(cannot_wake)
  implicit none
  ! Arguments

  !> Butler object reference
  class(butler_t), intent(in) :: this

  continue

  cannot_wake = (this%is_gone() .or. this%is_dead())

  return
end function butler_cannot_wake

!> @brief Function returning butler description index.
!!
!! @note Value is coerced to lie between 400 and 404; may mask errors
!! in butler_t%state.
integer function butler_des_index(this) result(II)
  implicit none
  ! Arguments

  !> Butler object reference
  class(butler_t), intent(in) :: this

  continue

  II = 400 + min(max(this%state, 0), 4)

  return
end function butler_des_index

!> @brief Function returning butler description index into `FORM2()/cq_form2()`.
!!
!! @note Value is coerced to lie between 31 to 35; may mask errors
!! in butler_t%state.
integer function butler_form2_index(this) result(II)
  implicit none
  ! Arguments

  !> Butler object reference
  class(butler_t), intent(in) :: this

  continue

  II = 31 + min(max(this%state, 0), 4)

  return
end function butler_form2_index

!> Change butler status to asleep (0)
subroutine butler_sleep(this)
  implicit none
  ! Arguments

  !> Butler object reference
  class(butler_t), intent(inout) :: this

  continue
  this%state = 0
  return
end subroutine butler_sleep

!> Change butler status to awake (1)
subroutine butler_wake(this)
  implicit none
  ! Arguments

  !> Butler object reference
  class(butler_t), intent(inout) :: this

  continue
  this%state = 1
  return
end subroutine butler_wake

!> Change butler status to holding note (2)
subroutine butler_write_note(this)
  implicit none
  ! Arguments

  !> Butler object reference
  class(butler_t), intent(inout) :: this

  continue
  this%state = 2
  return
end subroutine butler_write_note

!> Change butler status to gone
subroutine butler_pass_out(this)
  implicit none
  ! Arguments

  !> Butler object reference
  class(butler_t), intent(inout) :: this

  continue
  this%state = 3
  return
end subroutine butler_pass_out

!> Change butler status to dead
subroutine butler_expire(this)
  implicit none
  ! Arguments

  !> Butler object reference
  class(butler_t), intent(inout) :: this

  continue
  this%state = 4
  return
end subroutine butler_expire

!> Set initial state of bat
subroutine bat_init(this)
  implicit none
  ! Arguments

  !> Bat object reference
  class(bat_t), intent(inout) :: this

  continue
  this%name  = 'bat             '
  this%id    = N55_BAT
  this%state = 0
  return
end subroutine bat_init

!> @brief Logical function indicating bat is cranky and immobile with
!! hunger
!!
!! Also aliased as `to_logical()`
logical function bat_is_hungry(this) result(is_hungry)
  implicit none
  ! Arguments

  !> Bat object reference
  class(bat_t), intent(in) :: this

  continue

  is_hungry = (this%state <= 0)

  return
end function bat_is_hungry

!> Logical function indicating bat has left the game
logical function bat_is_gone(this) result(is_gone)
  implicit none
  ! Arguments

  !> Bat object reference
  class(bat_t), intent(in) :: this

  continue

  is_gone = (this%state >= 1)

  return
end function bat_is_gone

!> Transition bat status from hungry to gone
subroutine bat_eat(this)
  implicit none
  ! Arguments

  !> Bat object reference
  class(bat_t), intent(inout) :: this

  continue
  this%state = 1
  return
end subroutine bat_eat

!> Set bat state from logical value
subroutine bat_from_logical(this, lval)
  implicit none
  !> Bat object reference
  class(bat_t), intent(inout) :: this
  !> Logical state
  logical, intent(in) :: lval
  continue
  if (lval) then
    call this%init()
  else
    call this%eat()
  end if
  return
end subroutine bat_from_logical

!> Set initial state of wizard
subroutine wizard_init(this)
  implicit none
  ! Arguments

  !> Wizard object reference
  class(wizard_t), intent(inout) :: this

  continue
  this%name  = 'wizard          '
  this%id    = N80_WIZARD
  this%state = 0
  return
end subroutine wizard_init

!> @brief Logical function indicating wizard is blocking your way
!!
!! Also aliased as `to_logical()`
logical function wizard_blocks_way(this) result(blocks_way)
  implicit none
  ! Arguments

  !> Wizard object reference
  class(wizard_t), intent(in) :: this

  continue

  blocks_way = (this%state <= 0)

  return
end function wizard_blocks_way

!> Logical function indicating wizard has fled in terror
logical function wizard_is_gone(this) result(is_gone)
  implicit none
  ! Arguments

  !> Wizard object reference
  class(wizard_t), intent(in) :: this

  continue

  is_gone = (this%state >= 1)

  return
end function wizard_is_gone

!> @brief Transition wizard status from blocking way to gone.
!!
!! He runs away like a little baby.
subroutine wizard_flee(this)
  implicit none
  ! Arguments

  !> Wizard object reference
  class(wizard_t), intent(inout) :: this

  continue
  this%state = 1
  return
end subroutine wizard_flee

!> Set wizard state from logical value
subroutine wizard_from_logical(this, lval)
  implicit none
  !> Wizard object reference
  class(wizard_t), intent(inout) :: this
  !> Logical state
  logical, intent(in) :: lval
  continue
  if (lval) then
    call this%init()
  else
    call this%flee()
  end if
  return
end subroutine wizard_from_logical

!> Set initial state of the Master
subroutine master_init(this)
  implicit none
  ! Arguments

  !> Master object reference
  class(master_t), intent(inout) :: this

  continue
  this%name  = 'master          '
  this%id    = N39_MASTER
  this%state = 0
  return
end subroutine master_init

!> @brief Function returning master description index.
!!
!! @note Value is coerced to lie between 400 and 404; may mask errors
!! in master_t%state.
integer function master_des_index(this) result(II)
  implicit none
  ! Arguments

  !> Master object reference
  class(master_t), intent(in) :: this

  continue

  II = 423 + min(max(this%state, 0), 4)

  return
end function master_des_index

!> @brief Function returning master description index into `FORM2()/cq_form2()`.
!!
!! @note Value is coerced to lie between 54 and 58; may mask errors
!! in master_t%state.
integer function master_form2_index(this) result(II)
  implicit none
  ! Arguments

  !> Master object reference
  class(master_t), intent(in) :: this

  continue

  II = 54 + min(max(this%state, 0), 4)

  return
end function master_form2_index

!> Logical function indicating the Master is in his coffin
logical function master_in_coffin(this) result(in_coffin)
  implicit none
  ! Arguments

  !> master object reference
  class(master_t), intent(in) :: this

  continue

  in_coffin = (this%state <= 0)

  return
end function master_in_coffin

!> Logical function indicating the Master is sleeping
logical function master_is_sleeping(this) result(is_sleeping)
  implicit none
  ! Arguments

  !> master object reference
  class(master_t), intent(in) :: this

  continue

  is_sleeping = (this%state == 1)

  return
end function master_is_sleeping

!> Logical function indicating the Master is pinned
logical function master_is_pinned(this) result(is_pinned)
  implicit none
  ! Arguments

  !> master object reference
  class(master_t), intent(in) :: this

  continue

  is_pinned = (this%state == 2)

  return
end function master_is_pinned

!> Logical function indicating the Master is up
logical function master_is_up(this) result(is_up)
  implicit none
  ! Arguments

  !> master object reference
  class(master_t), intent(in) :: this

  continue

  is_up = (this%state == 3)

  return
end function master_is_up

!> Logical function indicating the Master is dead
logical function master_is_dead(this) result(is_dead)
  implicit none
  ! Arguments

  !> master object reference
  class(master_t), intent(in) :: this

  continue

  is_dead = (this%state >= 4)

  return
end function master_is_dead

!> Logical function indicating the Master is not dead
logical function master_is_alive(this) result(is_alive)
  implicit none
  ! Arguments

  !> master object reference
  class(master_t), intent(in) :: this

  continue

  is_alive = (.not. this%is_dead())

  return
end function master_is_alive

!> Put the Master to sleep
subroutine master_sleep(this)
  implicit none
  ! Arguments

  !> Master object reference
  class(master_t), intent(inout) :: this

  continue
  this%state = 1
  return
end subroutine master_sleep

!> Pin the Master
subroutine master_pin(this)
  implicit none
  ! Arguments

  !> Master object reference
  class(master_t), intent(inout) :: this

  continue
  this%state = 2
  return
end subroutine master_pin

!> The Master is up
subroutine master_rise(this)
  implicit none
  ! Arguments

  !> Master object reference
  class(master_t), intent(inout) :: this

  continue
  this%state = 3
  return
end subroutine master_rise

!> Kill the Master
subroutine master_expire(this)
  implicit none
  ! Arguments

  !> Master object reference
  class(master_t), intent(inout) :: this

  continue
  this%state = 4
  return
end subroutine master_expire

!> Set initial state of cyclops
subroutine cyclops_init(this)
  implicit none
  ! Arguments
  !> Cyclops object reference
  class(cyclops_t), intent(inout) :: this
  continue
  this%name  = 'cyclops         '
  this%id    = N78_CYCLOPS
  this%state = 0
  return
end subroutine cyclops_init

!> Logical function indicating cyclops is cranky and is blocking
!! your way
logical function cyclops_blocks_way(this) result(blocks_way)
  implicit none
  ! Arguments
  !> Cyclops object reference
  class(cyclops_t), intent(in) :: this
  continue
  blocks_way = (this%state <= 0)
  return
end function cyclops_blocks_way

!> @brief Logical function indicating cyclops has departed, leaving a hole
!! in the door
!!
!! Also aliased as `to_logical()`
logical function cyclops_left_hole(this) result(is_gone)
  implicit none
  ! Arguments
  !> Cyclops object reference
  class(cyclops_t), intent(in) :: this
  continue
  is_gone = (this%state >= 1)
  return
end function cyclops_left_hole

!> Make cyclops retreat (through closed door)
subroutine cyclops_flee(this)
  implicit none
  ! Arguments
  !> Cyclops object reference
  class(cyclops_t), intent(inout) :: this
  continue
  this%state = 1
  return
end subroutine cyclops_flee

!> Set cyclops state from logical value (HOLE)
subroutine cyclops_from_logical(this, lval)
  implicit none
  !> Cyclops object reference
  class(cyclops_t), intent(inout) :: this
  !> Logical state
  logical, intent(in) :: lval
  continue
  if (lval) then
    call this%flee()
  else
    call this%init()
  end if
  return
end subroutine cyclops_from_logical

!> Bulk initialize referenced objects
subroutine gameencounters_init_all(this)
  use m_object_id
  implicit none
  ! Arguments
  !> Bulk game items object
  class(gameencounters_t), intent(inout) :: this
  continue

  call this%a_gnome%init()
  call this%a_werewolf%init()
  call this%a_hunchback%init()
  call this%a_butler%init()
  call this%a_bat%init()
  call this%a_wizard%init()
  call this%a_master%init()
  call this%a_cyclops%init()

  return
end subroutine gameencounters_init_all

end module m_encounter
