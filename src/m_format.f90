!> @file m_format.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Common format strings (edit specifiers) and ASA character control manager

!> @brief Common format strings (edit specifiers) and ASA character control manager
module m_format
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  implicit none

  private

  public :: asa_t

  !> General 80-column text outout format
  character(len=*), parameter, public :: fmt_a80 = '(A80)'

  !> General text outout format
  character(len=*), parameter, public :: fmt_a = '(A)'

  !*** ASA formats

  !> ASA triple-space text
  character(len=*), parameter, public :: afmt_3s_a = "('-', 2X, A)"

  !> ASA double-space text
  character(len=*), parameter, public :: afmt_2s_a = "('0', 2X, A)"

  !> General text output format with ASA leader
  character(len=*), parameter, public :: afmt_a = '(3X, A)'

  !*** Plain formats

  !> Plain triple-space game message
  character(len=*), parameter, public :: cfmt_3s_a = "(//, 2X, A)"

  !> Plain double-space game message
  character(len=*), parameter, public :: cfmt_2s_a = "(/, 2X, A)"

  !> General game message output format (2 leading spaces)
  character(len=*), parameter, public :: cfmt_a = '(2X, A)'

  !> @class asa_t
  !! ASA output prefixer
  type :: asa_t
    !> True if ASA output is enabled
    logical :: use_asa = .true.
    !> Output file handle; default is OUTPUT_UNIT
    integer :: ounit = STDOUT
    ! logical :: use_asa
  contains
    !> Set initial value of ASA codes
    procedure :: init => asa_init
    !> ASA single space prefix (' ') when ASA is enabled; empty otherwise
    procedure :: ss => asa_ss
    !> Return ASA double space prefix ('0') when ASA is enabled; empty otherwise
    procedure :: ds => asa_ds
    !> Return ASA triple space prefix ('-') when ASA is enabled; empty otherwise
    procedure :: ts => asa_ts
    !> Output lines as a paragraph
    procedure :: write_para => asa_write_paragraph
    !> Output single-space line
    procedure :: write_line => asa_write_line
  end type asa_t

  !> Optional ASA character control object
  type(asa_t), public :: a = asa_t(.true.)

  !!! RESET DEFAULT AFTER ASA OBJECT IS ENCORPORATED
!  type(asa_t), public :: a = asa_t(.false., '', '')

contains

!> Set initial value of ASA codes
subroutine asa_init(this, asa_on, ounit)
  implicit none

  !> Object reference
  class(asa_t), intent(inout) :: this

  !> Optional flag to enable or disable ASA format codes
  logical, intent(in), optional :: asa_on

  !> Optional output file handle
  integer, intent(in), optional :: ounit

  continue

  if (present(asa_on)) then
    this%use_asa = asa_on
  else
    ! Defaults to ASA enabled. Change this later
    this%use_asa = .true.
  end if

  if (present(ounit)) then
    this%ounit = ounit
  else
    ! Defaults to STDOUT
    this%ounit = STDOUT
  end if

  return
end subroutine asa_init

!> @brief Return ASA single space code or empty string depending on mode
!! @returns ASA single space prefix (' ') when ASA is enabled; empty otherwise
function asa_ss(this) result (cc_str)
  implicit none

  character(len=:), allocatable :: cc_str

  !> Object reference
  class(asa_t), intent(in) :: this

  continue

  if (this%use_asa) then
    cc_str = ' '
  else
    cc_str = ''
  end if

  return
end function asa_ss

!> @brief Return ASA double space code or newline depending on mode
!! @returns ASA double space prefix ('0') when ASA is enabled, newline otherwise
function asa_ds(this) result (cc_str)
  implicit none

  character(len=:), allocatable :: cc_str

  class(asa_t), intent(in) :: this

  continue

  if (this%use_asa) then
    cc_str = '0'
  else
    cc_str = new_line("A")
  end if

  return
end function asa_ds

!> @brief Return ASA triple space code or 2 newlines depending on mode
!! @returns ASA triple space prefix ('-') when ASA is enabled, 2 newlines otherwise
function asa_ts(this) result (cc_str)
  implicit none

  character(len=:), allocatable :: cc_str

  class(asa_t), intent(in) :: this

  continue

  if (this%use_asa) then
    cc_str = '-'
  else
    cc_str = repeat(new_line("A"), 2)
  end if

  return
end function asa_ts

!> @brief Format line(s) as paragraph and write to specified output unit
subroutine asa_write_paragraph(this, line_1, line_n)
  implicit none

  !> Object reference
  class(asa_t), intent(in) :: this

  !> First line of paragraph
  character(len=*), intent(in) :: line_1

  !> Array containing subsequent lines of paragraph, optional.
  character(len=*), dimension(:), intent(in), optional :: line_n

  !> Local variables
  integer :: nbody
  integer :: j

  continue

  write(unit=this%ounit, fmt=fmt_a) this%ds() // line_1

  if (present(line_n)) then
    nbody = size(line_n)
    if (nbody > 0) then
      do j = 1, nbody
        call this%write_line(line_n(j))
      end do
    end if
  end if

  return
end subroutine asa_write_paragraph

!> @brief Format line(s) as single-space line and write to specified
!! output unit
subroutine asa_write_line(this, line, spacing)
  implicit none

  !> Object reference
  class(asa_t), intent(in) :: this

  !> Line of text
  character(len=*), intent(in) :: line

  !> Line spacing, optional. Allowable values are [1, 2, 3]; default is 1.
  integer, intent(in), optional :: spacing

  ! Local variables

  integer :: linespace

  continue

  if (present(spacing)) then
    linespace = max(1, min(3, spacing))
  else
    linespace = 1
  end if

  select case(linespace)
  case (2)
    ! Double space (ASA '0')
    write(unit=this%ounit, fmt=fmt_a) this%ds() // line
  case (3)
    ! Triple space (ASA '-')
    write(unit=this%ounit, fmt=fmt_a) this%ts() // line
  case default
    ! Default is single space (ASA ' ')
    write(unit=this%ounit, fmt=fmt_a) this%ss() // line
  end select

  return
end subroutine asa_write_line

end module m_format
