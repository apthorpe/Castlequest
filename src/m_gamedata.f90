!> @file m_gamedata.f90
!! @author Michael S. Holtzman
!! @author Mark Kershenblatt
!! @copyright See LICENSE
!!
!! @brief Contains legacy routines and structures for reading location,
!! object, and creature descriptions and game
!! instructions and hints from data files.

!> @brief Contains legacy routines and structures for reading location,
!! object, and creature descriptions and game
!! instructions and hints from data files.
module m_gamedata
  use :: m_where, only: NROOMS
  implicit none

  public :: load_game_data

  private

  !> Short location descriptions. Read from `short.dat`, 100 entries
  character(len=80), dimension(NROOMS), public :: FORM

  !> Short object/creature descriptions. Read from `object.dat`, 60 entries
  character(len=80), dimension(60), public :: FORM2

  !> Instructions and help text. Read from `inst.dat`, 23 entries
  character(len=80), dimension(50), public :: INST

  !> Long location descriptions. Read from `long.dat`, 340 entries
  character(len=80), dimension(400), public :: LONG

  ! !> Hint text. Read from `hint.dat`, 29 entries
  ! character(len=80), dimension(50), public :: HINT

contains

!> @brief Read game data from a single file into a character array
subroutine load_game_datafile(filename, dest, read_error)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT,       &
    STDERR => ERROR_UNIT
  implicit none

  ! Arguments

  !> Data file name
  character(len=*), intent(in) :: filename

  !> Destination array (character)
  character(len=80), dimension(:), intent(out) :: dest

  !> Overall file read status
  logical, intent(out) :: read_error

  ! Local variables

  integer :: i
  integer :: j1
  integer :: j2
  integer :: fh
  integer :: ierr
  character(len=80) :: errmsg

10 format(A80)
20 format(A, ' reading from ', A, ': (', I0, ') ', A)
30 format('Reading line ', I0)
! 40 format(2X, 'ierr = ', I0)

  continue

  j1 = lbound(dest, dim=1)
  j2 = ubound(dest, dim=1)

  ierr = 0
  errmsg = ''
  open(newunit=fh, file=filename, status='OLD', form='FORMATTED',  &
    iostat=ierr, iomsg=errmsg)
  if (ierr <= 0) then
    do i = j1, j2
      read(unit=fh, fmt=10, iostat=ierr, iomsg=errmsg) dest(i)
      if (ierr > 0) then
        write(unit=STDERR, fmt=30) i - j1 + 1
        write(unit=STDERR, fmt=20) 'Error', filename, ierr, trim(errmsg)
        exit
      else if (ierr < 0) then
        ! Expected condition (EOF); pad remaining array elements and exit
        dest(i:j2) = repeat(' ', 80)
        ! write(unit=STDERR, fmt=30) i
        ! write(unit=STDERR, fmt=20) 'Note while', filename, ierr, trim(errmsg)
        exit
      end if
    end do
  else
    write(unit=STDERR, fmt=20)  'Error', filename, ierr, trim(errmsg)
  end if

  ! Positive values indicate fatal error; 0 is OK, negative are
  ! expected conditions like EOR, EOF
  read_error = (ierr > 0)
  ! write(unit=STDERR, fmt=40) ierr

  close(unit=fh)

  return
end subroutine load_game_datafile

!> @brief Read game data from all files
subroutine load_game_data(read_error)
  implicit none

  ! Arguments

  !> Overall file read status
  logical, intent(out) :: read_error

  ! Local variables

  logical :: file_error

  continue

  read_error = .false.

  ! short.dat - Short location descriptions ("You are in the ...")
  ! 100 lines expected
  call load_game_datafile('short.dat', FORM, file_error)
  read_error = (file_error .or. read_error)

  ! object.dat - Short object/creature descriptions
  ! 60 lines expected
  call load_game_datafile('object.dat', FORM2, file_error)
  read_error = (file_error .or. read_error)

  ! ! inst.dat - Game instructions
  ! ! 23 lines expected
  ! call load_game_datafile('inst.dat', INST, file_error)
  ! read_error = (file_error .or. read_error)

  ! long.dat - Long location descriptions
  ! 340 lines expected
  call load_game_datafile('long.dat', LONG, file_error)
  read_error = (file_error .or. read_error)

  ! ! hint.dat - Hints
  ! ! 29 lines expected
  ! call load_game_datafile('hint.dat', HINT, file_error)
  ! read_error = (file_error .or. read_error)

  return
end subroutine load_game_data

end module m_gamedata
