!> @file m_gameoption.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Stores options which modify Castlequest game behavior

!> @brief Stores options which modify Castlequest game behavior
module m_gameoption
implicit none

public :: gameoption_t

private

!> Game configuration structure
type :: gameoption_t
  !> Diagnostic log flag
  logical           :: write_logs   = .false.
  !> Autoload the last save file, if any
  logical           :: auto_restore = .false.
  !> Enable or disable ASA formating codes
  logical           :: use_asa      = .false.
  !> Debug flag
  logical           :: debug_mode   = .false.
contains
  !> Initialize contents of gameoption object
  procedure :: init => gameoption_init
  !> Dump contents of appconfig object
  procedure :: dump => gameoption_dump
end type gameoption_t

contains

!> Initialize contents of gameoption object
subroutine gameoption_init(this)
  implicit none
  !> Object reference
  class(gameoption_t), intent(inout) :: this
  continue

  this%write_logs   = .false.
  this%auto_restore = .false.
  this%use_asa      = .false.
  this%debug_mode   = .false.

  return
end subroutine gameoption_init

!> Dump contents of gameoption object
subroutine gameoption_dump(this)
  use, intrinsic :: iso_fortran_env, only: stderr => ERROR_UNIT
  use m_format, only: fmt_a
  use m_textio, only: ltoa
  implicit none

  !> Object reference
  class(gameoption_t), intent(in) :: this

  continue

  write(unit=stderr, fmt=fmt_a) 'write_logs:     [' // ltoa(this%write_logs) // ']'
  write(unit=stderr, fmt=fmt_a) 'auto_restore:   [' // ltoa(this%auto_restore) // ']'
  write(unit=stderr, fmt=fmt_a) 'use_asa:        [' // ltoa(this%use_asa) // ']'
  write(unit=stderr, fmt=fmt_a) 'debug_mode:     [' // ltoa(this%debug_mode) // ']'

  return
end subroutine gameoption_dump

end module m_gameoption