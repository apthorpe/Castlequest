!> @file m_items.f90
!! @author Michael S. Holtzman
!! @author Mark Kershenblatt
!! @copyright See LICENSE
!! @brief Contains item-related objects, functions, and data

!> @brief Contains item-related objects, functions, and data
module m_items
  use :: m_object_id
  use :: m_stateful, only: stateful_t
  implicit none
  private

  public :: light_available
  public :: itemtracker_t
  public :: gun_t
  public :: lamp_t
  public :: match_t
  public :: note_t
  public :: rope_t
  public :: bottle_t
  public :: torch_t
  public :: window_t
  public :: sun_t
  public :: shutterpassage_t
  public :: gameitems_t

  ! This is public only for testing
  public :: item_t

  !> Total number of user-carryable items
  integer, parameter, public :: NITEMS = 30

  !> Maximum number of items allowed in player inventory
  integer, parameter, public :: NMAXINV = 10

  !> @class item_t
  !! @brief Carryable item structure
  type :: item_t
    !> ID code
    integer :: id = -1

    !> Keyword
    character(len=16) :: name = repeat(' ', 16)

    !> Value (0 for tools, >0 for treasures)
    integer :: value = -1

    !> Location
    !! (-3 = thrown?, -2 = ?, -1 = inventory, 0 = gone, >0 = room)
    integer :: place = 101

    !> Description
    character(len=60) :: desc = repeat(' ', 60)

  contains
    !> Write long object description
    procedure :: describe => item_describe
    !> True if item is carried by the player (`place` == -1)
    procedure :: is_carried => item_is_carried
    !> True if item is in the given room (`place`> 0)
    procedure :: in_room => item_in_room
    !> True if item is in the given room or carried by the player
    !! (`place` == [-1, 1 .. `NROOMS`])
    procedure :: at_hand_in => item_at_hand_in
    !> Consumes current item
    procedure :: consume => item_consume
    !> Puts current item in player inventory
    procedure :: carry => item_carry
    !> Drops current item in given room
    procedure :: place_in => item_place_in
    !> Place current item in random location
    procedure :: scatter => item_scatter
  end type item_t

  !> @class itemtracker_t
  !! @brief Master list of carryable items
  type :: itemtracker_t
    !> Item list
    type(item_t), dimension(NITEMS) :: item
  contains
    !> Item list initialization
    procedure :: init => itemtracker_init
    !> Update and describe objects at location and update player score
    procedure :: display_here => itemtracker_display_here
    !> Calculate player score considering deposited treasures and total
    !! moves
    procedure :: total_score => itemtracker_total_score
    !> Describe player inventory
    procedure :: show_inventory => itemtracker_show_inventory
    !> True if given item is carried by the player (`place` == -1)
    procedure :: is_carried => itemtracker_is_carried
    !> Consumes given item
    procedure :: consume => itemtracker_consume
    !> Places given item in player inventory
    procedure :: carry => itemtracker_carry
  end type itemtracker_t

  !> @class gun_t
  !! @brief Readable interpretation of the gun's loaded/unloaded status
  type, extends(stateful_t) :: gun_t
    !> Item instance
    type(item_t), pointer :: item
  contains
    !> Set initial state
    procedure :: init => gun_init
    !> True if the gun is carried by the player (`place` == -1)
    procedure :: is_carried => gun_is_carried
    !> True if the gun is loaded
    procedure :: is_loaded => gun_is_loaded
    !> True if the gun is not loaded
    procedure :: is_empty => gun_is_empty
    !> Set gun state to loaded
    procedure :: load => gun_load
    !> Set gun state to unloaded
    procedure :: unload => gun_unload
    !> Return gun state as logical
    procedure :: to_logical => gun_is_loaded
    !> Set gun state from logical
    procedure :: from_logical => gun_from_logical
  end type gun_t

  !> @class lamp_t
  !! @brief Readable interpretation of the kerosene lamp's status
  type, extends(stateful_t) :: lamp_t
    !> Item instance
    type(item_t), pointer :: item
    !> Turns of light produced since refueling
    integer :: turns_lit = 0
  contains
    !> Set initial state
    procedure :: init => lamp_init
    !> True if the lamp is carried by the player (`place` == -1)
    procedure :: is_carried => lamp_is_carried
    !> True if the lamp is off but has fuel and can be lit (state 0)
    procedure :: is_off => lamp_is_off
    !> True if the lamp is lit with plenty of fuel (state 1)
    procedure :: is_bright => lamp_is_bright
    !> True if the lamp is lit and low on fuel (state 2)
    procedure :: is_dim => lamp_is_dim
    !> True if the lamp has no fuel (state 3)
    procedure :: is_empty => lamp_is_empty
    !> True if the lamp is emitting light (state 1 or 2)
    procedure :: is_lit => lamp_is_lit
    !> True if the lamp is not emitting light (state 0 or 3)
    procedure :: is_dark => lamp_is_dark
    !> Turn lamp on (change to state 0->1 or 0->2)
    procedure :: turn_on => lamp_turn_on
    !> Turn lamp off (change to state 0)
    procedure :: turn_off => lamp_turn_off
    !> Add fuel to lamp. Increase the turns of light remaining,
    !! (change to state 0, 2->1)
    procedure :: refuel => lamp_refuel
    !> Add fuel to lamp. Increase the turns of light remaining,
    !! (change to state 1->2 or 2->3)
    procedure :: burn_one_turn => lamp_burn_one_turn
  end type lamp_t

  !> @class match_t
  !! @brief Readable interpretation of the reuasable match's status
  type, extends(stateful_t) :: match_t
    !> Item instance
    type(item_t), pointer :: item
    !> Turns of light produced
    integer :: turns_lit = 0
  contains
    !> Set initial state
    procedure :: init => match_init
    !> True if the match is carried by the player (`place` == -1)
    procedure :: is_carried => match_is_carried
    !> True if the match is off but can be lit (state 0)
    procedure :: is_off => match_is_off
    !> True if the match is emitting light (state 1)
    procedure :: is_lit => match_is_lit
    !> True if the match has no fuel (state 2)
    procedure :: is_empty => match_is_empty
    !> True if the match is not emitting light (state 0 or 2)
    procedure :: is_dark => match_is_dark
    !> Turn match off (change to state 0)
    procedure :: turn_off => match_turn_off
    !> Turn match on (change to state 1)
    procedure :: turn_on => match_turn_on
    !> Burn out (change to state 2, consume item)
    procedure :: burn_out => match_burn_out
    !> If the match is lit, increment turns_lit and detect burnout
    procedure :: burn_one_turn => match_burn_one_turn
  end type match_t

  !> @class note_t
  !! @brief Readable interpretation of the butler's note
  type, extends(stateful_t) :: note_t
    ! No new attributes
  contains
    !> Set initial state (state 0)
    procedure :: init => note_init
    !> True if the note is blank (state 0)
    procedure :: is_blank => note_is_blank
    !> True if the note is about the hunchback (state 1)
    procedure :: about_hunchback => note_about_hunchback
    !> True if the note is about the book (state 2)
    procedure :: about_book => note_about_book
    !> Set note to hunchback state (state 1)
    procedure :: write_about_hunchback => note_write_about_hunchback
    !> Set note to hunchback state (state 2)
    procedure :: write_about_book => note_write_about_book
  end type note_t

  !> @class rope_t
  !! @brief Readable interpretation of the state of the rope
  type, extends(stateful_t) :: rope_t
    !> Item instance
    type(item_t), pointer :: item
  contains
    !> Sets initial state of rope (0)
    procedure :: init=> rope_init
    !> True if the rope is carried by the player (`place` == -1)
    procedure :: is_carried=> rope_is_carried
    !> True if the rope is loose (state 0)
    procedure :: is_loose=> rope_is_loose
    !> True if the rope is tied to bed (state 1)
    procedure :: tied_to_bed=> rope_tied_to_bed
    !> True if the rope is hanging (state 2)
    procedure :: is_hanging=> rope_is_hanging
    !> True if the rope is tied to the grappling hook (state 3)
    procedure :: tied_to_hook=> rope_tied_to_hook
    !> True if the rope is gone and lost forever (state -2)
    procedure :: is_gone=> rope_is_gone
    !> Set the rope to be untied (state 0)
    procedure :: untie=> rope_untie
    !> Set the rope to be tied to the bed (state 1)
    procedure :: tie_to_bed=> rope_tie_to_bed
    !> Set the rope to be hanging (state 2)
    procedure :: hang => rope_hang
    !> Set the rope to be tied to the grappling hook (state 3)
    procedure :: tie_to_hook=> rope_tie_to_hook
    !> Set the rope to be gone and lost forever (state -2)
    procedure :: lose_forever=> rope_lose_forever
  end type rope_t

  !> @class bottle_t
  !! @brief Readable interpretation of the state of the bottle
  type, extends(stateful_t) :: bottle_t
    !> pointer to bottle item instance
    type(item_t), pointer :: item
    !> Pointer to blood item
    type(item_t), pointer :: blood
    !> Pointer to water item
    type(item_t), pointer :: water
    !> True if bottle is filled with blood
    logical :: filled_with_blood
    !> True if bottle is filled with water
    logical :: filled_with_water
  contains
    !> Sets initial state of bottle (F0:B0:W0)
    procedure :: init=> bottle_init
    !> True if the bottle is carried by the player (`place` == -1)
    procedure :: is_carried => bottle_is_carried
    !> True if the bottle is empty (F0:B0:W0)
    procedure :: is_empty => bottle_is_empty
    !> True if the bottle contains blood (F1:B1:W0)
    procedure :: contains_blood => bottle_contains_blood
    !> True if the bottle contains water (F1:B0:W1)
    procedure :: contains_water => bottle_contains_water
    !> True if the bottle is full (F1:B?:W?)
    procedure :: is_full => bottle_is_full
    !> Set bottle state to empty (F0:B0:W0)
    procedure :: clear => bottle_clear
    !> Fill bottle with blood (F1:B1:W0)
    procedure :: fill_with_blood => bottle_fill_with_blood
    !> Fill bottle with water (F1:B0:W1)
    procedure :: fill_with_water => bottle_fill_with_water
    !> Fill with ... something
    procedure :: fill_with => bottle_fill_with
    !> Return logical state of bottle for serialization
    procedure :: to_logical_bottle => bottle_is_full
    !> Return logical state of blood for serialization
    procedure :: to_logical_blood => bottle_contains_blood
    !> Return logical state of water for serialization
    procedure :: to_logical_water => bottle_contains_water
    !> Set state from logical arguments
    procedure :: from_logical => bottle_from_logical
  end type bottle_t

  !> @class torch_t
  !! @brief Readable interpretation of the torch's status
  type, extends(stateful_t) :: torch_t
    !> Item instance
    type(item_t), pointer :: item
  contains
    !> Set initial state
    procedure :: init => torch_init
    !> True if the torch is carried by the player (`place` == -1)
    procedure :: is_carried => torch_is_carried
    !> True if the torch is off (state 0)
    procedure :: is_off => torch_is_off
    !> True if the torch is lit (state 1)
    procedure :: is_lit => torch_is_lit
    !> Turn torch off (change to state 0)
    procedure :: turn_off => torch_turn_off
    !> Turn torch on (change to state 1)
    procedure :: turn_on => torch_turn_on
    !> Return logical state of torch for serialization
    procedure :: to_logical => torch_is_lit
    !> Set state of torch from logical value for deserialization
    procedure :: from_logical => torch_from_logical
  end type torch_t

  !> @class window_t
  !! @brief Readable interpretation of the window's status
  type, extends(stateful_t) :: window_t
    logical :: has_bars = .false.
  contains
    !> Set initial state
    procedure :: init => window_init
    !> True if the window is nailed shut (state 0)
    procedure :: is_nailed => window_is_nailed
    !> True if the window is broken (state 1)
    procedure :: is_broken => window_is_broken
    !> True if the window is barred (state 2)
    procedure :: is_barred => window_is_barred
    !> True if the window is open (state 3)
    procedure :: is_open => window_is_open
    !> True if the window is not open (states 0, 1, and possibly 2)
    procedure :: is_blocked => window_is_blocked
    !> Calculate DES index from window state
    procedure :: des_index => window_des_index
    !> Calculate FORM2 index from window state
    procedure :: form2_index => window_form2_index
    !> Set window to nailed shut (change to state 0)
    procedure :: secure => window_secure
    !> Set window to broken (change to state 1)
    procedure :: break => window_break
    !> Set window to barred (change to state 2)
    procedure :: bar => window_bar
    !> Set window to open (change to state 3)
    procedure :: open => window_open
  end type window_t

  !> @class sun_t
  !! @brief Readable interpretation of the sun's status
  type, extends(stateful_t) :: sun_t
  contains
    !> Set initial state
    procedure :: init => sun_init
    !> True if the sun is up (state 0)
    procedure :: is_up => sun_is_up
    !> True if the sun is down (state 1)
    procedure :: is_down => sun_is_down
    ! !> Calculate DES index from sun state
    ! procedure :: des_index => sun_des_index
    !> Set sun to up (change to state 0)
    procedure :: rise => sun_rise
    !> Set sun to down/set (change to state 1)
    procedure :: set => sun_set
  end type sun_t

  !> @class shutterpassage_t
  !! @brief Readable interpretation of open/close portal status
  !! (Window 1's shutter, secret passage to library)
  type, extends(stateful_t) :: shutterpassage_t
  contains
    !> Set initial state
    procedure :: init => shutterpassage_init
    !> True if the shutter is closed (state 0)
    procedure :: is_closed => shutterpassage_is_closed
    !> True if the shutter is open (state 1)
    procedure :: is_open => shutterpassage_is_open
    ! !> Calculate DES index from shutter state
    ! procedure :: des_index => shutterpassage_des_index
    !> Open shutter (change to state 1)
    procedure :: open => shutterpassage_open
    !> Close shutter (change to state 0)
    procedure :: close => shutterpassage_close
  end type shutterpassage_t

  !> Convenience class which groups game items (portable and geographic)
  !! to simplify initialization and transport
  type :: gameitems_t
    !> Reference to list of player-carryable items
    type(itemtracker_t) :: portable
    !> Reference to state of gun
    type(gun_t) :: s_gun
    !> Reference to state of lamp
    type(lamp_t) :: s_lamp
    !> Reference to state of match
    type(match_t) :: s_match
    !> Reference to state of Butler's note
    type(note_t) :: s_note
    !> Reference to state of rope
    type(rope_t) :: s_rope
    !> Reference to state of bottle, blood, and water
    type(bottle_t) :: s_bottle
    !> Reference to state of acetylene torch
    type(torch_t) :: s_torch
    !> Reference to state of window 1 (bedroom, barred)
    type(window_t) :: s_window_1
    !> Reference to state of window 2 (smoking room)
    type(window_t) :: s_window_2
    !> Reference to state of sun
    type(sun_t) :: s_sun
    !> Reference to state of Window 1 shutter
    type(shutterpassage_t) :: s_shutter
    !> Reference to state of library passage
    type(shutterpassage_t) :: s_passage
  contains
    !> Bulk initialize referenced objects
    procedure :: init_all => gameitems_init_all
  end type gameitems_t

contains

!> True if something is emitting light in the specified location
logical function light_available(room_id, s_lamp, s_match) result(lit)
  implicit none
  !> Current location (room ID)
  integer, intent(in) :: room_id
  !> Lamp object
  type(lamp_t), intent(in) :: s_lamp
  !> Match object
  type(match_t), intent(in) :: s_match
  continue

  lit =  (s_lamp%item%at_hand_in(room_id) .and. s_lamp%is_lit())         &
    .or. (s_match%item%at_hand_in(room_id) .and. s_match%is_lit())

  return
end function light_available

!> @brief Write item description
!!
!! @note Replaces procedural routine `DES(600 + i)` for
!! `i = [1 .. NITEMS]` or `DES(601)` through `DES(630)`
subroutine item_describe(this)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_text, only: cq_form2
  use :: m_format, only: a
  implicit none

  !> Object reference
  class(item_t), intent(in) :: this

  continue

  call a%write_para(cq_form2(this%id))

  return
end subroutine item_describe

!> @brief Flag showing the current item is in the player's invenetory
!!
!! @returns True if item is carried by the player (`place` == -1)
logical function item_is_carried(this) result(carried)
  implicit none

  !> Object reference
  class(item_t), intent(in) :: this

  continue

  carried = (this%place == -1)

  return
end function item_is_carried

!> @brief Flag showing the current item is in the room `this_room`
!!
!! @returns True if item is in the given room (`place == this_room`)
logical function item_in_room(this, this_room) result(is_here)
  implicit none

  !> Object reference
  class(item_t), intent(in) :: this

  !> Room/location number
  integer, intent(in) :: this_room

  continue

  is_here = (this%place == this_room)

  return
end function item_in_room

!> @brief Flag showing item is in the given room or carried by the player
!!
!! @returns True if item is in the given room or carried by the player
!! (`place` == [-1, 1 .. `NROOMS`])
logical function item_at_hand_in(this, this_room) result(is_available)
  implicit none

  !> Object reference
  class(item_t), intent(in) :: this

  !> Room/location number
  integer, intent(in) :: this_room

  continue

  is_available = (this%in_room(this_room) .or. this%is_carried())

  return
end function item_at_hand_in

!> @brief Consumes current item
!!
!! Sets `place = 0`
subroutine item_consume(this)
  implicit none

  !> Object reference
  class(item_t), intent(inout) :: this

  continue

  this%place = 0

  return
end subroutine item_consume

!> @brief Puts current item in player inventory
!!
!! Sets `place = -1`
subroutine item_carry(this)
  implicit none

  !> Object reference
  class(item_t), intent(inout) :: this

  continue

  this%place = -1

  return
end subroutine item_carry

!> @brief Puts current item in room `this_room`
subroutine item_place_in(this, this_room)
  implicit none

  !> Object reference
  class(item_t), intent(inout) :: this

  !> Room/location number
  integer, intent(in) :: this_room

  continue

  this%place = this_room

  return
end subroutine item_place_in

!> @brief Place current item in random location
subroutine item_scatter(this, lo_room, n_rooms, hi_room)
  use m_where, only: random_room
  use :: m_random, only: RDM
  implicit none

  !> Object reference
  class(item_t), intent(inout) :: this

  !> Lowest room to scatter to
  integer, intent(in) :: lo_room

  !> Total possible number of rooms to scatter to
  integer, intent(in), optional :: n_rooms

  !> Highest room to scatter to
  integer, intent(in), optional :: hi_room

  continue

  this%place = random_room(lo_room, n_rooms, hi_room)

  return
end subroutine item_scatter

!> @brief Update and describe objects at location and update player
!! score
!!
!! @note Replaces procedural routine `OBJ()`
subroutine itemtracker_display_here(this, p_score, ROOM,                &
  path_to_precipice)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: m_score, only: score_t
  implicit none

  !> Object reference
  class(itemtracker_t), intent(inout) :: this

  !> Player score
  type(score_t), intent(inout) :: p_score

  !> Current location index
  integer, intent(in) :: ROOM

  !> Precipice flag
  logical, intent(in) :: path_to_precipice

  ! Local variables

  integer :: I

  continue

  if (path_to_precipice) then
    this%item(O9_ROPE)%place = 0
    this%item(O16_HOOK)%place = 0
  end if

  do I = 1, NITEMS
    if (this%item(I)%place == ROOM) then
      ! Add and clear treasure discovery score
      call p_score%add_to_score(this%item(I)%value)
      this%item(I)%value = 0
      call this%item(I)%describe()
    end if
  end do

  if (path_to_precipice) then
    this%item(O9_ROPE)%place = ROOM
    this%item(O16_HOOK)%place = ROOM
  end if

  return
end subroutine itemtracker_display_here

!> @brief Item list initialization
!!
!! @note Item list is directly populated with hard-coded `item_t`
!! constructors because the game content should be constant. The
!! gameplay and narrative should not change as a result of this
!! modernization and extensibility and flexibility are not needed.
subroutine itemtracker_init(this)
  use m_object_id
  implicit none

  !> Object reference
  class(itemtracker_t), intent(inout) :: this

  continue

  this%item = (/                                                        &
    item_t(name='Kerosene        ',                                     &
      desc='There is a can of kerosene here.',                          &
      id=O1_KEROSENE,  place=R25_STORAGE_ROOM,      value= 0),          &
    item_t(name='Silver bullet   ',                                     &
      desc='There is a silver bullet here.',                            &
      id=O2_BULLET,    place=R1_BEDROOM,            value= 0),          &
    item_t(name='Bloody hatchet  ',                                     &
      desc='There is a blood stained hatchet here. ',                   &
      id=O3_AXE,       place=R33_ARMOR_ROOM,        value= 0),          &
    item_t(name='Skeleton key    ',                                     &
      desc='There is an ornate skeleton key here! ',                    &
      id=O4_KEY,       place=-3,                    value= 0),          &
    item_t(name='Blood in bottle ',                                     &
      desc='There is a small pool of blood here. ',                     &
      id=O5_BLOOD,     place=R18_DARK_EW_CORRIDOR,  value= 0),          &
    item_t(name='Wooden stake    ',                                     &
      desc='To one side lies a wooden stake. ',                         &
      id=O6_STAKE,     place=R12_GARDEN,            value= 0),          &
    item_t(name='Champagne       ',                                     &
      desc='There is a bottle of vintage champagne here!',              &
      id=O7_CHAMPAGNE, place=R55_HONEYMOON_SUITE,   value=10),          &
    item_t(name='Hunchback       ',                                     &
      desc='A nasty hunchback eyes you from a corner of the room. ',    &
      id=O8_HUNCHBACK, place=R35_TORTURE,           value= 0),          &
    item_t(name='Coil of rope    ',                                     &
      desc='There is a long piece of rope lying on the floor. ',        &
      id=O9_ROPE,      place=R19_DARK_ROOM,         value= 0),          &
    item_t(name='Writing paper   ',                                     &
      desc='There is some "HORROR HOTEL" writing paper here. ',         &
      id=O10_PAPER,    place=R3_PARLOR,             value= 0),          &
    item_t(name='Quill pen       ',                                     &
      desc='There is an old quill pen here. ',                          &
      id=O11_PEN,      place=R15_BOUDOIR,           value= 0),          &
    item_t(name='Ivory sword     ',                                     &
      desc='There is an ivory-handled sword here!',                     &
      id=O12_SWORD,    place=R88_CYCLOPS_LAIR,      value=10),          &
    item_t(name='Acetylene torch ',                                     &
      desc='There is a rusty acetylene torch here.',                    &
      id=O13_TORCH,    place=R36_SLAB,              value= 0),          &
    item_t(name='Rowboat         ',                                     &
      desc='Off to the side is an old rowboat. ',                       &
      id=O14_BOAT,     place=R32_SIDE_OF_MOAT,      value= 0),          &
    item_t(name='Reusable match  ',                                     &
      desc='There are matches from the Vampire Diner here. ',           &
      id=O15_MATCH,    place=R13_LIBRARY,           value= 0),          &
    item_t(name='Grappling Hook  ',                                     &
      desc='There is a heavy steel grappling hook here.',               &
      id=O16_HOOK,     place=R11_WORKSHOP,          value= 0),          &
    item_t(name='Gold statue     ',                                     &
      desc='A gold statue is glistening in the light!',                 &
      id=O17_STATUE,   place=R29_BELOW_WINDOW,      value=20),          &
    item_t(name='Empty bottle    ',                                     &
      desc='An empty bottle is discarded nearby. ',                     &
      id=O18_BOTTLE,   place=R31_PANTRY,            value= 0),          &
    item_t(name='Silver cross    ',                                     &
      desc='There is a silver cross nearby! ',                          &
      id=O19_CROSS,    place=0,                     value=25),          &
    item_t(name='Old gun         ',                                     &
      desc='There is an old gun here. ',                                &
      id=O20_GUN,      place=R3_PARLOR,             value= 0),          &
    item_t(name='Brass lantern   ',                                     &
      desc='There is a small kerosene lamp here. ',                     &
      id=O21_LAMP,     place=R22_CEDAR_CLOSET,      value= 0),          &
    item_t(name='Tasty food      ',                                     &
      desc='Somebody left some tasty food here. ',                      &
      id=O22_FOOD,     place=R6_KITCHEN,            value= 0),          &
    item_t(name='Large ruby      ',                                     &
      desc='There is a very large ruby here!',                          &
      id=O23_RUBY,     place=R81_ISLAND,            value=10),          &
    item_t(name='Jade figure     ',                                     &
      desc='Perched on the ground is a valuable jade figure!',          &
      id=O24_JADE,     place=R52_COMPASS_ROOM,      value=10),          &
    item_t(name='Flask of acid   ',                                     &
      desc='There is a small flask of nitric acid here. ',              &
      id=O25_ACID,     place=R14_LAB,               value= 0),          &
    item_t(name='Water in bottle ',                                     &
      desc='You are standing in a small puddle of water. ',             &
      id=O26_WATER,    place=0,                     value= 0),          &
    item_t(name='Cuban cigar     ',                                     &
      desc='There is a fine cuban cigar here. ',                        &
      id=O27_CIGAR,    place=R10_SMOKING_ROOM,      value= 0),          &
    item_t(name='Sapphire        ',                                     &
      desc='A sapphire sparkles on the ground nearby!',                 &
      id=O28_SAPPHIRE, place=R77_GRAFFITI_ROOM,     value=10),          &
    item_t(name='Lots of money   ',                                     &
      desc='There is lots of money here!',                              &
      id=O29_MONEY,    place=R94_WIZARDS_CACHE,     value=10),          &
    item_t(name='Crystal swan    ',                                     &
      desc='A delicate crystal swan lies off to one side!',             &
      id=O30_SWAN,     place=R89_INSIDE_GLACIER,    value=10)           &
  /)

  return
end subroutine itemtracker_init

!> @brief Calculate player score considering deposited treasures
!! and total moves
!!
!! @returns Integer score based on treasure location and number of moves
integer function itemtracker_total_score(this, NUMOVE) result(ISCORE)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use m_object_id
  implicit none

  !> Catalog of portable items (object reference)
  class(itemtracker_t), intent(in) :: this

  !> Number of moves taken
  integer, intent(in) :: NUMOVE

  continue

  ISCORE = 0
  if (this%item(O4_KEY)%place       == R72_VAULT)  ISCORE = ISCORE + 9  ! Skeleton key
  if (this%item(O7_CHAMPAGNE)%place == R72_VAULT)  ISCORE = ISCORE + 10 ! Champagne
  if (this%item(O12_SWORD)%place     == R72_VAULT)  ISCORE = ISCORE + 10 ! Ivory sword
  if (this%item(O17_STATUE)%place    == R72_VAULT)  ISCORE = ISCORE + 10 ! Gold statue
  if (this%item(O18_BOTTLE)%place    == R81_ISLAND) ISCORE = ISCORE + 1  ! Empty bottle
  if (this%item(O19_CROSS)%place     == R72_VAULT)  ISCORE = ISCORE + 10 ! Silver cross
  if (this%item(O23_RUBY)%place      == R72_VAULT)  ISCORE = ISCORE + 10 ! Large ruby
  if (this%item(O24_JADE)%place      == R72_VAULT)  ISCORE = ISCORE + 10 ! Jade figure
  if (this%item(O28_SAPPHIRE)%place  == R72_VAULT)  ISCORE = ISCORE + 10 ! Sapphire
  if (this%item(O29_MONEY)%place     == R72_VAULT)  ISCORE = ISCORE + 10 ! Lots of money
  if (this%item(O30_SWAN)%place      == R72_VAULT)  ISCORE = ISCORE + 10 ! Crystal swan

  ! Score is dependent on the number of moves.
  ! Docked 1 point for every 5 moves above 250
  ISCORE = ISCORE - int(max(NUMOVE - 250, 0) / 5)

  return
end function itemtracker_total_score

!> @brief Describe player inventory
subroutine itemtracker_show_inventory(this, NUMB, s_gun, s_bottle)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: m_format, only: fmt_a, a
  implicit none

  ! Parameters

  character(len=16), parameter :: LOADED = 'Bullet in gun   '
  character(len=*), parameter :: cqf_inv1000 =                          &
    "('  You are carrying the following ', I2, ' objects:')"
  character(len=*), parameter :: cq_inv1010 =                           &
    '  You are carrying the following object:'
  character(len=*), parameter :: cq_inv3000 =                           &
    '  You''re not carrying anything.'

  ! Arguments

  !> Object reference, catalog of carryable items
  class(itemtracker_t), intent(inout) :: this

  !> Number of items in player's inventory
  integer, intent(inout) :: NUMB

  !> Gun state
  type(gun_t), intent(in) :: s_gun

  !> Bottle state
  type(bottle_t), intent(in) :: s_bottle

  ! Local variables

  integer :: II
  character(len=80) :: msg

  ! Formats

1100 format(5X, A16)

  continue

  if (NUMB /= 0) then
    if (s_bottle%is_full()) then
      ! Temporarily consume the (empty) bottle
      s_bottle%item%place = 0
    end if

    if (NUMB == 1) then
      call a%write_para(cq_inv1010)
      ! write(STDOUT,1010)
    else
      write(unit=msg, fmt=cqf_inv1000) NUMB
      call a%write_para(msg)
      ! write(STDOUT,1000) NUMB
    end if

    NUMB = 0

    if (s_gun%is_loaded()) then
      s_gun%item%place = 0
      this%item(O2_BULLET)%place = 0
      ! write(STDOUT,2000) '     ' // LOADED
      write(unit=msg, fmt=1100) LOADED
      call a%write_line(msg)
    end if

    do II = 1, NITEMS
      if (this%item(II)%is_carried()) then
        ! write(STDOUT,2000) this%item(II)%name
        ! call a%write_line('     ' // this%item(II)%name)
        write(unit=msg, fmt=1100) this%item(II)%name
        call a%write_line(msg)
        NUMB = NUMB + 1
      end if
    end do

    if (s_bottle%is_full()) then
      call this%carry(O18_BOTTLE)
    end if

    if (s_gun%is_loaded()) then
      call this%carry(O20_GUN)
      call this%carry(O2_BULLET)
      NUMB = NUMB + 1
    end if

  end if

  if (NUMB < 1) then
    call a%write_para(cq_inv3000)
    ! write(STDOUT,3000)
  end if

  return
end subroutine itemtracker_show_inventory

!> @brief Flag showing player is carrying given item
!!
!! This is a convenience wrapper around `item_t%is_carried()`
!!
!! @returns True if player is carrying given item
logical function itemtracker_is_carried(this, oid) result(carried)
  implicit none

  ! Arguments

  !> Object reference, catalog of carryable items
  class(itemtracker_t), intent(in) :: this

  !> Object ID
  integer, intent(in) :: oid

  continue

  carried = this%item(oid)%is_carried()

  return
end function itemtracker_is_carried

!> @brief Consume given item
subroutine itemtracker_consume(this, oid)
  implicit none

  ! Arguments

  !> Object reference, catalog of carryable items
  class(itemtracker_t), intent(inout) :: this

  !> Object ID
  integer, intent(in) :: oid

  continue

  call this%item(oid)%consume()

  return
end subroutine itemtracker_consume

!> @brief Adds given item to player inventory.
!!
!! This is a convenience wrapper around `item_t%carry()`
subroutine itemtracker_carry(this, oid)
  implicit none

  ! Arguments

  !> Object reference, catalog of carryable items
  class(itemtracker_t), intent(inout) :: this

  !> Item ID number
  integer, intent(in) :: oid

  continue

  call this%item(oid)%carry()

  return
end subroutine itemtracker_carry

!> @brief Sets initial state of gun
subroutine gun_init(this, gun_ref)
  implicit none
  !> Gun state object
  class(gun_t), intent(inout) :: this
  !> Reference to `O20_GUN` item in `itemtracker_t`
  !! (e.g. `itemtracker_t%item(O20_GUN)`)
  type(item_t), intent(in), target :: gun_ref
  continue
  this%item => gun_ref
  this%id = this%item%id
  this%name = this%item%name
  call this%unload()
  return
end subroutine gun_init

!> True if the gun is carried by the player (`place` == -1)
logical function gun_is_carried(this) result(is_carried)
  implicit none
  ! Arguments
  !> Gun state object
  class(gun_t), intent(in) :: this
  continue
  if (associated(this%item)) then
    is_carried = this%item%is_carried()
  else
    is_carried = .false.
  end if
  return
end function gun_is_carried

!> @brief True if the gun is loaded
!!
!! Also aliased to `gun_t%to_logical()` for serialization
logical function gun_is_loaded(this) result(is_loaded)
  implicit none
  ! Arguments
  !> Gun state object
  class(gun_t), intent(in) :: this
  continue
  is_loaded = (this%state >= 1)
  return
end function gun_is_loaded

!> True if the gun is not loaded
logical function gun_is_empty(this) result(is_empty)
  implicit none
  ! Arguments
  !> Gun state object
  class(gun_t), intent(in) :: this
  continue
  is_empty = (.not. this%is_loaded())
  return
end function gun_is_empty

!> Set gun state to loaded
subroutine gun_load(this)
  implicit none
  ! Arguments
  !> Gun state object
  class(gun_t), intent(inout) :: this
  continue
  this%state = 1
  return
end subroutine gun_load

!> Set gun state to empty
subroutine gun_unload(this)
  implicit none
  ! Arguments
  !> Gun state object
  class(gun_t), intent(inout) :: this
  continue
  this%state = 0
  return
end subroutine gun_unload

!> @brief Set gun state from logical value
!!
!! Intended for deserialization
subroutine gun_from_logical(this, lval)
  implicit none
  ! Arguments
  !> Gun state object
  class(gun_t), intent(inout) :: this
  !> Logical value
  logical, intent(in) :: lval
  continue
  if (lval) then
    call this%load()
  else
    call this%unload()
  end if
  return
end subroutine gun_from_logical

!> @brief Sets initial state of lamp
subroutine lamp_init(this, lamp_ref)
  implicit none
  !> Lamp state object
  class(lamp_t), intent(inout) :: this
  !> Reference to `O21_LAMP` item in `itemtracker_t`
  !! (e.g. `itemtracker_t%item(O21_LAMP)`)
  type(item_t), intent(in), target :: lamp_ref
  continue
  this%item      => lamp_ref
  this%id        = this%item%id
  this%name      = this%item%name
  this%turns_lit = 0
  call this%set_state_(0)
  return
end subroutine lamp_init

!> True if the lamp is carried by the player (`place` == -1)
logical function lamp_is_carried(this) result(is_carried)
  implicit none
  ! Arguments
  !> Lamp state object
  class(lamp_t), intent(in) :: this
  continue
  if (associated(this%item)) then
    is_carried = this%item%is_carried()
  else
    is_carried = .false.
  end if
  return
end function lamp_is_carried

!> True if the lamp is off but can be turned on
logical function lamp_is_off(this) result(is_off)
  implicit none
  ! Arguments
  !> Lamp state object
  class(lamp_t), intent(in) :: this
  continue
  is_off = (this%state <= 0)
  return
end function lamp_is_off

!> @brief True if the lamp is bright
logical function lamp_is_bright(this) result(is_bright)
  implicit none
  ! Arguments
  !> Lamp state object
  class(lamp_t), intent(in) :: this
  continue
  is_bright = (this%state == 1)
  return
end function lamp_is_bright

!> @brief True if the lamp is dim
logical function lamp_is_dim(this) result(is_dim)
  implicit none
  ! Arguments
  !> Lamp state object
  class(lamp_t), intent(in) :: this
  continue
  is_dim = (this%state == 2)
  return
end function lamp_is_dim

!> True if the lamp is out of fuel and cannot be turned on without
!! refueling
logical function lamp_is_empty(this) result(is_empty)
  implicit none
  ! Arguments
  !> Lamp state object
  class(lamp_t), intent(in) :: this
  continue
  is_empty = (this%state >= 3)
  return
end function lamp_is_empty

!> @brief True if the lamp is lit, either bright or dim
logical function lamp_is_lit(this) result(is_lit)
  implicit none
  ! Arguments
  !> Lamp state object
  class(lamp_t), intent(in) :: this
  continue
  is_lit = (this%is_bright() .or. this%is_dim())
  return
end function lamp_is_lit

!> @brief True if the lamp is not lit, either empty or off
logical function lamp_is_dark(this) result(is_dark)
  implicit none
  ! Arguments
  !> Lamp state object
  class(lamp_t), intent(in) :: this
  continue
  is_dark = (.not. this%is_lit())
  return
end function lamp_is_dark

!> @brief Turn on lamp.
subroutine lamp_turn_on(this)
  implicit none
  ! Arguments
  !> Lamp state object
  class(lamp_t), intent(inout) :: this
  continue
  if (.not. this%is_empty()) then
    ! set state to dim or bright based on fuel level
    if (this%turns_lit > 75) then
      ! Dim
      call this%set_state_(2)
    else
      ! Bright
      call this%set_state_(1)
    end if
  end if
  return
end subroutine lamp_turn_on

!> @brief Turn off lamp.
subroutine lamp_turn_off(this)
  implicit none
  ! Arguments
  !> Lamp state object
  class(lamp_t), intent(inout) :: this
  continue
  if (.not. this%is_empty()) then
    ! set state to off if lamp has fuel and can be relit
    call this%set_state_(0)
  end if
  return
end subroutine lamp_turn_off

!> @brief Refuel lamp.
!!
!! @note This only sets the state of the lamp; consuming the kerosene is
!! a separate operation.
subroutine lamp_refuel(this, new_turns_lit)
  implicit none
  ! Arguments
  !> Lamp state object
  class(lamp_t), intent(inout) :: this
  !> Specify new number of turns lit for lamp
  integer, intent(in), optional :: new_turns_lit
  continue
  if (present(new_turns_lit)) then
    this%turns_lit = new_turns_lit
  else
    ! Q: Should this%turns_lit be set to -75 or 0?
    this%turns_lit = 0
  end if

  if (this%is_empty()) then
    call this%set_state_(0)
  else if (this%is_dim()) then
    call this%set_state_(1)
  end if
  return
end subroutine lamp_refuel

!> @brief Burn a unit of lamp fuel and update state to dim or empty
!!
!! If both `NUMB` and `s_kerosene` are provided, automatic refueling is
!! allowed. If the lamp grows dim and the player is carrying the
!! kerosene, the lamp's `turns_lit` will be adjusted and its `state`
!! will be set to bright (1), and the kerosene will be consumed
!! (`state` set to 0). If the player is not carrying the kerosene,
!! `s_kerosene` and `NUMB` may be omitted.
!!
!! @note Check `lamp%is_empty()` on exit; if true write line 1064 and
!! get the player's next action.
subroutine lamp_burn_one_turn(this, s_kerosene, NUMB)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT,       &
    STDERR => ERROR_UNIT
  use :: cquest_text
  use :: m_format, only: a
  implicit none

  ! Parameters
  character(len=*), parameter :: errmsg = '  ERROR: Supply both NUMB '  &
    // 'and s_kerosene to lamp_burn_one_turn() or neither'

  ! Arguments

  !> Lamp state object
  class(lamp_t), intent(inout) :: this

  !> Kerosene item, optional
  type(item_t), intent(inout), optional :: s_kerosene

  !> Number of items carried by player, optional
  integer, intent(inout), optional :: NUMB

  ! Local variables
  logical :: can_refuel

  continue

  if (present(s_kerosene)) then
    if (present(NUMB)) then
      can_refuel = s_kerosene%is_carried()
    else
      ! Bad argument list: this should not occur.
      call a%write_para(errmsg)
      stop
    end if
  else
    can_refuel = .false.
  end if

  ! If the lamp is lit, it burns fuel whether it's carried or not
  if (this%is_lit()) then
    this%turns_lit = this%turns_lit + 1
    if (this%turns_lit == 75) then
      ! Dim
      call this%set_state_(2)
      ! write(unit=STDOUT, fmt=1083)
      call a%write_para(cqm_f1083h, (/cqm_f1083b/))
      if (this%item%is_carried() .and. can_refuel) then
        ! write(unit=STDOUT, fmt=1085)
        call a%write_para(cqm_f1085)
        call s_kerosene%consume()
        NUMB = NUMB - 1
        ! Q: Should this%turns_lit be set to -75 or 0?
        this%turns_lit = -75
        call this%set_state_(1)
      end if
    else if (this%turns_lit == 100) then
      ! Empty
      call this%set_state_(3)
      ! write(STDOUT, 1009)
      call a%write_para(cqm_f1009)
    end if
  end if

  return
end subroutine lamp_burn_one_turn

!> @brief Sets initial state of match
subroutine match_init(this, match_ref)
  implicit none
  !> Match state object
  class(match_t), intent(inout) :: this
  !> Reference to `O15_MATCH` item in `itemtracker_t`
  !! (e.g. `itemtracker_t%item(O15_MATCH)`)
  type(item_t), intent(in), target :: match_ref
  continue
  this%item      => match_ref
  this%id        = this%item%id
  this%name      = this%item%name
  this%turns_lit = 0
  this%state     = 0
  return
end subroutine match_init

!> True if the match is carried by the player (`place` == -1)
logical function match_is_carried(this) result(is_carried)
  implicit none
  ! Arguments
  !> Match state object
  class(match_t), intent(in) :: this
  continue
  if (associated(this%item)) then
    is_carried = this%item%is_carried()
  else
    is_carried = .false.
  end if
  return
end function match_is_carried

!> True if the match is off but can be turned on
logical function match_is_off(this) result(is_off)
  implicit none
  ! Arguments
  !> Match state object
  class(match_t), intent(in) :: this
  continue
  is_off = (this%state <= 0)
  return
end function match_is_off

!> @brief True if the match is lit
logical function match_is_lit(this) result(is_lit)
  implicit none
  ! Arguments
  !> Match state object
  class(match_t), intent(in) :: this
  continue
  is_lit = (this%state == 1)
  return
end function match_is_lit

!> True if the matchbook is empty and cannot be lit
logical function match_is_empty(this) result(is_empty)
  implicit none
  ! Arguments
  !> Match state object
  class(match_t), intent(in) :: this
  continue
  is_empty = (this%state >= 2)
  return
end function match_is_empty

!> @brief True if the match is not lit, either empty or off
logical function match_is_dark(this) result(is_dark)
  implicit none
  ! Arguments
  !> Match state object
  class(match_t), intent(in) :: this
  continue
  is_dark = (.not. this%is_lit())
  return
end function match_is_dark

!> @brief Turn off match.
subroutine match_turn_off(this)
  implicit none
  ! Arguments
  !> Match state object
  class(match_t), intent(inout) :: this
  continue
  if (.not. this%is_empty()) then
    ! set state to off if match has fuel and can be relit
    call this%set_state_(0)
  end if
  return
end subroutine match_turn_off

!> @brief Turn on match.
subroutine match_turn_on(this)
  implicit none
  ! Arguments
  !> Match state object
  class(match_t), intent(inout) :: this
  continue
  if (.not. this%is_empty()) then
    call this%set_state_(1)
  end if
  return
end subroutine match_turn_on

!> @brief Burn the last of the matches.
subroutine match_burn_out(this, NUMB)
  implicit none
  ! Arguments
  !> Match state object
  class(match_t), intent(inout) :: this
  !> Number of items carried by player, optional
  integer, intent(inout), optional :: NUMB
  continue
  call this%item%consume()
  call this%set_state_(3)
  if (present(NUMB)) then
    NUMB = NUMB -1
  end if
  return
end subroutine match_burn_out

!> @brief If a the match is lit, burn a one turn of match light and
!! check for burnout
!!
!! @note Check `match%is_empty()` on exit; if true write line 1064 and
!! get the player's next action.
subroutine match_burn_one_turn(this, NUMB)
  implicit none
  ! Arguments
  !> Match state object
  class(match_t), intent(inout) :: this
  !> Number of items carried by player, optional
  integer, intent(inout), optional :: NUMB
  continue

  ! If the match is lit, it burns whether it's carried or not
  if (this%is_lit()) then
    this%turns_lit = this%turns_lit + 1
    if (this%turns_lit >= 10) then
      ! Empty
      if (present(NUMB) .and. this%is_carried()) then
        call this%burn_out(NUMB)
      else
        call this%burn_out()
      end if
    end if
  end if

  return
end subroutine match_burn_one_turn

!> @brief Sets initial state of note
subroutine note_init(this)
  implicit none
  !> Note state object
  class(note_t), intent(inout) :: this
  continue
  this%name      = "note            "
  this%id        = N52_NOTE
  call this%set_state_(0)
  return
end subroutine note_init

!> True if the note is blank
logical function note_is_blank(this) result(is_blank)
  implicit none
  ! Arguments
  !> Note state object
  class(note_t), intent(in) :: this
  continue
  is_blank = (this%state <= 0)
  return
end function note_is_blank

!> True if the note is about the hunchback
logical function note_about_hunchback(this) result(about_hunchback)
  implicit none
  ! Arguments
  !> Note state object
  class(note_t), intent(in) :: this
  continue
  about_hunchback = (this%state == 1)
  return
end function note_about_hunchback

!> @brief True if the note is about the book
logical function note_about_book(this) result(about_book)
  implicit none
  ! Arguments
  !> Note state object
  class(note_t), intent(in) :: this
  continue
  about_book = (this%state >= 2)
  return
end function note_about_book

!> Write about the hunchback
subroutine note_write_about_hunchback(this)
  implicit none
  ! Arguments
  !> Note state object
  class(note_t), intent(inout) :: this
  continue
  call this%set_state_(1)
  return
end subroutine note_write_about_hunchback

!> Write about the book
subroutine note_write_about_book(this)
  implicit none
  ! Arguments
  !> Note state object
  class(note_t), intent(inout) :: this
  continue
  call this%set_state_(2)
  return
end subroutine note_write_about_book

!> @brief Sets initial state of rope
subroutine rope_init(this, rope_ref)
  implicit none
  !> Rope state object
  class(rope_t), intent(inout) :: this
  !> Reference to `O9_ROPE` item in `itemtracker_t`
  !! (e.g. `itemtracker_t%item(O9_ROPE)`)
  type(item_t), intent(in), target :: rope_ref
  continue
  this%item      => rope_ref
  this%id        = this%item%id
  this%name      = this%item%name
  call this%untie()
  return
end subroutine rope_init

!> True if the rope is carried by the player (`place` == -1)
logical function rope_is_carried(this) result(is_carried)
  implicit none
  ! Arguments
  !> Rope state object
  class(rope_t), intent(in) :: this
  continue
  if (associated(this%item)) then
    is_carried = this%item%is_carried()
  else
    is_carried = .false.
  end if
  return
end function rope_is_carried
! 0=LOOSE, 1=TIED TO BED, 2=HANGING, -2=GONE, 3=TIED TO HOOK

!> True if the rope is loose (state 0)
logical function rope_is_loose(this) result(is_loose)
  implicit none
  ! Arguments
  !> Rope state object
  class(rope_t), intent(in) :: this
  continue
  is_loose = (this%state == 0)
  return
end function rope_is_loose

!> True if the rope is tied to bed (state 1)
logical function rope_tied_to_bed(this) result(tied_to_bed)
  implicit none
  ! Arguments
  !> Rope state object
  class(rope_t), intent(in) :: this
  continue
  tied_to_bed = (this%state == 1)
  return
end function rope_tied_to_bed

!> True if the rope is hanging (state 2)
logical function rope_is_hanging(this) result(is_hanging)
  implicit none
  ! Arguments
  !> Rope state object
  class(rope_t), intent(in) :: this
  continue
  is_hanging = (this%state == 2)
  return
end function rope_is_hanging

!> True if the rope is tied to the grappling hook (state 3)
logical function rope_tied_to_hook(this) result(tied_to_hook)
  implicit none
  ! Arguments
  !> Rope state object
  class(rope_t), intent(in) :: this
  continue
  tied_to_hook = (this%state == 3)
  return
end function rope_tied_to_hook

!> True if the rope is gone and lost forever (state -2)
logical function rope_is_gone(this) result(is_gone)
  implicit none
  ! Arguments
  !> Rope state object
  class(rope_t), intent(in) :: this
  continue
  is_gone = (this%state <= -2)
!  call this%item%consume()
  return
end function rope_is_gone

!> @brief Set the rope to be untied (state 0)
subroutine rope_untie(this)
  implicit none
  ! Arguments
  !> Rope state object
  class(rope_t), intent(inout) :: this
  continue
  call this%set_state_(0)
  return
end subroutine rope_untie

!> @brief Set the rope to be tied to the bed (state 1)
subroutine rope_tie_to_bed(this)
  implicit none
  ! Arguments
  !> Rope state object
  class(rope_t), intent(inout) :: this
  continue
  call this%set_state_(1)
  return
end subroutine rope_tie_to_bed

!> @brief Set the rope to be hanging (state 2)
subroutine rope_hang(this)
  implicit none
  ! Arguments
  !> Rope state object
  class(rope_t), intent(inout) :: this
  continue
  call this%set_state_(2)
  return
end subroutine rope_hang

!> @brief Set the rope to be tied to the grappling hook (state 3)
subroutine rope_tie_to_hook(this)
  implicit none
  ! Arguments
  !> Rope state object
  class(rope_t), intent(inout) :: this
  continue
  call this%set_state_(3)
  return
end subroutine rope_tie_to_hook

!> @brief Set the rope to be gone and lost forever (state -2)
!!
!! @note This only sets rope state; it does not change the location
!! to 0 (consumed) in itemlist.
subroutine rope_lose_forever(this)
  implicit none
  ! Arguments
  !> Rope state object
  class(rope_t), intent(inout) :: this
  continue
  call this%set_state_(-2)
  return
end subroutine rope_lose_forever

!> Sets initial state of bottle (F0:B0:W0)
subroutine bottle_init(this, bottle_ref, blood_ref, water_ref)
  implicit none
  ! Arguments
  !> Bottle state object
  class(bottle_t), intent(inout) :: this
  !> Reference to `O18_BOTTLE` item, e.g. `itemtracker_t%item(O18_BOTTLE)`
  type(item_t), intent(in), target :: bottle_ref
  !> Reference to `O5_BLOOD` item, e.g. `itemtracker_t%item(O5_BLOOD)`
  type(item_t), intent(in), target :: blood_ref
  !> Reference to `O26_WATER` item, e.g. `itemtracker_t%item(O26_WATER)`
  type(item_t), intent(in), target :: water_ref
  continue
  this%item           => bottle_ref
  this%blood          => blood_ref
  this%water          => water_ref
  this%id             = this%item%id
  this%name           = this%item%name
  call this%clear()
  return
end subroutine

!> True if the bottle is carried by the player (`place` == -1)
logical function bottle_is_carried(this) result(is_carried)
  implicit none
  ! Arguments
  !> Bottle state object
  class(bottle_t), intent(in) :: this
  continue
  is_carried = (this%item%is_carried())
  return
end function bottle_is_carried

!> True if the bottle is empty (F0:B0:W0)
logical function bottle_is_empty(this) result(is_empty)
  implicit none
  ! Arguments
  !> Bottle state object
  class(bottle_t), intent(in) :: this
  continue
  is_empty = (this%state == 0)
  return
end function bottle_is_empty

!> True if the bottle contains blood (F1:B1:W0)
logical function bottle_contains_blood(this) result(contains_blood)
  implicit none
  ! Arguments
  !> Bottle state object
  class(bottle_t), intent(in) :: this
  continue
  contains_blood = this%filled_with_blood
  return
end function bottle_contains_blood

!> True if the bottle contains water (F1:B0:W1)
logical function bottle_contains_water(this) result(contains_water)
  implicit none
  ! Arguments
  !> Bottle state object
  class(bottle_t), intent(in) :: this
  continue
  contains_water = this%filled_with_water
  return
end function bottle_contains_water

!> True if the bottle is full (F1:B?:W?)
logical function bottle_is_full(this) result(is_full)
  implicit none
  ! Arguments
  !> Bottle state object
  class(bottle_t), intent(in) :: this
  continue
  is_full = (.not. this%is_empty())
  return
end function bottle_is_full

!> Set bottle state to empty (F0:B0:W0)
subroutine bottle_clear(this)
  implicit none
  ! Arguments
  !> Bottle state object
  class(bottle_t), intent(inout) :: this
  continue
  this%filled_with_blood = .false.
  this%filled_with_water = .false.
  this%state             = 0
  return
end subroutine bottle_clear

!> Fill bottle with blood (F1:B1:W0)
subroutine bottle_fill_with_blood(this)
  implicit none
  ! Arguments
  !> Bottle state object
  class(bottle_t), intent(inout) :: this
  continue
  this%filled_with_blood = .true.
  this%filled_with_water = .false.
  this%state             = 1
  return
end subroutine bottle_fill_with_blood

!> Fill bottle with water (F1:B0:W1)
subroutine bottle_fill_with_water(this)
  implicit none
  ! Arguments
  !> Bottle state object
  class(bottle_t), intent(inout) :: this
  continue
  this%filled_with_blood = .false.
  this%filled_with_water = .true.
  this%state             = 1
  return
end subroutine bottle_fill_with_water

!> @brief Try to fill a bottle with arbitrary `OBJECT`
!!
!! The `fill_with()` operation only succeeds if the bottle
!! `is_carried()` and `is_empty()` and `OBJECT` is either
!! `O5_BLOOD` or `O26_WATER`.
!!
!! This is stricter than the `fill_with_blood()`,
!! `fill_with_water()`, and `clear()` operations by performing
!! higher-level reasonableness checks.
!!
!! @note Refactored from `take_fluid()`
subroutine bottle_fill_with(this, OBJECT)
  use, intrinsic :: iso_fortran_env, only:STDOUT => OUTPUT_UNIT
  use :: cquest_text!, only: cq_msg,                                     &
    ! acqm1008, acqm1074, acqm1099,                                       &
    !  cqm1008,  cqm1074,  cqm1099
  use :: m_format, only: a
  use :: m_object_id, only: O5_BLOOD, O26_WATER
  implicit none
  ! Arguments
  !> Bottle state object
  class(bottle_t), intent(inout) :: this
  !> Object ID
  integer, intent(in) :: OBJECT

  ! Formats
! 1 format('0  Bottle carried? ', L1, ' full? ', L1, ' ; filling with ', I0)
! 1008 format('0  OK')
! 1074 format('0  You have nothing to carry it in.')
! 1099 format('0  The bottle is already full.')
  continue

  ! write(unit=STDOUT, fmt=1) this%is_carried(), this%is_full(), OBJECT

  if (this%is_carried()) then
    if (this%is_full()) then
      ! Can't pick up; bottle is already full of something
      ! write(STDOUT, 1099)
      call a%write_para(cqm_f1099)
      ! if (use_asa) then
      !   write(unit=STDOUT, fmt=acqm1099)
      ! else
      !   write(unit=STDOUT, fmt=cqm1099)
      ! end if
      ! call cq_msg(fmt=cqm1099, afmt=acqm1099)
    else
      select case(OBJECT)
      case(O5_BLOOD)
        call this%fill_with_blood()
        call this%blood%carry()
      case(O26_WATER)
        call this%fill_with_water()
        call this%water%carry()
      end select
      ! write(STDOUT, 1008)
      call a%write_para(cqm_f1008)
      ! if (use_asa) then
      !   write(unit=STDOUT, fmt=acqm1008)
      ! else
      !   write(unit=STDOUT, fmt=cqm1008)
      ! end if
      ! call cq_msg(fmt=cqm1008, afmt=acqm1008)
    end if
  else
    ! Can't pick up; you don't have the bottle
    ! write(STDOUT, 1074)
    call a%write_para(cqm_f1074)
    ! if (use_asa) then
    !   write(unit=STDOUT, fmt=acqm1074)
    ! else
    !   write(unit=STDOUT, fmt=cqm1074)
    ! end if
    ! call cq_msg(fmt=cqm1074, afmt=acqm1074)
  end if

  return
end subroutine bottle_fill_with

!> @brief Set bottle state from logical variables
!!
!! This is used for deserializing (i.e. reconstituting the state of)
!! a `bottle_t` object when loading game save data.
!!
!! @note `BOTTLE`, `BLOOD`, and `WATER` correspond to
!! `LSAVAR(6)`, `LSAVAR(5)`, and `LSAVAR(7)`. This is intended to be called as
!! `call s_bottle%from_logical(LSAVAR(6), LSAVAR(5), LSAVAR(7))`
subroutine bottle_from_logical(this, BOTTLE, BLOOD, WATER)
  use, intrinsic :: iso_fortran_env, only:STDOUT => OUTPUT_UNIT
  implicit none
  ! Arguments
  !> Bottle state object
  class(bottle_t), intent(inout) :: this
  !> Is bottle full?
  logical, intent(in) :: BOTTLE
  !> Does bottle contain blood?
  logical, intent(in) :: BLOOD
  !> Does bottle contain water?
  logical, intent(in) :: WATER
  ! Formats
1 format('(3X, A)')
  continue

  if (BLOOD .and. WATER) then
    ! Problem! Cannot have both blood and water in the bottle.
    write(unit=STDOUT, fmt=1)                                           &
      'ERROR! Cannot have both blood and water in the bottle.'
    stop
  else if ((BLOOD .or. WATER) .and. .not. BOTTLE) then
    ! Problem! Cannot have blood or water and an empty bottle.
    write(unit=STDOUT, fmt=1)                                           &
      'ERROR! Cannot have blood or water and an empty bottle.'
    stop
  else if (BOTTLE .and. .not. (BLOOD .or. WATER)) then
    ! Problem! Cannot have a full bottle without blood or water
    write(unit=STDOUT, fmt=1)                                           &
      'ERROR! Cannot have a full bottle without blood or water.'
    stop
  else
    if (BLOOD) then
      call this%fill_with_blood()
    else if (WATER) then
      call this%fill_with_water()
    else
      call this%clear()
    end if
  end if

  return
end subroutine bottle_from_logical

!> @brief Sets initial state of torch
subroutine torch_init(this, torch_ref)
  implicit none
  !> Torch state object
  class(torch_t), intent(inout) :: this
  !> Reference to `O13_TORCH` item in `itemtracker_t`
  !! (e.g. `itemtracker_t%item(O13_TORCH)`)
  type(item_t), intent(in), target :: torch_ref
  continue
  this%item      => torch_ref
  this%id        = this%item%id
  this%name      = this%item%name
  this%state     = 0
  return
end subroutine torch_init

!> True if the torch is carried by the player (`place` == -1)
logical function torch_is_carried(this) result(is_carried)
  implicit none
  ! Arguments
  !> Torch state object
  class(torch_t), intent(in) :: this
  continue
  if (associated(this%item)) then
    is_carried = this%item%is_carried()
  else
    is_carried = .false.
  end if
  return
end function torch_is_carried

!> True if the torch is off but can be turned on
logical function torch_is_off(this) result(is_off)
  implicit none
  ! Arguments
  !> Torch state object
  class(torch_t), intent(in) :: this
  continue
  is_off = (this%state <= 0)
  return
end function torch_is_off

!> @brief True if the torch is lit
logical function torch_is_lit(this) result(is_lit)
  implicit none
  ! Arguments
  !> Torch state object
  class(torch_t), intent(in) :: this
  continue
  is_lit = (this%state >= 1)
  return
end function torch_is_lit

!> @brief Turn off torch.
subroutine torch_turn_off(this)
  implicit none
  ! Arguments
  !> Torch state object
  class(torch_t), intent(inout) :: this
  continue
  call this%set_state_(0)
  return
end subroutine torch_turn_off

!> @brief Turn on torch.
subroutine torch_turn_on(this)
  implicit none
  ! Arguments
  !> Torch state object
  class(torch_t), intent(inout) :: this
  continue
  call this%set_state_(1)
  return
end subroutine torch_turn_on

!> @brief Set torch state from logical value
!!
!! Intended for deserialization
subroutine torch_from_logical(this, lval)
  implicit none
  ! Arguments
  !> Torch state object
  class(torch_t), intent(inout) :: this
  !> Logical value
  logical, intent(in) :: lval
  continue
  if (lval) then
    call this%turn_on()
  else
    call this%turn_off()
  end if
  return
end subroutine torch_from_logical

!> @brief Sets initial state of window
subroutine window_init(this, name, barred)
  implicit none
  !> Window state object
  class(window_t), intent(inout) :: this
  !> Window name
  character(len=16), intent(in) :: name
  !> Window has bars
  logical, intent(in), optional :: barred
  continue
  this%id        = -1
  this%name      = name

  if (present(barred)) then
    this%has_bars = barred
  else
    this%has_bars = .false.
  end if

  call this%secure()

  return
end subroutine window_init

!> True if the window is nailed shut
logical function window_is_nailed(this) result(is_nailed)
  implicit none
  ! Arguments
  !> Window state object
  class(window_t), intent(in) :: this
  continue
  is_nailed = (this%state <= 0)
  return
end function window_is_nailed

!> @brief True if the window is broken
logical function window_is_broken(this) result(is_broken)
  implicit none
  ! Arguments
  !> Window state object
  class(window_t), intent(in) :: this
  continue
  is_broken = (this%state == 1)
  return
end function window_is_broken

!> @brief True if the window is barred
logical function window_is_barred(this) result(is_barred)
  implicit none
  ! Arguments
  !> Window state object
  class(window_t), intent(in) :: this
  continue
  is_barred = (this%state == 2)
  return
end function window_is_barred

!> @brief True if the window is open
logical function window_is_open(this) result(is_open)
  implicit none
  ! Arguments
  !> Window state object
  class(window_t), intent(in) :: this
  continue
  is_open = (this%state >= 3)
  return
end function window_is_open

!> @brief True if the window is blocked (not open)
logical function window_is_blocked(this) result(is_blocked)
  implicit none
  ! Arguments
  !> Window state object
  class(window_t), intent(in) :: this
  continue
  is_blocked = (.not. this%is_open())
  return
end function window_is_blocked

!> @brief Function returning window description index.
!!
!! @note Value is coerced to lie between 405 and 408; may mask errors   &
!! in window_t%state.
integer function window_des_index(this) result(II)
  implicit none
  ! Arguments

  !> Window object reference
  class(window_t), intent(in) :: this

  continue

  II = 405 + min(max(this%state, 0), 3)

  return
end function window_des_index

!> @brief Function returning window description index in `FORM2()/cq_form2()`.
!!
!! @note Value is coerced to lie between 36 and 39; may mask errors
!! in window_t%state.
integer function window_form2_index(this) result(II)
  implicit none
  ! Arguments

  !> Window object reference
  class(window_t), intent(in) :: this

  continue

  II = 36 + min(max(this%state, 0), 3)

  return
end function window_form2_index

!> @brief Secure window (state 0)
subroutine window_secure(this)
  implicit none
  ! Arguments
  !> Window state object
  class(window_t), intent(inout) :: this
  continue
  call this%set_state_(0)
  return
end subroutine window_secure

!> @brief Break window.
subroutine window_break(this)
  implicit none
  ! Arguments
  !> Window state object
  class(window_t), intent(inout) :: this
  continue
  call this%set_state_(1)
  return
end subroutine window_break

!> @brief Break window.
subroutine window_bar(this)
  implicit none
  ! Arguments
  !> Window state object
  class(window_t), intent(inout) :: this
  continue
  call this%set_state_(2)
  return
end subroutine window_bar

!> @brief Open window.
subroutine window_open(this)
  implicit none
  ! Arguments
  !> Window state object
  class(window_t), intent(inout) :: this
  continue
  call this%set_state_(3)
  return
end subroutine window_open

!> @brief Sets initial state of sun
subroutine sun_init(this)
  implicit none
  !> Sun state object
  class(sun_t), intent(inout) :: this
  continue
  this%id        = -1
  this%name      = 'sun             '

  call this%rise()

  return
end subroutine sun_init

!> True if the sun is up
logical function sun_is_up(this) result(is_up)
  implicit none
  ! Arguments
  !> Sun state object
  class(sun_t), intent(in) :: this
  continue
  is_up = (this%state <= 0)
  return
end function sun_is_up

!> @brief True if the sun is down
logical function sun_is_down(this) result(is_down)
  implicit none
  ! Arguments
  !> Sun state object
  class(sun_t), intent(in) :: this
  continue
  is_down = (this%state >= 1)
  return
end function sun_is_down

!> @brief Change sun state to up (state 0)
subroutine sun_rise(this)
  implicit none
  ! Arguments
  !> Sun state object
  class(sun_t), intent(inout) :: this
  continue
  call this%set_state_(0)
  return
end subroutine sun_rise

!> @brief Change sun state to down (state 1)
subroutine sun_set(this)
  implicit none
  ! Arguments
  !> Sun state object
  class(sun_t), intent(inout) :: this
  continue
  call this%set_state_(1)
  return
end subroutine sun_set

!> @brief Sets initial state of shutter
subroutine shutterpassage_init(this, name)
  implicit none
  !> Shutter state object
  class(shutterpassage_t), intent(inout) :: this
  !> Object name
  character(len=16), intent(in) :: name
  continue
  this%id        = -1
  this%name      = name

  call this%close()

  return
end subroutine shutterpassage_init

!> True if the shutter is closed
logical function shutterpassage_is_closed(this) result(is_closed)
  implicit none
  ! Arguments
  !> Shutter state object
  class(shutterpassage_t), intent(in) :: this
  continue
  is_closed = (this%state <= 0)
  return
end function shutterpassage_is_closed

!> @brief True if the shutter is open
logical function shutterpassage_is_open(this) result(is_open)
  implicit none
  ! Arguments
  !> Shutter state object
  class(shutterpassage_t), intent(in) :: this
  continue
  is_open = (this%state >= 1)
  return
end function shutterpassage_is_open

!> @brief Close shutter (state 0)
subroutine shutterpassage_close(this)
  implicit none
  ! Arguments
  !> Shutter state object
  class(shutterpassage_t), intent(inout) :: this
  continue
  call this%set_state_(0)
  return
end subroutine shutterpassage_close

!> @brief Open shutter (state 1)
subroutine shutterpassage_open(this)
  implicit none
  ! Arguments
  !> Shutter state object
  class(shutterpassage_t), intent(inout) :: this
  continue
  call this%set_state_(1)
  return
end subroutine shutterpassage_open

!> Bulk initialize referenced objects
subroutine gameitems_init_all(this)
  use m_object_id
  implicit none
  ! Arguments
  !> Bulk game items object
  class(gameitems_t), intent(inout) :: this
  continue

  call this%portable%init()
  call this%s_gun%init(this%portable%item(O20_GUN))
  call this%s_lamp%init(this%portable%item(O21_LAMP))
  call this%s_match%init(this%portable%item(O15_MATCH))
  call this%s_note%init()
  call this%s_rope%init(this%portable%item(O9_ROPE))
  call this%s_bottle%init(this%portable%item(O18_BOTTLE),               &
    this%portable%item(O5_BLOOD), this%portable%item(O26_WATER))
  call this%s_torch%init(this%portable%item(O13_TORCH))
  call this%s_window_1%init('window 1        ', .true.)
  call this%s_window_2%init('window 2        ', .false.)
  call this%s_sun%init()
  call this%s_shutter%init('shutter         ')
  call this%s_passage%init('passage         ')

  return
end subroutine gameitems_init_all

end module m_items