!> @file m_object_id.f90
!! @author Michael S. Holtzman
!! @author Mark Kershenblatt
!! @copyright See LICENSE
!! @brief Contains objects ID constants

!> @brief Contains objects ID constants
!!
!! @note All members are public
module m_object_id
  implicit none

  public

  !> Object id of Kerosene
  integer, parameter :: O1_KEROSENE  =  1

  !> Object id of Silver bullet
  integer, parameter :: O2_BULLET    =  2

  !> Object id of Bloody hatchet
  integer, parameter :: O3_AXE       =  3

  !> Object id of Skeleton key
  integer, parameter :: O4_KEY       =  4

  !> Object id of Blood in bottle
  integer, parameter :: O5_BLOOD     =  5

  !> Object id of Wooden stake
  integer, parameter :: O6_STAKE     =  6

  !> Object id of Champagne
  integer, parameter :: O7_CHAMPAGNE =  7

  !> Object id of Hunchback
  integer, parameter :: O8_HUNCHBACK =  8

  !> Object id of Coil of rope
  integer, parameter :: O9_ROPE      =  9

  !> Object id of Writing paper
  integer, parameter :: O10_PAPER     = 10

  !> Object id of Quill pen
  integer, parameter :: O11_PEN       = 11

  !> Object id of Ivory sword
  integer, parameter :: O12_SWORD     = 12

  !> Object id of Acetylene torch
  integer, parameter :: O13_TORCH     = 13

  !> Object id of Rowboat
  integer, parameter :: O14_BOAT      = 14

  !> Object id of Reusable match
  integer, parameter :: O15_MATCH     = 15

  !> Object id of Grappling Hook
  integer, parameter :: O16_HOOK      = 16

  !> Object id of Gold statue
  integer, parameter :: O17_STATUE    = 17

  !> Object id of Empty bottle
  integer, parameter :: O18_BOTTLE    = 18

  !> Object id of Silver cross
  integer, parameter :: O19_CROSS     = 19

  !> Object id of Old gun
  integer, parameter :: O20_GUN       = 20

  !> Object id of Brass lantern
  integer, parameter :: O21_LAMP      = 21

  !> Object id of Tasty food
  integer, parameter :: O22_FOOD      = 22

  !> Object id of Large ruby
  integer, parameter :: O23_RUBY      = 23

  !> Object id of Jade figure
  integer, parameter :: O24_JADE      = 24

  !> Object id of Flask of acid
  integer, parameter :: O25_ACID      = 25

  !> Object id of Water in bottle
  integer, parameter :: O26_WATER     = 26

  !> Object id of Cuban cigar
  integer, parameter :: O27_CIGAR     = 27

  !> Object id of Sapphire
  integer, parameter :: O28_SAPPHIRE  = 28

  !> Object id of Lots of money
  integer, parameter :: O29_MONEY     = 29

  !> Object id of Crystal swan
  integer, parameter :: O30_SWAN      = 30

  ! Non-portable 'nouns'

  !> Noun ID of general object 'all'
  integer, parameter :: N31_ALL       = 31

  !> Noun ID of Combination Lock
  integer, parameter :: N33_LOCK      = 33

  !> Noun ID of Wall of Fire
  integer, parameter :: N34_FIRE      = 34

  !> Noun ID of Window
  integer, parameter :: N37_WINDOW    = 37

  !> Noun ID of Shutters
  integer, parameter :: N38_SHUTTERS  = 38

  !> Noun ID of the Master
  integer, parameter :: N39_MASTER    = 39

  !> Noun ID of Coffin
  integer, parameter :: N40_COFFIN    = 40

  !> Noun ID of Tunnel
  integer, parameter :: N41_TUNNEL    = 41

  !> Lock combination number 1 (L 8)
  integer, parameter :: N42_COMBO1_L8 = 42

  !> Lock combination number 2 (R 31)
  integer, parameter :: N43_COMBO2_R31 = 43

  !> Lock combination number 3 (L 59)
  integer, parameter :: N44_COMBO3_L59 = 44

  !> Noun ID of general rooms
  integer, parameter :: N45_ROOM      = 45

  !> Noun ID of Library Book (Hamlet)
  integer, parameter :: N46_BOOK      = 46

  !> Noun ID of general doors
  integer, parameter :: N47_DOOR      = 47

  !> Noun ID of Drawer
  integer, parameter :: N48_DRAWER    = 48

  !> Noun ID of Butler
  integer, parameter :: N49_BUTLER    = 49

  !> Noun ID of Mirror
  integer, parameter :: N50_MIRROR    = 50

  !> Noun ID of Moat
  integer, parameter :: N51_MOAT      = 51

  !> Noun ID of Butler's note
  integer, parameter :: N52_NOTE      = 52

  !> Noun ID of Window Bars
  integer, parameter :: N54_BARS      = 54

  !> Noun ID of Bat
  integer, parameter :: N55_BAT       = 55

  !> Noun ID of Boards
  integer, parameter :: N56_BOARD     = 56

  !> Mystery Noun ID: Compost, Computer, Competitor?
  ! integer, parameter :: NID_COMP?     = 57

  !> Noun ID of Werewolf
  integer, parameter :: N76_WEREWOLF  = 76

  !> Noun ID of Gnome
  integer, parameter :: N77_GNOME     = 77

  !> Noun ID of Cyclops
  integer, parameter :: N78_CYCLOPS   = 78

  !> Noun ID of Glacier
  integer, parameter :: N79_GLACIER   = 79

  !> Noun ID of Wizard
  integer, parameter :: N80_WIZARD    = 80

  ! Verbs

  !> Verb ID of (move) north
  integer, parameter :: V1_NORTH = 1

  !> Verb ID of (move) northeast
  integer, parameter :: V2_NORTHEAST = 2

  !> Verb ID of (move) east
  integer, parameter :: V3_EAST = 3

  !> Verb ID of (move) southeast
  integer, parameter :: V4_SOUTHEAST = 4

  !> Verb ID of (move) south
  integer, parameter :: V5_SOUTH = 5

  !> Verb ID of (move) southwest
  integer, parameter :: V6_SOUTHWEST = 6

  !> Verb ID of (move) west
  integer, parameter :: V7_WEST = 7

  !> Verb ID of (move) northwest
  integer, parameter :: V8_NORTHWEST = 8

  !> Verb ID of (move) up
  integer, parameter :: V9_UP = 9

  !> Verb ID of (move) down
  integer, parameter :: V10_DOWN = 10

  !> Verb ID of take
  integer, parameter :: V11_TAKE = 11

  !> Verb ID of drop
  integer, parameter :: V12_DROP = 12

  !> Verb ID of (go) enter
  integer, parameter :: V13_ENTER = 13

  !> Verb ID of (go) exit
  integer, parameter :: V14_EXIT = 14

  !> Verb ID of attack
  integer, parameter :: V15_ATTACK = 15

  !> Verb ID of kill
  integer, parameter :: V16_KILL = 16

  !> Verb ID of throw
  integer, parameter :: V17_THROW = 17

  !> Verb ID of load (gun)
  integer, parameter :: V18_LOAD = 18

  !> Verb ID of **ahem**
  integer, parameter :: V19_AHEM = 19

  !> Verb ID of wave
  integer, parameter :: V20_WAVE = 20

  !> Verb ID of stab
  integer, parameter :: V21_STAB = 21

  !> Verb ID of feed
  integer, parameter :: V22_FEED = 22

  !> Verb ID of eat
  integer, parameter :: V23_EAT = 23

  !> Verb ID of drink
  integer, parameter :: V24_DRINK = 24

  !> Verb ID of jump
  integer, parameter :: V25_JUMP = 25

  !> Verb ID of inventory
  integer, parameter :: V26_INVENTORY = 26

  !> Verb ID of open
  integer, parameter :: V27_OPEN = 27

  !> Verb ID of close
  integer, parameter :: V28_CLOSE = 28

  !> Verb ID of lock
  integer, parameter :: V29_LOCK = 29

  !> Verb ID of unlock
  integer, parameter :: V30_UNLOCK = 30

  !> Verb ID of (turn) on
  integer, parameter :: V31_ON = 31

  !> Verb ID of light
  integer, parameter :: V32_LIGHT = 32

  !> Verb ID of (turn) off
  integer, parameter :: V33_OFF = 33

  !> Verb ID of extinguish
  integer, parameter :: V34_EXTINGUISH = 34

  !> Verb ID of look
  integer, parameter :: V35_LOOK = 35

  !> Verb ID of score (query)
  integer, parameter :: V36_SCORE = 36

  !> Verb ID of break
  integer, parameter :: V37_BREAK = 37

  !> Verb ID of water
  integer, parameter :: V38_WATER = 38

  !> Verb ID of pour
  integer, parameter :: V39_POUR = 39

  !> Verb ID of (go) back
  integer, parameter :: V40_BACK = 40

  !> Verb ID of swim
  integer, parameter :: V41_SWIM = 41

  !> Verb ID of melt
  integer, parameter :: V42_MELT = 42

  !> Verb ID of cross
  integer, parameter :: V43_CROSS = 43

  !> Verb ID of quit (game)
  integer, parameter :: V44_QUIT = 44

  !> Verb ID of HONK
  integer, parameter :: V45_HONK = 45

  !> Verb ID of tie
  integer, parameter :: V46_TIE  = 46

  !> Verb ID of untie
  integer, parameter :: V47_UNTIE = 47

  !> Verb ID of read
  integer, parameter :: V48_READ = 48

  !> Verb ID of fill
  integer, parameter :: V49_FILL = 49

  !> Verb ID of help or hint
  integer, parameter :: V50_HELP = 50

  !> Verb ID of goto (debug command)
  integer, parameter :: V51_GOTO = 51

  !> Verb ID of (turn lock) left
  integer, parameter :: V52_LEFT = 52

  !> Verb ID of (turn lock) right
  integer, parameter :: V53_RIGHT = 53

  !> Verb ID of shoot (gun)
  integer, parameter :: V54_SHOOT = 54

  !> Verb ID of wake
  integer, parameter :: V55_WAKE = 55

  !> Verb ID of POOF
  integer, parameter :: V56_POOF = 56

  !> Verb ID of save game
  integer, parameter :: V57_SAVE = 57

  !> Verb ID of restore game
  integer, parameter :: V58_RESTORE = 58

  !> Verb ID of debug (mode)
  integer, parameter :: V59_DEBUG = 59

  !> Verb ID of verbose (description mode)
  integer, parameter :: V60_VERBOSE = 60

  !> Verb ID of brief (description mode)
  integer, parameter :: V61_BRIEF = 61

  ! Room IDs

  !> Room 1
  integer, parameter :: R1_BEDROOM = 1

  !> Room 2
  integer, parameter :: R2_DIM_CORRIDOR = 2

  !> Room 3
  integer, parameter :: R3_PARLOR = 3

  !> Room 4
  integer, parameter :: R4_LOCKED_ROOM = 4

  !> Room 5
  integer, parameter :: R5_DINING_ROOM = 5

  !> Room 6
  integer, parameter :: R6_KITCHEN = 6

  !> Room 7
  integer, parameter :: R7_BRICK_WALL = 7

  !> Room 8
  integer, parameter :: R8_FOYER = 8

  !> Room 9
  integer, parameter :: R9_UPSTAIRS_HALLWAY = 9

  !> Room 10
  integer, parameter :: R10_SMOKING_ROOM = 10

  !> Room 11
  integer, parameter :: R11_WORKSHOP = 11

  !> Room 12
  integer, parameter :: R12_GARDEN = 12

  !> Room 13
  integer, parameter :: R13_LIBRARY = 13

  !> Room 14
  integer, parameter :: R14_LAB = 14

  !> Room 15
  integer, parameter :: R15_BOUDOIR = 15

  !> Room 16
  integer, parameter :: R16_DARK_EW_PASSAGE = 16

  !> Room 17
  integer, parameter :: R17_MIRROR = 17

  !> Room 18
  integer, parameter :: R18_DARK_EW_CORRIDOR = 18

  !> Room 19
  integer, parameter :: R19_DARK_ROOM = 19

  !> Room 20
  integer, parameter :: R20_ATTIC_ENTRANCE = 20

  !> Room 21
  integer, parameter :: R21_ATTIC = 21

  !> Room 22
  integer, parameter :: R22_CEDAR_CLOSET = 22

  !> Room 23
  integer, parameter :: R23_LOW_EW_PASSAGE = 23

  !> Room 24
  integer, parameter :: R24_LAUNDRY_ROOM = 24

  !> Room 25
  integer, parameter :: R25_STORAGE_ROOM = 25

  !> Room 26
  integer, parameter :: R26_SIDE_OF_ROAD = 26

  !> Room 27
  integer, parameter :: R27_MIRROR_MAZE = 27

  !> Room 28
  integer, parameter :: R28_L_CORRIDOR = 28

  !> Room 29
  integer, parameter :: R29_BELOW_WINDOW = 29

  !> Room 30
  integer, parameter :: R30_END_OF_ROAD = 30

  !> Room 31
  integer, parameter :: R31_PANTRY = 31

  !> Room 32
  integer, parameter :: R32_SIDE_OF_MOAT = 32

  !> Room 33
  integer, parameter :: R33_ARMOR_ROOM = 33

  !> Room 34
  integer, parameter :: R34_FORK = 34

  !> Room 35
  integer, parameter :: R35_TORTURE = 35

  !> Room 36
  integer, parameter :: R36_SLAB = 36

  !> Room 37
  integer, parameter :: R37_SPIRAL_STAIRS = 37

  !> Room 38
  integer, parameter :: R38_TOWER = 38

  !> Room 39
  integer, parameter :: R39_BELOW_SMALL_WINDOW = 39

  !> Room 40
  integer, parameter :: R40_FAR_SIDE_OF_MOAT = 40

  !> Room 41
  integer, parameter :: R41_HUGE_ANTEROOM = 41

  !> Room 42
  integer, parameter :: R42_STEEP_LEDGE = 42

  !> Room 43
  integer, parameter :: R43_MASTER_CHAMBER = 43

  !> Room 44
  integer, parameter :: R44_SQUARE_ROOM = 44

  !> Room 45
  integer, parameter :: R45_SLOPING_NS_PASSAGE = 45

  !> Room 46
  integer, parameter :: R46_NARROW_ROOM = 46

  !> Room 47
  integer, parameter :: R47_FIRE_ROOM = 47

  !> Room 48
  integer, parameter :: R48_GLOWING_ROCK_ROOM = 48

  !> Room 49
  integer, parameter :: R49_BLUE_ROOM = 49

  !> Room 50
  integer, parameter :: R50_NARROW_EW_PASSAGE = 50

  !> Room 51
  integer, parameter :: R51_TWISTY_TUNNEL = 51

  !> Room 52
  integer, parameter :: R52_COMPASS_ROOM = 52

  !> Room 53
  integer, parameter :: R53_OUTSIDE_SMALL_ROOM = 53

  !> Room 54
  integer, parameter :: R54_HEART_SHAPED_ARCH = 54

  !> Room 55
  integer, parameter :: R55_HONEYMOON_SUITE = 55

  !> Room 56
  integer, parameter :: R56_EMPTY_ROOM = 56

  !> Room 57
  integer, parameter :: R57_MAZE_SHORT_WINDING = 57

  !> Room 58
  integer, parameter :: R58_LONG_WINDING_MAZE = 58

  !> Room 59
  integer, parameter :: R59_WINDING_LONG_MAZE = 59

  !> Room 60
  integer, parameter :: R60_MAZE_LONG_WINDING = 60

  !> Room 61
  integer, parameter :: R61_MAZE_WINDING_LONG = 61

  !> Room 62
  integer, parameter :: R62_SHORT_WINDING_MAZE = 62

  !> Room 63
  integer, parameter :: R63_WINDING_MAZE_SHORT = 63

  !> Room 64
  integer, parameter :: R64_DEAD_END = 64

  !> Room 65
  integer, parameter :: R65_GLACIER_ROOM = 65

  !> Room 66
  integer, parameter :: R66_LONG_DAMP_TUNNEL = 66

  !> Room 67
  integer, parameter :: R67_ABOVE_WATERFALL = 67

  !> Room 68
  integer, parameter :: R68_BASE_OF_WATERFALL = 68

  !> Room 69
  integer, parameter :: R69_LOBBY = 69

  !> Room 70
  integer, parameter :: R70_ELEVATOR_TOP = 70

  !> Room 71
  integer, parameter :: R71_ELEVATOR_BOTTOM = 71

  !> Room 72
  integer, parameter :: R72_VAULT = 72

  !> Room 73
  integer, parameter :: R73_BURIAL_GROUNDS = 73

  !> Room 74
  integer, parameter :: R74_FOUR_TUNNEL_JUNCTION = 74

  !> Room 75
  integer, parameter :: R75_RAIN_FOREST = 75

  !> Room 76
  integer, parameter :: R76_SMALL_LEDGE = 76

  !> Room 77
  integer, parameter :: R77_GRAFFITI_ROOM = 77

  !> Room 78
  integer, parameter :: R78_DAMP_TUNNEL_EAST = 78

  !> Room 79
  integer, parameter :: R79_DAMP_TUNNEL_WEST = 79

  !> Room 80
  integer, parameter :: R80_WINE_CELLAR = 80

  !> Room 81
  integer, parameter :: R81_ISLAND = 81

  !> Room 82
  integer, parameter :: R82_IMMENSE_CAVERN_NORTH = 82

  !> Room 83
  integer, parameter :: R83_LARGE_CAVERN_SOUTH = 83

  !> Room 84
  integer, parameter :: R84_TOP_OF_PRECIPICE = 84

  !> Room 85
  integer, parameter :: R85_DISCO_ROOM = 85

  !> Room 86
  integer, parameter :: R86_TALL_TUNNEL = 86

  !> Room 87
  integer, parameter :: R87_LIVING_DEAD = 87

  !> Room 88
  integer, parameter :: R88_CYCLOPS_LAIR = 88

  !> Room 89
  integer, parameter :: R89_INSIDE_GLACIER = 89

  !> Room 90
  integer, parameter :: R90_COLD_TUNNEL = 90

  !> Room 91
  integer, parameter :: R91_BORDER = 91

  !> Room 92
  integer, parameter :: R92_FOREST = 92

  !> Room 93
  integer, parameter :: R93_THRONE_ROOM = 93

  !> Room 94
  integer, parameter :: R94_WIZARDS_CACHE = 94

  !> Room 95
  integer, parameter :: R95_WAREHOUSE_MAIN = 95

  !> Room 96
  integer, parameter :: R96_WAREHOUSE_WEST = 96

  !> Room 97
  integer, parameter :: R97_WAREHOUSE_NORTH = 97

  !> Room 98
  integer, parameter :: R98_WAREHOUSE_EAST = 98

  !> Room 99
  integer, parameter :: R99_ELEVATOR_BETWEEN_FLOORS = 99

  !> Room 100
  integer, parameter :: R100_ENDGAME = 100

end module m_object_id
