!> @file m_player.f90
!! @author Michael S. Holtzman
!! @author Mark Kershenblatt
!! @copyright See LICENSE
!! @brief Contains item-related objects, functions, and data

!> @brief Contains item-related objects, functions, and data
module m_player
  use :: m_object_id
  use :: m_score, only: score_t
  implicit none
  private

  public :: player_t

  !> @class player_t
  !! Player attribute class containing location, movement, encumbrance,
  !! and life status as well as communication mode (brief, verbose, debug)
  type :: player_t
    !> Current or intended location in map
    integer :: room       = R1_BEDROOM
    !> Previous or initial location before movement attempt
    integer :: last_room  = R1_BEDROOM
    !> Backup copy of previous location before movement attempt.
    integer :: rollback_  = R1_BEDROOM
    !> Number of moves made by player
    integer :: moves      = 0
    !> Number of items held by player. Note that a loaded gun or a
    !! filled bottle count as a single item
    integer :: items_held = 0
    !> Number of deaths experienced
    integer :: ndeaths    = 0
    !> Display brief descriptions if true
    logical :: brief_mode = .false.
    !> Score object
    type(score_t) :: score
    !> State of each hidden letter
    logical, dimension(4) :: revealed_letter = .false.
    !> Parts of combination lock entered successfully (not part of save vector)
    integer :: nlockparts_good = 0
  contains
    !> Initialize player object
    procedure :: init => player_init
    !> Translate `brief_mode` to an integer (`.true.` = 1,
    !! `.false.` = 0, )
    procedure :: brief_to_int => player_brief_to_int
    !> Set 'brief' mode based on legacy integer value
    procedure :: set_brief_from_int => player_set_brief_from_int
    !> Enable 'brief' mode
    procedure :: set_brief => player_set_brief
    !> Enable 'verbose' mode; disable 'brief' mode
    procedure :: clear_brief => player_clear_brief
    !> Gain item(s); increase inventory count
    procedure :: gain_item => player_gain_item
    !> Lose item(s); decrease inventory count
    procedure :: lose_item => player_lose_item
    !> True if player is at or above their encumbrance limit (10 items)
    procedure :: is_encumbered => player_is_encumbered
    !> Record one move for the player
    procedure :: advance => player_advance
    !> Move player to new room and update previous room
    procedure :: move_to => player_move_to
    !> Reverts last move
    procedure :: rollback_move => player_rollback_move
    !> True if player advanced to the given room
    procedure :: is_in => player_is_in
    !> True if player advanced from the given room
    procedure :: was_in => player_was_in
    !> True if player enters a room via a specific route
    procedure :: route_is => player_route_is
  end type player_t

contains

!> Initialize player object
subroutine player_init(this)
  implicit none

  !> Player object reference
  class(player_t), intent(inout) :: this

  continue

  this%room            = R1_BEDROOM
  this%last_room       = R1_BEDROOM
  this%rollback_       = R1_BEDROOM
  this%moves           = 0
  this%items_held      = 0
  this%ndeaths         = 0
  this%brief_mode      = .false.
  this%revealed_letter = .false.
  this%nlockparts_good = 0
  call this%score%init()

  return
end subroutine player_init

!> @brief Translate `brief_mode` to an integer (`.true.` = 1,
!! `.false.` = 0, )
integer function player_brief_to_int(this) result(ival)
  implicit none

  !> Player object reference
  class(player_t), intent(in) :: this

  continue

  if (this%brief_mode) then
    ival = 1
  else
    ival = 0
  end if

  return
end function player_brief_to_int

!> Enable 'brief' mode
subroutine player_set_brief_from_int(this, ival)
  implicit none

  !> Player object reference
  class(player_t), intent(inout) :: this

  !> Legacy integer value of IBRIEF
  integer, intent(in) :: ival

  continue

  this%brief_mode = (ival /= 0)

  return
end subroutine player_set_brief_from_int

!> Enable 'brief' mode
subroutine player_set_brief(this)
  implicit none

  !> Player object reference
  class(player_t), intent(inout) :: this

  continue

  this%brief_mode = .true.

  return
end subroutine player_set_brief

!> Enable 'verbose' mode; disable 'brief' mode
subroutine player_clear_brief(this)
  implicit none

  !> Player object reference
  class(player_t), intent(inout) :: this

  continue

  this%brief_mode = .false.

  return
end subroutine player_clear_brief

!> Acquire item and increment item count
subroutine player_gain_item(this, count)
  implicit none

  !> Player object reference
  class(player_t), intent(inout) :: this
  !> Number of items gained
  integer, intent(in), optional :: count

  continue

  if (present(count)) then
    this%items_held = this%items_held + count
  else
    this%items_held = this%items_held + 1
  end if

  return
end subroutine player_gain_item

!> Reduce inventory
subroutine player_lose_item(this, count)
  implicit none

  !> Player object reference
  class(player_t), intent(inout) :: this
  !> Number of items lost
  integer, intent(in), optional :: count

  continue

  if (present(count)) then
    this%items_held = this%items_held - count
  else
    this%items_held = this%items_held - 1
  end if

  return
end subroutine player_lose_item

!> True if player is at or above their encumbrance limit (10 items)
logical function player_is_encumbered(this) result(is_encumbered)
  implicit none

  !> Player object reference
  class(player_t), intent(in) :: this

  continue

  is_encumbered = (this%items_held >= 10)

  return
end function player_is_encumbered

!> Record one move for the player
subroutine player_advance(this)
  implicit none

  !> Player object reference
  class(player_t), intent(inout) :: this

  continue

  this%moves = this%moves + 1

  return
end subroutine player_advance

!> Record one move for the player
subroutine player_move_to(this, to, from, save_last, with_advance)
  implicit none

  !> Player object reference
  class(player_t), intent(inout) :: this
  !> Destination room ID
  integer, intent(in) :: to
  !> Origin room ID (`last_room`). Optional.
  integer, intent(in), optional :: from
  !> Saves `last_room` as `rollback_` if true. Optional. Defaults
  !! to false  if not specified.
  logical, intent(in), optional :: save_last
  !> Increments move counter if true. Optional. Defaults to false
  !! (no increment) if not specified.
  logical, intent(in), optional :: with_advance

  continue

  if (present(save_last)) then
    if (save_last) then
      this%rollback_ = this%last_room
    end if
  end if

  ! Set this%last_room to 'from' if supplied, to this%room if not
  if (present(from)) then
    this%last_room = from
  else
    this%last_room = this%room
  end if

  ! Set new location
  this%room = to

  if (present(with_advance)) then
    if (with_advance) then
      ! Increment move counter
      call this%advance()
    end if
  end if

  return
end subroutine player_move_to

!> @brief Revert last move
!!
!! `room` is reverted to `last_room`, `last_room` is reverted to the
!! value it held the last time `move_to` was called with
!! `save_last=.true.`
subroutine player_rollback_move(this)
  implicit none

  !> Player object reference
  class(player_t), intent(inout) :: this

  continue

  this%room = this%last_room
  this%last_room = this%rollback_

  return
end subroutine player_rollback_move

!> True if player advanced to the given room
logical function player_is_in(this, here) result(is_in)
  implicit none
  !> Player object reference
  class(player_t), intent(in) :: this
  !> Room ID
  integer, intent(in) :: here
  continue

  is_in = (this%room == here)

  return
end function player_is_in

!> True if player advanced from the given room
logical function player_was_in(this, there) result(was_in)
  implicit none
  !> Player object reference
  class(player_t), intent(in) :: this
  !> Room ID
  integer, intent(in) :: there
  continue

  was_in = (this%last_room == there)

  return
end function player_was_in

!> True if player enters a room via a specific route
logical function player_route_is(this, to, from, both_ways)  &
  result(route_is)
  implicit none
  !> Player object reference
  class(player_t), intent(in) :: this
  !> Destination room ID
  integer, intent(in) :: to
  !> Origin room ID (`last_room`)
  integer, intent(in) :: from
  !> Flag indicating both `from -> to` and `to -> from` are
  !! considered. Optional.
  logical, intent(in), optional :: both_ways
  ! Local variables
  logical :: check_all
  continue

  if (present(both_ways)) then
    check_all = both_ways
  else
    check_all = .false.
  end if

  route_is = (this%is_in(to) .and. this%was_in(from))

  if (check_all) then
    ! Only check for reverse transit if we know forward transit is false
    if (.not. route_is) then
      route_is = (this%is_in(from) .and. this%was_in(to))
    end if
  end if

  return
end function player_route_is

end module m_player