!> @file m_random.f90
!! @author Michael S. Holtzman
!! @author Mark Kershenblatt
!! @copyright See LICENSE
!! @brief Contains randomization routines

!> @brief Contains randomization routines
module m_random
    implicit none
    private

    public :: RSTART
    public :: RDM

contains

!> @brief Randomizer seeding routine
subroutine RSTART()
  implicit none
  continue

  call random_init(repeatable=.false., image_distinct=.false.)

  call random_seed()

  return
end subroutine RSTART

!> @brief Random number generator function
!!
!! @returns Pseudo-random number
real function RDM()
  implicit none

  real :: R

  continue

  call random_number(harvest=r)
  rdm = r

  return
end function RDM

end module m_random
