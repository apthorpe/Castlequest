!> @file m_score.f90
!! @author Michael S. Holtzman
!! @author Mark Kershenblatt
!! @copyright See LICENSE
!! @brief Contains player score classes and routines

!> @brief Contains player score classes and routines
module m_score
implicit none

private

public :: score_t

!> @class score_t
!!
!! Player score class
type :: score_t
  ! Current player score
  integer :: SCORE = 0
  !> Current point award for finding lock combination
  integer :: NOTVAL = 10
  !> Current point value for opening attic combination lock
  integer :: LOKVAL = 5
  !> Current point value for reading butler's note
  integer :: BUTVAL = 5
  !> Current point value for clearing a path out the bedroom window
  integer :: ROPVAL = 15
  !> Maximum possible treasure score; this may decrease if some
  !! treasures become unavailable due to loss or premature use of
  !! inventory items
  integer :: MMAX = 99
  !> Maximum possible game score
  integer :: MAXSCR = 300
  !> Maximum game score (?)
  integer :: MAXABS = 300
contains
  !> Initialize score object
  procedure :: init => score_init
  !> Apply bedroom window score bonus
  procedure :: apply_rope_score => score_apply_rope_score
  !> Apply butler's note score bonus
  procedure :: apply_butler_score => score_apply_butler_score
  !> Apply kitchen lock combination score bonus
  procedure :: apply_combination_score => score_apply_combination_score
  !> Apply attic combination lock score bonus
  procedure :: apply_unlock_score => score_apply_unlock_score
  !> Reduce MMAX by given amount
  procedure :: reduce_mmax => score_reduce_mmax
  !> Increase score by given amount (may be negative in case of death or hints)
  procedure :: add_to_score => score_add_to_score
  !> Show score and ranking on exit.
  procedure :: display_rank => score_display_rank
end type score_t

contains

!> Initialize score object
subroutine score_init(this)
  implicit none
  !> Score object reference
  class(score_t), intent(inout) :: this
  continue
  this%SCORE  = 0
  this%ROPVAL = 10
  this%BUTVAL = 5
  this%NOTVAL = 15
  this%LOKVAL = 5
  this%MMAX   = 99
  this%MAXSCR = 300
  this%MAXABS = this%MAXSCR
  return
end subroutine score_init

!> Apply bedroom window score bonus
subroutine score_apply_rope_score(this)
  implicit none
  !> Score object reference
  class(score_t), intent(inout) :: this
  continue
  call this%add_to_score(this%ROPVAL)
  this%ROPVAL = 0
  return
end subroutine score_apply_rope_score

!> Apply butler's note score bonus
subroutine score_apply_butler_score(this)
  implicit none
  !> Score object reference
  class(score_t), intent(inout) :: this
  continue
  call this%add_to_score(this%BUTVAL)
  this%BUTVAL = 0
  return
end subroutine score_apply_butler_score

!> Apply kitchen lock combination score bonus
subroutine score_apply_combination_score(this)
  implicit none
  !> Score object reference
  class(score_t), intent(inout) :: this
  continue
  call this%add_to_score(this%NOTVAL)
  this%NOTVAL = 0
  return
end subroutine score_apply_combination_score

!> Apply attic combination lock score bonus
subroutine score_apply_unlock_score(this)
  implicit none
  !> Score object reference
  class(score_t), intent(inout) :: this
  continue
  call this%add_to_score(this%LOKVAL)
  this%LOKVAL = 0
  return
end subroutine score_apply_unlock_score

!> Reduce MMAX by given amount
subroutine score_reduce_mmax(this, penalty)
  implicit none
  !> Score object reference
  class(score_t), intent(inout) :: this
  !> Decrement value
  integer, intent(in) :: penalty
  continue
  this%MMAX = this%MMAX - penalty
  return
end subroutine score_reduce_mmax

!> Increase score by given amount; note: may be negative in case of
!! death or hints
subroutine score_add_to_score(this, bonus)
  implicit none
  !> Score object reference
  class(score_t), intent(inout) :: this
  !> Increment value
  integer, intent(in) :: bonus
  continue
  this%SCORE = this%SCORE + bonus
  return
end subroutine score_add_to_score

!> @brief Show score and ranking on exit.
subroutine score_display_rank(this, move_and_item_score)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: m_format, only: a
  implicit none

  ! Parameters
  character(len=*), parameter :: cqm_s7001 =                            &
    '  This qualifies you as a "CLASS A" MASTER!'
  character(len=*), parameter :: cqm_s7002 =                            &
    '  You are a MASTER at CASTLEQUEST.'
  character(len=*), parameter :: cqm_s7003 =                            &
    '  You receive an EXPERT rating for your effort.'
  character(len=*), parameter :: cqm_s7004 =                            &
    '  You rate as a NOVICE EXPLORER for this game.'
  character(len=*), parameter :: cqm_s7005 =                            &
    '  You are a GREENHORN at this game!!'
  character(len=*), parameter :: cqm_s7006 =                            &
    '  You don''t deserve to WALK THE EARTH!!'

  ! Arguments

  !> Score object
  class(score_t), intent(in) :: this

  !> Partial score derived from item locations and number of moves
  !! taken. Typically obtained from `portable%total_score(NUMOVE)`
  integer, intent(in) :: move_and_item_score

  ! Local variables

  integer :: II
  integer :: JJ
  integer :: K
  integer :: L
  character(len=80) :: msg

  ! Formats

  9997 format('  You scored ', I4, ' out of ', I4, ' points.')

  continue

  !     ---END OF GAME (WINNER)---

  II = this%SCORE + move_and_item_score! portable%total_score(NUMOVE)

  write(unit=msg, fmt=9997) II, this%MAXABS
  call a%write_para(msg)

  if (II >= this%MAXABS) then
    call a%write_para(cqm_s7001)
  end if

  JJ = this%MAXABS / 4
  K = this%MAXABS - JJ

  if (II < this%MAXABS .and. II >= K) then
    call a%write_para(cqm_s7002)
  end if

  L = K - JJ

  if (II < K      .and. II >= L) then
    call a%write_para(cqm_s7003)
  end if

  K = L - JJ

  if (II < L      .and. II >= K) then
    call a%write_para(cqm_s7004)
  end if

  L = K - JJ

  if (II < K      .and. II >= L) then
    call a%write_para(cqm_s7005)
  end if

  if (II < L) then
    call a%write_para(cqm_s7006)
  end if

  return
end subroutine score_display_rank

end module m_score
