!> @file m_stateful.f90
!! @author Michael S. Holtzman
!! @author Mark Kershenblatt
!! @copyright See LICENSE
!! @brief Track actor and object state and basic properties
!--------------------------------------------------------------
!> @brief Track actor and object state and basic properties
module m_stateful
  implicit none

  private

  public :: stateful_t

  !> Basic properties of stateful actors and items
  type :: stateful_t
    !> Name
    character(len=16) :: name = 'ZZZZZZZZZZZZZZZZ'
    !> Identifier/index
    integer :: id = -1
    !> State, implemented as integer
    !!
    !! @note Should be private; does access by inheritance fail across module boundaries?
    integer :: state = -1
  contains
    !> Get accessor for state
    procedure :: get_state_ => stateful_get_state
    !> Set accessor for state
    procedure :: set_state_ => stateful_set_state
  end type stateful_t

contains

!> @brief Get accessor for state
!!
!! Needed for testing and creating game save file
integer function stateful_get_state(this) result (istate)
  implicit none
  !> Stateful object reference
  class(stateful_t), intent(in) :: this
  continue
  istate = this%state
  return
end function stateful_get_state

!> @brief Set accessor for state
!!
!! Needed for testing and loading game save file
subroutine stateful_set_state(this, istate)
  implicit none
  !> Stateful object reference
  class(stateful_t), intent(inout) :: this
  !> Literal state value
  integer, intent(in) :: istate
  continue
  this%state = istate
  return
end subroutine stateful_set_state

end module m_stateful
