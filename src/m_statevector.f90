!> @file m_statevector.f90
!! @author Michael S. Holtzman
!! @author Mark Kershenblatt
!! @copyright See LICENSE
!! @brief Contains variables from COMMON block /BLOCK2/

!> @brief Contains game state; player save vector
!!
!! Originally these were variables from COMMON block /BLOCK2/
module m_statevector
  implicit none

  public :: state_vector_t

  private

  ! Name of save file
  character(len=*), parameter :: DEFAULT_SAVEFILE = 'CQUEST.SAV'

  !> Maximum number of elements in integer state (player save) vector
  integer, parameter, public :: NSAVAR = 400

  !> Number of logical variables formerly stored in `SAVAR` but migrated
  !! to `LSAVAR`.
  integer, parameter, public :: NPATCHES = 18

  !> Positions in `SAVAR` which should be overwritten by corresponding
  !! member of `LSAVAR`. Avoids `EQUIVALENCE`ing logicals to integers.
  integer, dimension(NPATCHES), parameter, public :: IPATCH = (/        &
     63,  64,  85,  86,  91,  92,  93, 205, 206, 207,                   &
    208, 209, 210, 211, 212, 213, 214, 215 /)

!   ! Output TRANSFER():
!   do i = 1, NPATCHES
!     SAVAR(IPATCH(i)) = transfer(source=LSAVAR(i), mold=SAVAR(1))
!   end do

! ! Input TRANSFER():
!   do i = 1, NPATCHES
!     LSAVAR(i) = transfer(source=SAVAR(IPATCH(i)), mold=LSAVAR(1))
!   end do

  ! associate(                                                            &
  !   GUN    => LSAVAR( 1),                                               &
  !   BAT    => LSAVAR( 2),                                               &
  !   FIRE   => LSAVAR( 3),                                               &
  !   PREC   => LSAVAR( 4),                                               &
  !   BLOOD  => LSAVAR( 5),                                               &
  !   BOTTLE => LSAVAR( 6),                                               &
  !   WATER  => LSAVAR( 7),                                               &
  !   GNOME  => LSAVAR( 8),                                               &
  !   WOLF   => LSAVAR( 9),                                               &
  !   HOLE   => LSAVAR(10),                                               &
  !   MELT   => LSAVAR(11),                                               &
  !   TORCH  => LSAVAR(12),                                               &
  !   WIZ    => LSAVAR(13),                                               &
  !   MASECT => LSAVAR(14),                                               &
  !   MAS1   => LSAVAR(15),                                               &
  !   MAS2   => LSAVAR(16),                                               &
  !   MAS3   => LSAVAR(17),                                               &
  !   MAS4   => LSAVAR(18) )

  ! associate(                                                            &
  !   ITEMS  => SAVAR(1:30),                                              &
  !   IVALUE => SAVAR(31:60),                                             &
  !   ROOM   => SAVAR( 61),                                               &
  !   LROOM  => SAVAR( 62),                                               &
  !   GUN    => SAVAR( 63),                                               &
  !   BAT    => SAVAR( 64),                                               &
  !   BUT    => SAVAR( 65),                                               &
  !   SHUTTR => SAVAR( 66),                                               &
  !   ROPE   => SAVAR( 67),                                               &
  !   HUNCH  => SAVAR( 68),                                               &
  !   MATCH  => SAVAR( 69),                                               &
  !   MASTER => SAVAR( 70),                                               &
  !   WHER   => SAVAR(71:80),                                             &
  !   NOTVAL => SAVAR( 81),                                               &
  !   LOKVAL => SAVAR( 82),                                               &
  !   BUTVAL => SAVAR( 83),                                               &
  !   ROPVAL => SAVAR( 84),                                               &
  !   FIRE   => SAVAR( 85),                                               &
  !   PREC   => SAVAR( 86),                                               &
  !   WIND1  => SAVAR( 87),                                               &
  !   WIND2  => SAVAR( 88),                                               &
  !   SCORE  => SAVAR( 89),                                               &
  !   NUMOVE => SAVAR( 90),                                               &
  !   BLOOD  => SAVAR( 91),                                               &
  !   BOTTLE => SAVAR( 92),                                               &
  !   WATER  => SAVAR( 93),                                               &
  !   SUN    => SAVAR( 94),                                               &
  !   NUMB   => SAVAR( 95),                                               &
  !   NOTE   => SAVAR( 96),                                               &
  !   IPASS  => SAVAR( 97),                                               &
  !   LAMP   => SAVAR( 98),                                               &
  !   MMOVE  => SAVAR( 99),                                               &
  !   LMOVE  => SAVAR(100),                                               &
  !   DOOR   => SAVAR(101:200),                                           &
  !   NDEATH => SAVAR(201),                                               &
  !   MMAX   => SAVAR(202),                                               &
  !   MAXSCR => SAVAR(203),                                               &
  !   IBRIEF => SAVAR(204),                                               &
  !   GNOME  => SAVAR(205),                                               &
  !   WOLF   => SAVAR(206),                                               &
  !   HOLE   => SAVAR(207),                                               &
  !   MELT   => SAVAR(208),                                               &
  !   TORCH  => SAVAR(209),                                               &
  !   WIZ    => SAVAR(210),                                               &
  !   MASECT => SAVAR(211),                                               &
  !   MAS1   => SAVAR(212),                                               &
  !   MAS2   => SAVAR(213),                                               &
  !   MAS3   => SAVAR(214),                                               &
  !   MAS4   => SAVAR(215) )
  ! end associate

!> Single logical value stored as integer in state vector;
!! replaces elements of IPATCH, and LSAVAR
type :: lpatch_t
  !> Integer array index of logical value stored as integer
  !! (index into `SAVAR`)
  integer :: id   = -1
  !> Logical value
  logical :: lval = .false.
! contains
end type lpatch_t

!> Game state vector for serialization
type :: state_vector_t
  ! Name of save file
  character(len=80) :: savefile = DEFAULT_SAVEFILE

  !> Game state; player save vector
  integer, dimension(NSAVAR) :: SAVAR = 0

  !> Logical values stored as integers; replacement for IPATCH and
  !! LSAVAR arrays
  type(lpatch_t), dimension(NPATCHES) :: lpatch = (/                    &
    lpatch_t(id=63 ),                                                   &
    lpatch_t(id=64 ),                                                   &
    lpatch_t(id=85 ),                                                   &
    lpatch_t(id=86 ),                                                   &
    lpatch_t(id=91 ),                                                   &
    lpatch_t(id=92 ),                                                   &
    lpatch_t(id=93 ),                                                   &
    lpatch_t(id=205),                                                   &
    lpatch_t(id=206),                                                   &
    lpatch_t(id=207),                                                   &
    lpatch_t(id=208),                                                   &
    lpatch_t(id=209),                                                   &
    lpatch_t(id=210),                                                   &
    lpatch_t(id=211),                                                   &
    lpatch_t(id=212),                                                   &
    lpatch_t(id=213),                                                   &
    lpatch_t(id=214),                                                   &
    lpatch_t(id=215)                                                    &
  /)
contains
  !> Initialize state vector
  procedure :: init => svec_init
  !> Read state vector from file
  procedure :: read_file => svec_read_file
  !> Write state vector to file
  procedure :: write_file => svec_write_file
  !> Restore game objects from file
  procedure :: restore_game => svec_restore_game
  !> Save game objects to file
  procedure :: save_game => svec_save_game
end type state_vector_t

contains

!> @brief Build save vector from objects
subroutine obj_to_svec(player, map, items, encounters, SAVAR)
  use :: m_encounter, only: gameencounters_t
  use :: m_items, only: NITEMS, gameitems_t
  use :: m_player, only: player_t
  use :: m_where, only: NROOMS, dungeon_t
  implicit none

  !> Reference to player
  type(player_t), intent(in) :: player

  !> Reference to map
  type(dungeon_t), intent(in) :: map

  !> Bulk game items object
  type(gameitems_t), intent(in) :: items

  !> Bulk live encounters object
  type(gameencounters_t), intent(in) :: encounters

  !> Game state; player save vector
  integer, dimension(NSAVAR), intent(out) :: SAVAR

  ! Local variables
  integer :: J
  logical, dimension(NPATCHES) :: LSAVAR

  continue

  ! Clear save vector
  SAVAR = 0
  LSAVAR = .false.

  ! Initialize save vector from objects
  SAVAR(1:NITEMS) = items%portable%item(1:NITEMS)%place
  SAVAR(NITEMS+1:2*NITEMS) = items%portable%item(1:NITEMS)%value
  SAVAR( 61) = player%room
  SAVAR( 62) = player%last_room
  SAVAR( 65) = encounters%a_butler%get_state_()
  SAVAR( 66) = items%s_shutter%get_state_()
  SAVAR( 67) = items%s_rope%get_state_()
  SAVAR( 68) = encounters%a_hunchback%get_state_()
  SAVAR( 69) = items%s_match%get_state_()
  SAVAR( 70) = encounters%a_master%get_state_()
  SAVAR(71:80) = map%mirror_maze%state_to_array()

  SAVAR( 81) = player%score%NOTVAL
  SAVAR( 82) = player%score%LOKVAL
  SAVAR( 83) = player%score%BUTVAL
  SAVAR( 84) = player%score%ROPVAL

  SAVAR( 87) = items%s_window_1%get_state_()
  SAVAR( 88) = items%s_window_2%get_state_()
  SAVAR( 89) = player%score%SCORE
  SAVAR( 90) = player%moves
  SAVAR( 94) = items%s_sun%get_state_()
  SAVAR( 95) = player%items_held
  SAVAR( 96) = items%s_note%get_state_()
  SAVAR( 97) = items%s_passage%get_state_()
  SAVAR( 98) = items%s_lamp%get_state_()
  SAVAR( 99) = items%s_match%turns_lit
  SAVAR(100) = items%s_lamp%turns_lit

  SAVAR(101:100+NROOMS) = map%room(1:NROOMS)%door%state_
  SAVAR(201) = player%ndeaths
  SAVAR(202) = player%score%MMAX
  SAVAR(203) = player%score%MAXSCR
  SAVAR(204) = player%brief_to_int()

  LSAVAR(1)  = items%s_gun%to_logical()
  LSAVAR(2)  = encounters%a_bat%to_logical()
  LSAVAR(3)  = map%fire_blocks_way
  LSAVAR(4)  = map%path_to_precipice
  LSAVAR(5)  = items%s_bottle%contains_blood()
  LSAVAR(6)  = items%s_bottle%is_full()
  LSAVAR(7)  = items%s_bottle%contains_water()
  LSAVAR(8)  = encounters%a_gnome%to_logical()
  LSAVAR(9)  = encounters%a_werewolf%to_logical()
  LSAVAR(10) = encounters%a_cyclops%to_logical()
  LSAVAR(11) = map%path_through_glacier
  LSAVAR(11) = items%s_torch%is_lit()
  LSAVAR(13) = encounters%a_wizard%to_logical()
  LSAVAR(14) = map%path_through_final_door
  LSAVAR(15:18) = player%revealed_letter(1:4)

  ! Output TRANSFER(): Cast assorted logicals to ints at start
  do J = 1, NPATCHES
    SAVAR(IPATCH(J)) = transfer(source=LSAVAR(J), mold=SAVAR(1))
  end do

  return
end subroutine obj_to_svec

!> @brief Build save vector from objects
subroutine svec_to_obj(player, map, items, encounters, SAVAR)
  use :: m_encounter, only: gameencounters_t
  use :: m_items, only: NITEMS, gameitems_t
  use :: m_object_id
  use :: m_player, only: player_t
  use :: m_where, only: NROOMS, dungeon_t
  implicit none

  !> Reference to player
  type(player_t), intent(out) :: player

  !> Reference to map
  type(dungeon_t), intent(out) :: map

  !> Bulk game items object
  type(gameitems_t), intent(inout) :: items

  !> Bulk live encounters object
  type(gameencounters_t), intent(inout) :: encounters

  !> Game state; player save vector
  integer, dimension(NSAVAR), intent(in) :: SAVAR

  ! Local variables
  logical, dimension(NPATCHES) :: LSAVAR
  integer :: J

  continue

  LSAVAR = .false.
  ! Input TRANSFER(): Cast assorted ints to logicals
  do J = 1, NPATCHES
    LSAVAR(J) = transfer(source=SAVAR(IPATCH(J)), mold=LSAVAR(1))
  end do

  ! Initialize all the things
  call player%init()
  call map%init()
  call items%init_all()
  call encounters%init_all()

  ! Initialize objects from save vector
  player%room          = SAVAR( 61)
  player%last_room     = SAVAR( 62)
  player%moves         = SAVAR( 90)
  player%items_held    = SAVAR( 95)
  player%ndeaths       = SAVAR(201)
  call player%set_brief_from_int(SAVAR(204))
  player%score%NOTVAL  = SAVAR(81)
  player%score%LOKVAL  = SAVAR(82)
  player%score%BUTVAL  = SAVAR(83)
  player%score%ROPVAL  = SAVAR(84)
  player%score%SCORE   = SAVAR(89)
  player%score%MMAX    = SAVAR(202)
  player%score%MAXSCR  = SAVAR(203)
  player%revealed_letter(1:4) = LSAVAR(15:18)

  call encounters%a_bat%from_logical(LSAVAR(2))
  call encounters%a_butler%set_state_(SAVAR(65))
  call encounters%a_cyclops%from_logical(LSAVAR(10))
  call encounters%a_gnome%from_logical(LSAVAR(8))
  call encounters%a_hunchback%set_state_(SAVAR(68))
  call encounters%a_master%set_state_(SAVAR(70))
  call encounters%a_werewolf%from_logical(LSAVAR(9))
  call encounters%a_wizard%from_logical(LSAVAR(13))

  items%portable%item(1:NITEMS)%place = SAVAR(1:NITEMS)
  items%portable%item(1:NITEMS)%value = SAVAR(NITEMS+1:2*NITEMS)
  call items%s_rope%set_state_(SAVAR(67))
  call items%s_lamp%set_state_(SAVAR(98))
  items%s_lamp%turns_lit = SAVAR(100)
  call items%s_match%set_state_(SAVAR(69))
  items%s_match%turns_lit = SAVAR(99)
  call items%s_note%set_state_(SAVAR(96))
  call items%s_window_1%set_state_(SAVAR(87))
  call items%s_window_2%set_state_(SAVAR(88))
  call items%s_sun%set_state_(SAVAR(94))
  call items%s_shutter%set_state_(SAVAR(66))
  call items%s_passage%set_state_(SAVAR(97))
  call items%s_gun%from_logical(LSAVAR(1))
  call items%s_bottle%from_logical(LSAVAR(6), LSAVAR(5), LSAVAR(7))
  call items%s_torch%from_logical(LSAVAR(12))

  map%room(1:NROOMS)%door%state_ = SAVAR(101:100+NROOMS)
  call map%mirror_maze%array_to_state(SAVAR(71:80))
  map%path_through_glacier    = LSAVAR(11)
  map%fire_blocks_way         = LSAVAR(3)
  map%path_to_precipice       = LSAVAR(4)
  map%path_through_final_door = LSAVAR(14)

  return
end subroutine svec_to_obj

!> Initialize state vector with optional filename
subroutine svec_init(this, filename)
  implicit none

  ! Arguments

  !> State vector object
  class(state_vector_t), intent(inout) :: this

  !> Save file name, optional
  character(len=*), intent(in), optional :: filename

  continue

  this%savefile = DEFAULT_SAVEFILE
  if (present(filename)) then
    ! Minimum filename length is 5 (must be at least as long as A.SAV)
    if (len_trim(adjustl(filename)) > 5) then
      this%savefile = trim(adjustl(filename))
    end if
  end if

  this%SAVAR = 0

  this%lpatch = (/                                                      &
    lpatch_t(63,  .false.),                                             &
    lpatch_t(64,  .false.),                                             &
    lpatch_t(85,  .false.),                                             &
    lpatch_t(86,  .false.),                                             &
    lpatch_t(91,  .false.),                                             &
    lpatch_t(92,  .false.),                                             &
    lpatch_t(93,  .false.),                                             &
    lpatch_t(205, .false.),                                             &
    lpatch_t(206, .false.),                                             &
    lpatch_t(207, .false.),                                             &
    lpatch_t(208, .false.),                                             &
    lpatch_t(209, .false.),                                             &
    lpatch_t(210, .false.),                                             &
    lpatch_t(211, .false.),                                             &
    lpatch_t(212, .false.),                                             &
    lpatch_t(213, .false.),                                             &
    lpatch_t(214, .false.),                                             &
    lpatch_t(215, .false.)                                              &
  /)

  return
end subroutine svec_init

!> @brief Restores game from save file
subroutine svec_read_file(this, current_move_number)
  use :: cquest_log, only: log_notice, log_warning, log_error
  use :: m_format, only: a
  use :: m_textio, only: itoa, munch
  implicit none

  ! Parameters

  character(len=*), parameter :: cqf_sv500  =                           &
    "('  ERROR (', I0, '): ERROR reading save file: ', A)"
  ! character(len=*), parameter :: cqf_sv3002 = "(10Z8)"
  character(len=*), parameter :: cqf_sv3004 =                           &
    "('  Turn: ', I0, ' - cannot load now')"
  character(len=*), parameter :: cqm_sv3005 =                           &
    '  I can''t do that at this point in time.'

  ! Arguments

  !> State vector object
  class(state_vector_t), intent(inout) :: this

  !> Current player move number. Original code cannot load over
  !! an in-progress game
  integer, intent(in) :: current_move_number

  ! Local variables

  integer :: II
  integer :: K
  integer :: L

  integer :: fh
  integer :: ierr
  character(len=80) :: errmsg
  character(len=80) :: msg

  ! Formats

3002 format(10Z8)

  continue

  ! ---RESTORE---
  ! current_move_number = SAVAR(90) is NUMOVE
  ! can only restore before moving in a new game
  if (current_move_number > 1) then
    write(unit=msg, fmt=cqf_sv3004) current_move_number
    call a%write_para(msg)
    call a%write_para(cqm_sv3005)
    call log_warning("Cannot read save vector mid-game; turn "          &
      // itoa(current_move_number) // " (> 0), filename = "             &
      // munch(this%savefile))
  else
L1: do
      ierr = 0
      errmsg = ''
      !      CALL FTNCMD('ASSIGN 8=SAVEDQUEST;')
      open(newunit=fh, file=this%savefile, status='OLD',                &
        form='FORMATTED', iostat=ierr, iomsg=errmsg)

      if (ierr > 0) then
        ! Handle I/O errors
        call log_error("Error opening save file for reading: ("         &
          // itoa(ierr) // ") " // munch(errmsg))
        write(unit=msg, fmt=cqf_sv500) ierr, errmsg
        call a%write_para(msg)
        call a%write_para(cqm_sv3005)
        exit L1
      end if

      rewind fh
      L = 1
      do II = 1, 40
        K = L + 9
        read(unit=fh, fmt=3002, iostat=ierr, iomsg=errmsg)              &
          this%SAVAR(L:K)

        if (ierr > 0) then
          ! Handle I/O errors
          call log_error("Error reading save vector (between "          &
            // itoa(L) // " and " // itoa(K) // "): (" // itoa(ierr)    &
            // ") " // munch(errmsg))
            write(unit=msg, fmt=cqf_sv500) ierr, errmsg
            call a%write_para(msg)
            call a%write_para(cqm_sv3005)
          exit L1
        end if

        L = L + 10
      end do
      close(fh)

      ! Input TRANSFER(): Cast assorted ints to logicals after loading
      do II = 1, NPATCHES
        this%lpatch(II)%lval = transfer(                                &
          source=this%SAVAR(this%lpatch(II)%id),                        &
          mold=this%lpatch(1)%lval)
      end do

      call log_notice("Read " // itoa(K) // " values from save file "   &
        // munch(this%savefile))

      exit L1
    end do L1
  end if

  return
end subroutine svec_read_file

!> @brief Writes game state to save file
subroutine svec_write_file(this)
  use :: cquest_log, only: log_notice, log_error
  use :: m_format, only: a
  use :: m_textio, only: itoa, munch
  implicit none

  ! Parameters

  character(len=*), parameter :: cqf_sv500  =                           &
    "('  ERROR (', I0, '): ERROR writing save file: ', A)"

  ! Arguments

  !> State vector object
  class(state_vector_t), intent(inout) :: this

  ! Local variables

  integer :: II
  integer :: K
  integer :: L

  integer :: fh
  integer :: ierr
  character(len=80) :: errmsg
  character(len=80) :: msg

  ! Formats

3002 format(10Z8)

  continue

  ! Output TRANSFER(): Cast assorted logicals to ints before saving
  do II = 1, NPATCHES
    this%SAVAR(this%lpatch(II)%id)                                      &
      = transfer(source=this%lpatch(II)%lval, mold=this%SAVAR(1))
  end do

  ierr = 0
  errmsg = ''
  open(NEWUNIT=fh, FILE=this%savefile, STATUS='UNKNOWN',                &
    FORM='FORMATTED', IOSTAT=ierr, IOMSG=errmsg)
  if (ierr == 0) then
    rewind fh
    L = 1
    do II = 1, 40
      K = L + 9
      write(unit=fh, fmt=3002, iostat=ierr, iomsg=errmsg)               &
        this%SAVAR(L:K)

      if (ierr /= 0) then
        ! Trap error when writing to save file
        write(unit=msg, fmt=cqf_sv500) ierr, trim(errmsg)
        call a%write_para(msg)
        call log_error("Error writing save vector (between "            &
          // itoa(L) // " and " // itoa(K) // "): (" // itoa(ierr)      &
          // ") " // munch(errmsg))
        exit
      end if

      L = L + 10
    end do
    close(fh)
    call log_notice("Wrote " // itoa(K) // " values to save file "      &
      // munch(this%savefile))
  else
    ! Trap error when opening save file for writing
    call log_error("Error opening save file for writing: ("             &
      // itoa(ierr) // ") " // munch(errmsg))
    write(unit=msg, fmt=cqf_sv500) ierr, trim(errmsg)
    call a%write_para(msg)
  end if

  return
end subroutine svec_write_file

!> Restore game objects from file
subroutine svec_restore_game(this, current_move_number, player, map,    &
  items, encounters)
  use :: m_encounter, only: gameencounters_t
  use :: m_items, only: NITEMS, gameitems_t
  use :: m_object_id
  use :: m_player, only: player_t
  use :: m_where, only: NROOMS, dungeon_t
  implicit none

  !> State vector object
  class(state_vector_t), intent(inout) :: this

  !> Number of moves taken in current game
  integer, intent(in) :: current_move_number

  !> Reference to player
  type(player_t), intent(out) :: player

  !> Reference to map
  type(dungeon_t), intent(out) :: map

  !> Bulk game items object
  type(gameitems_t), intent(inout) :: items

  !> Bulk live encounters object
  type(gameencounters_t), intent(inout) :: encounters

  continue

  call this%read_file(current_move_number)
  call svec_to_obj(player, map, items, encounters, this%SAVAR)

  return
end subroutine svec_restore_game

!> Save game objects to file
subroutine svec_save_game(this, player, map, items, encounters)
  use :: m_encounter, only: gameencounters_t
  use :: m_items, only: NITEMS, gameitems_t
  use :: m_object_id
  use :: m_player, only: player_t
  use :: m_where, only: NROOMS, dungeon_t
  implicit none

  !> State vector object
  class(state_vector_t), intent(inout) :: this

  !> Reference to player
  type(player_t), intent(in) :: player

  !> Reference to map
  type(dungeon_t), intent(in) :: map

  !> Bulk game items object
  type(gameitems_t), intent(in) :: items

  !> Bulk live encounters object
  type(gameencounters_t), intent(in) :: encounters

  continue

  call obj_to_svec(player, map, items, encounters, this%SAVAR)

  call this%write_file()

  return
end subroutine svec_save_game

end module m_statevector
