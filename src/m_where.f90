!> @file m_where.f90
!! @author Michael S. Holtzman
!! @author Mark Kershenblatt
!! @copyright See LICENSE
!! @brief Contains room connectivity graph
!--------------------------------------------------------------
!> @brief Contains room connectivity graph
!!
!! Extracted from `move.f`
module m_where
  implicit none

  !> Number of player-accessible locations
  integer, parameter, public :: NROOMS = 100

  public :: dungeon_t
  public :: is_valid_room
  public :: is_impassible
  public :: near_doors
  public :: is_dark_room
  public :: is_wet_room
  public :: in_lower_realm
  public :: in_master_realm
  public :: in_werewolf_territory
  public :: in_gnome_territory
  public :: in_winding_maze
  public :: random_room

  ! For testing
  public :: room_t
  ! public :: mirror_maze_t

  private

  !> @class door_t
  !! @brief Door state and directionality class
  type :: door_t
    !> Door state; (-2=WILL NOT OPEN/CLOSE, -1=NO DOOR HERE, 0=LOCKED, 1=CLOSED, 2=OPEN)
    integer :: state_ = -1
    !> Enter direction
    integer :: in_dir = -1
    !> Leave direction
    integer :: out_dir = -1
  contains
    !> Door enter destination
    procedure :: enter_to => door_enter_to
    !> Door leave destination
    procedure :: leave_to => door_leave_to
    ! Returns true if door is locked (`state_ == 0`)
    procedure :: is_locked => door_is_locked
    ! Returns true if door is closed (`state_ == 1`)
    procedure :: is_closed => door_is_closed
    ! Returns true if door is open (`state_ == 2`)
    procedure :: is_open => door_is_open
    ! Returns true if door is does not change state (`state_ == -2`)
    procedure :: is_static => door_is_static
    ! Returns true if door does not exist (`state_ == -1`)
    procedure :: does_not_exist => door_does_not_exist
    ! Returns true if door exists (`state_ /= -1`)
    procedure :: exists => door_exists
    ! Lock door; set door `state_` to 0
    procedure :: lock => door_lock
    ! Unlock door; set door `state_` to 1 (closed but not locked)
    procedure :: unlock => door_close
    ! Close door; set door `state_` to 1
    procedure :: close => door_close
    ! Open door; set door `state_` to 2
    procedure :: open => door_open
  end type door_t

  !> @class room_t
  !! @brief Room connectivity and description class
  type :: room_t
    !> Room ID
    integer :: id = -1
    !> Door object reference
    type(door_t) :: door
    !> Integer connectivity vector. Represents connectivity
    !! (N, NE, E, SE, S, SW, W, NW, UP, DOWN) [1-10]
    integer, dimension(12) :: connects_ = 0
    !> Previous description level
    integer :: prev = -1
    !> Short description; taken from `short.dat`
    character(len=80) :: short_desc
    !> Long description; taken from `long.dat`
    character(len=80), dimension(:), allocatable :: long_desc
  contains
    !> Destination
    procedure :: leads_to => room_leads_to
    ! !> Get door state
    ! procedure :: get_door => room_get_door
    ! !> Set door state
    ! procedure :: set_door => room_set_door
    ! !> Has door
    ! procedure :: has_door => room_has_door
    !> Room light level
    procedure :: is_dark => room_is_dark
    !> Water accessible in room
    procedure :: has_water => room_has_water
    !> Room enter destination
    procedure :: enter_to => room_enter_to
    !> Room leave destination
    procedure :: leave_to => room_leave_to
    ! !> Room is in open 'upper' portion of map
    ! procedure :: in_upper_realm => room_in_upper_realm
    ! !> Room is in locked 'lower' portion of map
    ! procedure :: in_lower_realm => room_in_lower_realm
    ! !> Room is in final 'master' portion of map
    ! procedure :: in_master_realm => room_in_master_realm
    !> Show long or short room description
    procedure :: describe => room_describe
  end type room_t

  !> @class mirror_maze_t
  !! Resolve movement through Mirror Maze
  type :: mirror_maze_t
    !> Number of maze exits. Normally it is 10 but in an emergency it
    !! is 1
    integer, dimension(10) :: exits = 0

  contains
    !> Initialize object
    procedure :: init => mirror_maze_init
    !> Return integer array of the current maze exit room IDs
    procedure :: state_to_array => mirror_maze_state_to_array
    !> Set maze exit state from array (state vector)
    procedure :: array_to_state => mirror_maze_array_to_state
    !> Switch mirror maze routing to emergency mode
    procedure :: open_emergency_exits => mirror_maze_open_emergency_exits
    !> Return room ID of the current maze exit
    procedure :: find_exit => mirror_maze_find_exit
  end type mirror_maze_t

  !> @class dungeon_t
  !! @brief Class representing all locations
  type :: dungeon_t
    !> Array of rooms in map
    type(room_t), dimension(NROOMS) :: room
    !> State of path through glacier (`MELT`)
    logical :: path_through_glacier    = .false.
    !> State of wall of fire (`FIRE`)
    logical :: fire_blocks_way         = .true.
    !> State of path to precipice (`PREC`)
    logical :: path_to_precipice       = .false.
    !> State of path through final door (`MASECT`)
    logical :: path_through_final_door = .false.
    !> Reference to Mirror Maze
    type(mirror_maze_t) :: mirror_maze
  contains
    !> Initialize map
    procedure :: init => dungeon_init
    !> Simple valid room detection
    procedure, nopass :: is_valid_room
    !> Room light level
    procedure :: room_is_dark => dungeon_room_is_dark
    !> Water accessible in room
    procedure :: room_has_water => dungeon_room_has_water
    ! Returns true if door to room `room_id` is locked (`state_ == 0`)
    procedure :: door_is_locked => dungeon_door_is_locked
    ! Returns true if door to room `room_id` is closed (`state_ == 1`)
    procedure :: door_is_closed => dungeon_door_is_closed
    ! Returns true if door to room `room_id` is open (`state_ == 2`)
    procedure :: door_is_open => dungeon_door_is_open
    ! Returns true if door to room `room_id` is does not change state (`state_ == -2`)
    procedure :: door_is_static => dungeon_door_is_static
    ! Returns true if door to room `room_id` exists (`state_ /= -1`)
    procedure :: door_exists => dungeon_door_exists
  end type dungeon_t

  ! !> Index of room descriptions
  ! integer, dimension(NROOMS), public :: PREV = -1

  ! !> Direction moved when leaving room
  ! integer, dimension(NROOMS), parameter, public :: LEAVE = (/           &
  !   7, 0, 0, 3, 0, 0,  7, 0, 0, 1, 1, 0, 7, 0, 3,  0, 5, 0, 3,  10,     &
  !   0, 7, 0, 0, 3, 0,  0, 0, 0, 0, 5, 5, 5, 0, 2,  0, 0, 10, 0,  0,     &
  !   0, 9, 3, 9, 1, 0,  0, 0, 0, 0, 6, 3, 3, 0, 6,  0, 0,  0, 0,  0,     &
  !   1, 9, 9, 1, 0, 0, 10, 7, 1, 7, 7, 3, 0, 0, 1, 10, 3,  0, 0,  0,     &
  !   0, 3, 3, 0, 7, 3,  7, 1, 7, 0, 3, 0, 0, 3, 0,  0, 0,  0, 7,  0      &
  ! /)

  ! !> Direction moved when entering room
  ! !!
  ! !! @note Why does room 61 have an ENTER direction/action of (ahem)?
  ! integer, dimension(NROOMS), parameter, public :: ENTER = (/           &
  !   0,  0, 1, 0, 0, 3, 0, 0, 0, 0, 0, 3, 10, 0, 0, 0, 10, 1, 0, 1,      &
  !   3,  0, 0, 7, 0, 0, 0, 7, 0, 0, 0, 0,  0, 0, 0, 8,  0, 0, 0, 0,      &
  !   7,  0, 0, 5, 0, 0, 0, 1, 0, 0, 2, 0,  7, 0, 0, 0,  0, 0, 0, 0,      &
  !   19, 9, 1, 1, 1, 0, 7, 3, 3, 0, 3, 5,  0, 1, 0, 0,  0, 0, 7, 9,      &
  !   0,  3, 0, 3, 0, 5, 1, 0, 0, 9, 1, 0,  7, 0, 0, 0,  0, 0, 7, 0       &
  ! /)

!   !> Location connectivity graph
!   integer, dimension(10, NROOMS), parameter, public :: WHEREAMI =       &
!     reshape((/                                                          &
! !        N   NE    E   SE    S   SW    W   NW   UP  DOWN
!     (/   0,   0,   0,   0,   0,   0,   2,   0,   0,  -29 /),            &
!     (/   3,   0,   1,   0,   0,   0,  -4,   0,   0,    3 /),            &
!     (/  33,   0,   0,   0,   8,   0,   5,   0,  -2,    0 /),            &
!     (/   0,   0,  -2,   0,   0,   0,   0,   0,   0,   44 /),            &
!     (/   6,   0,   3,   0,   8,   0,   0,   0,   0,    0 /),            &
!     (/  31,   0,  -7,   0,   5,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,   0,   0,   6,   0,   0,    0 /),            &
!     (/   5,   0,   3,  10,  11,   0,  12,   0,   9,   24 /),            &
!     (/  28,   0,  13,   0,   0,   0,  15,   0,  20,    8 /),            &
!     (/   8,   0,   0,   0,   0,   0,   0,   0,   0,  -39 /),            &
! !        N   NE    E   SE    S   SW    W   NW   UP  DOWN
!     (/   8,   0,   0,   0,   0,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,   8,   0,   0,   0,  26,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,   0,   0,   9,   0,   0,  -17 /),            &
!     (/   0,   0,   0,   0,  16,   0,  18,   0,   0,    0 /),            &
!     (/   0,   0,   9,   0,   0,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,  14,   0,   0,   0,  34,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,  18,   0,   0,   0,   0,  -13 /),            &
!     (/  17,   0,  14,   0,   0,   0,  19,   0,   0,    0 /),            &
!     (/   0,   0,  18,   0,   0,   0,   0,   0,   0,    0 /),            &
!     (/ -21,   0,   0,   0,   0,   0,   0,   0,   0,    9 /),            &
! !        N   NE    E   SE    S   SW    W   NW   UP  DOWN
!     (/   0,   0,  22,   0, -20,   0, -23,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,   0,   0,  21,   0,   0,    0 /),            &
!     (/   0,   0,  21,   0,   0,   0,  41,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,   0,   0,  25,   0,   8,    0 /),            &
!     (/   0,   0,  24,   0,   0,   0,   0,   0,   0,    0 /),            &
!     (/  32,   0,  12,   0,  30,   0,   0,   0,   0,    0 /),            &
!     (/  -2,  -2, -16, -28, -28,  -2, -28,  -2, -28,  -28 /),            &
!     (/   0,   0,   0,   0,   9,   0,  27,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,   0,   0,   0,   0,  -1,    0 /),            &
!     (/  26,   0,   0,   0,   0,   0,  39,   0,   0,    0 /),            &
! !        N   NE    E   SE    S   SW    W   NW   UP  DOWN
!     (/   0,   0,   0,   0,   6,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,  26,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,   3,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,  16,   0,   0,  35,   0,  36,   0,    0 /),            &
!     (/   0,  34,   0,   0,   0,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,   0,  34,   0,   0,   0,  37,   0,    0 /),            &
!     (/   0,   0,   0,   0,  36,   0,   0,   0,  38,    0 /),            &
!     (/   0,   0,   0,   0,   0,   0,   0,   0,   0,   37 /),            &
!     (/   0,   0,  30,   0,   0,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,   0,   0,   0,   0,   0,  -80 /),            &
! !        N   NE    E   SE    S   SW    W   NW   UP  DOWN
!     (/   0,   0,  23,   0,   0,   0,  43,  42,   0,   42 /),            &
!     (/   0,   0,   0,  41,   0,   0,   0,   0,  41,    0 /),            &
!     (/   0,   0,  41,   0,   0,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,  45,   0,   0,   0,   4,    0 /),            &
!     (/  44,   0,   0,   0,  46,   0,   0,   0,  44,   46 /),            &
!     (/  45,   0,  54,   0,  47,   0,   0,   0,  45,   47 /),            &
!     (/  46, -48,   0,   0,   0,   0,  49,   0,  46,    0 /),            &
!     (/  56,   0,   0,   0,  69,   0,  47,   0,  69,    0 /),            &
!     (/  57,   0,  47,   0,  50,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,  49,   0,  66,   0,  53,   0,   0,    0 /),            &
! !        N   NE    E   SE    S   SW    W   NW   UP  DOWN
!     (/   0,  55,   0,   0,   0,  54,   0,   0,   0,    0 /),            &
!     (/   0,   0,  53,   0,   0,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,  50,   0,   0,   0,  52,   0,   0,    0 /),            &
!     (/   0,  51,   0,   0,   0,   0,  46,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,   0,  51,   0,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,  48,   0,   0,   0,   0,    0 /),            &
!     (/  58,  61,  60,  59,  57,  49,  57,  49,  57,   57 /),            &
!     (/  58,  60,  61,  59,  61,  63,  59,  57,  58,   58 /),            &
!     (/  59,  61,  58,  60,  63,  60,  57,  58,  59,   64 /),            &
!     (/  58,  60,  59,  61,  61,  57,  59,  62,  60,   60 /),            &
! !        N   NE    E   SE    S   SW    W   NW   UP  DOWN
!     (/  59,  61,  60,  58,  57,  60,  62,  58,  61,   61 /),            &
!     (/  57,  63,  58,  59,  60,  61,  64,  62,  62,   65 /),            &
!     (/  62,  57,  60,  61,  64,  63,  58,  59,  74,   63 /),            &
!     (/  58,  59,  60,  58,  59,  60,  58,  59,  60,   58 /),            &
!     (/ -90,   0,  62,   0,   0,  73,   0,   0,  78,    0 /),            &
!     (/  50,   0,  68,   0,   0,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,   0,   0,   0,   0,   0,   68 /),            &
!     (/   0,   0,   0,   0,   0,   0,  66,   0,  67,    0 /),            &
!     (/  48,   0,  70,   0,   0,   0,   0,   0,   0,   48 /),            &
!     (/   0,   0,   0,   0,   0,   0,  69,   0,   0,   71 /),            &
! !        N   NE    E   SE    S   SW    W   NW   UP  DOWN
!     (/   0,   0,   0,   0,   0,   0,  72,   0, -70,    0 /),            &
!     (/   0,   0,  71,   0,   0,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,  65,   0,  74,   0,   0,   0,   0,    0 /),            &
!     (/  73,   0,  63,   0,  75,   0,  77,   0,   0,    0 /),            &
!     (/  74,   0,   0,  76,   0,   0,  82,   0,  76,    0 /),            &
!     (/   0,   0,   0,   0,   0,   0,   0,  75,   0,   75 /),            &
!     (/   0,   0,  74,   0,  83,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,   0,   0,  79,   0,   0,   65 /),            &
!     (/   0,   0,  78,   0,   0,   0,  80,   0,   0,    0 /),            &
!     (/   0,   0,  79,   0,   0,   0,   0,   0, -40,    0 /),            &
! !        N   NE    E   SE    S   SW    W   NW   UP  DOWN
!     (/   0,   0,   0,   0,   0,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,  75,   0,  83,   0,   0,   0,   0,    0 /),            &
!     (/  82,   0,  77,   0,   0,   0,   0,   0, -84,    0 /),            &
!     (/   0,   0,  85,   0,   0,   0,  86,   0,   0,  -83 /),            &
!     (/   0,   0,   0,   0,  87,   0,  84,   0,   0,   61 /),            &
!     (/   0,   0,  84,  87, -88,   0,  91,   0,   0,    0 /),            &
!     (/  85,   0,   0,   0,   0,   0,  86,   0,   0,    0 /),            &
!     (/  86,   0,   0,   0,   0,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,   0,   0,  90,   0,   0,    0 /),            &
!     (/   0,   0,  89,   0,  65,   0,   0,   0,   0,    0 /),            &
! !        N   NE    E   SE    S   SW    W   NW   UP  DOWN
!     (/  92,   0,  86,   0,   0,   0,   0,   0,   0,    0 /),            &
!     (/ -93,   0,   0,   0,  91,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,  91,   0,  92,   0, -94,   0,   0,    0 /),            &
!     (/   0,   0,  93,   0,   0,   0,   0,   0,   0,    0 /),            &
!     (/  97,   0,  98,   0,  99,   0,  96,   0,   0,    0 /),            &
!     (/   0,   0,  95,   0,   0,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,  95,   0,   0,   0,   0,    0 /),            &
!     (/   0,   0,   0,   0,   0,   0,  95,   0,   0,    0 /),            &
!     (/ -95, -95, -95, -95, -95, -95, -95, -95,   0,    0 /),            &
!     (/   0,   0,   0,   0,   0,   0,   0,   0,   0,    0 /)/),          &
! !        N   NE    E   SE    S   SW    W   NW   UP  DOWN
!     (/10, NROOMS/) )

contains

!> Valid room detection based on array bounds `1 .. NROOMS`
pure logical function is_valid_room(room_id)
  implicit none
  !> Room ID
  integer, intent(in) :: room_id
  continue
  is_valid_room = ((room_id >= 1) .and. (room_id <= NROOMS))
  return
end function is_valid_room

!> Detect rooms with ID = 0
pure logical function is_impassible(room_id)
  implicit none
  !> Room ID
  integer, intent(in) :: room_id
  continue
  is_impassible = (room_id == 0)
  return
end function is_impassible

!> In a room near a door
pure logical function near_doors(room_id)
  implicit none
  !> Room ID
  integer, intent(in) :: room_id
  continue
  near_doors = (room_id <= 25)
  return
end function near_doors

!> In lower realm (rooms 41-100)
pure logical function in_lower_realm(room_id)
  implicit none
  !> Room ID
  integer, intent(in) :: room_id
  continue
  in_lower_realm = (room_id >= 41)
  return
end function in_lower_realm

!> In 'master' realm (rooms 95-99)
pure logical function in_master_realm(room_id)
  implicit none
  !> Room ID
  integer, intent(in) :: room_id
  continue
  in_master_realm = ((room_id >= 95) .and. (room_id <= 99))
  return
end function in_master_realm

!> In werewolf territory (4-25)
pure logical function in_werewolf_territory(room_id)
  implicit none
  !> Room ID
  integer, intent(in) :: room_id
  continue
  in_werewolf_territory = ((room_id >= 4) .and. (room_id <= 25))
  return
end function in_werewolf_territory

!> In gnome territory (45-92)
pure logical function in_gnome_territory(room_id)
  implicit none
  !> Room ID
  integer, intent(in) :: room_id
  continue
  in_gnome_territory = ((room_id >= 45) .and. (room_id <= 92))
  return
end function in_gnome_territory

!> Room darkness detection based on room ID
pure logical function is_dark_room(room_id)
  implicit none
  !> Room ID
  integer, intent(in) :: room_id
  continue
  is_dark_room = ((room_id >= 41) .and. (room_id <= 94))
  return
end function is_dark_room

!> Room water detection
pure logical function is_wet_room(room_id, omit_island)
  use m_object_id
  implicit none
  !> Room ID
  integer, intent(in) :: room_id
  !> Omit island from list of rooms with water, optional.
  logical, intent(in), optional :: omit_island
  continue
  select case (room_id)
  case(R32_SIDE_OF_MOAT, R40_FAR_SIDE_OF_MOAT,                          &
    R68_BASE_OF_WATERFALL, R81_ISLAND)
    is_wet_room = .true.
  case default
    is_wet_room = .false.
  end select
  if (present(omit_island)) then
    if (omit_island) then
      is_wet_room = (is_wet_room .and. (room_id /= R81_ISLAND))
    end if
  end if
  return
end function is_wet_room

!> Winding maze detection based on room ID
pure logical function in_winding_maze(room_id)
  implicit none
  !> Room ID
  integer, intent(in) :: room_id
  continue
  in_winding_maze = ((room_id >= 57) .and. (room_id <= 64))
  return
end function in_winding_maze

!> @brief Generate a random room ID from a range
integer function random_room(lo_room, n_rooms, hi_room) result(room_id)
  use :: m_random, only: RDM
  implicit none

  !> Lowest room to scatter to
  integer, intent(in) :: lo_room

  !> Total possible number of rooms to scatter to
  integer, intent(in), optional :: n_rooms

  !> Highest room to scatter to
  integer, intent(in), optional :: hi_room

  continue

  if (present(n_rooms)) then
    room_id = lo_room + int(RDM() * n_rooms)
  else if (present(hi_room)) then
    room_id = lo_room + int(RDM() * (hi_room - lo_room + 1))
  else
    room_id = lo_room
  end if

  return
end function random_room

!> Returns destination when door is entered through
pure integer function door_enter_to(this) result(enter_to)
  implicit none
  !> Door object reference
  class(door_t), intent(in) :: this
  continue
  enter_to = this%in_dir
  return
end function door_enter_to

!> Returns destination when door is left through
pure integer function door_leave_to(this) result(leave_to)
  implicit none
  !> Door object reference
  class(door_t), intent(in) :: this
  continue
  leave_to = this%out_dir
  return
end function door_leave_to

!> Returns true if door is locked (`state_ == 0`)
pure logical function door_is_locked(this) result(is_locked)
  implicit none
  !> Door object reference
  class(door_t), intent(in) :: this
  continue
  is_locked = (this%state_ == 0)
  return
end function door_is_locked

!> Returns true if door is closed (`state_ == 1`)
pure logical function door_is_closed(this) result(is_closed)
  implicit none
  !> Door object reference
  class(door_t), intent(in) :: this
  continue
  is_closed = (this%state_ == 1)
  return
end function door_is_closed

!> Returns true if door is open (`state_ == 2`)
pure logical function door_is_open(this) result(is_open)
  implicit none
  !> Door object reference
  class(door_t), intent(in) :: this
  continue
  is_open = (this%state_ == 2)
  return
end function door_is_open

!> Returns true if door is does not change state (`state_ == -2`)
pure logical function door_is_static(this) result(is_static)
  implicit none
  !> Door object reference
  class(door_t), intent(in) :: this
  continue
  is_static = (this%state_ == -2)
  return
end function door_is_static

!> Returns true if door does not exist (`state_ == -1`)
pure logical function door_does_not_exist(this) result(does_not_exist)
  implicit none
  !> Door object reference
  class(door_t), intent(in) :: this
  continue
  does_not_exist = (this%state_ == -1)
  return
end function door_does_not_exist

!> Returns true if door exists (`state_ /= -1`)
pure logical function door_exists(this) result(exists)
  implicit none
  !> Door object reference
  class(door_t), intent(in) :: this
  continue
  exists = (this%state_ /= -1)
  return
end function door_exists

!> Close door; set door `state_` to 0
subroutine door_lock(this)
  implicit none
  !> Door object reference
  class(door_t), intent(inout) :: this
  continue
  this%state_ = 0
  return
end subroutine door_lock

!> Close door; set door `state_` to 1
subroutine door_close(this)
  implicit none
  !> Door object reference
  class(door_t), intent(inout) :: this
  continue
  this%state_ = 1
  return
end subroutine door_close

!> Open door; set door `state_` to 2
subroutine door_open(this)
  implicit none
  !> Door object reference
  class(door_t), intent(inout) :: this
  continue
  this%state_ = 2
  return
end subroutine door_open

!> Returns destination when door is entered through
pure integer function room_enter_to(this) result(enter_to)
  implicit none
  !> Room object reference
  class(room_t), intent(in) :: this
  continue
  enter_to = this%door%in_dir
  return
end function room_enter_to

!> Returns destination when door is left through
pure integer function room_leave_to(this) result(leave_to)
  implicit none
  !> Room object reference
  class(room_t), intent(in) :: this
  continue
  leave_to = this%door%out_dir
  return
end function room_leave_to

!> Return destination (room ID) based on origin and direction
pure integer function room_leads_to(this, direction) result(destination)
  implicit none
  !> Room object reference
  class(room_t), intent(in) :: this
  !> Direction index (1..10 correspond to N, NE, E, SE, S, SW, W, NW,
  !! UP, DOWN)
  integer, intent(in) :: direction
  continue
  destination = this%connects_(direction)
  return
end function room_leads_to

!> Room darkness detection based on room ID
pure logical function room_is_dark(this) result(is_dark)
  implicit none
  !> Room object reference
  class(room_t), intent(in) :: this
  continue
  is_dark = is_dark_room(this%id)
  return
end function room_is_dark

!> Detect if room is in 'master' realm
pure logical function room_in_master_realm(this)
  implicit none
  !> Room object reference
  class(room_t), intent(in) :: this
  continue
  room_in_master_realm = in_master_realm(this%id)
  return
end function room_in_master_realm

!> Room water detection
pure logical function room_has_water(this, omit_island)                 &
  result(has_water)
  use m_object_id
  implicit none
  !> Room object reference
  class(room_t), intent(in) :: this
  !> Omit island from list of rooms with water, optional.
  logical, intent(in), optional :: omit_island
  continue
  if (present(omit_island)) then
    has_water = is_wet_room(this%id, omit_island)
  else
    has_water = is_wet_room(this%id)
  end if
  return
end function room_has_water

!> Write room description
subroutine room_describe(this, long)
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: m_format, only: a
  use :: m_object_id
  implicit none

  ! Arguments

  !> Room object reference
  class(room_t), intent(in) :: this

  !> Flag to display long description.
  !! Optional, defaults to false.
  logical, intent(in), optional :: long

  ! Local variables

  logical :: show_long

  continue

  if (present(long)) then
    show_long = long
  else
    show_long = .false.
  end if

  if (show_long) then
    if (size(this%long_desc) > 1) then
      call a%write_para(this%long_desc(1), this%long_desc(2:))
    else
      call a%write_para(this%long_desc(1))
    end if
  else
    call a%write_para(this%short_desc)
  end if

  return
end subroutine room_describe

!> Initialize game map
subroutine dungeon_init(this)
  use :: m_object_id
  implicit none
  !> Dungeon object reference
  class(dungeon_t), intent(inout) :: this
  continue

  ! Initialize Mirror maze exit manager
  call this%mirror_maze%init()

  ! Initialize unlockable path states

  ! State of path through glacier (`MELT`)
  this%path_through_glacier    = .false.
  ! State of wall of fire (`FIRE`)
  this%fire_blocks_way         = .true.
  ! State of path to precipice (`PREC`)
  this%path_to_precipice       = .false.
  ! State of path through final door (`MASECT`)
  this%path_through_final_door = .false.

  ! Define room array (in blocks of 10)
  this%room(1:10) = (/                                                  &
    room_t(R1_BEDROOM,                                                  &
      door_t( -2,  0, 7),                                               &
      (/   0,   0,   0,   0,   0,   0,   2,   0,   0,  -29 /),          &
      -1, "  You are in the bedroom.", (/ character(len=80) ::            &
"  You are in a large, tarnished brass bed in an old, musty bedroom.",    &
"  cobwebs hang from the ceiling.  A few rays of light filter through",   &
"  the shutters.  There is a nightstand nearby with a single wooden",     &
"  drawer.  The door west creaks in the breeze.  A macabre portrait",     &
"  hangs to the left of an empty fireplace."                              &
      /)),                                                              &
    room_t(R2_DIM_CORRIDOR,                                             &
           door_t(  0,  0, 0),                                          &
           (/   3,   0,   1,   0,   0,   0,  -4,   0,   0,    3 /),     &
           -1, "  You are in the dim corridor.", (/ character(len=80) ::  &
"  You are in a dim corridor lit by gaslight.  Doors exit",               &
"  to the east and west.  A stairway leads down."                         &
/)),                                                                    &
    room_t(R3_PARLOR,                                                   &
           door_t( -2,  1, 0),                                          &
           (/  33,   0,   0,   0,   8,   0,   5,   0,  -2,    0 /),     &
           -1, "  You're in the parlor.", (/ character(len=80) ::         &
"  You are in the parlor, an old fashioned sitting room.  A display",     &
"  case of dueling pistols hangs over the mantle. Stairs lead up to",     &
"  a dimly lit corridor.  Open double doors lead west.  Two wide",        &
"  hallways lead north and south."                                        &
/)),                                                                    &
    room_t(R4_LOCKED_ROOM,                                              &
           door_t(  2,  0, 3),                                          &
           (/   0,   0,  -2,   0,   0,   0,   0,   0,   0,   44 /),     &
           -1, "  You are in the locked room.", (/ character(len=80) ::   &
"  A cool wind blows up a stone stairway which descends",                 &
"  down into a large stone room.  A note written in blood",               &
'  reads "VERY CLEVER OF YOU TO MAKE IT THIS FAR".',                      &
"  The door leads east, back to the hall."                                &
 /)),                                                                   &
    room_t(R5_DINING_ROOM,                                              &
           door_t( -2,  0, 0),                                          &
           (/   6,   0,   3,   0,   8,   0,   0,   0,   0,    0 /),     &
           -1, "  You are in the dining room.", (/ character(len=80) ::          &
"  This is the dining room.  A long table is set for 12 guests.",                    &
"  A swinging door leads north, and an arched passage leads",                        &
"  south.  Open double doors exit to the east."                                     &
 /)),                                                                   &
    room_t(R6_KITCHEN,                                                  &
           door_t(  0,  3, 0),                                          &
           (/  31,   0,  -7,   0,   5,   0,   0,   0,   0,    0 /),     &
           -1, "  You are in the kitchen.", (/ character(len=80) ::          &
"  You are now in the kitchen.  Twelve Swanson's frozen entrees rest",               &
'  on the counter, below a microwave oven.  "THE BEGINNER''S GUIDE TO',               &
'  COOKING" lies on a small table.  A swinging door exits south.',                   &
"  Other doors lead east and north."                                                &
 /)),                                                                   &
    room_t(R7_BRICK_WALL,                                               &
           door_t(  2,  0, 7),                                          &
           (/   0,   0,   0,   0,   0,   0,   6,   0,   0,    0 /),     &
            -1, "  You're at a brick wall.", (/ character(len=80) ::          &
"  The door opens to a brick wall.  ---DEAD END---",                                 &
"  ",                                                                                &
'  A note on the wall reads "L 8 R 31 L 59".'                                       &
 /) ),                                                                  &
    room_t(R8_FOYER,                                                    &
           door_t( -2,  0, 0),                                          &
           (/   5,   0,   3,  10,  11,   0,  12,   0,   9,   24 /),     &
           -1, "  You're in foyer.", (/ character(len=80) ::          &
"  You are in the foyer.  An umbrella near the door is dripping on the",             &
"  thick pile carpet.  A black cape is draped neatly over the banister",             &
"  of a grand staircase leading up.  A magnificentl archway leads north.",           &
"  Corridors lead south and southeast, a small hallway heads west,",                 &
"  and a narrow stairway goes down."                                                &
/)),                                                                    &
    room_t(R9_UPSTAIRS_HALLWAY,                                         &
      door_t( -2,  0, 0),                                               &
      (/  28,   0,  13,   0,   0,   0,  15,   0,  20,    8 /),          &
      -1, "  You're in the upstairs hallway.", (/ character(len=80) ::    &
"  You are in the upstairs hallway, a long corridor with passages",       &
"  to the north, east, and west.  Stairs lead up and down."               &
 /) ),                                                                  &
    room_t(R10_SMOKING_ROOM,                                            &
           door_t( -2,  0, 1),                                          &
           (/   8,   0,   0,   0,   0,   0,   0,   0,   0,  -39 /),     &
           -1, "  This is the smoking room.", (/ character(len=80) ::     &
"  This is the smoking room.  Several cans of tobacco line a shelf",      &
"  above a small bookcase.  A large box of cigars lies on a table.",      &
"  A Honeywell air purifier hums quietly beneath the window.  The",       &
"  only exit is back north, the way you came in."                         &
 /)) /)

  this%room(11:20) = (/                                                 &
    room_t(R11_WORKSHOP,                                                &
    door_t( -2,  0, 1),                                                 &
    (/   8,   0,   0,   0,   0,   0,   0,   0,   0,    0 /),            &
           -1, "  You're in the workshop.", (/ character(len=80) ::       &
'  You are in the workshop.  A myriad of tools clutter the workbench',    &
'  and surrounding tables.  A thick layer of sawdust covers the floor.',  &
'  Footprints in the sawdust indicate that you are not alone.'            &
    /)),                                                                &
    room_t(R12_GARDEN,                                                  &
    door_t( -2,  3, 0),                                                 &
    (/   0,   0,   8,   0,   0,   0,  26,   0,   0,    0 /),            &
    -1, "  You are in the garden.", (/ character(len=80) ::               &
'  This is the garden.  Tomato plants are growing neatly in rows.',       &
'  A narrow path goes east and a wider one goes west.'                    &
    /)),                                                                &
    room_t(R13_LIBRARY,                                                 &
    door_t(  2, 10, 7),                                                 &
    (/   0,   0,   0,   0,   0,   0,   9,   0,   0,  -17 /),            &
           -1, "  You are in the library.", (/ character(len=80) ::       &
'  This is the library.  All four walls are lined with bookcases.',       &
'  The room is brightly lit, although there is no apparent source',       &
'  of light.'                                                             &
    /)),                                                                &
    room_t(R14_LAB,                                                     &
    door_t( -2,  0, 0),                                                 &
    (/   0,   0,   0,   0,  16,   0,  18,   0,   0,    0 /),            &
    -1, "  You are in the lab.", (/ character(len=80) ::                  &
'  You are in a vast room full of laboratory equipment.  Several',        &
'  experiments appear to be in progress, and the remnants of some',       &
'  that failed are strewn about the room. A giant door hewn out of',      &
'  granite leads south and an entrance leads west into darkness.'         &
    /)),                                                                &
    room_t(R15_BOUDOIR,                                                 &
    door_t( -2,  0, 3),                                                 &
    (/   0,   0,   9,   0,   0,   0,   0,   0,   0,    0 /),            &
    -1, "  You're in the boudoir.", (/ character(len=80) ::               &
'  You are in a small boudoir.  The pink walls reflect the',              &
'  soft lighting with a warm glow.  The door exits east.'                 &
    /)),                                                                &
    room_t(R16_DARK_EW_PASSAGE,                                         &
    door_t( -2,  0, 0),                                                 &
    (/   0,   0,  14,   0,   0,   0,  34,   0,   0,    0 /),            &
    -1, "  You are in a dark E/W passage.", (/ character(len=80) ::       &
'  You are in a dark stone E/W passage.'                                  &
    /)),                                                                &
    room_t(R17_MIRROR,                                                  &
    door_t( -2, 10, 5),                                                 &
    (/   0,   0,   0,   0,  18,   0,   0,   0,   0,  -13 /),            &
    -1, "  You're in single mirror chamber.", (/ character(len=80) ::     &
'  You are in a low, dark chamber.  A single mirror',                     &
'  is set into the far wall.  The exit goes south.'                       &
    /)),                                                                &
    room_t(R18_DARK_EW_CORRIDOR,                                        &
    door_t( -2,  1, 0),                                                 &
    (/  17,   0,  14,   0,   0,   0,  19,   0,   0,    0 /),            &
    -1, "  You're in a dark E/W corridor.", (/ character(len=80) ::       &
'  You are in a dark E/W corridor.  A small but walkable',                &
'  tunnel leads off to the north.'                                        &
    /)),                                                                &
    room_t(R19_DARK_ROOM,                                               &
    door_t( -2,  0, 3),                                                 &
    (/   0,   0,  18,   0,   0,   0,   0,   0,   0,    0 /),            &
    -1, "  You're in the dark room.", (/ character(len=80) ::             &
'  You are in an extremely dark, unfinished room.  The only light',       &
'  emanates from a narrow slit in the ceiling.'                           &
    /)),                                                                &
    room_t(R20_ATTIC_ENTRANCE,                                          &
    door_t( -2,  1, 10),                                                &
    (/ -21,   0,   0,   0,   0,   0,   0,   0,   0,    9 /),            &
    -1, "  You are in the attic entrance.", (/ character(len=80) ::       &
'  You are in a musty room that appears to be an entrance to',            &
'  an attic.  A small passage leads north and stairs descend',            &
'  down behind you.'                                                      &
    /)) /)

  this%room(21:30) = (/                                                 &
    room_t(R21_ATTIC,                                                   &
    door_t(  0,  3, 0),                                                 &
    (/   0,   0,  22,   0, -20,   0, -23,   0,   0,    0 /),            &
    -1, "  You are in an old attic.", (/ character(len=80) ::             &
'  You are in an old attic filled with old-fashioned clothes, a pile',    &
'  of newspapers and some antiques.  An entrance to a cedar closet',      &
'  is to the east and there is a door to a crawlspace to the west.'       &
    /)),                                                                &
    room_t(R22_CEDAR_CLOSET,                                            &
    door_t( -2,  0, 7),                                                 &
    (/   0,   0,   0,   0,   0,   0,  21,   0,   0,    0 /),            &
    -1, "  You're in a cedar closet.", (/ character(len=80) ::            &
"  You're in a cedar closet that smells of fresh cedar.  Racks of more",  &
'  old-fashioned clothes lie to your right and left.'                     &
    /)),                            &
    room_t(R23_LOW_EW_PASSAGE, &
    door_t( -2,  0, 0),                                                 &
    (/   0,   0,  21,   0,   0,   0,  41,   0,   0,    0 /),            &
    -1, "  You're in a low E/W passage.", (/ character(len=80) ::          &
'  You are crawling along a low passage that leads east and west.'                    &
    /)),                                                                &
    room_t(R24_LAUNDRY_ROOM,                                            &
    door_t( -1,  7, 0),                                                 &
    (/   0,   0,   0,   0,   0,   0,  25,   0,   8,    0 /),            &
    -1, "  You're in the laundry room.", (/ character(len=80) ::          &
'  This is the laundry room.  Tattered clothes are scattered about.',                 &
'  An old-fashioned washing machine sits rotting in the corner.',                     &
'  On a shelf lies a box of Snowy Bleach and some Bounce fabric',                     &
'  softener.  The exit leads west and stairs lead up.'                                &
    /)),                                                                &
    room_t(R25_STORAGE_ROOM,                                            &
    door_t( -1,  0, 3),                                                 &
    (/   0,   0,  24,   0,   0,   0,   0,   0,   0,    0 /),            &
    -1, "  You're in the storage room.", (/ character(len=80) ::          &
'  You are in a storage room filled with empty cardboard boxes',                      &
'  and some large crates filled with dirt. The door goes east.'                       &
    /)),                                                                &
    room_t(R26_SIDE_OF_ROAD,                                            &
    door_t( -1,  0, 0),                                                 &
    (/  32,   0,  12,   0,  30,   0,   0,   0,   0,    0 /),            &
    -1, "  You are at the side of the dirt road.", (/ character(len=80) ::          &
'  You are at the side of a dirt road that runs north and south.',                    &
'  Fresh tracks in the road seem to indicate that a horse-drawn',                     &
'  carriage has passed here recently.  A narrow path leads east.'                     &
    /)),                                                                &
    room_t(R27_MIRROR_MAZE,                                             &
    door_t( -1,  0, 0),                                                 &
    (/  -2,  -2, -16, -28, -28,  -2, -28,  -2, -28,  -28 /),            &
    -1, "  You are wandering around the mirror maze.", (/ character(len=80) ::          &
'  This is the mirror maze.  A myriad of mirrors reflect your',                       &
'  image in a dazzling array of light.  The reflections make it',                     &
'  impossible to discren a direction.'                                                &
    /)),                                                                &
    room_t(R28_L_CORRIDOR,                                              &
    door_t( -1,  7, 0),                                                 &
    (/   0,   0,   0,   0,   9,   0,  27,   0,   0,    0 /),            &
    -1, '  You are in "L" shaped corridor.', (/ character(len=80) ::          &
'  You are in a narrow "L" shaped corridor, which leads',                             &
'  west and south.'                                                                   &
    /)),                                                                &
    room_t(R29_BELOW_WINDOW,                                            &
    door_t( -1,  0, 0),                                                 &
    (/   0,   0,   0,   0,   0,   0,   0,   0,  -1,    0 /),            &
    -1, "  You're underneath a window.", (/ character(len=80) ::          &
'  You are standing far below a large window.  Rows of',                              &
'  tall, exotic flowers form a wall around you.'                                      &
    /)),                                                                &
    room_t(R30_END_OF_ROAD, &
    door_t( -1,  0, 0),                                                 &
    (/  26,   0,   0,   0,   0,   0,  39,   0,   0,    0 /),            &
    -1, "  You're at the end of the road.", (/ character(len=80) ::          &
"  You're at the end of a dirt road.  A woodland path",                               &
'  goes west.  The road leads north.'                                                 &
    /)) /)

  this%room(31:40) = (/                                                 &
    room_t(R31_PANTRY,                                                  &
    door_t( -1,  0, 5),                                                 &
    (/   0,   0,   0,   0,   6,   0,   0,   0,   0,    0 /),            &
    -1, "  You're in pantry.", (/ character(len=80) ::          &
'  You are in a small pantry full of various foodstuffs.  To',                        &
'  your left is a beautiful china closet filled with dishes',                         &
'  pilfered from various restaurants.  On you right is an',                           &
'  open drawer full of plastic knives and forks.'                                    &
    /)),                                                                &
    room_t(R32_SIDE_OF_MOAT,                                            &
    door_t( -1,  0, 5),                                                 &
    (/   0,   0,   0,   0,  26,   0,   0,   0,   0,    0 /),            &
    -1, "  You are at the side of the moat.", (/ character(len=80) ::          &
'  You are at the bank of a wide moat which surrounds the',                           &
'  castle.  A small town can be seen far in the distance.',                           &
'  The road goes south.'                                                             &
    /)),                                                                &
    room_t(R33_ARMOR_ROOM,                                              &
    door_t( -1,  0, 5),                                                 &
    (/   0,   0,   0,   0,   3,   0,   0,   0,   0,    0 /),            &
    -1, "  You are in the armor room.", (/ character(len=80) ::          &
'  This is the armor room.  Suits of armor are lined up in',                          &
'  rows throughout the room.  Medieval instruments of war',                           &
'  are on display along each wall.  The door exits south.'                           &
    /)),                                                                &
    room_t(R34_FORK,                                                    &
    door_t( -1,  0, 0),                                                 &
    (/   0,   0,  16,   0,   0,  35,   0,  36,   0,    0 /),            &
    -1, "  You're at the fork.", (/ character(len=80) ::          &
'  You are at the proverbial fork in the road.  Paths',                               &
'  lead east, northwest, and southwest.'                                             &
    /)),                                                                &
    room_t(R35_TORTURE,                                                 &
    door_t( -1,  0, 2),                                                 &
    (/   0,  34,   0,   0,   0,   0,   0,   0,   0,    0 /),            &
    -1, "  You're in torture chamber.", (/ character(len=80) ::          &
'  This is the torture chamber.  A matched set of thumb-',                            &
'  screws hangs on the far wall.  A large rack occupies',                             &
'  the center of the room.  A skeleton hangs from its',                               &
'  thumbs above you, swaying gently.  An arch leads NE.'                             &
    /)),                                                                &
    room_t(R36_SLAB,                                                    &
    door_t( -1,  8, 0),                                                 &
    (/   0,   0,   0,  34,   0,   0,   0,  37,   0,    0 /),            &
    -1, "  You're in slab room.", (/ character(len=80) ::          &
'  You are in a small room with a high ceiling.  A huge',                             &
'  granite slab rests in the middle of the room.  The',                               &
'  exits lead SE and NW.'                                                            &
    /) ),                                                               &
    room_t(R37_SPIRAL_STAIRS,                                           &
    door_t( -1,  0, 0),                                                 &
    (/   0,   0,   0,   0,  36,   0,   0,   0,  38,    0 /),            &
    -1, "  You are at bottom of spiral stairs.", (/ character(len=80) ::          &
'  You are at the bottom of a towering spiral stairway.',                             &
'  A low passage exits south.'                                                       &
    /) ),                                                               &
    room_t(R38_TOWER,                                                   &
    door_t( -1,  0, 10),                                                &
    (/   0,   0,   0,   0,   0,   0,   0,   0,   0,   37 /),            &
    -1, "  You're at top of tower.", (/ character(len=80) ::          &
'  You are at the top of the castle tower.  Far off in',                              &
'  the distance you can see a dirt road leading to the',                              &
'  moat.  Across the moat is a small village.  A small',                              &
'  path winds west from the south end of the road.'                                  &
    /) ),                                                               &
    room_t(R39_BELOW_SMALL_WINDOW,                                      &
    door_t( -1,  0, 0),                                                 &
    (/   0,   0,  30,   0,   0,   0,   0,   0,   0,    0 /),            &
    -1, "  You are beneath small window.", (/ character(len=80) ::          &
'  You are standing below a small window.  A small',                                  &
'  path winds its way east.'                                                         &
    /) ),                                                               &
    room_t(R40_FAR_SIDE_OF_MOAT,                                        &
    door_t( -1,  0, 0),                                                 &
    (/   0,   0,   0,   0,   0,   0,   0,   0,   0,  -80 /),            &
    -1, "  You're on the far side of the moat", (/ character(len=80) ::          &
'  You are on the far side of the moat.  You can see',                                &
'  a full view of the castle here in all its deadly',                                 &
'  splendor.  A small town can be glimpsed far off in',                               &
'  the distance.  An old sign nailed to a tree reads:',                               &
'       "YOU CAN''T REACH THE VILLAGE FROM HERE!"'                                    &
    /) ) /)

  this%room(41:50) = (/                                                 &
    room_t(R41_HUGE_ANTEROOM,                                           &
    door_t( -1,  7, 0),                                                 &
    (/   0,   0,  23,   0,   0,   0,  43,  42,   0,   42 /),            &
    -1, "  You're in a huge anteroom.", (/ character(len=80) ::          &
"  You are in a huge anteroom to an even larger, mysterious",                         &
"  chamber.  A chilling wind seems to blow at you from all",                          &
"  sides, and a deathlike, vapid black mist surrounds your",                          &
"  feet.  Hundreds of sinister looking bats cling to the",                            &
"  ceiling and eye you with a spine-tingling anticipatory",                           &
"  pleasure.  Two dark, foreboding passages exit to the east",                        &
"  and west, and a steep sloping corridor descends NW."                              &
    /) ),                                                               &
    room_t(R42_STEEP_LEDGE,                                             &
    door_t( -1,  0, 9),                                                 &
    (/   0,   0,   0,  41,   0,   0,   0,   0,  41,    0 /),            &
    -1, "  You're at a steep ledge.", (/ character(len=80) ::          &
"  You're on a steep ledge above a seemingly bottomless pit.",                        &
"  Skeletal remains still cling to various rocks jutting out",                        &
"  of the pit.  Horrible, blood-curdling wails can be heard",                         &
"  from deep within the pit, obviously cries from the dead.",                         &
"  A steep corridor rises to the SE."                                                &
    /) ),                                                               &
    room_t(R43_MASTER_CHAMBER,                                          &
    door_t( -1,  0, 3),                                                 &
    (/   0,   0,  41,   0,   0,   0,   0,   0,   0,    0 /),            &
    -1, "  You're in the Master chamber.", (/ character(len=80) ::          &
"  You are in the chamber of the master of the castle,",                              &
"  Count Vladimir!  Pictures depicting scenes of tranquil",                           &
"  Transylvanian countrysides line the walls.  A huge",                               &
"  portrait of Vladimir's brother, Count Dracula, hangs",                             &
"  upon the near wall.  In the center of the room is a",                              &
"  large, ominous, mahogany coffin."                                                 &
    /) ),                                                               &
    room_t(R44_SQUARE_ROOM,                                             &
    door_t( -1,  5, 9),                                                 &
    (/   0,   0,   0,   0,  45,   0,   0,   0,   4,    0 /),            &
    -1, "  You are in the square room.", (/ character(len=80) ::          &
"  You are in a perfectly square room carved out of solid",                           &
"  rock.  Stone steps lead up.  An arched passage exits",                             &
"  south.  Above the arch is carved the message:",                                    &
"  ",                                                                                 &
'          "ABANDON HOPE ALL YE WHO ENTER HERE".'                                     &
    /) ),                                                               &
    room_t(R45_SLOPING_NS_PASSAGE,                                      &
    door_t( -1,  0, 1),                                                 &
    (/  44,   0,   0,   0,  46,   0,   0,   0,  44,   46 /),            &
    -1, "  You're in a sloping N/S passage.", (/ character(len=80) ::          &
"  You are in a long sloping N/S passage.  The darkness",                             &
"  seems to thicken around you as you walk."                                         &
    /) ),                                                               &
    room_t(R46_NARROW_ROOM,                                             &
    door_t( -1,  0, 0),                                                 &
    (/  45,   0,  54,   0,  47,   0,   0,   0,  45,   47 /),            &
    -1, "  You're in narrow room.", (/ character(len=80) ::          &
"  You are in a narrow room which extends out of sight",                              &
"  to the east.  Sloping paths exit north and south.",                                &
"  It is getting warmer here."                                                       &
    /) ),                                                               &
    room_t(R47_FIRE_ROOM,                                               &
    door_t( -1,  0, 0),                                                 &
    (/  46, -48,   0,   0,   0,   0,  49,   0,  46,    0 /),            &
    -1, "  You are in the fire room.", (/ character(len=80) ::          &
"  This is the fire room.  The stone walls are gutted",                               &
"  from centuries of evil fires.  It is very hot here.",                              &
"  A low trail leads west and a smaller one leads NE.",                               &
"  A sloping trail goes north."                                                      &
    /) ),                                                               &
    room_t(R48_GLOWING_ROCK_ROOM,                                       &
    door_t( -1,  1, 0),                                                 &
    (/  56,   0,   0,   0,  69,   0,  47,   0,  69,    0 /),            &
    -1, "  You're in glowing rock room.", (/ character(len=80) ::          &
"  You're in a huge cavernous room carved out of a strange",                          &
"  glowing rock.  Small drops of water drip from various",                            &
"  limestone stalactites on the ceiling high above you.",                             &
"  A narrowing path heads west.  Another passage goes",                               &
"  north.  A large door with an awning goes south."                                  &
    /) ),                                                               &
    room_t(R49_BLUE_ROOM,                                               &
    door_t( -1,  0, 0),                                                 &
    (/  57,   0,  47,   0,  50,   0,   0,   0,   0,    0 /),            &
    -1, "  This is the blue room.", (/ character(len=80) ::          &
"  You are in the blue room.  The entire room is a deep",                             &
"  shade of royal blue.  Exits go north, south and east."                            &
    /) ),                                                               &
    room_t(R50_NARROW_EW_PASSAGE,                                       &
    door_t( -1,  0, 0),                                                 &
    (/   0,   0,  49,   0,  66,   0,  53,   0,   0,    0 /),            &
    -1, "  You are in narrow E/W passage.", (/ character(len=80) ::          &
"  You are in a narrow E/W passage.  A faint noise of",                               &
"  rushing water can be heard.  A small crawl goes south."                           &
    /) ) /)

  this%room(51:60) = (/                                                 &
    room_t(R51_TWISTY_TUNNEL,                                           &
    door_t( -1,  2, 6),                                                 &
    (/   0,  55,   0,   0,   0,  54,   0,   0,   0,    0 /),            &
    -1, "  You are in a twisty tunnel.", (/ character(len=80) ::          &
"  You are in a twisty tunnel that goes NE and SW.  It",                             &
"  seems to be cooler here."                                                        &
    /) ),                                                               &
    room_t(R52_COMPASS_ROOM,                                            &
    door_t( -1,  0, 3),                                                 &
    (/   0,   0,  53,   0,   0,   0,   0,   0,   0,    0 /),            &
    -1, "  You're in the compass room.", (/ character(len=80) ::          &
"  You are inside a small, low room with strange engravings",                        &
"  carved in all four walls.  Embedded in the rocky floor",                          &
"  is a large compass made of marble and mother-of-pearl."                          &
    /) ),                                                               &
    room_t(R53_OUTSIDE_SMALL_ROOM,                                      &
    door_t( -1,  7, 3),                                                 &
    (/   0,   0,  50,   0,   0,   0,  52,   0,   0,    0 /),            &
    -1, "  You are outside a small room.", (/ character(len=80) ::          &
"  You are outside a small room.  The path goes back",                               &
"  east, from whence you came."                                                     &
    /) ),                                                               &
    room_t(R54_HEART_SHAPED_ARCH,                                       &
    door_t( -1,  0, 0),                                                 &
    (/   0,  51,   0,   0,   0,   0,  46,   0,   0,    0 /),            &
    -1, "  You are at the heart shaped arch.", (/ character(len=80) ::          &
"  The path narrows here and continues NE under a",                                  &
"  carefully constructed heart-shaped arch.  A",                                     &
"  wider path leads west.  It is very quiet here."                                  &
    /) ),                                                               &
    room_t(R55_HONEYMOON_SUITE,                                         &
    door_t( -1,  0, 6),                                                 &
    (/   0,   0,   0,   0,   0,  51,   0,   0,   0,    0 /),            &
    -1, "  You're in honeymoon suite.", (/ character(len=80) ::          &
"  This is the honeymoon suite.  The entire room is",                                &
"  finished in red.  In the center of the room lies a",                              &
"  heart shaped bed.  To one side is a heart shaped",                                &
"  bath.  A large mirror is mounted on the ceiling above",                           &
"  the bed.  The only exit is back the way you came."                               &
    /) ),                                                               &
    room_t(R56_EMPTY_ROOM,                                              &
    door_t( -1,  0, 0),                                                 &
    (/   0,   0,   0,   0,  48,   0,   0,   0,   0,    0 /),            &
    -1, "  You are in the empty room.", (/ character(len=80) ::          &
"  This room has absolutely nothing in it.  Your footsteps",                         &
"  echo hollowly as you walk the length of the room.  On",                           &
'  the bare floor is scrawled the word "POOF".'                                     &
    /) ),                                                               &
    room_t(R57_MAZE_SHORT_WINDING,                                      &
    door_t( -1,  0, 0),                                                 &
    (/  58,  61,  60,  59,  57,  49,  57,  49,  57,   57 /),            &
    -1, "  You're in a maze of short and winding passages.", (/ character(len=80) ::          &
"  You're in a long and winding maze of passages."                                  &
    /) ),                                                               &
    room_t(R58_LONG_WINDING_MAZE,                                       &
    door_t( -1,  0, 0),                                                 &
    (/  58,  60,  61,  59,  61,  63,  59,  57,  58,   58 /),            &
    -1, "  You're in a long and winding maze of passages.", (/ character(len=80) ::          &
"  You're in a winding maze of long passages."                                      &
    /) ),                                                               &
    room_t(R59_WINDING_LONG_MAZE,                                       &
    door_t( -1,  0, 0),                                                 &
    (/  59,  61,  58,  60,  63,  60,  57,  58,  59,   64 /),            &
    -1, "  You're in a winding, long maze of passages.", (/ character(len=80) ::          &
"  You're in a maze of long and winding passages."                                  &
    /) ),                                                               &
    room_t(R60_MAZE_LONG_WINDING,                                       &
    door_t( -1,  0, 0),                                                 &
    (/  58,  60,  59,  61,  61,  57,  59,  62,  60,   60 /),            &
    -1, "  You're in a maze of long and winding passages.", (/ character(len=80) ::          &
"  You're in a long and winding maze of passages."                                  &
    /) ) /)

  this%room(61:70) = (/                                                 &
    room_t(R61_MAZE_WINDING_LONG,                                       &
    door_t( -1, 19, 1),                                                 &
    (/  59,  61,  60,  58,  57,  60,  62,  58,  61,   61 /),            &
    -1, "  You're in a maze of winding, long passages.", (/ character(len=80) ::          &
"  You're in a short and winding maze of passages."                                  &
    /) ),                                                               &
    room_t(R62_SHORT_WINDING_MAZE,                                      &
    door_t( -1,  9, 9),                                                 &
    (/  57,  63,  58,  59,  60,  61,  64,  62,  62,   65 /),            &
    -1, "  You're in a short and winding maze of passages.", (/ character(len=80) ::          &
"  You're in a maze of short and winding passages."                                  &
    /) ),                                                               &
    room_t(R63_WINDING_MAZE_SHORT,                                      &
    door_t( -1,  1, 9),                                                 &
    (/  62,  57,  60,  61,  64,  63,  58,  59,  74,   63 /),            &
    -1, "  You're in a winding maze of short passages.", (/ character(len=80) ::          &
"  You're in a maze of short and winding passages."                                  &
    /) ),                                                               &
    room_t(R64_DEAD_END,                                                &
    door_t( -1,  1, 1),                                                 &
    (/  58,  59,  60,  58,  59,  60,  58,  59,  60,   58 /),            &
    -1, "  Dead end.", (/ character(len=80) ::          &
"  DEAD END."                                                                        &
    /) ),                                                               &
    room_t(R65_GLACIER_ROOM,                                            &
    door_t( -1,  1, 0),                                                 &
    (/ -90,   0,  62,   0,   0,  73,   0,   0,  78,    0 /),            &
    -1, "  You are in the glacier room.", (/ character(len=80) ::          &
"  This is the glacier room.  The walls are covered with",                            &
"  dazzling shapes of ice which reflect the light from your",                         &
"  lamp in a million colors.  In the far side of the room",                           &
"  are magnificent ice sculptures of animals unknown to",                             &
'  Mankind.  A faint "X" is scratched in the ice on one wall.',                       &
"  Icy passages exit SW and east.  A steep trail goes up."                           &
    /) ),                                                               &
    room_t(R66_LONG_DAMP_TUNNEL,                                        &
    door_t( -1,  0, 0),                                                 &
    (/  50,   0,  68,   0,   0,   0,   0,   0,   0,    0 /),            &
    -1, "  You are in the long damp tunnel.", (/ character(len=80) ::          &
"  You are in a long bending tunnel which leads north",                               &
"  and east.  The walls are damp here, and you can",                                  &
"  distinctly hear the sound of rushing water."                                      &
    /) ),                                                               &
    room_t(R67_ABOVE_WATERFALL,                                         &
    door_t( -1,  7, 10),                                                &
    (/   0,   0,   0,   0,   0,   0,   0,   0,   0,   68 /),            &
    -1, "  You're overlooking waterfall.", (/ character(len=80) ::          &
"  You are on a ledge overlooking the underground waterfall.",                        &
"  Torrents of water cascade over smoothly polished rocks",                           &
"  and crash loudly several hundred feet below.  The water",                          &
"  sprays a fine, refreshing mist throughout the cavern.",                            &
"  Exotic marine creatures dance below the surface of the",                           &
"  still water at the base of the waterfall.  Thousands of",                          &
"  pennies are visible beneath the water where previous",                             &
"  explorers have wished for luck in escaping from the cave.",                        &
"  High above you a rainbow bridges the mist, displaying the",                        &
"  entire visible spectrum.  Stone steps lead down."                                 &
    /) ),                                                               &
    room_t(R68_BASE_OF_WATERFALL,                                       &
    door_t( -1,  3, 7),                                                 &
    (/   0,   0,   0,   0,   0,   0,  66,   0,  67,    0 /),            &
    -1, "  You are at the base of the waterfall.", (/ character(len=80) ::          &
"  You are at the base of a magnificent underground waterfall.",                      &
"  A cool mist rising off the surface of the water almost obscures",                  &
"  a small island.  A tunnel goes west and stone steps lead up."                     &
    /) ),                                                               &
    room_t(R69_LOBBY,                                                   &
    door_t( -1,  3, 1),                                                 &
    (/  48,   0,  70,   0,   0,   0,   0,   0,   0,   48 /),            &
    -1, "  This is the lobby.", (/ character(len=80) ::          &
"  This is the main lobby.  A gaudy crystal chandelier hangs",                        &
"  from the center of a tastelessly finished room.  The decor",                       &
"  seems to be from the depression, although junk like this",                         &
"  is timeless.  A passageway leads north and down.  To the",                         &
"  east is an open elevator."                                                        &
    /) ),                                                               &
    room_t(R70_ELEVATOR_TOP,                                            &
    door_t( -1,  0, 7),                                                 &
    (/   0,   0,   0,   0,   0,   0,  69,   0,   0,   71 /),            &
    -1, "  You're in elevator on top floor.", (/ character(len=80) ::          &
"  You are inside an elevator.  The light panel indicates that",                      &
'  you are on the upper of two levels.  The "THIS CAR NEXT"',                         &
"  sign is lit."                                                                     &
    /) ) /)

  this%room(71:80) = (/                                                 &
    room_t(R71_ELEVATOR_BOTTOM,                                         &
    door_t( -1,  3, 7),                                                 &
    (/   0,   0,   0,   0,   0,   0,  72,   0, -70,    0 /),            &
    -1, "  You're in elevator on bottom floor.", (/ character(len=80) ::          &
"  You are inside an elevator.  The light panel indicates that",                      &
'  you are on the lower of two levels.  The "THIS CAR NEXT"',                         &
"  sign is lit."                                                                     &
    /) ),                                                               &
    room_t(R72_VAULT,                                                   &
    door_t( -1,  5, 3),                                                 &
    (/   0,   0,  71,   0,   0,   0,   0,   0,   0,    0 /),            &
    -1, "  You are in the treasure vault.", (/ character(len=80) ::          &
"  This is the safe deposit vault, an immense room with polished",                   &
"  steel walls.  A closed circuit T.V. camera hums quietly above",                   &
"  you as it pans back and forth across the room.  To the east is",                  &
"  an open elevator.  Engraved on the far wall is the message:",                     &
'                      "DEPOSIT TREASURES HERE FOR FULL CREDIT"'                    &
    /) ),                                                               &
    room_t(R73_BURIAL_GROUNDS,                                          &
    door_t( -1,  0, 0),                                                 &
    (/   0,   0,  65,   0,  74,   0,   0,   0,   0,    0 /),            &
    -1, "  You are in the burial grounds.", (/ character(len=80) ::          &
"  You are in a room full of old and decaying skeletons.  The",                      &
"  rotting forms of many werewolves can be identified among",                        &
"  the bodies.  Along the near wall is a desk with a crystal",                       &
'  ball and a nameplaque that says "OLD GYPSY WOMAN".  The',                         &
"  gypsy is nowhere to be seen.  A path leads east and a small",                     &
"  tunnel goes south."                                                              &
    /) ),                                                               &
    room_t(R74_FOUR_TUNNEL_JUNCTION,                                    &
    door_t( -1,  1, 0),                                                 &
    (/  73,   0,  63,   0,  75,   0,  77,   0,   0,    0 /),            &
    -1, "  You are at four tunnel junction.", (/ character(len=80) ::          &
"  You are in a small room which is the junction of four small",                     &
"  tunnels.  The tunnels lead north, south, east and (you",                          &
"  guessed it) west."                                                               &
    /) ),                                                               &
    room_t(R75_RAIN_FOREST,                                             &
    door_t( -1,  0, 1),                                                 &
    (/  74,   0,   0,  76,   0,   0,  82,   0,  76,    0 /),            &
    -1, "  You are in rain forest.", (/ character(len=80) ::          &
"  You are in an immense cavern filled with tropical flora",                         &
"  and fauna, reminiscent of an Amazon rain forest.  Wild",                          &
"  parrots and toucans fly aimlessly overhead.  The sound",                          &
"  of rushing water is coming from the SE.  A small tunnel",                         &
"  goes north and a rocky trail leads west."                                        &
    /) ),                                                               &
    room_t(R76_SMALL_LEDGE,                                             &
    door_t( -1,  0, 10),                                                &
    (/   0,   0,   0,   0,   0,   0,   0,  75,   0,   75 /),            &
    -1, "  You are up on a small ledge.", (/ character(len=80) ::          &
"  You are up on a ledge which affords a partial view of a",                         &
"  large underground waterfall.  Across the water you can",                          &
"  see a larger ledge which provides a better vantage point.",                       &
"  A climbable cliff goes down and NW."                                             &
    /) ),                                                               &
    room_t(R77_GRAFFITI_ROOM,                                           &
    door_t( -1,  0, 3),                                                 &
    (/   0,   0,  74,   0,  83,   0,   0,   0,   0,    0 /),            &
    -1, "  You are in the graffiti room.", (/ character(len=80) ::          &
"  You are in a room whose walls are covered with graffiti.",                        &
'  The messages range from "FRODO LIVES" to filthy remarks',                         &
"  concerning the parental lineage of werewolves.  A rocky",                         &
"  path goes south.  A low tunnel leads east."                                      &
    /) ),                                                               &
    room_t(R78_DAMP_TUNNEL_EAST,                                        &
    door_t( -1,  0, 0),                                                 &
    (/   0,   0,   0,   0,   0,   0,  79,   0,   0,   65 /),            &
    -1, "  You're at east end of a damp, earthen tunnel.", (/ character(len=80) ::          &
"  You are at the east end of a long, damp earthen tunnel.  Above",                  &
"  you dangle rows of tomato plant roots arranged neatly in rows.",                  &
"  A steep but climbable trail plummets down."                                      &
    /) ),                                                               &
    room_t(R79_DAMP_TUNNEL_WEST,                                        &
    door_t( -1,  7, 0),                                                 &
    (/   0,   0,  78,   0,   0,   0,  80,   0,   0,    0 /),            &
    -1, "  You're at west end of a damp, earthen tunnel.", (/ character(len=80) ::          &
"  You are at the west end of a long, damp earthen tunnel.  Water",                  &
"  drips down from the roof and seeps into the muddy floor.  The",                   &
"  tunnel continues east, and a drier passage leads west."                          &
    /) ),                                                               &
    room_t(R80_WINE_CELLAR,                                             &
    door_t(  0,  9, 0),                                                 &
    (/   0,   0,  79,   0,   0,   0,   0,   0, -40,    0 /),            &
    -1, "  You are in the wine cellar.", (/ character(len=80) ::          &
"  You are in the remains of an old wine cellar, apparantly the",                    &
"  victim of a cave in.  Casks of once fine wine lie crushed in",                    &
"  the rubble.  A battered keg of GENESEE sits off in the corner.",                  &
"  The room smells like a Rathskellar band party.  A muddy path",                    &
"  goes east, and steps lead up to a door in the ceiling."                          &
    /) ) /)

    this%room(81:90) = (/                                                 &
    room_t(R81_ISLAND,                                                  &
    door_t( -1,  0, 0),                                                 &
    (/   0,   0,   0,   0,   0,   0,   0,   0,   0,    0 /),            &
    -1, "  You are on the island near the falls.", (/ character(len=80) ::          &
"  You are on a small island near a large waterfall.  The sound of",                 &
"  crashing surf can be clearly heard, although you cannot quite",                   &
"  make out the form of the waterfall through the thick mist.  A",                   &
'  message traced out in the sand reads "GILLIGAN WAS HERE".  There',                &
"  are pieces of a wreck (the S.S. MINNOW?) scattered about."                       &
    /) ),                                                               &
    room_t(R82_IMMENSE_CAVERN_NORTH,                                    &
    door_t( -1,  3, 3),                                                 &
    (/   0,   0,  75,   0,  83,   0,   0,   0,   0,    0 /),            &
    -1, "  You're at north end of an immense cavern.", (/ character(len=80) ::          &
"  You are in the north end of a large cavern that extends",                         &
"  far to the south.  A low crawl leads to the east."                               &
    /) ),                                                               &
    room_t(R83_LARGE_CAVERN_SOUTH,                                      &
    door_t( -1,  0, 3),                                                 &
    (/  82,   0,  77,   0,   0,   0,   0,   0, -84,    0 /),            &
    -1, "  You are at the south end of a large cavern.", (/ character(len=80) ::          &
"  You are in the south end of a large cavern.  The",                                &
"  cavern wall before you rises vertically, ending at a",                            &
"  ledge well out of your reach.  The cavern continues",                             &
"  north and out of sight.  A tiny trail heads east."                               &
    /) ),                                                               &
    room_t(R84_TOP_OF_PRECIPICE,                                        &
    door_t( -1,  3, 0),                                                 &
    (/   0,   0,  85,   0,   0,   0,  86,   0,   0,  -83 /),            &
    -1, "  You are at the top of a steep precipice.", (/ character(len=80) ::          &
"  You are at the edge of a sheer vertical drop overlooking",                        &
"  an immense N/S cavern.  Narrow paths head away to the",                           &
"  east and west."                                                                  &
    /) ),                                                               &
    room_t(R85_DISCO_ROOM,                                              &
    door_t( -1,  0, 7),                                                 &
    (/   0,   0,   0,   0,  87,   0,  84,   0,   0,   61 /),            &
    -1, "  This is the disco room.", (/ character(len=80) ::          &
"  This is the disco room.  Multicolored lasers pulsate",                            &
"  wildly to the beat of badly mixed music.  A stairway",                            &
"  down is barely visible through the glare.  A large",                              &
"  passage exits south, and a smaller one leads west."                              &
    /) ),                                                               &
    room_t(R86_TALL_TUNNEL,                                             &
    door_t( -1,  5, 3),                                                 &
    (/   0,   0,  84,  87, -88,   0,  91,   0,   0,    0 /),            &
    -1, "  You're in the center of a tall tunnel.", (/ character(len=80) ::          &
"  You are in a tall tunnel leading east and west.  A small",                        &
"  trail goes SE.  An immense wooden door heads south."                             &
    /) ),                                                               &
    room_t(R87_LIVING_DEAD,                                             &
    door_t( -1,  1, 7),                                                 &
    (/  85,   0,   0,   0,   0,   0,  86,   0,   0,    0 /),            &
    -1, "  You are in the land of the living dead.", (/ character(len=80) ::          &
"  You have entered the land of the living dead, a",                                 &
"  large, desolate room.  Although it is apparently",                                &
"  uninhabited, you can hear the awful sounds of",                                   &
"  thousands of lost souls weeping and moaning.",                                    &
"  In the east corner are stacked the remains of",                                   &
"  dozens of previous adventurers who were less",                                    &
"  fortunate than yourself.  To the north is a",                                     &
"  foreboding passage.  A path goes west."                                          &
    /) ),                                                               &
    room_t(R88_CYCLOPS_LAIR,                                            &
    door_t( -1,  0, 1),                                                 &
    (/  86,   0,   0,   0,   0,   0,   0,   0,   0,    0 /),            &
    -1, "  You are in the cyclops' lair.", (/ character(len=80) ::          &
"  You are in the lair of the cyclops.  The broken bones of",                        &
"  previous explorers are strewn about the room.  One huge",                         &
"  contact lens lies on a high table.  The cyclops seems",                           &
"  to be gone.  A splintered door exits north."                                     &
    /) ),                                                               &
    room_t(R89_INSIDE_GLACIER,                                          &
    door_t( -1,  0, 7),                                                 &
    (/   0,   0,   0,   0,   0,   0,  90,   0,   0,    0 /),            &
    -1, "  You are deep inside the glacier.", (/ character(len=80) ::          &
"  You are in an extremely cold chamber imbedded deep",                              &
"  within the glacier.  Stalagtites of ice hang from",                               &
"  the ceiling far above you.  An ominous tunnel leaves",                            &
"  to the west."                                                                    &
    /) ),                                                               &
    room_t(R90_COLD_TUNNEL,                                             &
    door_t( -1,  9, 0),                                                 &
    (/   0,   0,  89,   0,  65,   0,   0,   0,   0,    0 /),            &
    -1, "  You are in a cold, bending tunnel.", (/ character(len=80) ::          &
"  You are in a long, bending tunnel.  With your lamp",                              &
"  you can barely make out exits to the south and east."                            &
    /) ) /)

  this%room(91:100) = (/                                                &
    room_t(R91_BORDER,                                                  &
    door_t( -1,  1, 3),                                                 &
    (/  92,   0,  86,   0,   0,   0,   0,   0,   0,    0 /),            &
    -1, "  You're at the border.", (/ character(len=80) ::          &
"  You are in a tremendous cavern divided by a white line",                          &
"  through its center.  The north side of the cavern is",                            &
"  green and fresh, a startling change from the callous",                            &
"  terrain of the cave.  A sign at the border proclaims",                            &
"  this to be the edge of the wizard's realm.  A rocky",                             &
"  and forlorn trail leads east, and a plush green path",                            &
"  wanders north."                                                                  &
    /) ),                                                               &
    room_t(R92_FOREST,                                                  &
    door_t( -1,  0, 0),                                                 &
    (/ -93,   0,   0,   0,  91,   0,   0,   0,   0,    0 /),            &
    -1, "  You are in the forest of tall trees.", (/ character(len=80) ::          &
"  You are in an immense forest of tall trees.  Melodic",                            &
"  chanting can be heard in the distance.  The trees seem",                          &
"  to be guiding you along a N/S path."                                             &
    /) ),                                                               &
    room_t(R93_THRONE_ROOM,                                             &
    door_t( -1,  7, 0),                                                 &
    (/   0,   0,  91,   0,  92,   0, -94,   0,   0,    0 /),            &
    -1, "  You are in the throne room.", (/ character(len=80) ::          &
"  This is the wizard's throne room.  Scattered about",                              &
"  the room are various magical items.  A long message",                             &
"  in ancient runes is carved into the southern wall.  It",                          &
'  translates roughly as "Beware the power of the Wizard,',                          &
'  for he is master of this place".  Two green paths go',                            &
"  south and east, and a marble walk leads west."                                   &
    /) ),                                                               &
    room_t(R94_WIZARDS_CACHE,                                           &
    door_t( -1,  0, 3),                                                 &
    (/   0,   0,  93,   0,   0,   0,   0,   0,   0,    0 /),            &
    -1, "  This is the Wizard's cache.", (/ character(len=80) ::          &
"  You are in the wizard's cache, a large room whose walls",                         &
"  are inlaid with jewels.  A majestic marble walk leads to",                        &
"  the east."                                                                       &
    /) ),                                                               &
    room_t(R95_WAREHOUSE_MAIN,                                          &
    door_t( -1,  0, 0),                                                 &
    (/  97,   0,  98,   0,  99,   0,  96,   0,   0,    0 /),            &
    -1, "  You are in the main room of the warehouse.", (/ character(len=80) ::          &
"  You are in a room of mammoth proportions which seems",                            &
"  to be some sort of warehouse.  On a nearby table are",                            &
"  several clipboards and a massive pile of order forms.",                           &
"  To your right is a large loading dock and a truck bay.",                          &
"  The room opens to the north, east and west."                                     &
    /) ),                                                               &
    room_t(R96_WAREHOUSE_WEST,                                          &
    door_t( -1,  0, 0),                                                 &
    (/   0,   0,  95,   0,   0,   0,   0,   0,   0,    0 /),            &
    -1, "  You are in the west end of the warehouse.", (/ character(len=80) ::          &
"  You are in the western end of the Castlequest warehouse.",                        &
"  To one side sits an empty kerosene drum.  Scattered",                             &
"  all over the floor are shreds of foam rubber used to",                            &
"  pack delicate crystal swans for shipping.  The empty",                            &
"  tins of countless T.V. dinners are discarded around",                             &
"  a full garbage can."                                                             &
    /) ),                                                               &
    room_t(R97_WAREHOUSE_NORTH,                                         &
    door_t( -1,  0, 0),                                                 &
    (/   0,   0,   0,   0,  95,   0,   0,   0,   0,    0 /),            &
    -1, "  You are in the north end of the warehouse.", (/ character(len=80) ::          &
"  You are in the north end of the Castlequest warehouse.",                          &
"  There is a large box of BIC CLICs to your left, all",                             &
'  empty (they don''t write "first time, every time").',                              &
"  On a shelf to your right is a huge supply of Burpee",                             &
"  tomato plant seeds."                                                             &
    /) ),                                                               &
    room_t(R98_WAREHOUSE_EAST,                                          &
    door_t( -1,  0, 0),                                                 &
    (/   0,   0,   0,   0,   0,   0,  95,   0,   0,    0 /),            &
    -1, "  You are in the east end of the warehouse.", (/ character(len=80) ::          &
"  You are in the eastern end of the Castlequest warehouse.",                        &
"  In a box near the corner are the empty cartridges from",                          &
"  many silver bullets.  Through a small window you can spy",                        &
"  the Vampire Diner (talk about greasy spoons)."                                   &
    /) ),                                                               &
    room_t(R99_ELEVATOR_BETWEEN_FLOORS,                                 &
    door_t( -1,  7, 7),                                                 &
    (/ -95, -95, -95, -95, -95, -95, -95, -95,   0,    0 /),            &
    -1, "  You are in the elevator, stuck between floors.", (/ character(len=80) ::          &
"  The elevator has screeched to a halt between two floors."                        &
    /) ),                                                               &
    room_t(R100_ENDGAME,                                                &
    door_t( -1,  0, 0),                                                 &
    (/   0,   0,   0,   0,   0,   0,   0,   0,   0,    0 /),            &
    -1, "  END OF GAME", (/ character(len=80) ::          &
"  You feel the elevator jump as you are wisked up towards",                         &
"  ground level.  You emerge in the open air in the village",                        &
"  square amidst cheers from the local villagers.  Banners",                         &
"  proclaiming the death of count Vladimir hang from most",                          &
"  of the old buildings around the square.  The mayor",                              &
"  presents you with a key to the city and makes your",                              &
"  birthday a holiday.  You watch the sun rise as you",                              &
"  bask in your newfound fame."                                                     &
    /) ) /)

  return
end subroutine dungeon_init

!> Room darkness detection based on room ID
pure logical function dungeon_room_is_dark(this, room_id)               &
  result(is_dark)
  implicit none
  !> Map object reference
  class(dungeon_t), intent(in) :: this
  !> Room ID
  integer, intent(in) :: room_id
  continue
  is_dark = this%room(room_id)%is_dark()
  return
end function dungeon_room_is_dark

!> Room water detection based on room ID
pure logical function dungeon_room_has_water(this, room_id,             &
  omit_island) result(has_water)
  implicit none
  !> Map object reference
  class(dungeon_t), intent(in) :: this
  !> Room ID
  integer, intent(in) :: room_id
  !> Omit island from list of rooms with water, optional.
  logical, intent(in), optional :: omit_island
  continue
  if (present(omit_island)) then
    has_water = this%room(room_id)%has_water(omit_island)
  else
    has_water = this%room(room_id)%has_water()
  end if
  return
end function dungeon_room_has_water

!> Returns true if door is locked (`state_ == 0`)
pure logical function dungeon_door_is_locked(this, room_id)             &
  result(is_locked)
  implicit none
  !> Map object reference
  class(dungeon_t), intent(in) :: this
  !> Room ID
  integer, intent(in) :: room_id
  continue
  is_locked = this%room(room_id)%door%is_locked()
  return
end function dungeon_door_is_locked

!> Returns true if door is closed (`state_ == 1`)
pure logical function dungeon_door_is_closed(this, room_id)             &
  result(is_closed)
  implicit none
  !> Map object reference
  class(dungeon_t), intent(in) :: this
  !> Room ID
  integer, intent(in) :: room_id
  continue
  is_closed = this%room(room_id)%door%is_closed()
  return
end function dungeon_door_is_closed

!> Returns true if door is open (`state_ == 2`)
pure logical function dungeon_door_is_open(this, room_id)               &
  result(is_open)
  implicit none
  !> Map object reference
  class(dungeon_t), intent(in) :: this
  !> Room ID
  integer, intent(in) :: room_id
  continue
  is_open = this%room(room_id)%door%is_open()
  return
end function dungeon_door_is_open

!> Returns true if door is does not change state (`state_ == -2`)
pure logical function dungeon_door_is_static(this, room_id)             &
  result(is_static)
  implicit none
  !> Map object reference
  class(dungeon_t), intent(in) :: this
  !> Room ID
  integer, intent(in) :: room_id
  continue
  is_static = this%room(room_id)%door%is_static()
  return
end function dungeon_door_is_static

!> Returns true if door exists (`state_ /= -1`)
pure logical function dungeon_door_exists(this, room_id) result(exists)
  implicit none
  !> Map object reference
  class(dungeon_t), intent(in) :: this
  !> Room ID
  integer, intent(in) :: room_id
  continue
  exists = this%room(room_id)%door%exists()
  return
end function dungeon_door_exists

!> Initialize mirror maze object
subroutine mirror_maze_init(this)
  use m_object_id, only: R2_DIM_CORRIDOR, R9_UPSTAIRS_HALLWAY,          &
    R15_BOUDOIR, R28_L_CORRIDOR

  ! Mirror maze exit locations while the Master lives
  integer, dimension(10), parameter :: normal_exits = (/                &
    R28_L_CORRIDOR, R15_BOUDOIR, R28_L_CORRIDOR, R9_UPSTAIRS_HALLWAY,   &
    R28_L_CORRIDOR, R15_BOUDOIR, R15_BOUDOIR, R9_UPSTAIRS_HALLWAY,      &
    R28_L_CORRIDOR, R15_BOUDOIR /)

  !> Mirror maze object
  class(mirror_maze_t), intent(inout) :: this

  continue

  this%exits = normal_exits

  return
end subroutine mirror_maze_init

!> Return integer array of the current maze exit room IDs
function mirror_maze_state_to_array(this) result(svec)
  implicit none

  ! Output array of room IDs
  integer, dimension(10) :: svec

  !> Mirror maze object
  class(mirror_maze_t), intent(in) :: this
  continue

  svec = this%exits

  return
end function mirror_maze_state_to_array

!> Set maze exit state from array (state vector)
subroutine mirror_maze_array_to_state(this, svec)
  implicit none

  !> Mirror maze object
  class(mirror_maze_t), intent(inout) :: this

  !> Input array of room IDs
  integer, dimension(10), intent(in) :: svec

  continue

  this%exits(1:10) = svec(1:10)

  return
end subroutine mirror_maze_array_to_state

!> Set maze exits to emergency mode
subroutine mirror_maze_open_emergency_exits(this)
  use m_object_id, only: R2_DIM_CORRIDOR
  implicit none

  !> Mirror maze object
  class(mirror_maze_t), intent(inout) :: this

  continue

  this%exits = R2_DIM_CORRIDOR

  return
end subroutine mirror_maze_open_emergency_exits

!> Return room ID of the current maze exit
integer function mirror_maze_find_exit(this, emergency) result(room_id)
  use m_object_id, only: R2_DIM_CORRIDOR
  implicit none

  !> Mirror maze object
  class(mirror_maze_t), intent(inout) :: this

  !> State of emergency. Optional.
  logical, intent(in), optional :: emergency

  continue

  if (present(emergency)) then
    if (emergency) then
      call this%open_emergency_exits()
    end if
  end if

  ! Normal
  room_id = this%exits(random_room(lo_room=1, n_rooms=9))

  return
end function mirror_maze_find_exit

end module m_where
