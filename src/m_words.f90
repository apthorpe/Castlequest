!> @file m_words.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Data types representing nouns and verbs

!> @brief Data types representing nouns and verbs
module m_words
  use :: m_object_id
  implicit none

  private

  public :: init_wordlists
  public :: verb_list
  public :: noun_list

  !> @class word_t
  !! @brief Generic word identification structure
  type :: word_t
    !> Keyword
    character(len=4) :: word = '    '

    !> ID code
    integer :: id = -1
  end type word_t

  !> @class wordlist_t
  !! @brief Generic word list
  type :: wordlist_t
    !> List of supported words
    type(word_t), dimension(:), allocatable :: term
  contains
    !> Find index of work in wordlist
    procedure :: lookup => wordlist_lookup
  end type wordlist_t

  !> Public list of game verbs
  type(wordlist_t) :: verb_list

  !> Public list of game nouns
  type(wordlist_t) :: noun_list
contains

!> @brief Populate word lists
!!
!! @note The implicit constructor for `word_t` is used to populate each
!! element in each list. Arguments to `word_t` are assigned sequentially
!! to attributes; `word_t(arg1, arg2)` is equivalent to
!! `word_t%word = arg1`, `word_t%id = arg2`
subroutine init_wordlists()
  use :: m_object_id
  implicit none

  continue

  ! Populate verb list
  if (allocated(verb_list%term)) then
    deallocate(verb_list%term)
  end if
  verb_list%term = (/                                                   &
    word_t('ATTA', V15_ATTACK),                                         &
    word_t('BACK', V40_BACK),                                           &
    word_t('BREA', V37_BREAK),                                          &
    word_t('BRIE', V61_BRIEF),                                          &
    word_t('CHOP', V37_BREAK),                                          &
    word_t('CLIM', V9_UP),                                              &
    word_t('CLOS', V28_CLOSE),                                          &
    word_t('CROS', V43_CROSS),                                          &
    word_t('D   ', V10_DOWN),                                           &
    word_t('DEBU', V59_DEBUG),                                          &
    word_t('DOWN', V10_DOWN),                                           &
    word_t('DRIN', V24_DRINK),                                          &
    word_t('DROP', V12_DROP),                                           &
    word_t('E   ', V3_EAST),                                            &
    word_t('EAST', V3_EAST),                                            &
    word_t('EAT ', V23_EAT),                                            &
    word_t('ENTE', V13_ENTER),                                          &
    word_t('EXIT', V14_EXIT),                                           &
    word_t('EXTI', V34_EXTINGUISH),                                     &
    word_t('FEED', V22_FEED),                                           &
    word_t('FILL', V49_FILL),                                           &
    word_t('FIRE', V54_SHOOT),                                          &
    word_t('FUCK', V19_AHEM),                                           &
    word_t('GOTO', V51_GOTO),                                           &
    word_t('HELP', V50_HELP),                                           &
    word_t('HINT', V50_HELP),                                           &
    word_t('HONK', V45_HONK),                                           &
    word_t('IN  ', V13_ENTER),                                          &
    word_t('INVE', V26_INVENTORY),                                      &
    word_t('JUMP', V25_JUMP),                                           &
    word_t('KILL', V16_KILL),                                           &
    word_t('L   ', V52_LEFT),                                           &
    word_t('LEAV', V14_EXIT),                                           &
    word_t('LEFT', V52_LEFT),                                           &
    word_t('LIGH', V32_LIGHT),                                          &
    word_t('LOAD', V18_LOAD),                                           &
    word_t('LOCK', V29_LOCK),                                           &
    word_t('LONG', V60_VERBOSE),                                        &
    word_t('LOOK', V35_LOOK),                                           &
    word_t('MELT', V42_MELT),                                           &
    word_t('N   ', V1_NORTH),                                           &
    word_t('NE  ', V2_NORTHEAST),                                       &
    word_t('NORT', V1_NORTH),                                           &
    word_t('NW  ', V8_NORTHWEST),                                       &
    word_t('OFF ', V33_OFF),                                            &
    word_t('ON  ', V31_ON),                                             &
    word_t('OPEN', V27_OPEN),                                           &
    word_t('OUT ', V14_EXIT),                                           &
    word_t('POOF', V56_POOF),                                           &
    word_t('POUR', V39_POUR),                                           &
    word_t('QUIT', V44_QUIT),                                           &
    word_t('R   ', V53_RIGHT),                                          &
    word_t('READ', V48_READ),                                           &
    word_t('REST', V58_RESTORE),                                        &
    word_t('RIGH', V53_RIGHT),                                          &
    word_t('S   ', V5_SOUTH),                                           &
    word_t('SAVE', V57_SAVE),                                           &
    word_t('SCOR', V36_SCORE),                                          &
    word_t('SE  ', V4_SOUTHEAST),                                       &
    word_t('SHOO', V54_SHOOT),                                          &
    word_t('SHOW', V20_WAVE),                                           &
    word_t('SOUT', V5_SOUTH),                                           &
    word_t('STAB', V21_STAB),                                           &
    word_t('SUSP', V57_SAVE),                                           &
    word_t('SW  ', V6_SOUTHWEST),                                       &
    word_t('SWIM', V41_SWIM),                                           &
    word_t('T   ', V11_TAKE),                                           &
    word_t('TAKE', V11_TAKE),                                           &
    word_t('THRO', V17_THROW),                                          &
    word_t('TIE ', V46_TIE),                                            &
    word_t('U   ', V9_UP),                                              &
    word_t('UNLO', V30_UNLOCK),                                         &
    word_t('UNTI', V47_UNTIE),                                          &
    word_t('UP  ', V9_UP),                                              &
    word_t('VERB', V60_VERBOSE),                                        &
    word_t('W   ', V7_WEST),                                            &
    word_t('WAKE', V55_WAKE),                                           &
    word_t('WATE', V38_WATER),                                          &
    word_t('WAVE', V20_WAVE),                                           &
    word_t('WEST', V7_WEST) /)

  ! Populate noun list
  if (allocated(noun_list%term)) then
    deallocate(noun_list%term)
  end if
  noun_list%term = (/                                                   &
    word_t('ACID', O25_ACID),                                           &
    word_t('ALL ', N31_ALL),                                            &
    word_t('AXE ', O3_AXE),                                             &
    word_t('BARS', N54_BARS),                                           &
    word_t('BAT ', N55_BAT),                                            &
    word_t('BLOO', O5_BLOOD),                                           &
    word_t('BOAR', N56_BOARD),                                          &
    word_t('BOAT', O14_BOAT),                                           &
    word_t('BOOK', N46_BOOK),                                           &
    word_t('BOTT', O18_BOTTLE),                                         &
    word_t('BULL', O2_BULLET),                                          &
    word_t('BUTL', N49_BUTLER),                                         &
    word_t('CASK', N40_COFFIN),                                         &
    word_t('CHAM', O7_CHAMPAGNE),                                       &
    word_t('CIGA', O27_CIGAR),                                          &
    word_t('COFF', N40_COFFIN),                                         &
    word_t('COMP', 57),                                                 &
    word_t('COUN', N39_MASTER),                                         &
    word_t('CROS', O19_CROSS),                                          &
    word_t('CRYS', O30_SWAN),                                           &
    word_t('CYCL', N78_CYCLOPS),                                        &
    word_t('DOOR', N47_DOOR),                                           &
    word_t('DRAW', N48_DRAWER),                                         &
    word_t('FIGU', O24_JADE),                                           &
    word_t('FIRE', N34_FIRE),                                           &
    word_t('FLAS', O25_ACID),                                           &
    word_t('FOOD', O22_FOOD),                                           &
    word_t('GLAC', N79_GLACIER),                                        &
    word_t('GNOM', N77_GNOME),                                          &
    word_t('GRAP', O16_HOOK),                                           &
    word_t('GUN ', O20_GUN),                                            &
    word_t('HATC', O3_AXE),                                             &
    word_t('HOOK', O16_HOOK),                                           &
    word_t('HUNC', O8_HUNCHBACK),                                       &
    word_t('ICE ', N79_GLACIER),                                        &
    word_t('IVOR', O12_SWORD),                                          &
    word_t('JADE', O24_JADE),                                           &
    word_t('KERO', O1_KEROSENE),                                        &
    word_t('KEY ', O4_KEY),                                             &
    word_t('LAMP', O21_LAMP),                                           &
    word_t('LANT', O21_LAMP),                                           &
    word_t('LIGH', O21_LAMP),                                           &
    word_t('LOCK', N33_LOCK),                                           &
    word_t('MAST', N39_MASTER),                                         &
    word_t('MATC', O15_MATCH),                                          &
    word_t('MIRR', N50_MIRROR),                                         &
    word_t('MOAT', N51_MOAT),                                           &
    word_t('MONE', O29_MONEY),                                          &
    word_t('NOTE', N52_NOTE),                                           &
    word_t('OIL ', O1_KEROSENE),                                        &
    word_t('PAPE', O10_PAPER),                                          &
    word_t('PEN ', O11_PEN),                                            &
    word_t('PIST', O20_GUN),                                            &
    word_t('QUIL', O11_PEN),                                            &
    word_t('ROOM', N45_ROOM),                                           &
    word_t('ROPE', O9_ROPE),                                            &
    word_t('ROWB', O14_BOAT),                                           &
    word_t('RUBY', O23_RUBY),                                           &
    word_t('SAPP', O28_SAPPHIRE),                                       &
    word_t('SHUT', N38_SHUTTERS),                                       &
    word_t('STAK', O6_STAKE),                                           &
    word_t('STAT', O17_STATUE),                                         &
    word_t('SWAN', O30_SWAN),                                           &
    word_t('SWOR', O12_SWORD),                                          &
    word_t('TORC', O13_TORCH),                                          &
    word_t('TUNN', N41_TUNNEL),                                         &
    word_t('VAMP', N39_MASTER),                                         &
    word_t('VLAD', N39_MASTER),                                         &
    word_t('WATE', O26_WATER),                                          &
    word_t('WERE', N76_WEREWOLF),                                       &
    word_t('WIND', N37_WINDOW),                                         &
    word_t('WIZA', N80_WIZARD),                                         &
    word_t('WOLF', N76_WEREWOLF),                                       &
    word_t('31  ', N43_COMBO2_R31),                                     &
    word_t('59  ', N44_COMBO3_L59),                                     &
    word_t('8   ', N42_COMBO1_L8) /)

  return
end subroutine init_wordlists

!> Return id of searchterm or zero if not found
integer function wordlist_lookup(this, searchterm) result(term_id)
  implicit none
  ! Arguments
  !> Wordlist object
  class(wordlist_t), intent(in) :: this
  !> Search term
  character(len=4), intent(in) :: searchterm

  ! Local variables
  integer :: i
  integer :: nwords

  continue

  term_id = 0

  if (allocated(this%term)) then
    nwords = size(this%term)
    do i = 1, nwords
      if (searchterm(1:4) == this%term(i)%word(1:4)) then
        term_id = this%term(i)%id
        exit
      end if
    end do
  end if

  return
end function wordlist_lookup

end module m_words
