!> @file main.f90
!! @author Michael S. Holtzman
!! @author Mark Kershenblatt
!! @copyright See LICENSE
!! @brief Main program of Castlequest
!--------------------------------------------------------------
!
!     CASTLEQUEST BY MICHAEL S. HOLTZMAN/MARK A. KERSHENBLATT 2/80
!           Modified to run under IBM's CP/CMS, 9/81, by M. Holtzman
!           Updated to Fortran 2018, April 2021, by R. Apthorpe
!
!--------------------------------------------------------------
!     ITEMS  = LOCATIONS OF THE ITEMS (-2 = ?, -1 = inventory,
!              0 = gone, >0 = room) (see itemtracker_t)
!     VERBS  = LIST OF ALLOWABLE VERBS.
!     NOUNS  = LIST OF ALLOWABLE NOUNS.
!     ROOM   = CURRENT ROOM
!     LROOM  = LAST ROOM
!     NUMB   = NUMBER OF ITEMS BEING CARRIED.
!     GUN    = STAUS OF GUN (.false.=NOTHING,.true.=LOADED) (see gun_t)
!     LOCK   = 0=LOCKED,1=1ST #,2=2ND #,3=OPEN.
!     WIND1  = WINDOW IN ROOM 1 (0=NAILED,1=BROKEN,2=BARRED,3=OPEN)
!     DOOR(I)= DOOR IN ROOM I (0=LOCKED,1=CLOSED,2=OPEN)
!     SHUTTR = STATUS OF SHUTTER (0=CLOSED, 1=OPEN)
!     BUT    = BUTLER(0=SLEEPING,1=AWAKE,2=HOLDING NOTE,3=GONE FOR GOOD,
!                     4=DEAD AS A DOORNAIL. (see butler_t)
!     BAT    = STATUS OF BAT (0=GONE,1=BLOCKING WAY,HUNGRY) (see bat_t)
!     NOTE   = NUMBER OF NOTE BUTLER IS HOLDING.
!     IPASS  = SECRET LIBRARY PASSAGE FOUND?
!     NDEATH = NUMBER OF TIMES PLAYER HAS DIED.
!     LAMP   = STATUS OF LAMP (0=OFF,1=ON,2=DIM,3=EMPTY) (see lamp_t)
!     ROPE   = (0=LOOSE,1=TIED TO BED,2=HANGING,-2=GONE,3=TIED TO HOOK)
!     IVALUE = POINT VALUE OF FINDING EACH TREASURE. (see itemtracker_t)
!     HUNCH  = HUNCHBACK(0=HUNGRY,1=FOLLOWING,2=GONE) (see hunchback_t)
!     BOTTLE = BOTTLE FILLED?
!     BLOOD  = BLOOD IN BOTTLE?
!     WATER  = WATER IN BOTTLE?
!     MELT   = HOLE MELTED IN ICE?
!     HOLE   = CYCLOPS SHAPED HOLE IN DOOR? (see cyclops_t)
!     WIZ    = WIZARD STILL AROUND? (see wizard_t)
!     TORCH  = IS TORCH LIT?
!     PREC   = IS THE PRECIPICE ACCESSIBLE?
!     FIRE   = IS FIRE BURNING?
!     SUN    = IS SUN UP(0=UP,1=SETTING)
!     MASTER = (0=IN COFFIN,1=ASLEEP,2=PINNED,3=UP,4=DEAD) (see master_t)
!     LMOVE  = # MOVES WITH  LAMP   LIT. (see lamp_t)
!     MMOVE  = # MOVES WITH MATCHES LIT. (see match_t)
!     MATCH  = MATCHES(0=UNLIT,1=LIT,2=GONE) (see match_t)
!     SCORE  = ACCUMULATED SCORE.
!     NOTVAL = Score for finding lock combination
!     LOKVAL = Score for opening combination lock
!     BUTVAL = Score for reading butler's note
!     ROPVAL = Score for clearing a path out the bedroom window
!     MAS1,2,3,4 = MASTER SECTION STATUS (LOGICAL) (see revealed_letter)
!--------------------------------------------------------------
!> @brief Main program of Castlequest
!!
!! @returns Adventure!
program CQUEST
  use, intrinsic :: iso_fortran_env, only: STDOUT => OUTPUT_UNIT
  use :: cquest_appconfig, only: appconfig_t
  use :: cquest_log, only: initialize_logs, finalize_logs, log_notice
  use :: cquest_game, only: resolution_t, game_t
  use :: cquest_version, only: CODENAME, VERSION, BUILDDATE

  implicit none

  ! Parameters
  character(len=*), parameter :: msg_halt = "Ending game -- halt"

  ! Local variables

  ! Castlequest application configuration object
  type(appconfig_t) :: cquest_cfg

  ! Castlequest game implementation
  type(game_t) :: game

  ! Action outcome
  type(resolution_t) :: outcome

  continue

  ! Initialize application
  call cquest_cfg%init()
  call cquest_cfg%process_args()

  if (.not. cquest_cfg%halt) then

    call initialize_logs(cquest_cfg%gameoption%write_logs,              &
      cquest_cfg%logbase, CODENAME, VERSION, BUILDDATE,                 &
      cquest_cfg%logfiles)

    !=== Main event loop
    call game%init(cquest_cfg%gameoption)
    outcome = game%run()
    !=== End Main event loop

    !=== Wind down
    if (cquest_cfg%gameoption%write_logs) then
      call log_notice(msg_halt)
    end if
    call finalize_logs(cquest_cfg%logfiles)
    !=== End Wind down

  end if

  stop
end program CQUEST
