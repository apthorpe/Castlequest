!> @file ut_action.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Contains unit tests for m_action

!> @brief Contains unit tests for m_action
program ut_action
  use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
  use :: toast
  use :: m_action
  implicit none
  type(TestCase) :: test
  type(action_t) :: cmd
  continue

  call test%init(name="ut_action")

  call test%assertequal(cmd%action, 0,                                  &
    message="Empty action (u)")
  call test%assertequal(cmd%object, 0,                                  &
    message="Empty object (u)")
  call test%assertequal(cmd%verb4, 'VERB',                              &
    message="Default verb (u)")
  call test%assertequal(cmd%noun4, 'NOUN',                              &
    message="Default noun (u)")

  call cmd%init()

  call test%assertequal(cmd%action, 0,                                  &
    message="Empty action (i)")
  call test%assertequal(cmd%object, 0,                                  &
    message="Empty object (i)")
  call test%assertequal(cmd%verb4, '    ',                              &
    message="Default verb (i)")
  call test%assertequal(cmd%noun4, '    ',                              &
    message="Default noun (i)")

  ! Print summary at the end
  call printsummary(test)
  call jsonwritetofile(test, "ut_action.json")

  call test%checkfailure()
end program ut_action