!> @file ut_asa.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_format:asa_t

!! @brief Unit tests of m_format:asa_t
program ut_asa
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_object_id
    use m_format, only: asa_t
    implicit none

    character(len=*), parameter :: asa1_on  = ' '
    character(len=*), parameter :: asa1_off = ''
    character(len=*), parameter :: asa2_on  = '0'
    character(len=*), parameter :: asa2_off = new_line("A")

    type(asa_t) :: asa
    type(TestCase) :: test

    continue

    call test%init(name="ut_asa")

    ! asa created but not overtly initialized

    call test%asserttrue(asa%use_asa,                                   &
      message="ASA codes are enabled (u)")
    call test%assertequal(asa%ss(), asa1_on,                            &
      message="Single-space code is ' ' (u)")
    call test%assertequal(asa%ds(), asa2_on,                            &
      message="double-space code is '0' (u)")

    call asa%init()

    call test%asserttrue(asa%use_asa,                                   &
      message="ASA codes are enabled (ib)")
    call test%assertequal(asa%ss(), asa1_on,                            &
      message="Single-space code is ' ' (ib)")
    call test%assertequal(asa%ds(), asa2_on,                            &
      message="double-space code is '0' (ib)")

    call asa%init(asa_on=.false.)

    call test%assertfalse(asa%use_asa,                                  &
      message="ASA codes are not enabled (i-)")
    call test%assertequal(asa%ss(), asa1_off,                           &
      message="Single-space code is '' (i-)")
    call test%asserttrue(asa%ss() == asa1_off,                          &
      message="Single-space code is '' (i-)")
    call test%assertequal(asa%ds(), asa2_off,                           &
      message="double-space code is NL (i-=)")
    call test%asserttrue(asa%ds() == asa2_off,                          &
      message="double-space code is NL (i-T)")

    call asa%init()

    call test%asserttrue(asa%use_asa,                                   &
      message="ASA codes are enabled (ib)")
    call test%assertequal(asa%ds(), asa2_on,                            &
      message="double-space code is '0' (ib)")
    call test%assertequal(asa%ss(), asa1_on,                            &
      message="Single-space code is ' ' (ib)")

    call asa%init(.true.)

    call test%asserttrue(asa%use_asa,                                   &
      message="ASA codes are enabled (i+)")
    call test%assertequal(asa%ss(), asa1_on,                            &
      message="Single-space code is ' ' (i+)")
    call test%assertequal(asa%ds(), asa2_on,                            &
      message="double-space code is '0' (i+)")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_asa.json")

    call test%checkfailure()
end program ut_asa
