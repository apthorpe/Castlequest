!> @file ut_bottle.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_items:bottle_t

!! @brief Unit tests of m_items:bottle_t
program ut_bottle
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_gamedata
    use :: m_items
    use :: m_object_id
    use :: m_stateful
    implicit none

    integer, parameter :: p_pantry = 31
    integer, parameter :: p_carried = -1

    integer :: past_place

    logical :: read_error
    type(itemtracker_t), target :: inv
    type(item_t), pointer :: pi_bottle
    type(item_t), pointer :: pi_blood
    type(item_t), pointer :: pi_water
    type(stateful_t) :: bare
    type(bottle_t) :: s_bottle
    type(TestCase) :: test

    continue

    call test%init(name="ut_bottle")

    ! Check initial value of item pointer before initialization
    call test%assertfalse(associated(s_bottle%item),                      &
      message="Verify item pointer of uninitialized bottle is not associated")
    ! Check name of bottle before initialization
    call test%assertequal(s_bottle%name, bare%name,                       &
      message="Verify uninitialized bottle name matches stateful_t")
    ! Check id of bottle before initialization
    call test%assertequal(s_bottle%id, bare%id,                           &
      message="Verify uninitialized bottle id matches stateful_t")

    !!! Initialize itemtracker

    call inv%init()

    call load_game_data(read_error)
    call test%assertfalse(read_error,                                   &
      message="Read all game data")

    !!! Check initialized item attributes are in expected range

    ! Initialize bottle state

    pi_bottle => inv%item(O18_BOTTLE)
    pi_blood  => inv%item(O5_BLOOD)
    pi_water  => inv%item(O26_WATER)
    call s_bottle%init(inv%item(O18_BOTTLE), inv%item(O5_BLOOD),       &
      inv%item(O26_WATER))

    ! Check initial value of item pointer after initialization
    call test%asserttrue(associated(s_bottle%item),                     &
      message="Verify item pointer of initialized bottle is associated")
    ! Check name of bottle after initialization
    call test%asserttrue(associated(s_bottle%item, pi_bottle),          &
      message="Verify initialized bottle item bottlees target")
    ! Check id of bottle after initialization
    call test%assertequal(s_bottle%id, O18_BOTTLE,                      &
      message="Verify initialized bottle id bottlees O18_BOTTLE")

    call test%assertequal(s_bottle%item%place, p_pantry,                &
      message="Verify bottle place is reset by init")

    call test%asserttrue(s_bottle%is_empty(),                           &
      message="Verify bottle is empty (i)")
    call test%assertfalse(s_bottle%contains_blood(),                    &
      message="Verify bottle does not contain blood (i)")
    call test%assertfalse(s_bottle%contains_water(),                    &
      message="Verify bottle does not contain water (i)")
    call test%assertfalse(s_bottle%is_full(),                           &
      message="Verify bottle is not full (i)")

    past_place = s_bottle%item%place

    ! Carry/carried check

    ! Bottle in room
    call test%assertequal(s_bottle%item%place, past_place,              &
      message="Verify bottle is in room")
    call test%assertfalse(s_bottle%item%is_carried(),                   &
      message="Verify bottle is not carried (by item)")
    call test%assertfalse(inv%is_carried(O18_BOTTLE),                   &
      message="Verify bottle is not carried (by itemtracker)")
    call test%asserttrue(s_bottle%item%in_room(past_place),             &
      message="Verify bottle place is in original room")
    call test%asserttrue(s_bottle%item%at_hand_in(past_place),          &
      message="Verify bottle place is available (in room)")

    ! Pick up bottle
    call inv%carry(O18_BOTTLE)
    call test%assertequal(s_bottle%item%place, p_carried,               &
      message="Verify bottle place is -1 (carried, itemlist)")
    call test%asserttrue(s_bottle%item%is_carried(),                    &
      message="Verify bottle is carried (by item)")
    call test%asserttrue(inv%is_carried(O18_BOTTLE),                    &
      message="Verify bottle is carried (by itemtracker)")
    call test%assertfalse(s_bottle%item%in_room(past_place),            &
      message="Verify bottle is not in original room")
    call test%asserttrue(s_bottle%item%at_hand_in(past_place),          &
      message="Verify bottle is available (via inventory)")

    ! Put it back where you found it
    call s_bottle%item%place_in(past_place)
    call test%assertequal(s_bottle%item%place, past_place,              &
      message="Verify bottle is in room")
    call test%assertfalse(s_bottle%item%is_carried(),                   &
      message="Verify bottle is not carried (by item)")
    call test%assertfalse(inv%is_carried(O18_BOTTLE),                   &
      message="Verify bottle is not carried (by itemtracker)")
    call test%asserttrue(s_bottle%item%in_room(past_place),             &
      message="Verify bottle is in original room")
    call test%asserttrue(s_bottle%item%at_hand_in(past_place),          &
      message="Verify bottle is available (in room)")

    call s_bottle%fill_with_water()
    call test%assertfalse(s_bottle%is_empty(),                          &
      message="Verify bottle is not empty (w)")
    call test%assertfalse(s_bottle%contains_blood(),                    &
      message="Verify bottle does not contain blood (w)")
    call test%asserttrue(s_bottle%contains_water(),                     &
      message="Verify bottle contains water (w)")
    call test%asserttrue(s_bottle%is_full(),                            &
      message="Verify bottle is full (w)")

    call s_bottle%fill_with_blood()
    call test%assertfalse(s_bottle%is_empty(),                          &
      message="Verify bottle is not empty (b)")
    call test%asserttrue(s_bottle%contains_blood(),                     &
      message="Verify bottle contains blood (b)")
    call test%assertfalse(s_bottle%contains_water(),                    &
      message="Verify bottle does not contain water (b)")
    call test%asserttrue(s_bottle%is_full(),                            &
      message="Verify bottle is full (b)")

    call s_bottle%clear()
    call test%asserttrue(s_bottle%is_empty(),                           &
      message="Verify bottle is empty (eb)")
    call test%assertfalse(s_bottle%contains_blood(),                    &
      message="Verify bottle does not contain blood (eb)")
    call test%assertfalse(s_bottle%contains_water(),                    &
      message="Verify bottle does not contain water (eb)")
    call test%assertfalse(s_bottle%is_full(),                           &
      message="Verify bottle is not full (eb)")

    call s_bottle%fill_with_water()
    call test%assertfalse(s_bottle%is_empty(),                          &
      message="Verify bottle is not empty (we)")
    call test%assertfalse(s_bottle%contains_blood(),                    &
      message="Verify bottle does not contain blood (we)")
    call test%asserttrue(s_bottle%contains_water(),                     &
      message="Verify bottle contains water (we)")
    call test%asserttrue(s_bottle%is_full(),                            &
      message="Verify bottle is full (we)")

    call s_bottle%clear()
    call test%asserttrue(s_bottle%is_empty(),                           &
      message="Verify bottle is empty (ew)")
    call test%assertfalse(s_bottle%contains_blood(),                    &
      message="Verify bottle does not contain blood (ew)")
    call test%assertfalse(s_bottle%contains_water(),                    &
      message="Verify bottle does not contain water (ew)")
    call test%assertfalse(s_bottle%is_full(),                           &
      message="Verify bottle is not full (ew)")

    ! Cannot fill an uncarried empty bottle with water
    call s_bottle%fill_with(O26_WATER)
    call test%asserttrue(s_bottle%is_empty(),                          &
      message="Verify bottle is empty (mwC)")
    call test%assertfalse(s_bottle%contains_blood(),                    &
      message="Verify bottle does not contain blood (mwC)")
    call test%assertfalse(s_bottle%contains_water(),                    &
      message="Verify bottle does not contain water (mwC)")
    call test%assertfalse(s_bottle%is_full(),                           &
      message="Verify bottle is not full (mwC)")

    ! Can fill a carried empty bottle with water
    call s_bottle%item%carry()
    call s_bottle%fill_with(O26_WATER)
    call test%assertfalse(s_bottle%is_empty(),                          &
      message="Verify bottle is not empty (mw)")
    call test%assertfalse(s_bottle%contains_blood(),                    &
      message="Verify bottle does not contain blood (mw)")
    call test%asserttrue(s_bottle%contains_water(),                     &
      message="Verify bottle contains water (mw)")
    call test%asserttrue(s_bottle%is_full(),                            &
      message="Verify bottle is full (mw)")

    ! Cannot fill a full bottle (already holds water)
    call s_bottle%item%carry()
    call s_bottle%fill_with(O5_BLOOD)
    call test%assertfalse(s_bottle%is_empty(),                          &
      message="Verify bottle is not empty (mbF)")
    call test%assertfalse(s_bottle%contains_blood(),                    &
      message="Verify bottle does not contain blood (mbF)")
    call test%asserttrue(s_bottle%contains_water(),                     &
      message="Verify bottle contains water (mbF)")
    call test%asserttrue(s_bottle%is_full(),                            &
      message="Verify bottle is full (mbF)")

    ! Can fill an empty bottle with blood
    call s_bottle%item%carry()
    call s_bottle%clear()
    call s_bottle%fill_with(O5_BLOOD)
    call test%assertfalse(s_bottle%is_empty(),                          &
      message="Verify bottle is not empty (mb)")
    call test%asserttrue(s_bottle%contains_blood(),                    &
      message="Verify bottle contains blood (mb)")
    call test%assertfalse(s_bottle%contains_water(),                     &
      message="Verify bottle does not contain water (mb)")
    call test%asserttrue(s_bottle%is_full(),                            &
      message="Verify bottle is full (mb)")

    ! Hunchback does not fit in bottle
    call s_bottle%item%carry()
    call s_bottle%clear()
    call s_bottle%fill_with(O8_HUNCHBACK)
    call test%asserttrue(s_bottle%is_empty(),                          &
      message="Verify bottle is empty (mhF)")
    call test%assertfalse(s_bottle%contains_blood(),                    &
      message="Verify bottle does not contain blood (mhF)")
    call test%assertfalse(s_bottle%contains_water(),                     &
      message="Verify bottle does not contain water (mhF)")
    call test%assertfalse(s_bottle%is_full(),                            &
      message="Verify bottle is not full (mhF)")

        ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_bottle.json")

    call test%checkfailure()
end program ut_bottle
