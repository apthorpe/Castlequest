!> @file ut_dungeon.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_where:dungeon_t

!! @brief Unit tests of m_where:dungeon_t
program ut_dungeon
  use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
  use :: toast
  use :: cquest_game, only: DES
  use :: m_format, only: a
  use :: m_gamedata, only: load_game_data!, DES
  use :: m_object_id
  use :: m_where

  implicit none

  logical :: read_error
  integer :: room_id

  type(room_t) :: room
  type(dungeon_t) :: map
  type(TestCase) :: test

5 format(3X, A, ": #", I0)

  continue

  call test%init(name="ut_dungeon")

  ! ! Check initial value of item pointer before initialization
  ! call test%assertequal(s_map%state, -1,                              &
  !   message="Verify state of uninitialized map is -1")
  ! ! Check name of map before initialization
  ! call test%assertequal(s_map%name, bare%name,                        &
  !   message="Verify uninitialized map name matches stateful_t")
  ! ! Check id of map before initialization
  ! call test%assertequal(s_map%id, bare%id,                           &
  !   message="Verify uninitialized map id matches stateful_t")

  !!! Check initialized item attributes are in expected range

  call load_game_data(read_error)
  call test%assertfalse(read_error,                                   &
    message="Successfully read game data")

  call a%init(asa_on=.true.)

  ! Initialize map state

  call map%init()

  do room_id = -3, 103
    if (room_id == 0) then
      call test%asserttrue(is_impassible(room_id),                    &
        message="Room is impassible")
    else
      call test%assertfalse(is_impassible(room_id),                   &
        message="Room is not impassible")
    end if

    ! Valid room index check
    if (room_id >= 1 .and. room_id <= 100) then
      room = map%room(room_id)
      call test%asserttrue(map%is_valid_room(room_id),                &
        message="In-bounds room is valid")

      ! Water check
      select case (room_id)
      case(32, 40, 68)
        call test%asserttrue(map%room_has_water(room_id),             &
          message="Room has water (map, <>81, --)")
        call test%asserttrue(room%has_water(),                        &
          message="Room has water (room, <>81, --)")
        call test%asserttrue(map%room_has_water(room_id,              &
          omit_island=.false.),                                       &
          message="Room has water (map, <>81, omit=F)")
        call test%asserttrue(room%has_water(omit_island=.false.),     &
          message="Room has water (room, <>81, omit=F)")
        call test%asserttrue(map%room_has_water(room_id,              &
          omit_island=.true.),                                        &
          message="Room has water (map, <>81, omit=T)")
        call test%asserttrue(room%has_water(omit_island=.true.),      &
          message="Room has water (room, <>81, omit=T)")
      case(81)
        call test%asserttrue(map%room_has_water(room_id),             &
          message="Room has water (map, 81, --)")
        call test%asserttrue(room%has_water(),                        &
          message="Room has water (room, 81, --)")
        call test%asserttrue(map%room_has_water(room_id,              &
          omit_island=.false.),                                       &
          message="Room has water (map, 81, omit=F)")
        call test%asserttrue(room%has_water(omit_island=.false.),     &
          message="Room has water (room, 81, omit=F)")
        call test%assertfalse(map%room_has_water(room_id,             &
          omit_island=.true.),                                        &
          message="Room does not have water (map, 81, omit=T)")
        call test%assertfalse(room%has_water(omit_island=.true.),     &
          message="Room does not have water (room, 81, omit=T)")
      case default
        call test%assertfalse(map%room_has_water(room_id),            &
          message="Room does not have water (map, <>W, --)")
        call test%assertfalse(room%has_water(),                       &
          message="Room does not have water (room, <>W, --)")
          call test%assertfalse(map%room_has_water(room_id,           &
          omit_island=.false.),                                       &
          message="Room not have water  (map, <>W, omit=F)")
        call test%assertfalse(room%has_water(omit_island=.false.),    &
          message="Room not have water  (room, <>W, omit=F)")
        call test%assertfalse(map%room_has_water(room_id,             &
          omit_island=.true.),                                        &
          message="Room does not have water (map, <>W, omit=T)")
        call test%assertfalse(room%has_water(omit_island=.true.),     &
          message="Room does not have water (room, <>W, omit=T)")
      end select

      ! Door exists check
      select case (room_id)
      case(1:23, 80)
        call test%asserttrue(map%door_exists(room_id),                &
          message="Door exists (map)")
        call test%asserttrue(room%door%exists(),                      &
          message="Door exists (room)")
      case default
        call test%assertfalse(map%door_exists(room_id),               &
          message="Door does not exist (map)")
        call test%assertfalse(room%door%exists(),                     &
          message="Door does not exist (room)")
      end select

      ! Dark check
      select case (room_id)
      case(41:94)
        call test%asserttrue(is_dark_room(room_id),                   &
          message="Is dark room (id)")
        call test%asserttrue(map%room_is_dark(room_id),               &
          message="Is dark room (map)")
        call test%asserttrue(room%is_dark(),                          &
          message="Is dark room (room)")
      case default
        call test%assertfalse(is_dark_room(room_id),                  &
          message="Is not dark room (id)")
          call test%assertfalse(map%room_is_dark(room_id),            &
          message="Is not dark room (map)")
        call test%assertfalse(room%is_dark(),                         &
          message="Is not dark room (room)")
      end select

      ! Werewolf check
      select case (room_id)
      case(4:25)
        call test%asserttrue(in_werewolf_territory(room_id),          &
          message="In werewolf territory")
      case default
        call test%assertfalse(in_werewolf_territory(room_id),         &
          message="Not in werewolf territory")
      end select

      ! Werewolf check
      select case (room_id)
      case(45:92)
        call test%asserttrue(in_gnome_territory(room_id),             &
          message="In gnome territory")
      case default
        call test%assertfalse(in_gnome_territory(room_id),            &
          message="Not in gnome territory")
      end select

      ! In lower realm
      select case (room_id)
      case(41:100)
        call test%asserttrue(in_lower_realm(room_id),                 &
          message="In lower realm")
      case default
        call test%assertfalse(in_lower_realm(room_id),                &
          message="Not in lower realm")
      end select

      ! In master realm
      select case (room_id)
      case(95:99)
        call test%asserttrue(in_master_realm(room_id),                &
          message="In master realm")
      case default
        call test%assertfalse(in_master_realm(room_id),               &
          message="Not in master realm")
      end select

      ! Describe demo (ASA)
      call a%init(asa_on=.true.)

      write(unit=stdout, fmt='("--- DEMO: ASA ON ---")')
      write(unit=stdout, fmt=5) 'Short (5x)', room_id
      call DES(200 + room_id)
      call room%describe()
      call room%describe(long=.false.)
      call map%room(room_id)%describe()
      call map%room(room_id)%describe(long=.false.)

      write(unit=stdout, fmt=5) 'Long (3x)', room_id
      call DES(room_id)
      call room%describe(long=.true.)
      call map%room(room_id)%describe(long=.true.)

      call a%init(asa_on=.false.)

      write(unit=stdout, fmt='("--- DEMO: ASA OFF ---")')
      write(unit=stdout, fmt=5) 'Short (5x)', room_id
      call DES(200 + room_id)
      call room%describe()
      call room%describe(long=.false.)
      call map%room(room_id)%describe()
      call map%room(room_id)%describe(long=.false.)

      write(unit=stdout, fmt=5) 'Long (3x)', room_id
      call DES(room_id)
      call room%describe(long=.true.)
      call map%room(room_id)%describe(long=.true.)

      ! Spot check connects_
      select case(room_id)
      case(6)
        !     (/  31,   0,  -7,   0,   5,   0,   0,   0,   0,    0 /),            &
        ! Leave 0, enter 3, door

        ! By map
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V1_NORTH),                      &
          31, message="m: R6(N) -> 31")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V2_NORTHEAST),                  &
          0, message="m: R6(NE) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V3_EAST),                       &
          -7, message="m: R6(E) -> -7")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V4_SOUTHEAST),                  &
          0, message="m: R6(SE) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V5_SOUTH),                      &
          5, message="m: R6(S) -> 5")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V6_SOUTHWEST),                  &
          0, message="m: R6(SW) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V7_WEST),                       &
          0, message="m: R6(W) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V8_NORTHWEST),                  &
          0, message="m: R6(NW) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V9_UP),                         &
          0, message="m: R6(U) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V10_DOWN),                       &
          0, message="m: R6(D) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%door%enter_to(),                          &
          3, message="m: R6 E -> 3")
        call test%assertequal(                                        &
          map%room(room_id)%door%leave_to(),                          &
          0, message="m: R6 L -> 0")
        ! By room
        call test%assertequal(                                        &
          room%leads_to(V1_NORTH),                                   &
          31, message="r: R6(N) -> 31")
        call test%assertequal(                                        &
          room%leads_to(V2_NORTHEAST),                               &
          0, message="r: R6(NE) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V3_EAST),                                    &
          -7, message="r: R6(E) -> -7")
        call test%assertequal(                                        &
          room%leads_to(V4_SOUTHEAST),                               &
          0, message="r: R6(SE) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V5_SOUTH),                                   &
          5, message="r: R6(S) -> 5")
        call test%assertequal(                                        &
          room%leads_to(V6_SOUTHWEST),                               &
          0, message="r: R6(SW) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V7_WEST),                                    &
          0, message="r: R6(W) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V8_NORTHWEST),                               &
          0, message="r: R6(NW) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V9_UP),                                      &
          0, message="r: R6(U) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V10_DOWN),                                    &
          0, message="r: R6(D) -> 0")
        call test%assertequal(                                        &
          room%door%enter_to(),                                       &
          3, message="m: R6 E -> 3")
        call test%assertequal(                                        &
          room%door%leave_to(),                                       &
          0, message="m: R6 L -> 0")

      case(27)
        !     (/  -2,  -2, -16, -28, -28,  -2, -28,  -2, -28,  -28 /),            &
        ! Leave 0, enter 0, door
        ! By map
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V1_NORTH),                      &
          -2, message="m: R27(N) -> -2")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V2_NORTHEAST),                  &
          -2, message="m: R27(NE) -> -2")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V3_EAST),                       &
          -16, message="m: R27(E) -> -16")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V4_SOUTHEAST),                  &
          -28, message="m: R27(SE) -> -28")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V5_SOUTH),                      &
          -28, message="m: R27(S) -> -28")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V6_SOUTHWEST),                  &
          -2, message="m: R27(SW) -> -2")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V7_WEST),                       &
          -28, message="m: R27(W) -> -28")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V8_NORTHWEST),                  &
          -2, message="m: R27(NW) -> -2")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V9_UP),                         &
          -28, message="m: R27(U) -> -28")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V10_DOWN),                       &
          -28, message="m: R27(D) -> -28")
        call test%assertequal(                                        &
          map%room(room_id)%door%enter_to(),                          &
          0, message="m: R27 E -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%door%leave_to(),                          &
          0, message="m: R27 L -> 0")
        ! By room
        call test%assertequal(                                        &
          room%leads_to(V1_NORTH),                                   &
          -2, message="r: R27(N) -> -2")
        call test%assertequal(                                        &
          room%leads_to(V2_NORTHEAST),                               &
          -2, message="r: R27(NE) -> -2")
        call test%assertequal(                                        &
          room%leads_to(V3_EAST),                                    &
          -16, message="r: R27(E) -> -16")
        call test%assertequal(                                        &
          room%leads_to(V4_SOUTHEAST),                               &
          -28, message="r: R27(SE) -> -28")
        call test%assertequal(                                        &
          room%leads_to(V5_SOUTH),                                   &
          -28, message="r: R27(S) -> -28")
        call test%assertequal(                                        &
          room%leads_to(V6_SOUTHWEST),                               &
          -2, message="r: R27(SW) -> -2")
        call test%assertequal(                                        &
          room%leads_to(V7_WEST),                                    &
          -28, message="r: R27(W) -> -28")
        call test%assertequal(                                        &
          room%leads_to(V8_NORTHWEST),                               &
          -2, message="r: R27(NW) -> -2")
        call test%assertequal(                                        &
          room%leads_to(V9_UP),                                      &
          -28, message="r: R27(U) -> -28")
        call test%assertequal(                                        &
          room%leads_to(V10_DOWN),                                    &
          -28, message="r: R27(D) -> -28")
        call test%assertequal(                                        &
          room%door%enter_to(),                                       &
          0, message="r: R27 E -> 0")
        call test%assertequal(                                        &
          room%door%leave_to(),                                       &
          0, message="r: R27 L -> 0")
      case(75)
        !     (/  74,   0,   0,  76,   0,   0,  82,   0,  76,    0 /),            &
        ! Leave 1, enter 0, door
        ! By map
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V1_NORTH),                      &
          74, message="m: R75(N) -> 74")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V2_NORTHEAST),                  &
          0, message="m: R75(NE) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V3_EAST),                       &
          0, message="m: R75(E) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V4_SOUTHEAST),                  &
          76, message="m: R75(SE) -> 76")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V5_SOUTH),                      &
          0, message="m: R75(S) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V6_SOUTHWEST),                  &
          0, message="m: R75(SW) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V7_WEST),                       &
          82, message="m: R75(W) -> 82")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V8_NORTHWEST),                  &
          0, message="m: R75(NW) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V9_UP),                         &
          76, message="m: R75(U) -> 76")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V10_DOWN),                       &
          0, message="m: R75(D) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%door%enter_to(),                          &
          0, message="m: R75 E -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%door%leave_to(),                          &
          1, message="m: R75 L -> 1")
        ! By room
        call test%assertequal(                                        &
          room%leads_to(V1_NORTH),                                   &
          74, message="r: R75(N) -> 74")
        call test%assertequal(                                        &
          room%leads_to(V2_NORTHEAST),                               &
          0, message="r: R75(NE) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V3_EAST),                                    &
          0, message="r: R75(E) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V4_SOUTHEAST),                               &
          76, message="r: R75(SE) -> 76")
        call test%assertequal(                                        &
          room%leads_to(V5_SOUTH),                                   &
          0, message="r: R75(S) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V6_SOUTHWEST),                               &
          0, message="r: R75(SW) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V7_WEST),                                    &
          82, message="r: R75(W) -> 82")
        call test%assertequal(                                        &
          room%leads_to(V8_NORTHWEST),                               &
          0, message="r: R75(NW) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V9_UP),                                      &
          76, message="r: R75(U) -> 76")
        call test%assertequal(                                        &
          room%leads_to(V10_DOWN),                                    &
          0, message="r: R75(D) -> 0")
        call test%assertequal(                                        &
          room%door%enter_to(),                                       &
          0, message="r: R75 E -> 0")
        call test%assertequal(                                        &
          room%door%leave_to(),                                       &
          1, message="r: R75 L -> 1")
      case(95)
        !     (/  97,   0,  98,   0,  99,   0,  96,   0,   0,    0 /),            &
        ! Leave 0, enter 0, door
        ! By map
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V1_NORTH),                      &
          97, message="m: R95(N) -> 97")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V2_NORTHEAST),                  &
          0, message="m: R95(NE) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V3_EAST),                       &
          98, message="m: R95(E) -> 98")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V4_SOUTHEAST),                  &
          0, message="m: R95(SE) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V5_SOUTH),                      &
          99, message="m: R95(S) -> 99")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V6_SOUTHWEST),                  &
          0, message="m: R95(SW) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V7_WEST),                       &
          96, message="m: R95(W) -> 96")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V8_NORTHWEST),                  &
          0, message="m: R95(NW) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V9_UP),                         &
          0, message="m: R95(U) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%leads_to(V10_DOWN),                       &
          0, message="m: R95(D) -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%door%enter_to(),                          &
          0, message="m: R95 E -> 0")
        call test%assertequal(                                        &
          map%room(room_id)%door%leave_to(),                          &
          0, message="m: R95 L -> 0")
        ! By room
        call test%assertequal(                                        &
          room%leads_to(V1_NORTH),                                   &
          97, message="r: R95(N) -> 97")
        call test%assertequal(                                        &
          room%leads_to(V2_NORTHEAST),                               &
          0, message="r: R95(NE) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V3_EAST),                                    &
          98, message="r: R95(E) -> 98")
        call test%assertequal(                                        &
          room%leads_to(V4_SOUTHEAST),                               &
          0, message="r: R95(SE) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V5_SOUTH),                                   &
          99, message="r: R95(S) -> 99")
        call test%assertequal(                                        &
          room%leads_to(V6_SOUTHWEST),                               &
          0, message="r: R95(SW) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V7_WEST),                                    &
          96, message="r: R95(W) -> 96")
        call test%assertequal(                                        &
          room%leads_to(V8_NORTHWEST),                               &
          0, message="r: R95(NW) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V9_UP),                                      &
          0, message="r: R95(U) -> 0")
        call test%assertequal(                                        &
          room%leads_to(V10_DOWN),                                    &
          0, message="r: R95(D) -> 0")
        call test%assertequal(                                        &
          room%door%enter_to(),                                       &
          0, message="r: R95 E -> 0")
        call test%assertequal(                                        &
          room%door%leave_to(),                                       &
          0, message="r: R95 L -> 0")
      end select

    else
      call test%assertfalse(map%is_valid_room(room_id),               &
        message="Out-of-bounds room is not valid")
    end if
  end do

  ! call test%assertequal(s_map%name, 'map             ',               &
  !   message="Verify map name is set by init")

  ! call test%asserttrue(s_map%is_up(),                                 &
  !   message="Verify map is up (i)")
  ! call test%assertfalse(s_map%is_down(),                              &
  !   message="Verify map is not down (i)")

  ! Print summary at the end
  call printsummary(test)
  call jsonwritetofile(test, "ut_dungeon.json")

  call test%checkfailure()
end program ut_dungeon
