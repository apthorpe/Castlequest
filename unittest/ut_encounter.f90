!> @file ut_encounter.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests to verify expected monster behavior

!> @brief Unit tests to verify expected monster behavior
program ut_encounter
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_encounter
    use :: m_gamedata
    use :: m_items
    use :: m_object_id
    use :: m_player, only: player_t
    use :: m_random
    implicit none

    type(TestCase) :: test

    type(itemtracker_t) :: portable
    type(player_t) :: player

    type(gnome_t) :: gnome
    type(werewolf_t) :: werewolf
    type(hunchback_t) :: hunchback
    type(butler_t) :: butler
    type(bat_t) :: bat
    type(wizard_t) :: wizard
    type(master_t) :: master
    type(cyclops_t) :: cyclops

    integer :: rounds
    integer :: player_wins
    integer :: monster_wins
    integer :: monster_retreats
    integer :: i
    integer :: I_AM_DEAD
    integer :: SCORE

! 10  format(A, ' in ', I0)
20  format('*** Player won ', I0, ', ', A, ' won ', I0, ' and quit ',   &
           I0, ' - Total = ', I0)

    continue

    call test%init(name="ut_encounter")

    !!! Test exposed functions

    call rstart()
    call portable%init()
    ! FORM2 = "(' BUTTS!')"

    call player%init()

    ! Gnome tests

    call test%assertequal(gnome%get_state_(), -1, "Uninitialized gnome state (-1)")
    call test%assertfalse(gnome%is_attacking(), "Gnome not attacking pre-init")
    call test%asserttrue(gnome%is_calm(), "Gnome is calm on pre-init")

    call gnome%init()

    call test%assertequal(gnome%get_state_(), 0, "Gnome initial state (0)")
    call test%assertfalse(gnome%is_attacking(), "Gnome not attacking on init")
    call test%asserttrue(gnome%is_calm(), "Gnome is calm on reset")

    call gnome%attack()

    call test%assertequal(gnome%get_state_(), 1, "Gnome attack state (1)")
    call test%asserttrue(gnome%is_attacking(), "Gnome attacking on attack command")
    call test%assertfalse(gnome%is_calm(), "Gnome is not calm on attack command")

    call gnome%calm()

    call test%assertequal(gnome%get_state_(), 0, "Gnome calm state (0)")
    call test%assertfalse(gnome%is_attacking(), "Gnome not attacking on calm command")
    call test%asserttrue(gnome%is_calm(), "Gnome is calm on calm command")

    call gnome%reset()

    call test%assertequal(gnome%get_state_(), 0, "Gnome reset state (0)")
    call test%assertfalse(gnome%is_attacking(), "Gnome not attacking on reset")
    call test%asserttrue(gnome%is_calm(), "Gnome is calm on reset")

    ! Gnome statistical attack demo
    player_wins = 0
    monster_wins = 0
    monster_retreats = 0
    rounds = 0
    do i = 1, 1000!00
      rounds = rounds + 1
      if (RDM() < (1.0 - gnome%p_defeat)) then
        I_AM_DEAD = 1
        call gnome%resolve_fight(I_AM_DEAD)
        if (I_AM_DEAD == 1) then
          monster_wins = monster_wins + 1
          ! write(unit=stdout, fmt=10) 'gnome', rounds
          rounds = 0
          call gnome%reset()
        else
          if (gnome%is_attacking()) then
            ! Draw
          else
            monster_retreats = monster_retreats + 1
            ! write(unit=stdout, fmt=10) 'gnome quits', rounds
            rounds = 0
            call gnome%reset()
          end if
        end if
      else
        player_wins = player_wins + 1
        ! write(unit=stdout, fmt=10) 'player', rounds
        rounds = 0
        call gnome%reset()
      end if
    end do
    write(unit=stdout, fmt=20) player_wins, "Gnome", monster_wins,   &
      monster_retreats, (player_wins + monster_wins + monster_retreats)

    ! Gnome statistical blade attack demo
    call player%init()
    player_wins = 0
    monster_wins = 0
    monster_retreats = 0
    rounds = 0
    do i = 1, 1000!00
      SCORE = 50
      player%items_held = 5
      player%room = R80_WINE_CELLAR
      call portable%carry(O12_SWORD)
      call portable%carry(O21_LAMP)
      call portable%carry(O26_WATER)
      call portable%carry(O7_CHAMPAGNE)
      call portable%carry(O1_KEROSENE)
      rounds = rounds + 1
      call gnome%resolve_blade_fight(I_AM_DEAD, player,         &
        portable, O12_SWORD, .false.)
      if (I_AM_DEAD == 1) then
        monster_wins = monster_wins + 1
        ! write(unit=stdout, fmt=10) 'gnome', rounds
        rounds = 0
        call gnome%reset()
      else
        if (gnome%is_attacking()) then
          ! Draw
        else
          player_wins = player_wins + 1
          ! write(unit=stdout, fmt=10) 'player', rounds
          rounds = 0
          call gnome%reset()
        end if
      end if
    end do
    write(unit=stdout, fmt=20) player_wins, "Stabby gnome", monster_wins,   &
      monster_retreats, (player_wins + monster_wins + monster_retreats)

    ! Werewolf tests

    call test%assertequal(werewolf%get_state_(), -1, "Uninitialized werewolf state (-1)")
    call test%assertfalse(werewolf%is_attacking(), "Werewolf not attacking pre-init")
    call test%asserttrue(werewolf%is_calm(), "Werewolf is calm on pre-init")

    call werewolf%init()

    call test%assertequal(werewolf%get_state_(), 0, "Werewolf initial state (0)")
    call test%assertfalse(werewolf%is_attacking(), "Werewolf not attacking on init")
    call test%asserttrue(werewolf%is_calm(), "Werewolf is calm on reset")

    call werewolf%attack()

    call test%assertequal(werewolf%get_state_(), 1, "Werewolf attack state (1)")
    call test%asserttrue(werewolf%is_attacking(), "Werewolf attacking on attack command")
    call test%assertfalse(werewolf%is_calm(), "Werewolf is not calm on attack command")

    call werewolf%calm()

    call test%assertequal(werewolf%get_state_(), 0, "Werewolf calm state (0)")
    call test%assertfalse(werewolf%is_attacking(), "Werewolf not attacking on calm command")
    call test%asserttrue(werewolf%is_calm(), "Werewolf is calm on calm command")

    call werewolf%reset()

    call test%assertequal(werewolf%get_state_(), 0, "Werewolf reset state (0)")
    call test%assertfalse(werewolf%is_attacking(), "Werewolf not attacking on reset")
    call test%asserttrue(werewolf%is_calm(), "Werewolf is calm on reset")

    ! Werewolf statistical attack demo
    player_wins = 0
    monster_wins = 0
    monster_retreats = 0
    rounds = 0
    do i = 1, 1000!00
      rounds = rounds + 1
      if (RDM() < (1.0 - werewolf%p_defeat)) then
        I_AM_DEAD = 1
        ! call WWOLF(a_werewolf, II)
        call werewolf%resolve_fight(I_AM_DEAD)
        if (I_AM_DEAD == 1) then
          monster_wins = monster_wins + 1
          ! write(unit=stdout, fmt=10) 'werewolf', rounds
          rounds = 0
          call werewolf%reset()
        else
          if (werewolf%is_attacking()) then
            ! Draw
          else
            monster_retreats = monster_retreats + 1
            ! write(unit=stdout, fmt=10) 'werewolf quits', rounds
            rounds = 0
            call werewolf%reset()
          end if
        end if
      else
        player_wins = player_wins + 1
        ! write(unit=stdout, fmt=10) 'player', rounds
        rounds = 0
        call werewolf%reset()
      end if
    end do
    write(unit=stdout, fmt=20) player_wins, "Werewolf", monster_wins,   &
      monster_retreats, (player_wins + monster_wins + monster_retreats)

    ! Hunchback tests
    call test%assertequal(hunchback%get_state_(), -1, "Uninitialized hunchback state (-1)")
    call test%asserttrue(hunchback%is_hungry(), "Hunchback is hungry (-1)")
    call test%assertfalse(hunchback%will_follow(), "Hunchback will not follow (-1)")
    call test%assertfalse(hunchback%is_gone(), "Hunchback is not gone (-1)")
    call hunchback%init()
    call test%assertequal(hunchback%get_state_(), 0, "Initialized hunchback state (0)")
    call test%asserttrue(hunchback%is_hungry(), "Hunchback is hungry (0)")
    call test%assertfalse(hunchback%will_follow(), "Hunchback will not follow (0)")
    call test%assertfalse(hunchback%is_gone(), "Hunchback is not gone (0)")
    call hunchback%eat()
    call test%assertequal(hunchback%get_state_(), 1, "Fed hunchback state (1)")
    call test%assertfalse(hunchback%is_hungry(), "Hunchback is not hungry (1)")
    call test%asserttrue(hunchback%will_follow(), "Hunchback will follow (1)")
    call test%assertfalse(hunchback%is_gone(), "Hunchback is not gone (1)")
    call hunchback%expire()
    call test%assertequal(hunchback%get_state_(), 2, "Dead hunchback state (2)")
    call test%assertfalse(hunchback%is_hungry(), "Hunchback is not hungry (2)")
    call test%assertfalse(hunchback%will_follow(), "Hunchback will not follow (2)")
    call test%asserttrue(hunchback%is_gone(), "Hunchback is gone (2)")
    ! Bad state (low)
    call hunchback%set_state_(-18)
    call test%assertequal(hunchback%get_state_(), -18, "Bad hunchback state (-18); hungry")
    call test%asserttrue(hunchback%is_hungry(), "Hunchback is hungry (-18)")
    call test%assertfalse(hunchback%will_follow(), "Hunchback will not follow (-18)")
    call test%assertfalse(hunchback%is_gone(), "Hunchback is not gone (-18)")
    ! Bad state (high)
    call hunchback%set_state_(47)
    call test%assertequal(hunchback%get_state_(), 47, "Bad hunchback state (47); gone")
    call test%assertfalse(hunchback%is_hungry(), "Hunchback is not hungry (47)")
    call test%assertfalse(hunchback%will_follow(), "Hunchback will not follow (47)")
    call test%asserttrue(hunchback%is_gone(), "Hunchback is gone (47)")

    ! Butler tests
    call test%assertequal(butler%get_state_(), -1, "Uninitialized butler state (-1)")
    call test%asserttrue(butler%is_sleeping(), "Butler is sleeping (-1)")
    call test%assertfalse(butler%is_awake(), "Butler is not awake (-1)")
    call test%assertfalse(butler%holding_note(), "Butler is not holding note (-1)")
    call test%assertfalse(butler%is_gone(), "Butler is not gone (-1)")
    call test%assertfalse(butler%is_dead(), "Butler is not dead (-1)")
    call test%assertfalse(butler%is_alert(), "Butler is not alert (-1)")
    call test%assertfalse(butler%cannot_wake(), "Butler is wakeable (-1)")
    call test%assertequal(butler%des_index(), 400, "Butler index is 400 (-1)")
    call butler%init()
    call test%assertequal(butler%get_state_(), 0, "Initialized butler state (0;init)")
    call test%asserttrue(butler%is_sleeping(), "Butler is sleeping (0;init)")
    call test%assertfalse(butler%is_awake(), "Butler is not awake (0;init)")
    call test%assertfalse(butler%holding_note(), "Butler is not holding note (0;init)")
    call test%assertfalse(butler%is_gone(), "Butler is not gone (0;init)")
    call test%assertfalse(butler%is_dead(), "Butler is not dead (0;init)")
    call test%assertfalse(butler%is_alert(), "Butler is not alert (0;init)")
    call test%assertfalse(butler%cannot_wake(), "Butler is wakeable (0;init)")
    call test%assertequal(butler%des_index(), 400, "Butler index is 400 (0;init)")
    call butler%sleep()
    call test%assertequal(butler%get_state_(), 0, "Butler state is sleeping (0;sleep)")
    call test%asserttrue(butler%is_sleeping(), "Butler is sleeping (0;sleep)")
    call test%assertfalse(butler%is_awake(), "Butler is not awake (0;sleep)")
    call test%assertfalse(butler%holding_note(), "Butler is not holding note (0;sleep)")
    call test%assertfalse(butler%is_gone(), "Butler is not gone (0;sleep)")
    call test%assertfalse(butler%is_dead(), "Butler is not dead (0;sleep)")
    call test%assertfalse(butler%is_alert(), "Butler is not alert (0;sleep)")
    call test%assertfalse(butler%cannot_wake(), "Butler is wakeable (0;sleep)")
    call test%assertequal(butler%des_index(), 400, "Butler index is 400 (0;sleep)")
    call butler%wake()
    call test%assertequal(butler%get_state_(), 1, "Awakened butler state (1)")
    call test%assertfalse(butler%is_sleeping(), "Butler is not sleeping (1)")
    call test%asserttrue(butler%is_awake(), "Butler is awake (1)")
    call test%assertfalse(butler%holding_note(), "Butler is not holding note (1)")
    call test%assertfalse(butler%is_gone(), "Butler is not gone (1)")
    call test%assertfalse(butler%is_dead(), "Butler is not dead (1)")
    call test%asserttrue(butler%is_alert(), "Butler is alert (1)")
    call test%assertfalse(butler%cannot_wake(), "Butler is wakeable (1)")
    call test%assertequal(butler%des_index(), 401, "Butler index is 401 (1)")
    call butler%write_note()
    call test%assertequal(butler%get_state_(), 2, "Butler holding note state (2)")
    call test%assertfalse(butler%is_sleeping(), "Butler is not sleeping (2)")
    call test%assertfalse(butler%is_awake(), "Butler is not awake (2)")
    call test%asserttrue(butler%holding_note(), "Butler is holding note (2)")
    call test%assertfalse(butler%is_gone(), "Butler is not gone (2)")
    call test%assertfalse(butler%is_dead(), "Butler is not dead (2)")
    call test%asserttrue(butler%is_alert(), "Butler is alert (2)")
    call test%assertfalse(butler%cannot_wake(), "Butler is wakeable (2)")
    call test%assertequal(butler%des_index(), 402, "Butler index is 402 (2)")
    call butler%pass_out()
    call test%assertequal(butler%get_state_(), 3, "Comatose butler state (3)")
    call test%assertfalse(butler%is_sleeping(), "Butler is not sleeping (3)")
    call test%assertfalse(butler%is_awake(), "Butler is not awake (3)")
    call test%assertfalse(butler%holding_note(), "Butler is not holding note (3)")
    call test%asserttrue(butler%is_gone(), "Butler is gone (3)")
    call test%assertfalse(butler%is_dead(), "Butler is not dead (3)")
    call test%assertfalse(butler%is_alert(), "Butler is not alert (3)")
    call test%asserttrue(butler%cannot_wake(), "Butler is not wakeable (3)")
    call test%assertequal(butler%des_index(), 403, "Butler index is 403 (3)")
    call butler%expire()
    call test%assertequal(butler%get_state_(), 4, "Dead butler state (4)")
    call test%assertfalse(butler%is_sleeping(), "Butler is not sleeping (4)")
    call test%assertfalse(butler%is_awake(), "Butler is not awake (4)")
    call test%assertfalse(butler%holding_note(), "Butler is not holding note (4)")
    call test%assertfalse(butler%is_gone(), "Butler is not gone (4)")
    call test%asserttrue(butler%is_dead(), "Butler is dead (4)")
    call test%assertfalse(butler%is_alert(), "Butler is not alert (4)")
    call test%asserttrue(butler%cannot_wake(), "Butler is not wakeable (4)")
    call test%assertequal(butler%des_index(), 404, "Butler index is 404 (4)")
    ! Bad state check (low)
    call butler%set_state_(-24)
    call test%asserttrue(butler%is_sleeping(), "Butler is sleeping (-24)")
    call test%assertfalse(butler%is_awake(), "Butler is not awake (-24)")
    call test%assertfalse(butler%holding_note(), "Butler is not holding note (-24)")
    call test%assertfalse(butler%is_gone(), "Butler is not gone (-24)")
    call test%assertfalse(butler%is_dead(), "Butler is not dead (-24)")
    call test%assertfalse(butler%is_alert(), "Butler is not alert (-24)")
    call test%assertfalse(butler%cannot_wake(), "Butler is wakeable (-24)")
    call test%assertequal(butler%des_index(), 400, "Butler index is 400 (-24)")
    ! Bad state check (high)
    call butler%set_state_(737)
    call test%assertfalse(butler%is_sleeping(), "Butler is not sleeping (737)")
    call test%assertfalse(butler%is_awake(), "Butler is not awake (737)")
    call test%assertfalse(butler%holding_note(), "Butler is not holding note (737)")
    call test%assertfalse(butler%is_gone(), "Butler is not gone (737)")
    call test%asserttrue(butler%is_dead(), "Butler is dead (737)")
    call test%assertfalse(butler%is_alert(), "Butler is not alert (737)")
    call test%asserttrue(butler%cannot_wake(), "Butler is not wakeable (737)")
    call test%assertequal(butler%des_index(), 404, "Butler index is 404 (737)")

    ! Bat tests
    call test%assertequal(bat%get_state_(), -1, "Uninitialized bat state (-1)")
    call test%assertfalse(bat%is_gone(), "Bat is not gone (-1)")
    call test%asserttrue(bat%is_hungry(), "Bat is hungry (-1)")
    call bat%init()
    call test%assertequal(bat%get_state_(), 0, "Initialized bat state (0)")
    call test%assertfalse(bat%is_gone(), "Bat is not gone (0)")
    call test%asserttrue(bat%is_hungry(), "Bat is hungry (0)")
    call bat%eat()
    call test%assertequal(bat%get_state_(), 1, "Fed bat state (1)")
    call test%asserttrue(bat%is_gone(), "Bat is gone (1)")
    call test%assertfalse(bat%is_hungry(), "Bat is not hungry (1)")
    ! Bad state check (low)
    call bat%set_state_(-999)
    call test%assertfalse(bat%is_gone(), "Bat is not gone (-999)")
    call test%asserttrue(bat%is_hungry(), "Bat is hungry (-999)")
    ! Bad state check (high)
    call bat%set_state_(404)
    call test%asserttrue(bat%is_gone(), "Bat is gone (404)")
    call test%assertfalse(bat%is_hungry(), "Bat is not hungry (404)")

    ! Wizard tests
    call test%assertequal(wizard%get_state_(), -1, "Uninitialized wizard state (-1)")
    call test%asserttrue(wizard%blocks_way(), "Wizard blocks the way (-1)")
    call test%assertfalse(wizard%is_gone(), "Wizard is gone (-1)")
    call wizard%init()
    call test%assertequal(wizard%get_state_(), 0, "Initialized wizard state (0)")
    call test%asserttrue(wizard%blocks_way(), "Wizard blocks the way (0)")
    call test%assertfalse(wizard%is_gone(), "Wizard is not gone (0)")
    call wizard%flee()
    call test%assertequal(wizard%get_state_(), 1, "Wizard retreat state (1)")
    call test%assertfalse(wizard%blocks_way(), "Wizard blocks the way (1)")
    call test%asserttrue(wizard%is_gone(), "Wizard is gone (1)")
    ! Bad state check (low)
    call wizard%set_state_(-321)
    call test%asserttrue(wizard%blocks_way(), "Wizard blocks the way (-321)")
    call test%assertfalse(wizard%is_gone(), "Wizard is not gone (-321)")
    ! Bad state check (high)
    call wizard%set_state_(1620)
    call test%assertfalse(wizard%blocks_way(), "Wizard blocks the way (1620)")
    call test%asserttrue(wizard%is_gone(), "Wizard is gone (1620)")

    ! Master tests
    call test%assertequal(master%get_state_(), -1, "Uninitialized Master state (-1)")
    call test%asserttrue(master%in_coffin(), "Master is in his coffin (-1)")
    call test%assertfalse(master%is_sleeping(), "Master is not sleeping (-1)")
    call test%assertfalse(master%is_pinned(), "Master is not pinned (-1)")
    call test%assertfalse(master%is_up(), "Master is not up (-1)")
    call test%assertfalse(master%is_dead(), "Master is not dead (-1)")
    call test%assertequal(master%des_index(), 423, "Master index is 423 (-1)")
    call master%init()
    call test%assertequal(master%get_state_(), 0, "Initialized Master state (0)")
    call test%asserttrue(master%in_coffin(), "Master is in his coffin (0)")
    call test%assertfalse(master%is_sleeping(), "Master is not sleeping (0)")
    call test%assertfalse(master%is_pinned(), "Master is not pinned (0)")
    call test%assertfalse(master%is_up(), "Master is not up (0)")
    call test%assertfalse(master%is_dead(), "Master is not dead (0)")
    call test%assertequal(master%des_index(), 423, "Master index is 423 (0)")
    call master%sleep()
    call test%assertequal(master%get_state_(), 1, "Sleeping Master state (1)")
    call test%assertfalse(master%in_coffin(), "Master is not in his coffin (1)")
    call test%asserttrue(master%is_sleeping(), "Master is sleeping (1)")
    call test%assertfalse(master%is_pinned(), "Master is not pinned (1)")
    call test%assertfalse(master%is_up(), "Master is not up (1)")
    call test%assertfalse(master%is_dead(), "Master is not dead (1)")
    call test%assertequal(master%des_index(), 424, "Master index is 424 (1)")
    call master%pin()
    call test%assertequal(master%get_state_(), 2, "Pinned Master state (2)")
    call test%assertfalse(master%in_coffin(), "Master is in not his coffin (2)")
    call test%assertfalse(master%is_sleeping(), "Master is not sleeping (2)")
    call test%asserttrue(master%is_pinned(), "Master is pinned (2)")
    call test%assertfalse(master%is_up(), "Master is not up (2)")
    call test%assertfalse(master%is_dead(), "Master is not dead (2)")
    call test%assertequal(master%des_index(), 425, "Master index is 425 (2)")
    call master%rise()
    call test%assertequal(master%get_state_(), 3, "Master up state (3)")
    call test%assertfalse(master%in_coffin(), "Master is not in his coffin (3)")
    call test%assertfalse(master%is_sleeping(), "Master is not sleeping (3)")
    call test%assertfalse(master%is_pinned(), "Master is not pinned (3)")
    call test%asserttrue(master%is_up(), "Master is up (3)")
    call test%assertfalse(master%is_dead(), "Master is not dead (3)")
    call test%assertequal(master%des_index(), 426, "Master index is 426 (3)")
    call master%expire()
    call test%assertequal(master%get_state_(), 4, "Dead Master state (4)")
    call test%assertfalse(master%in_coffin(), "Master is not in his coffin (4)")
    call test%assertfalse(master%is_sleeping(), "Master is not sleeping (4)")
    call test%assertfalse(master%is_pinned(), "Master is not pinned (4)")
    call test%assertfalse(master%is_up(), "Master is not up (4)")
    call test%asserttrue(master%is_dead(), "Master is dead (4)")
    call test%assertequal(master%des_index(), 427, "Master index is 427 (4)")
    ! Bad state check (low)
    call master%set_state_(-666)
    call test%asserttrue(master%in_coffin(), "Master is in his coffin (-666)")
    call test%assertfalse(master%is_sleeping(), "Master is not sleeping (-666)")
    call test%assertfalse(master%is_pinned(), "Master is not pinned (-666)")
    call test%assertfalse(master%is_up(), "Master is not up (-666)")
    call test%assertfalse(master%is_dead(), "Master is not dead (-666)")
    call test%assertequal(master%des_index(), 423, "Master index is 423 (-666)")
    ! Bad state check (high)
    call master%set_state_(151)
    call test%assertfalse(master%in_coffin(), "Master is not in his coffin (151)")
    call test%assertfalse(master%is_sleeping(), "Master is not sleeping (151)")
    call test%assertfalse(master%is_pinned(), "Master is not pinned (151)")
    call test%assertfalse(master%is_up(), "Master is not up (151)")
    call test%asserttrue(master%is_dead(), "Master is dead (151)")
    call test%assertequal(master%des_index(), 427, "Master index is 427 (151)")

    ! Cyclops tests
    call test%assertequal(cyclops%get_state_(), -1, "Uninitialized cyclops state (-1)")
    call test%asserttrue(cyclops%blocks_way(), "Cyclops blocks the way (-1)")
    call test%assertfalse(cyclops%left_hole(), "Cyclops did not leave a hole (-1)")
    call cyclops%init()
    call test%assertequal(cyclops%get_state_(), 0, "Initialized cyclops state (0)")
    call test%asserttrue(cyclops%blocks_way(), "Cyclops blocks the way (0)")
    call test%assertfalse(cyclops%left_hole(), "Cyclops did not leave a hole (0)")
    call cyclops%flee()
    call test%assertequal(cyclops%get_state_(), 1, "Fled cyclops state (1)")
    call test%assertfalse(cyclops%blocks_way(), "Cyclops does not block way (1)")
    call test%asserttrue(cyclops%left_hole(), "Cyclops left hole (1)")
    ! Bad state check (low)
    call cyclops%set_state_(-19)
    call test%asserttrue(cyclops%blocks_way(), "Cyclops blocks the way (-19)")
    call test%assertfalse(cyclops%left_hole(), "Cyclops is not hungry (-19)")
    ! Bad state check (high)
    call cyclops%set_state_(223)
    call test%assertfalse(cyclops%blocks_way(), "Cyclops does not block way (223)")
    call test%asserttrue(cyclops%left_hole(), "Cyclops left hole (223)")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_encounter.json")

    call test%checkfailure()
end program ut_encounter
