!> @file ut_gun.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_items:gun_t

!! @brief Unit tests of m_items:gun_t
program ut_gun
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_gamedata
    use :: m_items
    use :: m_object_id
    use :: m_stateful
    implicit none

    integer, parameter :: p_parlor = 3
    integer, parameter :: p_carried = -1

    ! integer :: id
    ! integer :: turn
    ! integer :: inv_ct
    integer :: past_place

    logical :: read_error
    type(itemtracker_t), target :: inv
    type(item_t), pointer :: pi_gun
    type(stateful_t) :: bare
    type(gun_t) :: s_gun
    type(TestCase) :: test

    continue

    call test%init(name="ut_gun")

    ! Check initial value of item pointer before initialization
    call test%assertfalse(associated(s_gun%item),                      &
      message="Verify item pointer of uninitialized gun is not associated")
    ! Check name of gun before initialization
    call test%assertequal(s_gun%name, bare%name,                       &
      message="Verify uninitialized gun name matches stateful_t")
    ! Check id of gun before initialization
    call test%assertequal(s_gun%id, bare%id,                           &
      message="Verify uninitialized gun id matches stateful_t")

    !!! Initialize itemtracker

    call inv%init()

    call load_game_data(read_error)
    call test%assertfalse(read_error,                                   &
      message="Read all game data")

    !!! Check initialized item attributes are in expected range

    ! Initialize gun state

    pi_gun => inv%item(O20_GUN)
    call s_gun%init(inv%item(O20_GUN))

    ! Check initial value of item pointer after initialization
    call test%asserttrue(associated(s_gun%item),                        &
      message="Verify item pointer of initialized gun is associated")
    ! Check name of gun after initialization
    call test%asserttrue(associated(s_gun%item, pi_gun),                &
      message="Verify initialized gun item gunes target")
    ! Check id of gun after initialization
    call test%assertequal(s_gun%id, O20_GUN,                            &
      message="Verify initialized gun id gunes O20_GUN")

    call test%assertequal(s_gun%item%place, p_parlor,                   &
      message="Verify gun place is reset by init")

    past_place = s_gun%item%place

    ! Carry/carried check

    ! Gun in room
    call test%assertequal(s_gun%item%place, past_place,                 &
      message="Verify gun is in room")
    call test%assertfalse(s_gun%item%is_carried(),                      &
      message="Verify gun is not carried (by item)")
    call test%assertfalse(inv%is_carried(O20_GUN),                      &
      message="Verify gun is not carried (by itemtracker)")
    call test%asserttrue(s_gun%item%in_room(past_place),                &
      message="Verify gun place is in original room")
    call test%asserttrue(s_gun%item%at_hand_in(past_place),             &
      message="Verify gun place is available (in room)")

    ! Pick up gun
    call inv%carry(O20_GUN)
    call test%assertequal(s_gun%item%place, p_carried,                  &
      message="Verify gun place is -1 (carried, itemlist)")
    call test%asserttrue(s_gun%item%is_carried(),                       &
      message="Verify gun is carried (by item)")
    call test%asserttrue(inv%is_carried(O20_GUN),                       &
      message="Verify gun is carried (by itemtracker)")
    call test%assertfalse(s_gun%item%in_room(past_place),               &
      message="Verify gun is not in original room")
    call test%asserttrue(s_gun%item%at_hand_in(past_place),             &
      message="Verify gun is available (via inventory)")

    ! Put it back where you found it
    call s_gun%item%place_in(past_place)
    call test%assertequal(s_gun%item%place, past_place,                 &
      message="Verify gun is in room")
    call test%assertfalse(s_gun%item%is_carried(),                      &
      message="Verify gun is not carried (by item)")
    call test%assertfalse(inv%is_carried(O20_GUN),                      &
      message="Verify gun is not carried (by itemtracker)")
    call test%asserttrue(s_gun%item%in_room(past_place),                &
      message="Verify gun is in original room")
    call test%asserttrue(s_gun%item%at_hand_in(past_place),             &
      message="Verify gun is available (in room)")

    ! Carry the gun
    call s_gun%item%carry()
    ! Load the gun
    call s_gun%load()

    call test%asserttrue(s_gun%item%place /= past_place,                &
      message="Verify gun is not in room")
    call test%asserttrue(s_gun%item%is_carried(),                       &
      message="Verify gun is carried (by item)")
    call test%asserttrue(inv%is_carried(O20_GUN),                       &
      message="Verify gun is carried (by itemtracker)")
    call test%assertfalse(s_gun%item%in_room(past_place),               &
      message="Verify gun is not in original room")
    call test%asserttrue(s_gun%item%at_hand_in(past_place),             &
      message="Verify gun is available)")
    call test%asserttrue(s_gun%is_loaded(),                             &
      message="Verify gun is loaded)")

    ! Unoad the gun
    call s_gun%unload()

    call test%asserttrue(s_gun%item%place /= past_place,                &
      message="Verify gun is not in room")
    call test%asserttrue(s_gun%item%is_carried(),                       &
      message="Verify gun is carried (by item)")
    call test%asserttrue(inv%is_carried(O20_GUN),                       &
      message="Verify gun is carried (by itemtracker)")
    call test%assertfalse(s_gun%item%in_room(past_place),               &
      message="Verify gun is not in original room")
    call test%asserttrue(s_gun%item%at_hand_in(past_place),             &
      message="Verify gun is available)")
    call test%asserttrue(s_gun%is_empty(),                             &
      message="Verify gun is not loaded)")

    ! Reset items
    call inv%init()

    call test%asserttrue(associated(s_gun%item, pi_gun),                &
      message="Verify gun is associated with an gun item")
    call test%assertequal(s_gun%item%place, inv%item(O20_GUN)%place,    &
      message="Verify gun and item(gun) in same place")
    call test%assertequal(s_gun%id, O20_GUN,                            &
      message="Verify gun has proper ID")
    call test%asserttrue(s_gun%item%is_carried()                        &
      .eqv. inv%item(O20_GUN)%is_carried(),                             &
      message="Verify gun and item(gun) have same carried status")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_gun.json")

    call test%checkfailure()
end program ut_gun
