!> @file ut_itemtracker.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_items:itemtracker_t

!! @brief Unit tests of m_items:itemtracker_t
program ut_itemtracker
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_gamedata
    use :: m_items
    use :: m_object_id
    use :: m_score, only: score_t
    implicit none

    integer, parameter :: p_vault = 72
    integer, parameter :: p_island = 81
    integer, parameter :: p_carried = -1
    integer, parameter :: p_consumed = 0

    integer :: id
    integer :: expected_score
    integer :: past_place
    integer :: how_many

    logical :: read_error
    type(itemtracker_t) :: inv
    type(score_t) :: p_score
    type(gun_t) :: s_gun
    type(bottle_t) :: s_bottle
    type(TestCase) :: test

    continue

    call test%init(name="ut_itemtracker")

    !!! Test uninitialized item attributes

    ! Check uninitialized item attributes match default (canary)
    ! values: id is -1, name is blank, value is -1, and place is 101

    do id = 1, NITEMS
      call test%assertequal(inv%item(id)%id, -1,                        &
        message="Item id canary")
      call test%assertequal(len_trim(inv%item(id)%name), 0,             &
        message="Item name canary")
      call test%assertequal(inv%item(id)%value, -1,                     &
        message="Item value canary")
      call test%assertequal(inv%item(id)%place, 101,                    &
        message="Item place canary")
    end do

    call inv%init()

    call s_bottle%init(inv%item(O18_BOTTLE), inv%item(O5_BLOOD),       &
      inv%item(O26_WATER))

    call load_game_data(read_error)
    call test%assertfalse(read_error,                                   &
      message="Read all game data")

    !!! Check initialized item attributes are in expected range

    ! id matches index, name at least as long as "Rowboat" but no
    ! longer than "Acetylene torch", value is non-negative and no
    ! more than 25, place is in [-3 .. 94]

    do id = 1, NITEMS
      call test%assertequal(inv%item(id)%id, id,                        &
        message="Item id initialized ")
      call test%asserttrue(len_trim(inv%item(id)%name) >= 7,            &
        message="Item name no shorter than 7 chars")
      call test%asserttrue(len_trim(inv%item(id)%name) <= 15,           &
        message="Item name no longer than 15 chars")
      call test%asserttrue(inv%item(id)%value >= 0,                     &
        message="Item value at least zero")
        call test%asserttrue(inv%item(id)%value <= 25,                  &
        message="Item value no more than 25")
      call test%asserttrue(abs(inv%item(id)%place) < 100,               &
        message="Item place roughly initialized")
      call test%asserttrue(inv%item(id)%place >= -3,                    &
        message="Item place no smaller than -3")
      call test%asserttrue(inv%item(id)%place <= 94,                    &
        message="Item place no greater than 94")
    end do

    ! Initialize gun state

    call s_gun%init(inv%item(O20_GUN))

    ! Test move/loot scoring

    call test%assertequal(inv%total_score(0), 0,                        &
      message="Loot(0) and move(0) score = 0")
    call test%assertequal(inv%total_score(417), -33,                    &
      message="Loot(0) and move(417) score = -33")

    inv%item(O4_KEY)%place       = p_vault
    inv%item(O7_CHAMPAGNE)%place = p_vault
    inv%item(O12_SWORD)%place     = p_vault
    inv%item(O17_STATUE)%place    = p_vault
    inv%item(O18_BOTTLE)%place    = p_island

    call test%assertequal(inv%total_score(249), 40,                     &
      message="Loot(5) and move(249) score = 40")
    call test%assertequal(inv%total_score(254), 40,                     &
      message="Loot(5) and move(254) score = 40")

    inv%item(O19_CROSS)%place     = p_vault
    inv%item(O23_RUBY)%place      = p_vault
    inv%item(O24_JADE)%place      = p_vault
    inv%item(O28_SAPPHIRE)%place  = p_vault
    inv%item(O29_MONEY)%place     = p_vault
    inv%item(O30_SWAN)%place      = p_vault

    call test%assertequal(inv%total_score(250), 100,                    &
      message="Loot(10) and move(250) score = 100")
    call test%assertequal(inv%total_score(311), 88,                     &
      message="Loot(10) and move(311) score = 88")

    ! Reset itemtracker
    call inv%init()

    past_place = inv%item(O25_ACID)%place
    call test%asserttrue(past_place /= p_vault,                         &
      message="Verify item place is reset by init")

    ! Carry/carried check

    ! Cross in room
    call test%assertequal(inv%item(O25_ACID)%place, past_place,         &
      message="Verify cross is in room")
    call test%assertfalse(inv%item(O25_ACID)%is_carried(),              &
      message="Verify cross is not carried (by item)")
    call test%assertfalse(inv%is_carried(O25_ACID),                     &
      message="Verify cross is not carried (by itemtracker)")
    call test%asserttrue(inv%item(O25_ACID)%in_room(past_place),        &
      message="Verify cross place is in original room")
    call test%asserttrue(inv%item(O25_ACID)%at_hand_in(past_place),     &
      message="Verify cross place is available (in room)")

    ! Pick up cross
    call inv%carry(O25_ACID)
    call test%assertequal(inv%item(O25_ACID)%place, p_carried,          &
      message="Verify cross place is -1 (carried, itemlist)")
    call test%asserttrue(inv%item(O25_ACID)%is_carried(),               &
      message="Verify cross is carried (by item)")
    call test%asserttrue(inv%is_carried(O25_ACID),                      &
      message="Verify cross is carried (by itemtracker)")
    call test%assertfalse(inv%item(O25_ACID)%in_room(past_place),       &
      message="Verify cross is not in original room")
    call test%asserttrue(inv%item(O25_ACID)%at_hand_in(past_place),     &
      message="Verify cross is available (via inventory)")

    ! Put it back where you found it
    call inv%item(O25_ACID)%place_in(past_place)

    call test%assertequal(inv%item(O25_ACID)%place, past_place,         &
      message="Verify cross is in room")
    call test%assertfalse(inv%item(O25_ACID)%is_carried(),              &
      message="Verify cross is not carried (by item)")
    call test%assertfalse(inv%is_carried(O25_ACID),                     &
      message="Verify cross is not carried (by itemtracker)")
    call test%asserttrue(inv%item(O25_ACID)%in_room(past_place),        &
      message="Verify cross is in original room")
    call test%asserttrue(inv%item(O25_ACID)%at_hand_in(past_place),     &
      message="Verify cross is available (in room)")

    ! Consume it (via itemtracker)
    call inv%consume(O25_ACID)
    ! call inv%item(O25_ACID)%consume()

    call test%asserttrue(inv%item(O25_ACID)%place /= past_place,        &
      message="Verify cross is not in room")
    call test%assertfalse(inv%item(O25_ACID)%is_carried(),              &
      message="Verify cross is not carried (by item)")
    call test%assertfalse(inv%is_carried(O25_ACID),                     &
      message="Verify cross is not carried (by itemtracker)")
    call test%assertfalse(inv%item(O25_ACID)%in_room(past_place),       &
      message="Verify cross is not in original room")
    call test%assertfalse(inv%item(O25_ACID)%at_hand_in(past_place),    &
      message="Verify cross is not available)")

    ! Carry a bunch of stuff
    call inv%carry(O8_HUNCHBACK)
    call inv%carry(O14_BOAT)
    call inv%carry(O3_AXE)
    call inv%carry(O18_BOTTLE)
    call inv%carry(O26_WATER)
    ! Manually put water into bottle
    call s_bottle%fill_with_water()

    how_many = count(inv%item(:)%place == p_carried)
    call test%assertequal(how_many, 5,                                  &
      message="Inventory contains five items by count")

    ! Show inventory; note the game considers a full bottle to be
    ! one item, not two. Here displayed count is 5; it should be 4.
    call s_gun%unload()
    call inv%show_inventory(NUMB=how_many, s_gun=s_gun, s_bottle=s_bottle)
    call test%assertequal(how_many, 4,                                  &
      message="After showing inventory, NUMB is four")
    ! write(unit=STDOUT, fmt="('NUMB = ', I0)") NUMB

    ! Show inventory again; displayed count should be 4.
    call inv%show_inventory(NUMB=how_many, s_gun=s_gun, s_bottle=s_bottle)
    call test%assertequal(how_many, 4,                                  &
      message="After showing inventory again, NUMB is four")

    how_many = count(inv%item(:)%place == p_carried)
    call test%assertequal(how_many, 5,                                  &
      message="Inventory still contains five items by count")

    write(unit=stdout, fmt="('Actually carrying ', I0, ' items (direct item place check):')")     &
      how_many
    do id = 1, NITEMS
      if (inv%item(id)%place == p_carried) then
        write(unit=stdout, fmt="(' - ', I2, ': ', A16)")                &
          inv%item(id)%id, inv%item(id)%name
      end if
    end do

    write(unit=stdout, fmt="('Actually carrying ', I0, ' items (itemtracker check):')") &
      how_many
    do id = 1, NITEMS
      if (inv%is_carried(id)) then
        write(unit=stdout, fmt="(' - ', I2, ': ', A16)")                &
          inv%item(id)%id, inv%item(id)%name
      end if
    end do

    write(unit=stdout, fmt="('Actually carrying ', I0, ' items (per-item check):')") &
      how_many
    do id = 1, NITEMS
      if (inv%item(id)%is_carried()) then
        write(unit=stdout, fmt="(' - ', I2, ': ', A16)")                &
          inv%item(id)%id, inv%item(id)%name
      end if
    end do

    call inv%carry(O20_GUN)
    call inv%carry(O2_BULLET)
    call inv%consume(O26_WATER)
    ! Manually empty the bottle
    call s_bottle%clear()
    how_many = 6

    ! Show inventory again; displayed count should be 6.
    call inv%show_inventory(NUMB=how_many, s_gun=s_gun, s_bottle=s_bottle)
    call test%assertequal(how_many, 6,                                  &
      message="After updating inventory again, NUMB is six")

    call s_gun%load()
    ! Show inventory again; displayed count should be 6 (wrong).
    call inv%show_inventory(NUMB=how_many, s_gun=s_gun, s_bottle=s_bottle)
    call test%assertequal(how_many, 5,                                  &
      message="After updating inventory again, NUMB is five")

    ! Show inventory again; displayed count should be 5 (correct).
    call inv%show_inventory(NUMB=how_many, s_gun=s_gun, s_bottle=s_bottle)

    ! Scatter demo
    do id = 1, NITEMS
      if (inv%item(id)%is_carried()) then
        call inv%item(id)%scatter(lo_room=2, n_rooms=9)
        write(unit=stdout, fmt="(' - ', I2, ': ', A16, ' flung to ', I0)") &
          inv%item(id)%id, inv%item(id)%name, inv%item(id)%place
        call test%asserttrue((inv%item(id)%place >= 2 .and. inv%item(id)%place <= 10), &
          message="Item scattered in range")
      end if
    end do

    ! Scatter demo: Check by inspection
    write(unit=stdout, fmt="('*** Fly, be free!!! ***')")
    id = O30_SWAN
    do how_many = 1, 100
      call inv%item(id)%scatter(lo_room=2, n_rooms=9)
      write(unit=stdout, fmt="(' - ', I2, ': ', A16, ' flung to ', I0)") &
        inv%item(id)%id, inv%item(id)%name, inv%item(id)%place
      call test%asserttrue((inv%item(id)%place >= 2 .and. inv%item(id)%place <= 10), &
        message="Swan scattered in range")
    end do

    write(unit=stdout, fmt="('*** Fly, be free!? ***')")
    id = O14_BOAT
    do how_many = 1, 300
      call inv%item(id)%scatter(lo_room=40, hi_room=98)
      write(unit=stdout, fmt="(' - ', I2, ': ', A16, ' flung to ', I0)") &
        inv%item(id)%id, inv%item(id)%name, inv%item(id)%place
      call test%asserttrue((inv%item(id)%place >= 40 .and. inv%item(id)%place <= 98), &
        message="Boat scattered in range")
    end do

    write(unit=stdout, fmt="('*** You? Just sit there. ***')")
    id = O8_HUNCHBACK
    do how_many = 1, 10
      call inv%item(id)%scatter(lo_room=8)
      write(unit=stdout, fmt="(' - ', I2, ': ', A16, ' flung to ', I0)") &
        inv%item(id)%id, inv%item(id)%name, inv%item(id)%place
      call test%assertequal(inv%item(id)%place, 8, &
        message="Hunchback randomly wanders room 8")
    end do

    ! Scatter away from 33; ensures the room contents test (next)
    ! is deterministic
    write(unit=stdout, fmt="('*** Away! ***')")
    do id = 1, NITEMS
      call inv%item(id)%scatter(lo_room=34, hi_room=94)
      write(unit=stdout, fmt="(' - ', I2, ': ', A16, ' flung to ', I0)") &
        inv%item(id)%id, inv%item(id)%name, inv%item(id)%place
      call test%asserttrue((inv%item(id)%place >= 34 .and. inv%item(id)%place <= 94), &
        message="Item scattered in range")
    end do

    ! Carry a bunch of stuff
    call inv%item(O9_ROPE)%place_in(33)
    call inv%item(O16_HOOK)%place_in(33)
    call inv%item(O25_ACID)%place_in(33)
    call inv%item(O20_GUN)%place_in(33)
    call inv%item(O21_LAMP)%place_in(33)
    call inv%item(O12_SWORD)%place_in(33)

    write(unit=stdout, fmt="('*** An unexpected precipice! ***')")

    call p_score%add_to_score(30)
    expected_score = p_score%SCORE + inv%item(O12_SWORD)%value
    ! Show stuff in room
    call inv%display_here(p_score, ROOM=33, path_to_precipice=.true.)

    call test%assertequal(p_score%SCORE, expected_score,                &
      message="Discovery score is 40")

    write(unit=stdout, fmt="('*** Someone stole our precipice! ***')")

    ! Show stuff in room
    call inv%display_here(p_score, ROOM=33, path_to_precipice=.false.)

    call test%assertequal(p_score%SCORE, expected_score,                &
      message="Discovery score is still 40")

    call test%assertequal(inv%item(O12_SWORD)%value, 0,                 &
      message="Sword discovery score is zero now")

    ! Description demo; verify by inspection
    write(unit=stdout, fmt="('*** Lo! ***')")
    do id = 1, NITEMS
      write(unit=stdout, fmt="('* Item ', I2, ': ', A16, ' is')") &
        inv%item(id)%id, inv%item(id)%name
      call inv%item(id)%describe()
      call inv%item(id)%scatter(lo_room=2, hi_room=94)
    end do

    call inv%carry(O20_GUN)
    how_many = 1
    call s_gun%unload()
    ! Show inventory again; displayed count should be 6 (wrong).
    call inv%show_inventory(NUMB=how_many, s_gun=s_gun, s_bottle=s_bottle)

    call inv%item(O20_GUN)%place_in(8)
    how_many = 0
    ! Show inventory again; displayed count should be 6 (wrong).
    call inv%show_inventory(NUMB=how_many, s_gun=s_gun, s_bottle=s_bottle)

      ! @todo Convert this to a proper dump() routine
      ! write(unit=stdout, fmt="('ID ', I0, ' -> ', I0)")                 &
      !   O25_ACID, inv%item(O25_ACID)%id
      ! write(unit=stdout, fmt="('NAME is ', A)")                         &
      !   inv%item(O25_ACID)%name
      ! write(unit=stdout, fmt="('VALUE is ', I0)")                       &
      !   inv%item(O25_ACID)%value
      ! write(unit=stdout, fmt="('PLACE ', I0, ' /= ', I0, ' -> 0')")     &
      !   past_place, inv%item(O25_ACID)%place
      ! write(unit=stdout, fmt="('Carried? ', L1, ' (item)')")            &
      !   inv%item(O25_ACID)%is_carried()
      ! write(unit=stdout, fmt="('Carried? ', L1, ' (itemlist)')")        &
      !   inv%is_carried(O25_ACID)

      ! write(unit=stdout, fmt="('In room ', I0, ' ? ', L1)")             &
      !   past_place, inv%item(O25_ACID)%in_room(past_place)
      ! write(unit=stdout, fmt="('Is at hand in room ', I0, ' ? ', L1)")  &
      !   past_place, inv%item(O25_ACID)%at_hand_in(past_place)


    ! call test%assertequal(NITEMS, 30,                                   &
    !   message="Proper number of carryable items found (30)")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_itemtracker.json")

    call test%checkfailure()
end program ut_itemtracker
