!> @file ut_lamp.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_items:lamp_t

!! @brief Unit tests of m_items:lamp_t
program ut_lamp
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_gamedata
    use :: m_items
    use :: m_object_id
    use :: m_stateful
    implicit none

    ! integer, parameter :: p_vault = 72
    ! integer, parameter :: p_island = 81
    integer, parameter :: p_attic_east = 22
    integer, parameter :: p_carried = -1
    ! integer, parameter :: p_consumed = 0

    ! integer :: id
    integer :: turn
    integer :: inv_ct
    integer :: past_place
    ! integer :: expected_score
    ! integer :: how_many

    ! integer :: room

    logical :: read_error
    type(itemtracker_t), target :: inv
    type(item_t), pointer :: pi_lamp
    type(stateful_t) :: bare
    type(lamp_t) :: s_lamp
    type(TestCase) :: test

    continue

    call test%init(name="ut_lamp")

    ! Check initial value of item pointer before initialization
    call test%assertfalse(associated(s_lamp%item),                      &
      message="Verify item pointer of uninitialized lamp is not associated")
    ! Check name of lamp before initialization
    call test%assertequal(s_lamp%name, bare%name,                       &
      message="Verify uninitialized lamp name matches stateful_t")
    ! Check id of lamp before initialization
    call test%assertequal(s_lamp%id, bare%id,                           &
      message="Verify uninitialized lamp id matches stateful_t")

    !!! Initialize itemtracker

    call inv%init()

    call load_game_data(read_error)
    call test%assertfalse(read_error,                                   &
      message="Read all game data")

    !!! Check initialized item attributes are in expected range

    ! Initialize lamp state

    pi_lamp => inv%item(O21_LAMP)
    call s_lamp%init(inv%item(O21_LAMP))

    ! Check initial value of item pointer after initialization
    call test%asserttrue(associated(s_lamp%item),                       &
      message="Verify item pointer of initialized lamp is associated")
    ! Check name of lamp after initialization
    call test%asserttrue(associated(s_lamp%item, pi_lamp),   &
      message="Verify initialized lamp item matches target")
    ! Check id of lamp after initialization
    call test%assertequal(s_lamp%id, O21_LAMP,                          &
      message="Verify initialized lamp id matches O21_LAMP")

    call test%assertequal(inv%item(O21_LAMP)%place, p_attic_east,       &
      message="Verify lamp place is reset by init")

    past_place = inv%item(O21_LAMP)%place

    ! Carry/carried check

    ! Lamp in room
    call test%assertequal(inv%item(O21_LAMP)%place, past_place,         &
      message="Verify lamp is in room")
    call test%assertfalse(inv%item(O21_LAMP)%is_carried(),              &
      message="Verify lamp is not carried (by item)")
    call test%assertfalse(inv%is_carried(O21_LAMP),                     &
      message="Verify lamp is not carried (by itemtracker)")
    call test%asserttrue(inv%item(O21_LAMP)%in_room(past_place),        &
      message="Verify lamp place is in original room")
    call test%asserttrue(inv%item(O21_LAMP)%at_hand_in(past_place),     &
      message="Verify lamp place is available (in room)")

    ! Pick up lamp
    call inv%carry(O21_LAMP)
    call test%assertequal(inv%item(O21_LAMP)%place, p_carried,          &
      message="Verify lamp place is -1 (carried, itemlist)")
    call test%asserttrue(inv%item(O21_LAMP)%is_carried(),               &
      message="Verify lamp is carried (by item)")
    call test%asserttrue(inv%is_carried(O21_LAMP),                      &
      message="Verify lamp is carried (by itemtracker)")
    call test%assertfalse(inv%item(O21_LAMP)%in_room(past_place),       &
      message="Verify lamp is not in original room")
    call test%asserttrue(inv%item(O21_LAMP)%at_hand_in(past_place),     &
      message="Verify lamp is available (via inventory)")

    ! Put it back where you found it
    call inv%item(O21_LAMP)%place_in(past_place)

    call test%assertequal(inv%item(O21_LAMP)%place, past_place,         &
      message="Verify lamp is in room")
    call test%assertfalse(inv%item(O21_LAMP)%is_carried(),              &
      message="Verify lamp is not carried (by item)")
    call test%assertfalse(inv%is_carried(O21_LAMP),                     &
      message="Verify lamp is not carried (by itemtracker)")
    call test%asserttrue(inv%item(O21_LAMP)%in_room(past_place),        &
      message="Verify lamp is in original room")
    call test%asserttrue(inv%item(O21_LAMP)%at_hand_in(past_place),     &
      message="Verify lamp is available (in room)")

    ! Consume it (via itemtracker)
    call inv%consume(O21_LAMP)
    ! call inv%item(O21_LAMP)%consume()

    call test%asserttrue(inv%item(O21_LAMP)%place /= past_place,        &
      message="Verify lamp is not in room")
    call test%assertfalse(inv%item(O21_LAMP)%is_carried(),              &
      message="Verify lamp is not carried (by item)")
    call test%assertfalse(inv%is_carried(O21_LAMP),                     &
      message="Verify lamp is not carried (by itemtracker)")
    call test%assertfalse(inv%item(O21_LAMP)%in_room(past_place),       &
      message="Verify lamp is not in original room")
    call test%assertfalse(inv%item(O21_LAMP)%at_hand_in(past_place),    &
      message="Verify lamp is not available)")

    ! Reset items
    call inv%init()

    call test%asserttrue(associated(s_lamp%item, pi_lamp),              &
      message="Verify lamp is associated with an lamp item")
    call test%assertequal(s_lamp%item%place, inv%item(O21_LAMP)%place,  &
      message="Verify lamp and item(lamp) in same place")
    call test%assertequal(s_lamp%id, O21_LAMP,                          &
      message="Verify lamp has proper ID")
    call test%asserttrue(s_lamp%item%is_carried()                       &
      .eqv. inv%item(O21_LAMP)%is_carried(),                            &
      message="Verify lamp and item(lamp) have same carried status")

    ! Continuous burn with auto refill
    call s_lamp%item%carry()
    call inv%carry(O1_KEROSENE)
    inv_ct = 2

    call s_lamp%turn_on()
    do turn = 1, 280
      ! Complex burn, check if auto refill is possible (yes)
      call s_lamp%burn_one_turn(inv%item(O1_KEROSENE), inv_ct)

      ! Check for auto refill
      if (turn < 75) then
        call test%asserttrue(inv%item(O1_KEROSENE)%is_carried(),       &
          message="Turns 1-75, kerosene is in inventory")
        call test%assertfalse(inv%item(O1_KEROSENE)%place == 0,        &
          message="Turns 1-75, kerosene has not been consumed")
        call test%assertequal(inv_ct, 2,                                &
          message="Turns 1-75, two items in inventory")
      else
        call test%assertfalse(inv%item(O1_KEROSENE)%is_carried(),      &
          message="Turns 76+, kerosene is not in inventory")
        call test%asserttrue(inv%item(O1_KEROSENE)%place == 0,         &
          message="Turns 76+, kerosene has been consumed")
        call test%assertequal(inv_ct, 1,                                &
          message="Turns 76+, one item in inventory")
      end if

      ! Check stages of burnout
      if (turn < 225) then
        call test%assertfalse(s_lamp%is_off(),                          &
          message="Turns 1-224, lamp is not off")
        call test%asserttrue(s_lamp%is_bright(),                        &
          message="Turns 1-224, lamp is bright")
        call test%assertfalse(s_lamp%is_dim(),                          &
          message="Turns 1-224, lamp is not dim")
        call test%assertfalse(s_lamp%is_empty(),                        &
          message="Turns 1-224, lamp is not empty")
        call test%asserttrue(s_lamp%is_lit(),                           &
          message="Turns 1-224, lamp is lit")
        call test%assertfalse(s_lamp%is_dark(),                         &
          message="Turns 1-224, lamp is not dark")
        call test%asserttrue(s_lamp%turns_lit < 75,                     &
          message="Turns 1-224, lamp has burned less than 75 turns")
      else if (turn < 250) then
        call test%assertfalse(s_lamp%is_off(),                          &
          message="Turns 225-249, lamp is not off")
        call test%assertfalse(s_lamp%is_bright(),                       &
          message="Turns 225-249, lamp is not bright")
        call test%asserttrue(s_lamp%is_dim(),                           &
          message="Turns 225-249, lamp is dim")
        call test%assertfalse(s_lamp%is_empty(),                        &
          message="Turns 225-249, lamp is not empty")
        call test%asserttrue(s_lamp%is_lit(),                           &
          message="Turns 225-249, lamp is lit")
        call test%assertfalse(s_lamp%is_dark(),                         &
          message="Turns 225-249, lamp is not dark")
        call test%asserttrue(s_lamp%turns_lit < 100,                    &
          message="Turns 225-249, lamp has burned less than 100 turns")
      else
        call test%assertfalse(s_lamp%is_off(),                          &
          message="Turns 250-280, lamp is not off")
        call test%assertfalse(s_lamp%is_bright(),                       &
          message="Turns 250-280, lamp is not bright")
        call test%assertfalse(s_lamp%is_dim(),                          &
          message="Turns 250-280, lamp is not dim")
        call test%asserttrue(s_lamp%is_empty(),                         &
          message="Turns 250-280, lamp is empty")
        call test%assertfalse(s_lamp%is_lit(),                          &
          message="Turns 250-280, lamp is not lit")
        call test%asserttrue(s_lamp%is_dark(),                          &
          message="Turns 250-280, lamp is dark")
        call test%asserttrue(s_lamp%turns_lit >= 100,                   &
          message="Turns 250-280, lamp has burned at least 100 turns")
      end if
    end do

    ! Continuous burn without refill
    call inv%init()
    call s_lamp%init(inv%item(O21_LAMP))

    call inv%consume(O1_KEROSENE)
    call s_lamp%item%carry()
    inv_ct = 1

    call s_lamp%turn_on()
    do turn = 1, 110
      ! Complex burn, check if auto refill is possible (no)
      call s_lamp%burn_one_turn(inv%item(O1_KEROSENE), inv_ct)

      ! Check stages of burnout
      if (turn < 75) then
        call test%assertfalse(s_lamp%is_off(),                          &
          message="Turns 1-74, lamp is not off")
        call test%asserttrue(s_lamp%is_bright(),                        &
          message="Turns 1-74, lamp is bright")
        call test%assertfalse(s_lamp%is_dim(),                          &
          message="Turns 1-74, lamp is not dim")
        call test%assertfalse(s_lamp%is_empty(),                        &
          message="Turns 1-74, lamp is not empty")
        call test%asserttrue(s_lamp%is_lit(),                           &
          message="Turns 1-74, lamp is lit")
        call test%assertfalse(s_lamp%is_dark(),                         &
          message="Turns 1-74, lamp is not dark")
        call test%asserttrue(s_lamp%turns_lit < 75,                     &
          message="Turns 1-74, lamp has burned less than 75 turns")
      else if (turn < 100) then
        call test%assertfalse(s_lamp%is_off(),                          &
          message="Turns 75-99, lamp is not off")
        call test%assertfalse(s_lamp%is_bright(),                       &
          message="Turns 75-99, lamp is not bright")
        call test%asserttrue(s_lamp%is_dim(),                           &
          message="Turns 75-99, lamp is dim")
        call test%assertfalse(s_lamp%is_empty(),                        &
          message="Turns 75-99, lamp is not empty")
        call test%asserttrue(s_lamp%is_lit(),                           &
          message="Turns 75-99, lamp is lit")
        call test%assertfalse(s_lamp%is_dark(),                         &
          message="Turns 75-99, lamp is not dark")
        call test%asserttrue(s_lamp%turns_lit < 100,                    &
          message="Turns 75-99, lamp has burned less than 100 turns")
      else
        call test%assertfalse(s_lamp%is_off(),                          &
          message="Turns 100-110, lamp is not off")
        call test%assertfalse(s_lamp%is_bright(),                       &
          message="Turns 100-110, lamp is not bright")
        call test%assertfalse(s_lamp%is_dim(),                          &
          message="Turns 100-110, lamp is not dim")
        call test%asserttrue(s_lamp%is_empty(),                         &
          message="Turns 100-110, lamp is empty")
        call test%assertfalse(s_lamp%is_lit(),                          &
          message="Turns 100-110, lamp is not lit")
        call test%asserttrue(s_lamp%is_dark(),                          &
          message="Turns 100-110, lamp is dark")
        call test%asserttrue(s_lamp%turns_lit >= 100,                   &
          message="Turns 100-110, lamp has burned at least 100 turns")
      end if
    end do

    ! Refuel from empty
    call inv%carry(O1_KEROSENE)
    call test%asserttrue(inv%item(O1_KEROSENE)%is_carried(),           &
      message="Kerosene magically appears as carried")

    ! Lamp should now have 0 turns_lit and be off (in state 0)
    call s_lamp%refuel()

    call test%asserttrue(s_lamp%turns_lit == 0,                         &
      message="Refueled from empty, lamp has burned 0 turns")
    call test%asserttrue(s_lamp%is_off(),                               &
      message="Refueled from empty, lamp is off")
    call test%asserttrue(s_lamp%is_dark(),                              &
      message="Refueled from empty, lamp is not lit")
    call test%assertfalse(s_lamp%is_empty(),                            &
      message="Refueled from empty, lamp is not empty")

    call test%asserttrue(inv%item(O1_KEROSENE)%is_carried(),           &
      message="Kerosene magically has not been consumed after refueling")

    call inv%consume(O1_KEROSENE)
    inv_ct = 1

    ! Light lamp (filled from empty) and burn until empty again
    call s_lamp%turn_on()

    call test%assertfalse(s_lamp%is_off(),                              &
      message="Freshly lit lamp is not off")
    call test%asserttrue(s_lamp%is_bright(),                            &
      message="Freshly lit lamp is bright")

    do turn = 1, 110
      ! Simple burn, cannot auto refill
      call s_lamp%burn_one_turn()

      ! Check stages of burnout
      if (turn < 75) then
        call test%assertfalse(s_lamp%is_off(),                          &
          message="Turns 1-74, lamp is not off")
        call test%asserttrue(s_lamp%is_bright(),                        &
          message="Turns 1-74, lamp is bright")
        call test%assertfalse(s_lamp%is_dim(),                          &
          message="Turns 1-74, lamp is not dim")
        call test%assertfalse(s_lamp%is_empty(),                        &
          message="Turns 1-74, lamp is not empty")
        call test%asserttrue(s_lamp%is_lit(),                           &
          message="Turns 1-74, lamp is lit")
        call test%assertfalse(s_lamp%is_dark(),                         &
          message="Turns 1-74, lamp is not dark")
        call test%asserttrue(s_lamp%turns_lit < 75,                     &
          message="Turns 1-74, lamp has burned less than 75 turns")
      else if (turn < 100) then
        call test%assertfalse(s_lamp%is_off(),                          &
          message="Turns 75-99, lamp is not off")
        call test%assertfalse(s_lamp%is_bright(),                       &
          message="Turns 75-99, lamp is not bright")
        call test%asserttrue(s_lamp%is_dim(),                           &
          message="Turns 75-99, lamp is dim")
        call test%assertfalse(s_lamp%is_empty(),                        &
          message="Turns 75-99, lamp is not empty")
        call test%asserttrue(s_lamp%is_lit(),                           &
          message="Turns 75-99, lamp is lit")
        call test%assertfalse(s_lamp%is_dark(),                         &
          message="Turns 75-99, lamp is not dark")
        call test%asserttrue(s_lamp%turns_lit < 100,                    &
          message="Turns 75-99, lamp has burned less than 100 turns")
      else
        call test%assertfalse(s_lamp%is_off(),                          &
          message="Turns 100-110, lamp is not off")
        call test%assertfalse(s_lamp%is_bright(),                       &
          message="Turns 100-110, lamp is not bright")
        call test%assertfalse(s_lamp%is_dim(),                          &
          message="Turns 100-110, lamp is not dim")
        call test%asserttrue(s_lamp%is_empty(),                         &
          message="Turns 100-110, lamp is empty")
        call test%assertfalse(s_lamp%is_lit(),                          &
          message="Turns 100-110, lamp is not lit")
        call test%asserttrue(s_lamp%is_dark(),                          &
          message="Turns 100-110, lamp is dark")
        call test%asserttrue(s_lamp%turns_lit >= 100,                   &
          message="Turns 100-110, lamp has burned at least 100 turns")
      end if
    end do

    ! Should be off with 0 turns lit
    call s_lamp%refuel()
    call test%asserttrue(s_lamp%turns_lit == 0,                         &
      message="Second refuel, lamp has burned 0 turns")
    call test%asserttrue(s_lamp%is_off(),                               &
      message="Second refuel, lamp is off")

    ! Put kerosene in the bedroom
    call inv%item(O1_KEROSENE)%place_in(1)
    inv_ct = 1

    call s_lamp%turn_on()
    do turn = 1, 210
      ! Complex burn, check if auto refill is possible (no )
      call s_lamp%burn_one_turn(inv%item(O1_KEROSENE), inv_ct)

      select case(turn)
      case(62) ! R=0, TL 62 -> 0
        call test%asserttrue(s_lamp%turns_lit == 62,                    &
           message="Before final refuel, lamp has burned 62 turns (62)")
        ! Manual refueling only adds 100 turns vs 175 for autofueling
        call inv%carry(O1_KEROSENE)
        call s_lamp%refuel()
        call inv%consume(O1_KEROSENE)
        ! Resets turns_lit to 0
      case(83) ! R+21, TL 21
        ! Turn off the lamp; should not burn any fuel even if
        ! burn_one_turn() is called
        call test%asserttrue(s_lamp%is_bright(),                        &
           message="Final refuel + 21, lamp is bright")
        call test%asserttrue(s_lamp%turns_lit == 21,                    &
           message="Lamp turned off 21 turns after refuel (83)")
        call s_lamp%turn_off()
      case(104) ! R+42, TL 21
        call test%asserttrue(s_lamp%turns_lit == 21,                    &
           message="Lamp still has only burned 21 turns after refuel (104)")
        call test%asserttrue(s_lamp%is_off(),                           &
          message="Final refuel + 21, lamp still off (104)")
        call test%asserttrue(s_lamp%is_dark(),                          &
          message="Final refuel + 21, lamp still dark (104)")
        call test%assertfalse(s_lamp%is_bright(),                       &
          message="Final refuel + 21, lamp is not bright (104)")
        call test%assertfalse(s_lamp%is_dim(),                          &
          message="Final refuel + 21, lamp is not dim (104)")
        ! Turn on the lamp
        call s_lamp%turn_on()
      case(136) ! R+74, TL 21 + 32 -> TL 53
        call test%asserttrue(s_lamp%turns_lit == 53,                    &
          message="After relight, lamp has burned 53 turns (136)")
        call test%asserttrue(s_lamp%is_bright(),                        &
          message="TL 75, lamp is bright (136)")
          ! Lamp is on, burning in the bedroom
        call s_lamp%item%place_in(1)
      case(148) ! R+86, TL 53 + 12 -> TL 65
        call test%asserttrue(s_lamp%turns_lit == 65,                    &
          message="After drop, lamp has burned 65 turns (148)")
        call test%asserttrue(s_lamp%is_bright(),                        &
          message="TL 75, lamp is bright (148)")
        ! Pick up lamp again
        call s_lamp%item%carry()
      case(158) ! R+96, TL 75
        call test%asserttrue(s_lamp%turns_lit == 75,                    &
          message="After pickup, lamp has burned 75 turns (158)")
        ! Lamp goes dim
        call test%assertfalse(s_lamp%is_bright(),                       &
          message="TL 75, lamp is not bright (158)")
        call test%asserttrue(s_lamp%is_dim(),                           &
          message="TL 75, lamp is dim (158)")
        call test%assertfalse(s_lamp%is_off(),                          &
          message="TL 75, lamp is not off (158)")
        call test%assertfalse(s_lamp%is_dark(),                         &
          message="TL 75, lamp is dark (158)")
        call test%assertfalse(s_lamp%is_empty(),                        &
          message="TL 75, lamp is empty (158)")
      case(183) ! R+121, TL 100
        call test%asserttrue(s_lamp%turns_lit == 100,                   &
          message="After dimming, lamp has burned 100 turns")
        ! Lamp goes out
        call test%assertfalse(s_lamp%is_bright(),                       &
          message="Dim + 25, lamp is not bright (183)")
        call test%assertfalse(s_lamp%is_dim(),                          &
          message="Dim + 25, lamp is not dim (183)")
        call test%assertfalse(s_lamp%is_off(),                          &
          message="Dim + 25, lamp is not off (183)")
        call test%asserttrue(s_lamp%is_dark(),                          &
          message="Dim + 25, lamp is dark (183)")
        call test%asserttrue(s_lamp%is_empty(),                         &
          message="Dim + 25, lamp is empty (183)")
      case default
      end select

      if (turn >= 1 .and. turn < 83) then
        call test%asserttrue(s_lamp%is_lit(),                           &
          message="Lamp is lit (1-82)")
      else if (turn >= 83 .and. turn < 104) then
        call test%asserttrue(s_lamp%is_dark(),                          &
          message="Lamp is dark (83-103)")
      else if (turn >= 104 .and. turn < 183) then
        call test%asserttrue(s_lamp%is_lit(),                           &
          message="Lamp is lit (1-82)")
      else if (turn >= 183) then
        call test%asserttrue(s_lamp%is_dark(),                          &
          message="Lamp is lit (183-210)")
      end if

    end do

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_lamp.json")

    call test%checkfailure()
end program ut_lamp
