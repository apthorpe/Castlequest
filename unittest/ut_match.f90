!> @file ut_match.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_items:match_t

!! @brief Unit tests of m_items:match_t
program ut_match
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_gamedata
    use :: m_items
    use :: m_object_id
    use :: m_stateful
    implicit none

    integer, parameter :: p_library = 13
    integer, parameter :: p_carried = -1

    ! integer :: id
    integer :: turn
    integer :: inv_ct
    integer :: past_place

    logical :: read_error
    type(itemtracker_t), target :: inv
    type(item_t), pointer :: pi_match
    type(stateful_t) :: bare
    type(match_t) :: s_match
    type(TestCase) :: test

    continue

    call test%init(name="ut_match")

    ! Check initial value of item pointer before initialization
    call test%assertfalse(associated(s_match%item),                      &
      message="Verify item pointer of uninitialized match is not associated")
    ! Check name of match before initialization
    call test%assertequal(s_match%name, bare%name,                       &
      message="Verify uninitialized match name matches stateful_t")
    ! Check id of match before initialization
    call test%assertequal(s_match%id, bare%id,                           &
      message="Verify uninitialized match id matches stateful_t")

    !!! Initialize itemtracker

    call inv%init()

    call load_game_data(read_error)
    call test%assertfalse(read_error,                                   &
      message="Read all game data")

    !!! Check initialized item attributes are in expected range

    ! Initialize match state

    pi_match => inv%item(O15_MATCH)
    call s_match%init(inv%item(O15_MATCH))

    ! Check initial value of item pointer after initialization
    call test%asserttrue(associated(s_match%item),                      &
      message="Verify item pointer of initialized match is associated")
    ! Check name of match after initialization
    call test%asserttrue(associated(s_match%item, pi_match),            &
      message="Verify initialized match item matches target")
    ! Check id of match after initialization
    call test%assertequal(s_match%id, O15_MATCH,                        &
      message="Verify initialized match id matches O15_MATCH")

    call test%assertequal(inv%item(O15_MATCH)%place, p_library,      &
      message="Verify match place is reset by init")

    past_place = inv%item(O15_MATCH)%place

    ! Carry/carried check

    ! Match in room
    call test%assertequal(inv%item(O15_MATCH)%place, past_place,         &
      message="Verify match is in room")
    call test%assertfalse(inv%item(O15_MATCH)%is_carried(),              &
      message="Verify match is not carried (by item)")
    call test%assertfalse(inv%is_carried(O15_MATCH),                     &
      message="Verify match is not carried (by itemtracker)")
    call test%asserttrue(inv%item(O15_MATCH)%in_room(past_place),        &
      message="Verify match place is in original room")
    call test%asserttrue(inv%item(O15_MATCH)%at_hand_in(past_place),     &
      message="Verify match place is available (in room)")

    ! Pick up match
    call inv%carry(O15_MATCH)
    call test%assertequal(inv%item(O15_MATCH)%place, p_carried,          &
      message="Verify match place is -1 (carried, itemlist)")
    call test%asserttrue(inv%item(O15_MATCH)%is_carried(),               &
      message="Verify match is carried (by item)")
    call test%asserttrue(inv%is_carried(O15_MATCH),                      &
      message="Verify match is carried (by itemtracker)")
    call test%assertfalse(inv%item(O15_MATCH)%in_room(past_place),       &
      message="Verify match is not in original room")
    call test%asserttrue(inv%item(O15_MATCH)%at_hand_in(past_place),     &
      message="Verify match is available (via inventory)")

    ! Put it back where you found it
    call inv%item(O15_MATCH)%place_in(past_place)

    call test%assertequal(inv%item(O15_MATCH)%place, past_place,         &
      message="Verify match is in room")
    call test%assertfalse(inv%item(O15_MATCH)%is_carried(),              &
      message="Verify match is not carried (by item)")
    call test%assertfalse(inv%is_carried(O15_MATCH),                     &
      message="Verify match is not carried (by itemtracker)")
    call test%asserttrue(inv%item(O15_MATCH)%in_room(past_place),        &
      message="Verify match is in original room")
    call test%asserttrue(inv%item(O15_MATCH)%at_hand_in(past_place),     &
      message="Verify match is available (in room)")

    ! Consume it (via itemtracker)
    call s_match%item%consume()
    ! call inv%item(O15_MATCH)%consume()

    call test%asserttrue(inv%item(O15_MATCH)%place /= past_place,        &
      message="Verify match is not in room")
    call test%assertfalse(inv%item(O15_MATCH)%is_carried(),              &
      message="Verify match is not carried (by item)")
    call test%assertfalse(inv%is_carried(O15_MATCH),                     &
      message="Verify match is not carried (by itemtracker)")
    call test%assertfalse(inv%item(O15_MATCH)%in_room(past_place),       &
      message="Verify match is not in original room")
    call test%assertfalse(inv%item(O15_MATCH)%at_hand_in(past_place),    &
      message="Verify match is not available)")

    ! Reset items
    call inv%init()

    call test%asserttrue(associated(s_match%item, pi_match),             &
      message="Verify match is associated with an match item")
    call test%assertequal(s_match%item%place, inv%item(O15_MATCH)%place, &
      message="Verify match and item(match) in same place")
    call test%assertequal(s_match%id, O15_MATCH,                         &
      message="Verify match has proper ID")
    call test%asserttrue(s_match%item%is_carried()                       &
      .eqv. inv%item(O15_MATCH)%is_carried(),                            &
      message="Verify match and item(match) have same carried status")

    ! Continuous burn
    call s_match%item%carry()
    inv_ct = 1

    call s_match%turn_on()
    do turn = 1, 15
      ! Complex burn
      call s_match%burn_one_turn(inv_ct)

      ! Check for burn out
      if (turn < 10) then
        call test%asserttrue(s_match%item%is_carried(),                 &
          message="Turns 1-9, matches are in inventory (1)")
        call test%assertequal(inv_ct, 1,                                &
          message="Turns 1-9, one item in inventory (1)")
      else
        call test%asserttrue(inv%item(O15_MATCH)%place == 0,            &
          message="Turns 10+, matches are consumed (1)")
        call test%assertequal(inv_ct, 0,                                &
          message="Turns 10+, empty inventory (1)")
      end if
    end do

    ! Continuous burn with drop
    call inv%init()
    call s_match%init(inv%item(O15_MATCH))

    call s_match%item%carry()
    inv_ct = 1

    call s_match%turn_on()
    call s_match%item%place_in(p_library)
    do turn = 1, 13
      ! Complex burn
      call s_match%burn_one_turn(inv_ct)

      ! Check for burn out
      if (turn < 10) then
        call test%asserttrue(s_match%is_lit(),                          &
          message="Turns 1-9, match is lit (2)")
        call test%assertfalse(s_match%is_carried(),                     &
          message="Turns 1-9, matches are in inventory (2)")
        call test%assertequal(inv_ct, 1,                                &
          message="Turns 1-9, one item in inventory (2)")
      else
        call test%asserttrue(s_match%is_empty(),                        &
          message="Turns 10+, matchbook is empty (2)")
        call test%asserttrue(inv%item(O15_MATCH)%place == 0,            &
          message="Turns 10+, matches are consumed (2)")
        call test%assertequal(inv_ct, 1,                                &
          message="Turns 10+, one item in inventory (2)")
      end if
    end do

    ! Continuous burn with drop
    call inv%init()
    call s_match%init(inv%item(O15_MATCH))

    call s_match%item%carry()
    inv_ct = 1

    call s_match%turn_on()
    do turn = 1, 18
      ! Simple burn
      call s_match%burn_one_turn()

      if (turn == 4) then
        call s_match%item%place_in(1)
      end if

      ! Check for burn out
      if (turn < 10) then
        if (turn < 4) then
          call test%asserttrue(s_match%is_carried(),                    &
            message="Turns 1-3, matches are in inventory (3)")
        else
          call test%asserttrue(s_match%item%place == 1,                 &
            message="Turns 4-9, matches are in bedroom (3)")
        end if
        call test%asserttrue(s_match%is_lit(),                          &
          message="Turns 1-9, match is lit (3)")
        call test%assertequal(inv_ct, 1,                                &
          message="Turns 1-9, one item in inventory (3)")
      else
        call test%asserttrue(s_match%is_empty(),                        &
          message="Turns 10+, matchbook is empty (3)")
        call test%asserttrue(inv%item(O15_MATCH)%place == 0,            &
          message="Turns 10+, matches are consumed (3)")
        call test%assertequal(inv_ct, 1,                                &
          message="Turns 10+, one item in inventory (3)")
      end if
    end do

    ! call s_match%init(inv%item(O15_MATCH))
    ! inv_ct = 1

    ! call s_match%turn_on()
    ! do turn = 1, 30
    !   ! Complex burn, check if auto refill is possible (no )
    !   call s_match%burn_one_turn(inv%item(O1_KEROSENE), inv_ct)

    !   select case(turn)
    !   case(62) ! R=0, TL 62 -> 0
    !     call test%asserttrue(s_match%turns_lit == 62,                    &
    !        message="Before final refuel, match has burned 62 turns (62)")
    !     ! Manual refueling only adds 100 turns vs 175 for autofueling
    !     call inv%carry(O1_KEROSENE)
    !     call s_match%refuel()
    !     call inv%consume(O1_KEROSENE)
    !     ! Resets turns_lit to 0
    !   case(83) ! R+21, TL 21
    !     ! Turn off the match; should not burn any fuel even if
    !     ! burn_one_turn() is called
    !     call test%asserttrue(s_match%is_bright(),                        &
    !        message="Final refuel + 21, match is bright")
    !     call test%asserttrue(s_match%turns_lit == 21,                    &
    !        message="Match turned off 21 turns after refuel (83)")
    !     call s_match%turn_off()
    !   case(104) ! R+42, TL 21
    !     call test%asserttrue(s_match%turns_lit == 21,                    &
    !        message="Match still has only burned 21 turns after refuel (104)")
    !     call test%asserttrue(s_match%is_off(),                           &
    !       message="Final refuel + 21, match still off (104)")
    !     call test%asserttrue(s_match%is_dark(),                          &
    !       message="Final refuel + 21, match still dark (104)")
    !     call test%assertfalse(s_match%is_bright(),                       &
    !       message="Final refuel + 21, match is not bright (104)")
    !     call test%assertfalse(s_match%is_dim(),                          &
    !       message="Final refuel + 21, match is not dim (104)")
    !     ! Turn on the match
    !     call s_match%turn_on()
    !   case(136) ! R+74, TL 21 + 32 -> TL 53
    !     call test%asserttrue(s_match%turns_lit == 53,                    &
    !       message="After relight, match has burned 53 turns (136)")
    !     call test%asserttrue(s_match%is_bright(),                        &
    !       message="TL 75, match is bright (136)")
    !       ! Match is on, burning in the bedroom
    !     call s_match%item%place_in(1)
    !   case(148) ! R+86, TL 53 + 12 -> TL 65
    !     call test%asserttrue(s_match%turns_lit == 65,                    &
    !       message="After drop, match has burned 65 turns (148)")
    !     call test%asserttrue(s_match%is_bright(),                        &
    !       message="TL 75, match is bright (148)")
    !     ! Pick up match again
    !     call s_match%item%carry()
    !   case(158) ! R+96, TL 75
    !     call test%asserttrue(s_match%turns_lit == 75,                    &
    !       message="After pickup, match has burned 75 turns (158)")
    !     ! Match goes dim
    !     call test%assertfalse(s_match%is_bright(),                       &
    !       message="TL 75, match is not bright (158)")
    !     call test%asserttrue(s_match%is_dim(),                           &
    !       message="TL 75, match is dim (158)")
    !     call test%assertfalse(s_match%is_off(),                          &
    !       message="TL 75, match is not off (158)")
    !     call test%assertfalse(s_match%is_dark(),                         &
    !       message="TL 75, match is dark (158)")
    !     call test%assertfalse(s_match%is_empty(),                        &
    !       message="TL 75, match is empty (158)")
    !   case(183) ! R+121, TL 100
    !     call test%asserttrue(s_match%turns_lit == 100,                   &
    !       message="After dimming, match has burned 100 turns")
    !     ! Match goes out
    !     call test%assertfalse(s_match%is_bright(),                       &
    !       message="Dim + 25, match is not bright (183)")
    !     call test%assertfalse(s_match%is_dim(),                          &
    !       message="Dim + 25, match is not dim (183)")
    !     call test%assertfalse(s_match%is_off(),                          &
    !       message="Dim + 25, match is not off (183)")
    !     call test%asserttrue(s_match%is_dark(),                          &
    !       message="Dim + 25, match is dark (183)")
    !     call test%asserttrue(s_match%is_empty(),                         &
    !       message="Dim + 25, match is empty (183)")
    !   case default
    !   end select

    !   if (turn >= 1 .and. turn < 83) then
    !     call test%asserttrue(s_match%is_lit(),                           &
    !       message="Match is lit (1-82)")
    !   else if (turn >= 83 .and. turn < 104) then
    !     call test%asserttrue(s_match%is_dark(),                          &
    !       message="Match is dark (83-103)")
    !   else if (turn >= 104 .and. turn < 183) then
    !     call test%asserttrue(s_match%is_lit(),                           &
    !       message="Match is lit (1-82)")
    !   else if (turn >= 183) then
    !     call test%asserttrue(s_match%is_dark(),                          &
    !       message="Match is lit (183-210)")
    !   end if

    ! end do

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_match.json")

    call test%checkfailure()
end program ut_match
