!> @file ut_mirror_maze.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_where:mirror_maze_t

!! @brief Unit tests of m_where:mirror_maze_t
program ut_mirror_maze
  use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
  use :: toast
  use :: m_object_id
  use :: m_where

  implicit none

  integer, dimension(10), parameter :: normal_exits = (/                &
    R28_L_CORRIDOR, R15_BOUDOIR, R28_L_CORRIDOR, R9_UPSTAIRS_HALLWAY,   &
    R28_L_CORRIDOR, R15_BOUDOIR, R15_BOUDOIR, R9_UPSTAIRS_HALLWAY,      &
    R28_L_CORRIDOR, R15_BOUDOIR /)

  integer, dimension(10), parameter :: emergency_exits = R2_DIM_CORRIDOR

  integer, dimension(10) :: svec
  integer, dimension(10) :: lvec

  integer :: j
  integer :: room_id

  type(dungeon_t) :: m
  ! type(mirror_maze_t) :: maze
  type(TestCase) :: test

  continue



  call test%init(name="ut_mirror_maze")

  ! Check initial value of mirror exit array before initialization
  do j = 1, 10
    lvec(j) = j + 13
    call test%assertequal(m%mirror_maze%exits(j), 0,                             &
      message="Verify exits(j) is 0")
    call test%assertequal(m%mirror_maze%find_exit(), 0,                          &
      message="Verify exits(j) is 0")
  end do

  svec = m%mirror_maze%state_to_array()
  call test%assertfalse(all(normal_exits == svec),                      &
    message="Verify save vector does not match a normal exit vector")
  call test%assertfalse(all(emergency_exits == svec),                   &
    message="Verify save vector does not match "                        &
    // "an emergency exit vector")

  ! Initialize map state

  call m%mirror_maze%init()

  !!! Check initialized maze attributes are in expected range

  svec = m%mirror_maze%state_to_array()

  call test%asserttrue(all(normal_exits == svec),                       &
    message="Verify save vector matches a normal exit vector")
  call test%assertfalse(all(emergency_exits == svec),                   &
    message="Verify save vector does not match "                        &
    // "an emergency exit vector")

  do j = 1, 10
    call test%assertequal(m%mirror_maze%exits(j), normal_exits(j),               &
      message="Verify expected value of exits(j)")
    room_id = m%mirror_maze%find_exit()
    call test%asserttrue(any(normal_exits == room_id),                  &
      message="Verify random exit is a normal exit")
    call test%assertfalse(R2_DIM_CORRIDOR == room_id,                   &
      message="Verify random exit is not an emergency exit")
  end do

  call m%mirror_maze%open_emergency_exits()

  svec = m%mirror_maze%state_to_array()

  call test%assertfalse(all(normal_exits == svec),                      &
    message="Verify save vector does not match a normal exit vector (oee n=s)")
  call test%asserttrue(all(emergency_exits == svec),                    &
    message="Verify save vector matches an emergency exit vector")

  do j = 1, 10
    call test%assertequal(m%mirror_maze%exits(j), R2_DIM_CORRIDOR,               &
      message="Verify exits(j) is R2")
    room_id = m%mirror_maze%find_exit()
    call test%assertfalse(any(normal_exits == room_id),                 &
      message="Verify random exit is not a normal exit")
    call test%asserttrue(R2_DIM_CORRIDOR == room_id,                   &
      message="Verify random exit is an emergency exit")
  end do

  call m%mirror_maze%init()

  room_id = m%mirror_maze%find_exit(emergency=.false.)
  call test%asserttrue(any(normal_exits == room_id),                    &
    message="Verify random exit is a normal exit (E=F)")
  room_id = m%mirror_maze%find_exit()
  call test%asserttrue(any(normal_exits == room_id),                    &
    message="Verify random exit is a normal exit (f)")
  room_id = m%mirror_maze%find_exit(emergency=.true.)
  call test%assertequal(room_id, R2_DIM_CORRIDOR,                       &
    message="Verify random exit is an emergency exit (E=T)")
  room_id = m%mirror_maze%find_exit()
  call test%assertequal(room_id, R2_DIM_CORRIDOR,                       &
    message="Verify random exit is an emergency exit (t)")

  call m%mirror_maze%array_to_state(lvec)
  svec = m%mirror_maze%state_to_array()
  do j = 1, 10
    call test%assertequal(j + 13, svec(j),                              &
    message="Verify save vector matches expected values (N)")
  end do

  call m%mirror_maze%open_emergency_exits()
  svec = m%mirror_maze%state_to_array()

  do j = 1, 10
    call test%assertequal(emergency_exits(j), svec(j),                  &
      message="Verify save vector matches expected values (E)")
    call test%assertequal(R2_DIM_CORRIDOR, svec(j),                     &
      message="Verify save vector matches expected values (R2)")
  end do

  ! Print summary at the end
  call printsummary(test)
  call jsonwritetofile(test, "ut_mirror_maze.json")

  call test%checkfailure()
end program ut_mirror_maze
