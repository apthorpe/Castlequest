!> @file ut_note.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_items:note_t

!! @brief Unit tests of m_items:note_t
program ut_note
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_items
    use :: m_object_id
    use :: m_stateful
    implicit none

    type(note_t) :: s_note
    type(stateful_t) :: bare
    type(TestCase) :: test

    continue

    call test%init(name="ut_note")

    ! Check values before initialization
    ! Check name of note before initialization
    call test%assertequal(s_note%name, bare%name,                       &
      message="Verify uninitialized note name matches stateful_t")
    ! Check id of note before initialization
    call test%assertequal(s_note%id, bare%id,                           &
      message="Verify uninitialized note id matches stateful_t")
    ! Check state of note before initialization
    call test%assertequal(s_note%state, bare%state,                     &
      message="Verify uninitialized note state matches stateful_t")

    !!! Check initialized item attributes are in expected range

    ! Initialize note state
    call s_note%init()

    ! Check values after initialization
    ! Check name of note after initialization
    call test%asserttrue(s_note%name == 'note        ',                 &
      message="Verify initialized note name is as expected")
    ! Check id of note after initialization
    call test%assertequal(s_note%id, N52_NOTE,                                &
      message="Verify initialized note ID is 52 (OBJECT = 52)")

    ! Check state of note after initialization
    call test%assertequal(s_note%state, 0,                              &
      message="Verify initialized(b) note state")
    ! Check state of note after initialization
    call test%asserttrue(s_note%is_blank(),                             &
      message="Verify initialized(b) note is blank")
    call test%assertfalse(s_note%about_hunchback(),                     &
      message="Verify initialized(b) note is not about hunchback")
    call test%assertfalse(s_note%about_book(),                          &
      message="Verify initialized(b) note is not about book")

    ! Check state of note after write_about_hunchback()
    call s_note%write_about_hunchback()
    call test%assertequal(s_note%state, 1,                              &
      message="Verify set(h1) note state")
    call test%assertfalse(s_note%is_blank(),                            &
      message="Verify set(h1) note is not blank")
    call test%asserttrue(s_note%about_hunchback(),                      &
      message="Verify set(h1) note is about hunchback")
    call test%assertfalse(s_note%about_book(),                          &
      message="Verify set(h1) note is not about book")

    ! Check state of note after write_about_book()
    call s_note%write_about_book()
    call test%assertequal(s_note%state, 2,                              &
      message="Verify set(b1) note state")
    call test%assertfalse(s_note%is_blank(),                            &
      message="Verify set(h1) note is not blank")
    call test%assertfalse(s_note%about_hunchback(),                     &
      message="Verify set(b1) note is not about hunchback")
    call test%asserttrue(s_note%about_book(),                           &
      message="Verify set(b1) note is about book")

    ! Check state of note after write_about_book() - redundant
    call s_note%write_about_book()
    call test%assertequal(s_note%state, 2,                              &
      message="Verify set(b2) note state")
    call test%assertfalse(s_note%is_blank(),                            &
      message="Verify set(b2) note is not blank")
    call test%assertfalse(s_note%about_hunchback(),                     &
      message="Verify set(b2) note is not about hunchback")
    call test%asserttrue(s_note%about_book(),                           &
      message="Verify set(b2) note is about book")

    ! Check state of note after write_about_hunchback()
    call s_note%write_about_hunchback()
    call test%assertequal(s_note%state, 1,                              &
      message="Verify set(h2) note state")
    call test%assertfalse(s_note%is_blank(),                            &
      message="Verify set(h2) note is not blank")
    call test%asserttrue(s_note%about_hunchback(),                      &
      message="Verify set(h2) note is about hunchback")
    call test%assertfalse(s_note%about_book(),                          &
      message="Verify set(h2) note is not about book")

    ! Check state of note after write_about_hunchback() - redundant
    call s_note%write_about_hunchback()
    call test%assertequal(s_note%state, 1,                              &
      message="Verify set(h3) note state")
    call test%assertfalse(s_note%is_blank(),                            &
      message="Verify set(h3) note is not blank")
    call test%asserttrue(s_note%about_hunchback(),                      &
      message="Verify set(h3) note is about hunchback")
    call test%assertfalse(s_note%about_book(),                          &
      message="Verify set(h3) note is not about book")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_note.json")

    call test%checkfailure()
end program ut_note
