!> @file ut_null.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Null unit test to verify TOAST library is properly linked

!> @brief Null unit test to verify TOAST library is properly linked and
!! Castlequest modules can be imported.
program ut_null
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_gamedata
    use :: m_debug
    use :: m_items
    use :: m_encounter
    use :: m_random
    use :: m_statevector
    use :: m_where
    use :: m_words
    implicit none

    type(TestCase) :: test

    continue

    call test%init(name="ut_null")

    !!! Test exposed functions

    ! id = lua_version(c_null_ptr)
    ! write(unit=stdout, fmt='(A, I8)') "Lua version ", id
    call test%assertequal(NITEMS, 30,                                   &
      message="Proper number of carryable items found (30)")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_null.json")

    call test%checkfailure()
end program ut_null
