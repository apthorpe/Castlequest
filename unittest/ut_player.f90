!> @file ut_player.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_player:player_t

!! @brief Unit tests of m_player:player_t
program ut_player
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_object_id
    ! use :: m_gamedata, only: load_game_data
    ! use :: m_items, only: itemtracker_t
    use m_player, only: player_t
    implicit none

    ! logical :: read_error
    ! type(itemtracker_t) :: inv
    integer :: from
    integer :: to
    type(player_t) :: player
    type(TestCase) :: test

    continue

    call test%init(name="ut_player")

    ! call load_game_data(read_error)

    ! call inv%init()

    ! Pre-initialized player state
    call test%assertequal(player%room, R1_BEDROOM,                      &
      message="Player is in R1 (u)")
    call test%assertequal(player%last_room, R1_BEDROOM,                 &
      message="Player was in R1 (u)")
    call test%assertequal(player%ndeaths, 0,                            &
      message="Player has not died. Yet. (u)")
    call test%assertequal(player%moves, 0,                              &
      message="Player has not moved. (u)")
    call test%assertequal(player%items_held, 0,                         &
      message="Player holds nothing. (u)")
    call test%assertfalse(player%brief_mode,                            &
      message="Player has not enabled brief descriptions (u)")
    call test%assertequal(player%brief_to_int(), 0,                     &
      message="Disenabled brief mode = 0 (u)")

    ! Initialize player state
    call player%init()

    call test%assertequal(player%room, R1_BEDROOM,                      &
      message="Player is in R1 (i)")
    call test%assertequal(player%last_room, R1_BEDROOM,                 &
      message="Player was in R1 (i)")
    call test%assertequal(player%ndeaths, 0,                            &
      message="Player has not died. Yet. (i)")
    call test%assertequal(player%moves, 0,                              &
      message="Player has not moved. (i)")
    call test%assertequal(player%items_held, 0,                         &
      message="Player holds nothing. (i)")
    call test%assertfalse(player%brief_mode,                            &
      message="Player has not enabled brief descriptions (i)")
    call test%assertequal(player%brief_to_int(), 0,                     &
      message="Disabled brief mode = 0 (i)")

    call player%set_brief()
    call test%asserttrue(player%brief_mode,                             &
      message="Player has enabled brief descriptions (m)")
    call test%assertequal(player%brief_to_int(), 1,                     &
      message="Enabled brief mode = 1 (m)")
    call player%clear_brief()
    call test%assertfalse(player%brief_mode,                            &
      message="Player has not enabled brief descriptions (m)")
    call test%assertequal(player%brief_to_int(), 0,                     &
      message="Disabled brief mode = 0 (m)")
    call player%clear_brief()
    call test%assertfalse(player%brief_mode,                            &
      message="Player has not enabled brief descriptions (2x)")
    call test%assertequal(player%brief_to_int(), 0,                     &
      message="Disabled brief mode = 0 (2x)")
    call player%set_brief()
    call test%asserttrue(player%brief_mode,                             &
      message="Player has enabled brief descriptions (m2)")
    call test%assertequal(player%brief_to_int(), 1,                     &
      message="Enabled brief mode = 1 (m2)")

    call player%set_brief_from_int(0)
    call test%assertfalse(player%brief_mode,                            &
      message="Player has disabled brief descriptions (0->s)")
    call player%set_brief_from_int(1)
    call test%asserttrue(player%brief_mode,                             &
      message="Player has enabled brief descriptions (1->s)")
    call player%set_brief_from_int(3)
    call test%asserttrue(player%brief_mode,                             &
      message="Player has enabled brief descriptions (3->s)")
    call player%set_brief_from_int(0)
    call test%assertfalse(player%brief_mode,                            &
      message="Player has disabled brief descriptions (0->s)")
    call player%set_brief_from_int(-2)
    call test%asserttrue(player%brief_mode,                             &
      message="Player has enabled brief descriptions (-2->s)")

    call player%gain_item()
    call test%assertequal(player%items_held, 1,                         &
      message="Player holds 1 thing. (m)")
    call player%gain_item(4)
    call test%assertequal(player%items_held, 5,                         &
      message="Player holds 5 things. (m)")
    call player%lose_item()
    call test%assertequal(player%items_held, 4,                         &
      message="Player holds 4 things. (m)")
    call player%lose_item(1)
    call test%assertequal(player%items_held, 3,                         &
      message="Player holds 3 things. (m)")
    call player%lose_item(3)
    call test%assertequal(player%items_held, 0,                         &
        message="Player holds 0 things. (m)")
    call player%gain_item(10)
    call test%assertequal(player%items_held, 10,                        &
      message="Player holds 10 things. (m)")
    call test%asserttrue(player%is_encumbered(),                        &
      message="Player is encumbered (10)")
    call player%lose_item()
    call test%assertequal(player%items_held, 9,                         &
      message="Player holds 9 things. (m)")
    call test%assertfalse(player%is_encumbered(),                       &
      message="Player is not encumbered (9)")
    call player%gain_item(2)
    call test%assertequal(player%items_held, 11,                        &
      message="Player holds 11 things; should not happen. (m)")
    call test%asserttrue(player%is_encumbered(),                        &
      message="Player is encumbered (11)")
    call player%lose_item(11)
    call test%assertequal(player%items_held, 0,                         &
      message="Player holds 0 things. (m)")
    call test%assertfalse(player%is_encumbered(),                       &
      message="Player is not encumbered (0)")

    call player%advance()
    call player%advance()
    call test%assertequal(player%moves, 2,                              &
      message="Player has taken 2 steps. (m)")
    call player%advance()
    call player%advance()
    call player%advance()
    call player%advance()
    call test%assertequal(player%moves, 6,                              &
      message="Player has taken 6 steps. (m)")

    ! Simulate first game moves
    call test%asserttrue(player%is_in(R1_BEDROOM),                      &
      message="Player is currently in R1. (adv)")
    call test%asserttrue(player%was_in(R1_BEDROOM),                     &
      message="Player was previously in R1. (adv)")
    call test%assertfalse(player%was_in(R6_KITCHEN),                    &
      message="Player was not previously in R6. (adv)")
    call test%assertfalse(player%was_in(R99_ELEVATOR_BETWEEN_FLOORS),   &
      message="Player is not currently in R99. (adv)")
    ! Starting location is R1_BEDROOM
    to = R2_DIM_CORRIDOR
    call player%move_to(to, with_advance=.true.)

    call test%asserttrue(player%is_in(R2_DIM_CORRIDOR),                 &
      message="Player is currently in R2. (adv)")
    call test%asserttrue(player%was_in(R1_BEDROOM),                     &
      message="Player was previously in R1. (adv)")
    call test%assertfalse(player%was_in(R6_KITCHEN),                    &
      message="Player was not previously in R6. (adv)")
    call test%assertfalse(player%is_in(R99_ELEVATOR_BETWEEN_FLOORS),    &
      message="Player is not currently in R99. (adv)")

    call test%assertequal(player%room, R2_DIM_CORRIDOR,                 &
      message="Player is in R2 (adv)")
    call test%assertequal(player%last_room, R1_BEDROOM,                 &
      message="Player was in R1 (adv)")
    call test%assertequal(player%moves, 7,                              &
      message="Player has taken 7 steps. (adv)")
    call test%asserttrue(                                               &
      player%route_is(from=R1_BEDROOM, to=R2_DIM_CORRIDOR),             &
      message="Player entered R2 from R1. (adv)")
    call test%assertfalse(                                              &
      player%route_is(from=R2_DIM_CORRIDOR, to=R1_BEDROOM),             &
      message="Player did not enter R1 from R2. (adv)")
    call test%assertfalse(                                              &
      player%route_is(from=R2_DIM_CORRIDOR, to=R1_BEDROOM,              &
      both_ways=.false.),                                               &
      message="Player specifically did not enter R1 from R2. (adv)")
    call test%asserttrue(                                               &
      player%route_is(from=R2_DIM_CORRIDOR, to=R1_BEDROOM,              &
      both_ways=.true.),                                                &
      message="Player moved between R1 and R2. (adv)")

    ! Specify both endpoints. Note that move_to() specifically does
    ! not check map%room(i)%connect_(dir) for legality of move. In the
    ! actual game there is no direct path between these rooms.
    from = R5_DINING_ROOM
    to = R22_CEDAR_CLOSET

    call player%move_to(to, from)

    call test%assertequal(player%moves, 7,                              &
      message="Player has taken 7 steps. (noadv1)")
    call test%asserttrue(player%is_in(R22_CEDAR_CLOSET),                &
      message="Player is currently in R22. (noadv1)")
    call test%asserttrue(player%was_in(R5_DINING_ROOM),                 &
      message="Player was previously in R5. (noadv1)")
    call test%assertfalse(player%was_in(R2_DIM_CORRIDOR),               &
      message="Player was not previously in R2. (noadv1)")
    call test%assertfalse(player%was_in(R1_BEDROOM),                    &
      message="Player was not previously in R1. (noadv1)")
    call test%assertfalse(player%is_in(R2_DIM_CORRIDOR),                &
      message="Player is not currently in R2. (noadv1)")
    call test%assertfalse(player%is_in(R5_DINING_ROOM),                 &
      message="Player is not currently in R5. (noadv1)")
    call test%assertequal(player%room, R22_CEDAR_CLOSET,                &
      message="Player is in R22 (noadv1)")
    call test%assertequal(player%last_room, R5_DINING_ROOM,             &
      message="Player was in R5 (noadv1)")

    call player%move_to(to, from, with_advance=.false.)

    call test%assertequal(player%moves, 7,                              &
      message="Player has taken 7 steps. (noadv2)")
    call test%asserttrue(player%is_in(R22_CEDAR_CLOSET),                &
      message="Player is currently in R22. (noadv2)")
    call test%asserttrue(player%was_in(R5_DINING_ROOM),                 &
      message="Player was previously in R5. (noadv2)")
    call test%assertfalse(player%was_in(R2_DIM_CORRIDOR),               &
      message="Player was not previously in R2. (noadv2)")
    call test%assertfalse(player%was_in(R1_BEDROOM),                    &
      message="Player was not previously in R1. (noadv2)")
    call test%assertfalse(player%is_in(R2_DIM_CORRIDOR),                &
      message="Player is not currently in R2. (noadv2)")
    call test%assertfalse(player%is_in(R5_DINING_ROOM),                 &
      message="Player is not currently in R5. (noadv2)")
    call test%assertequal(player%room, R22_CEDAR_CLOSET,                &
      message="Player is in R22 (noadv2)")
    call test%assertequal(player%last_room, R5_DINING_ROOM,             &
      message="Player was in R5 (noadv2)")

    call player%move_to(to, from, with_advance=.true.)

    call test%asserttrue(player%is_in(R22_CEDAR_CLOSET),                &
      message="Player is currently in R22. (adv)")
    call test%asserttrue(player%was_in(R5_DINING_ROOM),                 &
      message="Player was previously in R5. (adv)")
    call test%assertfalse(player%was_in(R2_DIM_CORRIDOR),               &
      message="Player was not previously in R2. (adv)")
    call test%assertfalse(player%was_in(R1_BEDROOM),                    &
      message="Player was not previously in R1. (adv)")
    call test%assertfalse(player%is_in(R2_DIM_CORRIDOR),                &
      message="Player is not currently in R2. (adv)")
    call test%assertfalse(player%is_in(R5_DINING_ROOM),                 &
      message="Player is not currently in R5. (adv)")

    call test%assertequal(player%room, R22_CEDAR_CLOSET,                &
      message="Player is in R22 (adv)")
    call test%assertequal(player%last_room, R5_DINING_ROOM,             &
      message="Player was in R5 (adv)")
    call test%assertequal(player%moves, 8,                              &
      message="Player has taken 8 steps. (adv)")
    call test%asserttrue(                                               &
      player%route_is(from=R5_DINING_ROOM, to=R22_CEDAR_CLOSET),        &
      message="Player entered R22 from R5. (adv)")
    call test%assertfalse(                                              &
      player%route_is(from=R22_CEDAR_CLOSET, to=R5_DINING_ROOM),        &
      message="Player did not enter R5 from R22. (adv)")
    call test%assertfalse(                                              &
      player%route_is(from=R22_CEDAR_CLOSET, to=R5_DINING_ROOM,         &
      both_ways=.false.),                                               &
      message="Player specifically did not enter R5 from R22. (adv)")
    call test%asserttrue(                                               &
      player%route_is(from=R22_CEDAR_CLOSET, to=R5_DINING_ROOM,         &
      both_ways=.true.),                                                &
      message="Player moved between R5 and R22. (adv)")

    ! rollback = R5_DINING_ROOM
    ! from     = R22_CEDAR_CLOSET
    to       = R21_ATTIC

    call player%move_to(to, save_last=.true.)

    call test%assertequal(player%room, R21_ATTIC,                       &
      message="Player was in R21 (rev1)")
    call test%assertequal(player%last_room, R22_CEDAR_CLOSET,           &
      message="Player was in R22 (rev1)")
    call test%assertequal(player%rollback_, R5_DINING_ROOM,             &
      message="Rollback cache is R5 (rev1)")

    call player%rollback_move()

    call test%assertequal(player%room, R22_CEDAR_CLOSET,                &
      message="Player is in R22 (rev2)")
    call test%assertequal(player%last_room, R5_DINING_ROOM,             &
      message="Player was in R5 (rev2)")
    call test%assertequal(player%rollback_, R5_DINING_ROOM,             &
      message="Rollback cache is R5 (rev1)")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_player.json")

    call test%checkfailure()
end program ut_player
