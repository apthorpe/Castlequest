!> @file ut_resolution.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests for m_event:resolution_t class

!> @brief Unit tests for m_event:resolution_t class
program ut_resolution
  use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
  use :: toast
  use :: cquest_game, only: resolution_t
  implicit none

  type(resolution_t) :: res
  type(TestCase) :: test
  continue

  call test%init(name="ut_resolution")

  ! Test pre-init behavior
  call test%assertfalse(res%to_next_action,                             &
    message="F: Process next action (u)")
  call test%assertfalse(res%reprocess_action,                           &
    message="F: Reevaluate current action (u)")
  call test%assertfalse(res%to_next_area,                               &
    message="F: Reevaluate current action (u)")
  call test%assertfalse(res%to_next_life,                               &
    message="F: Player is temporarily dead (u)")
  call test%assertfalse(res%permadeath,                                 &
    message="F: Player is irrevocably dead (u)")
  call test%assertfalse(res%end_game,                                   &
    message="F: Game is over (u)")

  ! Change internal state
  res%to_next_area     = .true.
  res%to_next_action   = .true.
  res%reprocess_action = .true.
  res%to_next_life     = .true.
  res%permadeath       = .true.
  res%end_game         = .true.

  ! Initialize object
  call res%init()

  ! Verify initialized state
  call test%assertfalse(res%to_next_action,                             &
    message="F: Process next action (i)")
  call test%assertfalse(res%reprocess_action,                           &
    message="F: Reevaluate current action (i)")
  call test%assertfalse(res%to_next_area,                               &
    message="F: Reevaluate current action (i)")
  call test%assertfalse(res%to_next_life,                               &
    message="F: Player is temporarily dead (i)")
  call test%assertfalse(res%permadeath,                                 &
    message="F: Player is irrevocably dead (i)")
  call test%assertfalse(res%end_game,                                   &
    message="F: Game is over (i)")

  ! Change internal state
  res%to_next_area     = .true.
  res%to_next_action   = .true.
  res%reprocess_action = .true.
  res%to_next_life     = .true.
  res%permadeath       = .true.
  res%end_game         = .true.

  ! Verify persistence of new internal state
  call test%asserttrue(res%to_next_action,                              &
    message="T: Process next action (m)")
  call test%asserttrue(res%reprocess_action,                            &
    message="T: Reevaluate current action (m)")
  call test%asserttrue(res%to_next_area,                                &
    message="T: Reevaluate current action (m)")
  call test%asserttrue(res%to_next_life,                                &
    message="T: Player is temporarily dead (m)")
  call test%asserttrue(res%permadeath,                                  &
    message="T: Player is irrevocably dead (m)")
  call test%asserttrue(res%end_game,                                    &
    message="T: Game is over (m)")

  ! Reset object
  call res%reset()

  ! Verify reset state
  call test%assertfalse(res%to_next_action,                             &
    message="F: Process next action (r)")
  call test%assertfalse(res%reprocess_action,                           &
    message="F: Reevaluate current action (r)")
  call test%assertfalse(res%to_next_area,                               &
    message="F: Reevaluate current action (r)")
  call test%assertfalse(res%to_next_life,                               &
    message="F: Player is temporarily dead (r)")
  call test%assertfalse(res%permadeath,                                 &
    message="F: Player is irrevocably dead (r)")
  call test%assertfalse(res%end_game,                                   &
    message="F: Game is over (r)")

  ! Print summary at the end
  call printsummary(test)
  call jsonwritetofile(test, "ut_resolution.json")

  call test%checkfailure()
end program ut_resolution