!> @file ut_rope.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_items:rope_t

!! @brief Unit tests of m_items:rope_t
program ut_rope
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_gamedata
    use :: m_items
    use :: m_object_id
    use :: m_stateful
    implicit none

    integer, parameter :: p_dark_room = 19
    integer, parameter :: p_carried = -1

    integer :: past_place

    logical :: read_error
    type(itemtracker_t), target :: inv
    type(item_t), pointer :: pi_rope
    type(stateful_t) :: bare
    type(rope_t) :: s_rope
    type(TestCase) :: test

    continue

    call test%init(name="ut_rope")

    ! Check initial value of item pointer before initialization
    call test%assertfalse(associated(s_rope%item),                      &
      message="Verify item pointer of uninitialized rope is not associated")
    ! Check name of rope before initialization
    call test%assertequal(s_rope%name, bare%name,                       &
      message="Verify uninitialized rope name matches stateful_t")
    ! Check id of rope before initialization
    call test%assertequal(s_rope%id, bare%id,                           &
      message="Verify uninitialized rope id matches stateful_t")

    !!! Initialize itemtracker

    call inv%init()

    call load_game_data(read_error)
    call test%assertfalse(read_error,                                   &
      message="Read all game data")

    !!! Check initialized item attributes are in expected range

    ! Initialize rope state

    pi_rope => inv%item(O9_ROPE)
    call s_rope%init(inv%item(O9_ROPE))

    ! Check initial value of item pointer after initialization
    call test%asserttrue(associated(s_rope%item),                       &
      message="Verify item pointer of initialized rope is associated")
    ! Check name of rope after initialization
    call test%asserttrue(associated(s_rope%item, pi_rope),              &
      message="Verify initialized rope item ropees target")
    ! Check id of rope after initialization
    call test%assertequal(s_rope%id, O9_ROPE,                          &
      message="Verify initialized rope id ropees O9_ROPE")

    call test%assertequal(s_rope%item%place, p_dark_room,               &
      message="Verify rope place is reset by init")

    call test%asserttrue(s_rope%is_loose(),                             &
      message="Verify rope is loose (i)")
    call test%assertfalse(s_rope%tied_to_bed(),                         &
      message="Verify rope is not tied to bed (i)")
    call test%assertfalse(s_rope%is_hanging(),                          &
      message="Verify rope is not hanging (i)")
    call test%assertfalse(s_rope%tied_to_hook(),                        &
      message="Verify rope is not tied to hook (i)")
    call test%assertfalse(s_rope%is_gone(),                     &
      message="Verify rope is not gone forever (i)")


    past_place = s_rope%item%place

    ! Carry/carried check

    ! Rope in room
    call test%assertequal(s_rope%item%place, past_place,                &
      message="Verify rope is in room")
    call test%assertfalse(s_rope%item%is_carried(),                     &
      message="Verify rope is not carried (by item)")
    call test%assertfalse(inv%is_carried(O9_ROPE),                     &
      message="Verify rope is not carried (by itemtracker)")
    call test%asserttrue(s_rope%item%in_room(past_place),               &
      message="Verify rope place is in original room")
    call test%asserttrue(s_rope%item%at_hand_in(past_place),            &
      message="Verify rope place is available (in room)")

    ! Pick up rope
    call inv%carry(O9_ROPE)
    call test%assertequal(s_rope%item%place, p_carried,                 &
      message="Verify rope place is -1 (carried, itemlist)")
    call test%asserttrue(s_rope%item%is_carried(),                      &
      message="Verify rope is carried (by item)")
    call test%asserttrue(inv%is_carried(O9_ROPE),                      &
      message="Verify rope is carried (by itemtracker)")
    call test%assertfalse(s_rope%item%in_room(past_place),              &
      message="Verify rope is not in original room")
    call test%asserttrue(s_rope%item%at_hand_in(past_place),            &
      message="Verify rope is available (via inventory)")

    ! Put it back where you found it
    call s_rope%item%place_in(past_place)
    call test%assertequal(s_rope%item%place, past_place,                &
      message="Verify rope is in room")
    call test%assertfalse(s_rope%item%is_carried(),                     &
      message="Verify rope is not carried (by item)")
    call test%assertfalse(inv%is_carried(O9_ROPE),                     &
      message="Verify rope is not carried (by itemtracker)")
    call test%asserttrue(s_rope%item%in_room(past_place),               &
      message="Verify rope is in original room")
    call test%asserttrue(s_rope%item%at_hand_in(past_place),            &
      message="Verify rope is available (in room)")

    call s_rope%untie()
    call test%asserttrue(s_rope%is_loose(),                             &
      message="Verify rope is loose (0)")
    call test%assertfalse(s_rope%tied_to_bed(),                         &
      message="Verify rope is not tied to bed (0)")
    call test%assertfalse(s_rope%is_hanging(),                          &
      message="Verify rope is not hanging (0)")
    call test%assertfalse(s_rope%tied_to_hook(),                        &
      message="Verify rope is not tied to hook (0)")
    call test%assertfalse(s_rope%is_gone(),                             &
      message="Verify rope is not gone forever (0)")

    call s_rope%tie_to_bed()
    call test%assertfalse(s_rope%is_loose(),                            &
      message="Verify rope is not loose (1)")
    call test%asserttrue(s_rope%tied_to_bed(),                          &
      message="Verify rope is tied to bed (1)")
    call test%assertfalse(s_rope%is_hanging(),                          &
      message="Verify rope is not hanging (1)")
    call test%assertfalse(s_rope%tied_to_hook(),                        &
      message="Verify rope is not tied to hook (1)")
    call test%assertfalse(s_rope%is_gone(),                             &
      message="Verify rope is not gone forever (1)")

    call s_rope%hang()
    call test%assertfalse(s_rope%is_loose(),                            &
      message="Verify rope is not loose (2)")
    call test%assertfalse(s_rope%tied_to_bed(),                         &
      message="Verify rope is not tied to bed (2)")
    call test%asserttrue(s_rope%is_hanging(),                           &
      message="Verify rope is hanging (2)")
    call test%assertfalse(s_rope%tied_to_hook(),                        &
      message="Verify rope is not tied to hook (2)")
    call test%assertfalse(s_rope%is_gone(),                             &
      message="Verify rope is not gone forever (2)")

    call s_rope%tie_to_hook()
    call test%assertfalse(s_rope%is_loose(),                            &
      message="Verify rope is not loose (3)")
    call test%assertfalse(s_rope%tied_to_bed(),                         &
      message="Verify rope is not tied to bed (3)")
    call test%assertfalse(s_rope%is_hanging(),                          &
      message="Verify rope is not hanging (3)")
    call test%asserttrue(s_rope%tied_to_hook(),                         &
      message="Verify rope is tied to hook (3)")
    call test%assertfalse(s_rope%is_gone(),                             &
      message="Verify rope is not gone forever (3)")

    call s_rope%lose_forever()
    call test%assertfalse(s_rope%is_loose(),                            &
      message="Verify rope is not loose (-2)")
    call test%assertfalse(s_rope%tied_to_bed(),                         &
      message="Verify rope is not tied to bed (-2)")
    call test%assertfalse(s_rope%is_hanging(),                          &
      message="Verify rope is not hanging (-2)")
    call test%assertfalse(s_rope%tied_to_hook(),                        &
      message="Verify rope is not tied to hook (-2)")
    call test%asserttrue(s_rope%is_gone(),                              &
      message="Verify rope is gone forever (-2)")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_rope.json")

    call test%checkfailure()
end program ut_rope
