!> @file ut_score.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_score:score_t

!! @brief Unit tests of m_score:score_t
program ut_score
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_format, only: a
    use :: m_gamedata, only: load_game_data
    use :: m_items, only: itemtracker_t
    use :: m_score, only: score_t
    use :: m_stateful
    implicit none

    integer :: move
    integer :: move_and_item_score
    logical :: read_error
    type(itemtracker_t) :: inv
    type(score_t) :: score
    type(TestCase) :: test

    continue

    call test%init(name="ut_score")

    ! Check initial value of item pointer before initialization
    ! call test%assertequal(score%state, -1,                              &
    !   message="Verify state of uninitialized score is -1")
    ! ! Check name of score before initialization
    ! call test%assertequal(score%name, bare%name,                        &
    !   message="Verify uninitialized score name matches stateful_t")
    ! ! Check id of score before initialization
    ! call test%assertequal(score%id, bare%id,                           &
    !   message="Verify uninitialized score id matches stateful_t")

    !!! Check initialized item attributes are in expected range

    call load_game_data(read_error)

    call inv%init()
    call a%init()

    move = 210
    move_and_item_score = inv%total_score(move)

    ! Initialize score state
    call score%init()

    call test%assertequal(score%MMAX, 99,                               &
      message="MMAX is 99 (i)")
    call score%reduce_mmax(4)
    call test%assertequal(score%MMAX, 95,                               &
      message="MMAX is 95 (m)")

    call test%assertequal(score%SCORE, 0,                               &
      message="Score is 0 (i)")
    call test%assertequal(score%ROPVAL, 10,                             &
      message="ROPVAL is 10 (i)")
    call test%assertequal(score%BUTVAL, 5,                              &
      message="BUTVAL is 5 (i)")
    call test%assertequal(score%NOTVAL, 15,                             &
      message="NOTVAL is 15 (i)")
    call test%assertequal(score%LOKVAL, 5,                              &
      message="LOKVAL is 5 (i)")

    call score%apply_combination_score()
    call test%assertequal(score%SCORE, 15,                              &
      message="Combination score is +15 = 15 (i)")
    call score%apply_rope_score()
    call test%assertequal(score%SCORE, 25,                              &
      message="Rope score is +10 = 25 (i)")
    call score%apply_unlock_score()
    call test%assertequal(score%SCORE, 30,                              &
      message="Unlock score is +5 = 30 (i)")
    call score%apply_butler_score()
    call test%assertequal(score%SCORE, 35,                              &
      message="Butler score is +5 = 35 (i)")

    call score%apply_combination_score()
    call test%assertequal(score%SCORE, 35,                              &
      message="Repeat combination score is +0 = 35 (i)")

    call test%assertequal(score%ROPVAL, 0,                              &
      message="ROPVAL is 0 (applied)")
    call test%assertequal(score%BUTVAL, 0,                              &
      message="BUTVAL is 0 (applied)")
    call test%assertequal(score%NOTVAL, 0,                              &
      message="NOTVAL is 0 (applied)")
    call test%assertequal(score%LOKVAL, 0,                              &
      message="LOKVAL is 0 (applied)")

    call score%add_to_score(20)
    call test%assertequal(score%SCORE, 55,                              &
      message="Manual score adjust is +20 = 55 (m)")

    call score%add_to_score(-5)
    call test%assertequal(score%SCORE, 50,                              &
      message="Manual score adjust is -5 = 50 (m)")

    ! Demonstrate display_rank with and without ASA formatting
    write(unit=stdout, fmt='(A)') 'Display rank (3x, 2 ASA, 1 plain)'
    call score%display_rank(move_and_item_score)
    call a%init(asa_on=.true.)
    call score%display_rank(move_and_item_score)
    call a%init(asa_on=.false.)
    call score%display_rank(move_and_item_score)

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_score.json")

    call test%checkfailure()
end program ut_score
