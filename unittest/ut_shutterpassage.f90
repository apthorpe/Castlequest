!> @file ut_shutterpassage.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_items:shutterpassage_t

!! @brief Unit tests of m_items:shutterpassage_t
program ut_shutterpassage
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_items
    use :: m_stateful
    implicit none

    type(stateful_t) :: bare
    type(shutterpassage_t) :: s_shutter
    type(TestCase) :: test

    continue

    call test%init(name="ut_shutterpassage")

    ! Check initial value of item pointer before initialization
    call test%assertequal(s_shutter%state, -1,                          &
      message="Verify state of uninitialized shutter is -1")
    ! Check name of shutter before initialization
    call test%assertequal(s_shutter%name, bare%name,                    &
      message="Verify uninitialized shutter name matches stateful_t")
    ! Check id of shutter before initialization
    call test%assertequal(s_shutter%id, bare%id,                        &
      message="Verify uninitialized shutter id matches stateful_t")

    !!! Check initialized item attributes are in expected range

    ! Initialize shutter state

    call s_shutter%init('shutter         ')

    call test%assertequal(s_shutter%state, 0,                           &
      message="Verify shutter state is 0 (i)")
    call test%asserttrue(s_shutter%is_closed(),                         &
      message="Verify shutter is closed (i)")
    call test%assertfalse(s_shutter%is_open(),                          &
      message="Verify shutter is not open (i)")

    ! Shutterset
    call s_shutter%open()

    call test%assertequal(s_shutter%state, 1,                           &
      message="Verify shutter state is 1 (1)")
    call test%assertfalse(s_shutter%is_closed(),                        &
      message="Verify shutter is not closed (1)")
    call test%asserttrue(s_shutter%is_open(),                           &
      message="Verify shutter is open (1)")

    ! Shutterrise
    call s_shutter%close()

    call test%assertequal(s_shutter%state, 0,                           &
      message="Verify shutter state is 0 (0)")
    call test%asserttrue(s_shutter%is_closed(),                         &
      message="Verify shutter is closed (0)")
    call test%assertfalse(s_shutter%is_open(),                          &
      message="Verify shutter is not open (0)")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_shutterpassage.json")

    call test%checkfailure()
end program ut_shutterpassage
