!> @file ut_sun.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_items:sun_t

!! @brief Unit tests of m_items:sun_t
program ut_sun
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_items
    use :: m_stateful
    implicit none

    type(stateful_t) :: bare
    type(sun_t) :: s_sun
    type(TestCase) :: test

    continue

    call test%init(name="ut_sun")

    ! Check initial value of item pointer before initialization
    call test%assertequal(s_sun%state, -1,                              &
      message="Verify state of uninitialized sun is -1")
    ! Check name of sun before initialization
    call test%assertequal(s_sun%name, bare%name,                        &
      message="Verify uninitialized sun name matches stateful_t")
    ! Check id of sun before initialization
    call test%assertequal(s_sun%id, bare%id,                           &
      message="Verify uninitialized sun id matches stateful_t")

    !!! Check initialized item attributes are in expected range

    ! Initialize sun state

    call s_sun%init()

    call test%assertequal(s_sun%name, 'sun             ',               &
      message="Verify sun name is set by init")

    call test%asserttrue(s_sun%is_up(),                                 &
      message="Verify sun is up (i)")
    call test%assertfalse(s_sun%is_down(),                              &
      message="Verify sun is not down (i)")

    ! Sunset
    call s_sun%set()

    call test%assertfalse(s_sun%is_up(),                                &
      message="Verify sun is not up (1)")
    call test%asserttrue(s_sun%is_down(),                               &
      message="Verify sun is down (1)")

    ! Sunrise
    call s_sun%rise()

    call test%asserttrue(s_sun%is_up(),                                 &
      message="Verify sun is up (1)")
    call test%assertfalse(s_sun%is_down(),                              &
      message="Verify sun is not down (1)")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_sun.json")

    call test%checkfailure()
end program ut_sun
