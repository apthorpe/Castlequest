!> @file ut_torch.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_items:torch_t

!! @brief Unit tests of m_items:torch_t
program ut_torch
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_gamedata
    use :: m_items
    use :: m_object_id
    use :: m_stateful
    implicit none

    integer, parameter :: p_slab = 36
    integer, parameter :: p_carried = -1

    ! integer :: id
    ! integer :: turn
    ! integer :: inv_ct
    integer :: past_place

    logical :: read_error
    type(itemtracker_t), target :: inv
    type(item_t), pointer :: pi_torch
    type(stateful_t) :: bare
    type(torch_t) :: s_torch
    type(TestCase) :: test

    continue

    call test%init(name="ut_torch")

    ! Check initial value of item pointer before initialization
    call test%assertfalse(associated(s_torch%item),                      &
      message="Verify item pointer of uninitialized torch is not associated")
    ! Check name of torch before initialization
    call test%assertequal(s_torch%name, bare%name,                       &
      message="Verify uninitialized torch name matches stateful_t")
    ! Check id of torch before initialization
    call test%assertequal(s_torch%id, bare%id,                           &
      message="Verify uninitialized torch id matches stateful_t")

    !!! Initialize itemtracker

    call inv%init()

    call load_game_data(read_error)
    call test%assertfalse(read_error,                                   &
      message="Read all game data")

    !!! Check initialized item attributes are in expected range

    ! Initialize torch state

    pi_torch => inv%item(O13_TORCH)
    call s_torch%init(inv%item(O13_TORCH))

    ! Check initial value of item pointer after initialization
    call test%asserttrue(associated(s_torch%item),                        &
      message="Verify item pointer of initialized torch is associated")
    ! Check name of torch after initialization
    call test%asserttrue(associated(s_torch%item, pi_torch),                &
      message="Verify initialized torch item torches target")
    ! Check id of torch after initialization
    call test%assertequal(s_torch%id, O13_TORCH,                            &
      message="Verify initialized torch id torches O13_TORCH")

    call test%assertequal(s_torch%item%place, p_slab,                   &
      message="Verify torch place is reset by init")

    past_place = s_torch%item%place

    ! Carry/carried check

    ! Torch in room
    call test%assertequal(s_torch%item%place, past_place,                 &
      message="Verify torch is in room")
    call test%assertfalse(s_torch%item%is_carried(),                      &
      message="Verify torch is not carried (by item)")
    call test%assertfalse(inv%is_carried(O13_TORCH),                      &
      message="Verify torch is not carried (by itemtracker)")
    call test%asserttrue(s_torch%item%in_room(past_place),                &
      message="Verify torch place is in original room")
    call test%asserttrue(s_torch%item%at_hand_in(past_place),             &
      message="Verify torch place is available (in room)")

    ! Pick up torch
    call inv%carry(O13_TORCH)
    call test%assertequal(s_torch%item%place, p_carried,                  &
      message="Verify torch place is -1 (carried, itemlist)")
    call test%asserttrue(s_torch%item%is_carried(),                       &
      message="Verify torch is carried (by item)")
    call test%asserttrue(inv%is_carried(O13_TORCH),                       &
      message="Verify torch is carried (by itemtracker)")
    call test%assertfalse(s_torch%item%in_room(past_place),               &
      message="Verify torch is not in original room")
    call test%asserttrue(s_torch%item%at_hand_in(past_place),             &
      message="Verify torch is available (via inventory)")

    ! Put it back where you found it
    call s_torch%item%place_in(past_place)
    call test%assertequal(s_torch%item%place, past_place,                 &
      message="Verify torch is in room")
    call test%assertfalse(s_torch%item%is_carried(),                      &
      message="Verify torch is not carried (by item)")
    call test%assertfalse(inv%is_carried(O13_TORCH),                      &
      message="Verify torch is not carried (by itemtracker)")
    call test%asserttrue(s_torch%item%in_room(past_place),                &
      message="Verify torch is in original room")
    call test%asserttrue(s_torch%item%at_hand_in(past_place),             &
      message="Verify torch is available (in room)")

    ! Carry the torch
    call s_torch%item%carry()
    ! Turn the torch on
    call s_torch%turn_on()

    call test%asserttrue(s_torch%item%place /= past_place,                &
      message="Verify torch is not in room")
    call test%asserttrue(s_torch%item%is_carried(),                       &
      message="Verify torch is carried (by item)")
    call test%asserttrue(inv%is_carried(O13_TORCH),                       &
      message="Verify torch is carried (by itemtracker)")
    call test%assertfalse(s_torch%item%in_room(past_place),               &
      message="Verify torch is not in original room")
    call test%asserttrue(s_torch%item%at_hand_in(past_place),             &
      message="Verify torch is available)")
    call test%asserttrue(s_torch%is_lit(),                                &
      message="Verify torch is lit)")
    call test%assertfalse(s_torch%is_off(),                               &
      message="Verify torch is not off)")

    ! Turn off the torch
    call s_torch%turn_off()

    call test%asserttrue(s_torch%item%place /= past_place,                &
      message="Verify torch is not in room")
    call test%asserttrue(s_torch%item%is_carried(),                       &
      message="Verify torch is carried (by item)")
    call test%asserttrue(inv%is_carried(O13_TORCH),                       &
      message="Verify torch is carried (by itemtracker)")
    call test%assertfalse(s_torch%item%in_room(past_place),               &
      message="Verify torch is not in original room")
    call test%asserttrue(s_torch%item%at_hand_in(past_place),             &
      message="Verify torch is available)")
    call test%assertfalse(s_torch%is_lit(),                                &
      message="Verify torch is not lit)")
    call test%asserttrue(s_torch%is_off(),                                &
      message="Verify torch is off)")

    ! Reset items
    call inv%init()

    call test%asserttrue(associated(s_torch%item, pi_torch),                &
      message="Verify torch is associated with an torch item")
    call test%assertequal(s_torch%item%place, inv%item(O13_TORCH)%place,    &
      message="Verify torch and item(torch) in same place")
    call test%assertequal(s_torch%id, O13_TORCH,                            &
      message="Verify torch has proper ID")
    call test%asserttrue(s_torch%item%is_carried()                        &
      .eqv. inv%item(O13_TORCH)%is_carried(),                             &
      message="Verify torch and item(torch) have same carried status")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_torch.json")

    call test%checkfailure()
end program ut_torch
