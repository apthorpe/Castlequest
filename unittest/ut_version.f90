!> @file ut_version.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of cquest_version

!! @brief Unit tests of cquest_version
program ut_version
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: cquest_version
    implicit none

    type(TestCase) :: test

    continue

    call test%init(name="ut_version")

    call test%asserttrue(CODENAME == 'Castlequest',                     &
      message="Verify code name")

    call test%asserttrue(len_trim(VERSION) >= 7,                        &
      message="Verify code version meets minimum length requirement")

    call test%asserttrue(len_trim(BUILDDATE) > 10,                      &
      message="Verify build date meets minimum length requirement")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_version.json")

    call test%checkfailure()
end program ut_version
