!> @file ut_window.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests of m_items:window_t

!! @brief Unit tests of m_items:window_t
program ut_window
    use, intrinsic :: iso_fortran_env, only: stdout=>OUTPUT_UNIT
    use :: toast
    use :: m_gamedata
    use :: m_items
    use :: m_object_id
    use :: m_stateful
    implicit none

    ! integer, parameter :: p_carried = -1

    ! logical :: read_error
    type(stateful_t) :: bare
    type(window_t) :: s_window
    type(TestCase) :: test

    continue

    call test%init(name="ut_window")

    ! Check initial value of item pointer before initialization
    call test%assertfalse(s_window%has_bars,                            &
      message="Verify uninitialized window has_bars is false")
    ! ! Check name of window before initialization
    call test%assertequal(s_window%name, bare%name,                     &
      message="Verify uninitialized window name matches stateful_t")
    ! Check id of window before initialization
    call test%assertequal(s_window%id, bare%id,                         &
      message="Verify uninitialized window id matches stateful_t")

    !!! Check initialized item attributes are in expected range

    ! Initialize window state

    call s_window%init('window 1        ', .true.)

    call test%asserttrue(s_window%has_bars,                             &
      message="Verify initialized window 1 has_bars is true")
    call test%asserttrue(s_window%name == 'window 1        ',           &
      message="Verify initialized window 1 name matches")

    call test%assertequal(s_window%des_index(), 405,                    &
      message="Verify initialized window 1 DES index is 405 (i)")
    call test%asserttrue(s_window%is_nailed(),                          &
      message="Verify initialized window 1 is nailed shut (i)")
    call test%assertfalse(s_window%is_broken(),                         &
      message="Verify initialized window 1 is not broken (i)")
    call test%assertfalse(s_window%is_barred(),                         &
      message="Verify initialized window 1 is not barred (i)")
    call test%assertfalse(s_window%is_open(),                           &
      message="Verify initialized window 1 is not open (i)")
    call test%asserttrue(s_window%is_blocked(),                         &
      message="Verify initialized window 1 is blocked (i)")

    call s_window%break()
    call test%assertequal(s_window%des_index(), 406,                    &
      message="Verify broken window 1 DES index is 406 (1)")
    call test%assertfalse(s_window%is_nailed(),                         &
      message="Verify broken window 1 is not nailed shut (1)")
    call test%asserttrue(s_window%is_broken(),                          &
      message="Verify broken window 1 is broken (1)")
    call test%assertfalse(s_window%is_barred(),                         &
      message="Verify broken window 1 is not barred (1)")
    call test%assertfalse(s_window%is_open(),                           &
      message="Verify broken window 1 is not open (1)")
    call test%asserttrue(s_window%is_blocked(),                         &
      message="Verify broken window 1 is blocked (1)")

    call s_window%bar()
    call test%assertequal(s_window%des_index(), 407,                    &
      message="Verify barred window 1 DES index is 407 (2)")
    call test%assertfalse(s_window%is_nailed(),                         &
      message="Verify barred window 1 is not nailed shut (2)")
    call test%assertfalse(s_window%is_broken(),                         &
      message="Verify barred window 1 is not broken (2)")
    call test%asserttrue(s_window%is_barred(),                          &
      message="Verify barred window 1 is barred (2)")
    call test%assertfalse(s_window%is_open(),                           &
      message="Verify barred window 1 is not open (2)")
    call test%asserttrue(s_window%is_blocked(),                         &
      message="Verify barred window 1 is blocked (2)")

    call s_window%open()
    call test%assertequal(s_window%des_index(), 408,                    &
      message="Verify opened window 1 DES index is 408 (3)")
    call test%assertfalse(s_window%is_nailed(),                         &
      message="Verify opened window 1 is not nailed shut (3)")
    call test%assertfalse(s_window%is_broken(),                         &
      message="Verify opened window 1 is not broken (3)")
    call test%assertfalse(s_window%is_barred(),                         &
      message="Verify opened window 1 is not barred (3)")
    call test%asserttrue(s_window%is_open(),                            &
      message="Verify opened window 1 is open (3)")
    call test%assertfalse(s_window%is_blocked(),                        &
      message="Verify opened window 1 is not blocked (3)")

    ! Secure the window
      call s_window%secure()
      call test%assertequal(s_window%des_index(), 405,                    &
        message="Verify secured window 1 DES index is 405 (0)")
      call test%asserttrue(s_window%is_nailed(),                          &
        message="Verify secured window 1 is nailed shut (0)")
      call test%assertfalse(s_window%is_broken(),                         &
        message="Verify secured window 1 is not broken (0)")
      call test%assertfalse(s_window%is_barred(),                         &
        message="Verify secured window 1 is not barred (0)")
      call test%assertfalse(s_window%is_open(),                           &
        message="Verify secured window 1 is not open (0)")
      call test%asserttrue(s_window%is_blocked(),                         &
        message="Verify secured window 1 is blocked (0)")


    ! call test%asserttrue(s_window%is_lit(),                             &
    !   message="Verify window is lit)")
    ! call test%assertfalse(s_window%is_off(),                            &
    !   message="Verify window is not off)")

    ! ! Turn off the window
    ! call s_window%turn_off()

    ! call test%assertfalse(s_window%is_lit(),                            &
    !   message="Verify window is not lit)")
    ! call test%asserttrue(s_window%is_off(),                             &
    !   message="Verify window is off)")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_window.json")

    call test%checkfailure()
end program ut_window
